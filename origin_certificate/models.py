from odoo import models, fields, api
class origin_certificate(models.Model):
      _name='origin.certificate'

      origin_id=fields.One2many('origin.line','certificate_line')
      origin_pack_id=fields.One2many('origin.pack.line','pack_order_line')

      certificate_no = fields.Char(string="certificate number", required=False, )
      receipt_no = fields.Char(string="receipt number", required=False, )
      company = fields.Many2one('res.partner')
      nano_sale_id = fields.Many2one('nano.logistic')
      address = fields.Char(string="address of our company", required=False, )
      consigned = fields.Many2one('res.partner')
      address2 = fields.Char(string="address of consigned", required=False, )
      vessel = fields.Char(string="vessel", required=False, )
      belong = fields.Many2one('res.partner')
      polica_no= fields.Char(string="polica no", required=False, )
      date = fields.Date(string="date", required=False, )

      signature1=fields.Many2one('res.partner')
      signature2=fields.Many2one('res.partner')





class origin_line(models.Model):
    _name = 'origin.line'

    certificate_line=fields.Many2one('origin.certificate')

    product = fields.Many2one(comodel_name="product.product",required=False, )
    describtion=fields.Char()
    unit = fields.Many2one('product.uom')
    quantity = fields.Float( required=False, )
    gross_weight = fields.Float( required=False, )
    net_weight = fields.Float( required=False, )
    price_unit = fields.Float(required=False,)
    subtotal = fields.Float( required=False,)
class origin_pack_line(models.Model):
    _name='origin.pack.line'
    pack_order_line=fields.Many2one('origin.certificate')

    product = fields.Many2one(comodel_name="product.product",required=False, )
    quantity = fields.Float( required=False, )