# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class ResPartnerInherit(models.Model):
    _inherit = "res.partner"

    credit_limit = fields.Float('Credit Limit')
    check_credit = fields.Boolean('Check Credit')
    on_hold = fields.Boolean('On Hold')
    on_account_vendor = fields.Many2one(comodel_name="account.account", domain=[('deprecated', '=', False)],string="On Account", required=False, )
    on_advance = fields.Many2one(comodel_name="account.account", domain=[('deprecated', '=', False)],string="On Advance", required=False, )
    lc_account = fields.Many2one(comodel_name="account.account", domain=[('deprecated', '=', False)],string="LC", required=False, )


class SaleOrderInherit(models.Model):
    _inherit = "sale.order"

    @api.multi
    def action_confirm(self):
        for order in self:
            partner_obj = self.env['res.partner'].search([('id', '=', order.partner_id.id)])
            account_obj = self.env['account.invoice'].search([('partner_id', '=', order.partner_id.id)])
            outstanding = 0
            # Summ of account.amount_due
            for obj in account_obj:
                outstanding += obj.residual_signed

            if partner_obj.check_credit == True:
                if partner_obj.credit_limit >= (order.amount_total + outstanding):
                    return super(SaleOrderInherit, self).action_confirm()
                else:
                    exceeded_amount = partner_obj.credit_limit - outstanding
                    self.state = 'draft'
                    raise ValidationError(_(
                        'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
                                              partner_obj.name, partner_obj.credit_limit, outstanding, order.name,
                                              partner_obj.on_hold, exceeded_amount))

            elif partner_obj.check_credit == False:
                if partner_obj.on_hold == True:
                    exceeded_amount = partner_obj.credit_limit - outstanding
                    raise ValidationError(_(
                        'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
                                              partner_obj.name, partner_obj.credit_limit, outstanding, order.name,
                                              partner_obj.on_hold, exceeded_amount))
                else:
                    return super(SaleOrderInherit, self).action_confirm()

            else:
                return super(SaleOrderInherit, self).action_confirm()

        return True

    @api.multi
    def action_quotation_send(self):
        self.ensure_one()
        partner_obj = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
        account_obj = self.env['account.invoice'].search([('partner_id', '=', self.partner_id.id)])
        outstanding = 0
        # Summ of account.amount_due
        for obj in account_obj:
            outstanding += obj.residual_signed
        if partner_obj.check_credit == True:
            if partner_obj.credit_limit >= (self.amount_total + outstanding):
                return super(SaleOrderInherit, self).action_quotation_send()
            else:
                exceeded_amount = partner_obj.credit_limit - outstanding
                ir_model_data = self.env['ir.model.data']
                try:
                    template_id = ir_model_data.get_object_reference('sale', 'email_template_edi_sale')[1]
                except ValueError:
                    template_id = False
                ctx = dict()
                ctx.update({
                    'default_model': 'sale.order',
                    'default_res_id': self.ids[0],
                    'default_use_template': bool(template_id),
                    'default_template_id': template_id,
                    'default_composition_mode': 'comment',
                    'mark_so_as_sent': False,
                    'custom_layout': "sale.mail_template_data_notification_email_sale_order"
                })
                raise ValidationError(_(
                    'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
                                          partner_obj.name, partner_obj.credit_limit, outstanding, self.name,
                                          partner_obj.on_hold, exceeded_amount))


        elif partner_obj.check_credit == False:
            if partner_obj.on_hold == True:
                exceeded_amount = partner_obj.credit_limit - outstanding
                raise ValidationError(_(
                    'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
                                          partner_obj.name, partner_obj.credit_limit, outstanding, self.name,
                                          partner_obj.on_hold, exceeded_amount))

            else:
                return super(SaleOrderInherit, self).action_quotation_send()

        else:
            return super(SaleOrderInherit, self).action_quotation_send()

    # @api.multi
    # def plan_management_flow(self):
    #     partner_obj = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
    #     account_obj = self.env['account.invoice'].search([('partner_id', '=', self.partner_id.id)])
    #     outstanding = 0
    #     for obj in account_obj:
    #         outstanding += obj.residual_signed
    #     if partner_obj.check_credit == True:
    #         print "outstanding    :     ", outstanding
    #         print "amount_total   :     ", self.amount_total
    #         print "credit_limit   :     ", partner_obj.credit_limit
    #
    #         if partner_obj.credit_limit >= (self.amount_total + outstanding):
    #             print "YES"
    #             return super(SaleOrderInherit, self).plan_management_flow()
    #         else:
    #             exceeded_amount = partner_obj.credit_limit - outstanding
    #             self.state = 'draft'
    #             print "NO"
    #             print ">>>>>>>>>>>>>>>>>>>>>>>>>>", partner_obj.name, ">>>>>>>>>>>>>>>", partner_obj.credit_limit, ">>>>>>>>>>", outstanding, ">>>>>>>>>", self.name, ">>>>>>>>>", partner_obj.on_hold, ">>>>>>>>>", exceeded_amount
    #             raise ValidationError(_(
    #                 'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
    #                                       partner_obj.name, partner_obj.credit_limit, outstanding, self.name,
    #                                       partner_obj.on_hold, exceeded_amount))
    #     elif partner_obj.check_credit == False:
    #         if partner_obj.on_hold == True:
    #             print "On Hold"
    #             exceeded_amount = partner_obj.credit_limit - outstanding
    #             print ">>>>>>>>>>>>>>>>>>>>>>>>>>", partner_obj.name, ">>>>>>>>>>>>>>>", partner_obj.credit_limit, ">>>>>>>>>>", outstanding, ">>>>>>>>>", self.name, ">>>>>>>>>", partner_obj.on_hold, ">>>>>>>>>", exceeded_amount
    #             raise ValidationError(_(
    #                 'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
    #                                       partner_obj.name, partner_obj.credit_limit, outstanding, self.name,
    #                                       partner_obj.on_hold, exceeded_amount))
    #         else:
    #             return super(SaleOrderInherit, self).plan_management_flow()
    #
    #     else:
    #         return super(SaleOrderInherit, self).plan_management_flow()
