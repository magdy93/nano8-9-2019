# -*- coding: utf-8 -*-

{
    'name': 'Nano Customer Credit',
    'description': 'Nano Customer Credit Limit',
    'summary': 'UNSS-Sales Module',
    'category': 'Sales',
    'author': 'Universal Selective Systems',
    'depends': ['base', 'sale', 'account_accountant', 'nano_plan'],
    'data': [
        'views/customer_credit_limit.xml',
    ]
}
