# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SalePackage(models.Model):
    _name = 'sale.package'
    name = fields.Char(string="Name", required=False, )
class SaleOrderPackage(models.Model):
    _name = 'sale.order.packages'
    package_ids = fields.One2many(comodel_name="package.order.lines", inverse_name="package_sale_id", string="Package Lines", )
    package_order_line_id = fields.Many2one(comodel_name="sale.order.line", string="Package", required=False, )
    package_code_id = fields.Many2one(comodel_name="sale.package", string="Package Code", required=False, )
    name = fields.Char(string="Name", )
    user_id = fields.Many2one(comodel_name="res.users", string="Created By", default=lambda self: self.env.user, )
    date = fields.Date(string="Created Date", default=fields.date.today())
    number_of_package = fields.Integer(string="Number Of Package", compute='get_number_of_package', )
    total_price = fields.Float(string="Total Price", compute='get_number_of_package', )

    @api.depends('package_ids')
    def get_number_of_package(self):
        for line in self:
            line.number_of_package = len(self.package_ids)
            line.total_price=sum(record.unit_price for record in line.package_ids)

    @api.onchange('package_code_id')
    def get_name(self):
        self.name = self.package_code_id.name
class PackageOrderLine(models.Model):
    _name = 'package.order.lines'
    package_sale_id = fields.Many2one(comodel_name="sale.order.packages", string="Package", required=False, )
    sale_order_line_id = fields.Many2one(comodel_name="sale.order.line", string="sale", required=False, )
    product_id = fields.Many2one(comodel_name="product.product", string="Package", )
    name = fields.Char(string="Description", required=False, )
    quantity = fields.Float(string="Quantity",  required=False,default="1" )
    unit_price = fields.Float(string="Unit Price", )

    @api.onchange('product_id')
    def get_description(self):
        self.ensure_one()
        self.name = self.product_id.name
        self.quantity = 1
        self.unit_price = self.product_id.list_price
