# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    package_tag_ids = fields.Many2one(comodel_name="sale.order.packages",string="package Code", )
    sale_package_ids = fields.One2many(comodel_name="package.order.lines", inverse_name="sale_order_line_id",
                                       string="Package",store=True,related= 'package_tag_ids.package_ids', required=False, )
    @api.onchange('sale_package_ids')
    def get_price_unit(self):
        total = 0.0
        total_cost = 0.0
        for rec in self:
            for line in rec.sale_package_ids :
                total += line.quantity * line.unit_price
                total_cost += line.quantity * line.product_id.standard_price
        self.price_unit += total
        self.purchase_price += total_cost
        # print ("total :",total)
        # print ("self.price_unit :",self.price_unit)

    # @api.onchange('package_tag_ids')
    # def get_total_price_unit(self):
    #     for r in self:
    #         for line in r.package_tag_ids :
    #             r.sale_package_ids |= line.package_ids

class SaleOrder(models.Model):
      _inherit='sale.order'

      # @api.one
      # @api.constrains('partner_id')
      # def get_package_data(self):
      #     print "gggggggggg"
      #     for line in self.order_line:
      #         print "ssssssss"
      #         for pack in line.sale_package_ids:
      #             print("dddddd")
      #             self.env['sale.order.line'].create({
      #                           'order_id':self.id,
      #                           'product_id':pack.product_id.id,
      #                           # 'package_order':[(4, pack.id)],
      #                           'product_uom_qty':pack.quantity,
      #                           'price_unit':0.0,
      #                           'price_subtotal':0.0,
      #                           # 'customer_lead':0.0,
      #                     })
      #             print("sss")
      def write(self, values):
          res=super(SaleOrder, self).write(values)
          print "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
          for line in self.order_line:
              for pack in line.sale_package_ids:
                  print("dddddd")
                  self.env['sale.order.line'].create({
                                'order_id':self.id,
                                'product_id':pack.product_id.id,
                                # 'package_order':[(4, pack.id)],
                                'product_uom_qty':pack.quantity,
                                'price_unit':0.0,
                                'price_subtotal':0.0,
                                # 'customer_lead':0.0,
                          })
                  print("sss")
          return res