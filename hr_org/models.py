from odoo import models, fields, api
class HrEmployee(models.Model):
       _inherit='hr.employee'
       head_quarter = fields.Many2one(comodel_name="hr.employee", string="HQ", required=False, )
       child_id = fields.One2many('hr.child', 'parent_id', string='Subordinates')

class HrChild(models.Model):
      _name='hr.child'

      parent_id = fields.Many2one('hr.employee', string='Manager')

      department_id = fields.Many2one('hr.department', string='Department')
      address_id = fields.Many2one('res.partner', string='Working Address')
      address_home_id = fields.Many2one('res.partner', string='Home Address')
      bank_account_id = fields.Many2one('res.partner.bank', string='Bank Account Number',
        domain="[('partner_id', '=', address_home_id)]", help='Employee bank salary account')
      work_phone = fields.Char('Work Phone')
      mobile_phone = fields.Char('Work Mobile')
      work_email = fields.Char('Work Email')
      work_location = fields.Char('Work Location')
      notes = fields.Text('Notes')
