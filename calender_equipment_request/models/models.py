# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CalenderEquipmentRequest(models.Model):
      _name = 'calender.equipment.request'
      equipment_line_id = fields.One2many(comodel_name="calender.equipment.request.line", inverse_name="equipment_id",)
      equipment_purchase_id = fields.One2many(comodel_name="calender.purchase.request.line", inverse_name="purchase_equipment_id",)
      package_id = fields.One2many(comodel_name="calender.package.request.line", inverse_name="package_equipment_id",)

      equipment_sequence = fields.Char(string="sequence",readonly=True, )
      equipment = fields.Many2one('maintenance.equipment', string="equipment", required=False, )
      request_date = fields.Datetime(string="request date", required=False, )
      sale_order = fields.Many2one(comodel_name="sale.order", string="sale order", required=False, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      describtion = fields.Char(string="describtion", required=False, )
      operation = fields.Selection(string="operation", selection=[('deliver', 'deliver'), ('receipt', 'receipt'), ],default='deliver', required=True,)
      state = fields.Selection(selection=[('draft', 'Draft'),
                                          ('logistic', 'logistic'),
                                          ('inventory', 'inventory'),
                                          ('manufacturing', 'manufacturing'), ],default='draft',string='State' )

      # warehouse
      warehouse_line = fields.One2many(comodel_name="calender.warehouse.request.line", inverse_name="warehouse_id",)

      warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
      warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
      schedule_date = fields.Datetime(string="schedule date", required=False, )
      # warehouse

      # manufacturing
      manufacturing_line = fields.One2many(comodel_name="calender.manufacturing.request.line", inverse_name="manufacturing_id",)

      purchase_id = fields.Many2one(comodel_name="mrp.production", string="Manufactoring order", required=False, )
      manufacturing_warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
      manufacturing_warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
      manufacturing_schedule_date = fields.Datetime(string="schedule date", required=False, )
      manufacturing_describtion = fields.Text(string="describtion", required=False, )
      # manufacturing


      def _prepare_invoice_line_from_so_line(self, line):
        data = {
            'sale_line_id': line.id,
            'quantity': line.product_uom_qty,
            'product': line.product_id.id,
        }
        return data

      @api.onchange('sale_order')
      def purchase_order_change(self):
        if not self.sale_order:
            return {}
        new_lines = self.env['calender.equipment.request.line']
        for line in self.sale_order.order_line - self.equipment_line_id.mapped('sale_line_id'):
            data = self._prepare_invoice_line_from_so_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines += new_line

        self.equipment_line_id = new_lines
        return {}

      def _prepare_package_line_from_po_line(self, line):
        data = {
            'pack_line_id': line.id,
            'quantity': line.quantity,
            'product': line.packaging.id,
        }
        return data

      @api.onchange('sale_order')
      def package_order_change(self):
        if not self.sale_order:
            return {}
        new_lines = self.env['calender.package.request.line']
        for line in self.sale_order.package_order_line - self.package_id.mapped('pack_line_id'):
            data = self._prepare_package_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._package_additional_fields(self)
            new_lines += new_line

        self.package_id = new_lines
        return {}


      def _prepare_purchase_line_from_po_line(self, line):
        data = {
            'purchase_line_id': line.id,
            'quantity': line.product_qty,
            'product': line.product_id.id,
        }
        return data

      @api.onchange('purchase_order')
      def purchase_change(self):
        if not self.purchase_order:
            return {}
        new_lines = self.env['calender.purchase.request.line']
        for line in self.purchase_order.order_line - self.equipment_purchase_id.mapped('purchase_line_id'):
            data = self._prepare_purchase_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional(self)
            new_lines += new_line

        self.equipment_purchase_id = new_lines
        return {}


      # manufacturing order line

      def _prepare_invoice_line_from_po_line(self, line):
        data = {
            'purchase_line_id': line.id,
            'quantity': line.product_uom_qty,
            'product': line.product_id.id,
        }
        return data

      @api.onchange('purchase_id')
      def manufacturing_order_change(self):
        if not self.purchase_id:
            return {}
        new_lines = self.env['calender.manufacturing.request.line']
        for line in self.purchase_id.move_finished_ids - self.manufacturing_line.mapped('purchase_line_id'):
            data = self._prepare_invoice_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines += new_line
        self.manufacturing_line = new_lines
        return {}

      # manufacturing order line

      @api.multi
      def logistic_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'logistic',
          })
        equipment=self.env['equipment.request']
        logistic_sale=self.env['equipment.request.line']
        logistic_purchase=self.env['purchase.request.line']
        logistic_package=self.env['package.request.line']

        logistic_id=equipment.search([('calendar_id', '=',self.id)])
        if logistic_id:
           print ">>>>>>>>id found"
           return {
                'type': 'ir.actions.act_window',
                'name': 'logistic request',
                'res_model': 'equipment.request',
                'res_id': logistic_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
                }
        else:
            print "<<<<<<<<<< new id"
            if self.operation=='deliver':
                for maint in self:
                    main_id=equipment.create({
                        'equipment':maint.equipment.id,
                        'request_date':maint.request_date,
                        'operation':'deliver',
                        'sale_order':maint.sale_order.id,
                        'describtion':maint.describtion,
                        'calendar_id':self.id,
                    })
                    for line in maint.equipment_line_id:
                        print "<<<<<<<<< ",line.product.name
                        print "<<<<<<<<< ",line.quantity
                        logistic_sale.create({
                            'equipment_id':main_id.id,
                            'product':line.product.id,
                            'quantity':line.quantity,
                        })
                    for pack in maint.package_id:
                        print "<<<<<<<<< ",pack.product.name
                        print "<<<<<<<<< ",pack.quantity
                        logistic_package.create({
                            'package_equipment_id':main_id.id,
                            'product':pack.product.id,
                            'quantity':pack.quantity,
                        })

            elif self.operation=='receipt':
                print"<<<<<<<<<<<<<< receipt >>>>>>>>>>>>> "
                for maint in self:
                    main_id=equipment.create({
                        'equipment_sequence':maint.equipment_sequence,
                        'equipment':maint.equipment.id,
                        'request_date':maint.request_date,
                        'operation':'receipt',
                        'purchase_order':maint.purchase_order.id,
                        'describtion':maint.describtion,
                        'calendar_id':self.id,
                    })
                for purchase in maint.equipment_purchase_id:
                        print "<<<<<<<<< ",purchase.product.name
                        print "<<<<<<<<< ",purchase.quantity
                        logistic_purchase.create({
                            'purchase_equipment_id':main_id.id,
                            'product':purchase.product.id,
                            'quantity':purchase.quantity,
                        })
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'logistic request',
                    'res_model': 'equipment.request',
                    'res_id': main_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                    }

      @api.multi
      def inventory_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'inventory',
            })
        warehouse=self.env['warehouse.request']
        warehouse_package=self.env['warehouse.request.line']

        warehouse_id=warehouse.search([('calendar_id', '=',self.id)])
        if warehouse_id:
           print ">>>>>>>>id found"
           return {
                'type': 'ir.actions.act_window',
                'name': 'logistic request',
                'res_model': 'warehouse.request',
                'res_id': warehouse_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
                }
        else:
            for ware in self:
                ware_id=warehouse.create({
                    # 'equipment_sequence':maint.equipment_sequence,
                    'warehouse_from':ware.warehouse_from.id,
                    'warehouse_to':ware.warehouse_to.id,
                    'schedule_date':ware.schedule_date,
                    'calendar_id':self.id,
                })
                for line in ware.warehouse_line:
                    warehouse_package.create({
                        'warehouse_id':ware_id.id,
                        'product':line.product.id,
                        'quantity':line.quantity,
                    })

            return {
                    'type': 'ir.actions.act_window',
                    'name': 'warehouse request',
                    'res_model': 'warehouse.request',
                    'res_id': ware_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                    }

      @api.multi
      def manufacturing_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'manufacturing',
        })
        manufacturing=self.env['manufacturing.equipment']
        warehouse_package=self.env['manufacturing.request.line']

        manufacturing_id=manufacturing.search([('calendar_id', '=',self.id)])
        if manufacturing_id:
           print ">>>>>>>>id found"
           return {
                'type': 'ir.actions.act_window',
                'name': 'logistic request',
                'res_model': 'manufacturing.equipment',
                'res_id': manufacturing_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
                }
        else:
            for manu in self:
                manu_id=manufacturing.create({
                'purchase_id':manu.purchase_id.id,
                'warehouse_from':manu.manufacturing_warehouse_from.id,
                'warehouse_to':manu.manufacturing_warehouse_to.id,
                'schedule_date':manu.manufacturing_schedule_date,
                'calendar_id':self.id,
                })
                for line in manu.manufacturing_line:
                    warehouse_package.create({
                        'manufacturing_id':manu_id.id,
                        'product':line.product.id,
                        'quantity':line.quantity,
                    })
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'manufacturing request',
                    'res_model': 'manufacturing.equipment',
                    'res_id': manu_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                    }
class equipment_request_line(models.Model):
      _name = 'calender.equipment.request.line'

      equipment_id = fields.Many2one(comodel_name="calender.equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      sale_line_id = fields.Many2one('sale.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      sale_order = fields.Many2one('sale.order', related='sale_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _set_additional_fields(self,invoice):
        pass
class purchase_request_line(models.Model):
      _name = 'calender.purchase.request.line'

      purchase_equipment_id = fields.Many2one(comodel_name="calender.equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      purchase_order = fields.Many2one('purchase.order', related='purchase_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _set_additional(self,invoice):
        pass
class package_request_line(models.Model):
      _name = 'calender.package.request.line'

      package_equipment_id = fields.Many2one(comodel_name="calender.equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      pack_line_id = fields.Many2one('package.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      sale_order = fields.Many2one('sale.order', related='pack_line_id.package_line', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _package_additional_fields(self,invoice):
        pass
class warehouse_request_line(models.Model):
      _name = 'calender.warehouse.request.line'

      warehouse_id = fields.Many2one(comodel_name="calender.equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)
class manufacturing_request_line(models.Model):
      _name = 'calender.manufacturing.request.line'

      manufacturing_id = fields.Many2one(comodel_name="calender.equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      purchase_line_id = fields.Many2one('stock.move', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      purchase_id = fields.Many2one('mrp.production', related='purchase_line_id.production_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _set_additional_fields(self,invoice):
         pass
