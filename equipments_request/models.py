from odoo import models, fields, api
import datetime

class maintainance_request(models.Model):
      _name = 'maintainance.request'

      sale_line_id = fields.One2many(comodel_name="logistic.sale.line", inverse_name="logistic_sale_id",)
      purchase_line_id = fields.One2many(comodel_name="logistic.purchase.line", inverse_name="logistic_purchase_id",)
      package_line_id = fields.One2many(comodel_name="logistic.package.line", inverse_name="logistic_package_id",)

      maintainance_sequence = fields.Char(string="Maintenance sequence",readonly=True, )
      equipment_sequence = fields.Char(string="Equipment sequence",readonly=True, )
      equipment = fields.Many2one('maintenance.equipment', string="equipment", required=False, )
      driver = fields.Many2one(comodel_name="res.partner", string="driver", required=False, )
      request_date = fields.Datetime(string="request date", required=False, )
      sale_order = fields.Many2one(comodel_name="sale.order", string="sale order", required=False, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      describtion = fields.Char(string="describtion", required=False, )
      operation = fields.Selection(string="operation", selection=[('deliver', 'deliver'), ('receipt', 'receipt'), ], required=True,)
      state = fields.Selection(selection=[('waiting', 'waiting bocking'),
                                          ('approved', 'approved'),
                                          ('done', 'done'), ],string='State' )

      technician_user_id = fields.Many2one('res.users', string='Owner', track_visibility='onchange')
      duration = fields.Float(help="Duration in minutes and seconds.")

      @api.model
      def create(self, vals):
        vals['maintainance_sequence'] = self.env['ir.sequence'].next_by_code('maintenance.seq')
        return super(maintainance_request, self).create(vals)

      @api.multi
      def waiting_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'waiting',
        })
        equipment=self.env['equipment.request']
        equipment_id=equipment.search([('equipment_sequence', '=',self.equipment_sequence)])
        if equipment_id:
            for equip in equipment_id:
                equip.write({
                    'state': 'waiting',
                })


      @api.multi
      def approved_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'approved',
            })
        equipment=self.env['equipment.request']
        equipment_id=equipment.search([('equipment_sequence', '=',self.equipment_sequence)])
        if equipment_id:
            for equip in equipment_id:
                equip.write({
                    'state': 'bocked',
                })


      @api.multi
      def done_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'done',
        })
        equipment=self.env['equipment.request']
        equipment_id=equipment.search([('equipment_sequence', '=',self.equipment_sequence)])
        if equipment_id:
            for equip in equipment_id:
                equip.write({
                    'state': 'done',
                })
class logistic_sale_line(models.Model):
      _name = 'logistic.sale.line'
      logistic_sale_id = fields.Many2one(comodel_name="maintainance.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)
class logistic_purchase_line(models.Model):
      _name = 'logistic.purchase.line'
      logistic_purchase_id = fields.Many2one(comodel_name="maintainance.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)
class logistic_package_line(models.Model):
      _name = 'logistic.package.line'
      logistic_package_id = fields.Many2one(comodel_name="maintainance.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

class equipment_request(models.Model):
      _name = 'equipment.request'
      # _inherit='maintainance.request'

      equipment_line_id = fields.One2many(comodel_name="equipment.request.line", inverse_name="equipment_id",)
      equipment_purchase_id = fields.One2many(comodel_name="purchase.request.line", inverse_name="purchase_equipment_id",)
      package_id = fields.One2many(comodel_name="package.request.line", inverse_name="package_equipment_id",)

      equipment_sequence = fields.Char(string="sequence",readonly=True, )
      equipment = fields.Many2one('maintenance.equipment', string="equipment", required=False, )
      request_date = fields.Datetime(string="request date", required=False, )
      sale_order = fields.Many2one(comodel_name="sale.order", string="sale order", required=False, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      describtion = fields.Char(string="describtion", required=False, )
      operation = fields.Selection(string="operation", selection=[('deliver', 'deliver'), ('receipt', 'receipt'), ],default='deliver', required=True,)

      calendar_id = fields.Many2one('calender.equipment.request', string="calendar", required=False, )

      state = fields.Selection(selection=[('draft', 'request sent'),
                                                          ('manager', 'manager'),
                                                          ('waiting', 'waiting bocking'),
                                                          ('bocked', 'bocked'),
                                                          ('done', 'done'), ],default='draft',string='State' )

      def _prepare_invoice_line_from_po_line(self, line):
        data = {
            'sale_line_id': line.id,
            'quantity': line.product_uom_qty,
            'product': line.product_id.id,
        }
        return data

      @api.onchange('sale_order')
      def purchase_order_change(self):
        if not self.sale_order:
            return {}
        new_lines = self.env['equipment.request.line']
        for line in self.sale_order.order_line - self.equipment_line_id.mapped('sale_line_id'):
            data = self._prepare_invoice_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines += new_line

        self.equipment_line_id = new_lines
        return {}

      def _prepare_package_line_from_po_line(self, line):
        data = {
            'pack_line_id': line.id,
            'quantity': line.quantity,
            'product': line.packaging.id,
        }
        return data

      @api.onchange('sale_order')
      def package_order_change(self):
        if not self.sale_order:
            return {}
        new_lines = self.env['package.request.line']
        for line in self.sale_order.package_order_line - self.package_id.mapped('pack_line_id'):
            data = self._prepare_package_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._package_additional_fields(self)
            new_lines += new_line

        self.package_id = new_lines
        return {}


      def _prepare_purchase_line_from_po_line(self, line):
        data = {
            'purchase_line_id': line.id,
            'quantity': line.product_qty,
            'product': line.product_id.id,
        }
        return data

      @api.onchange('purchase_order')
      def purchase_change(self):
        if not self.purchase_order:
            return {}
        new_lines = self.env['purchase.request.line']
        for line in self.purchase_order.order_line - self.equipment_purchase_id.mapped('purchase_line_id'):
            data = self._prepare_purchase_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional(self)
            new_lines += new_line

        self.equipment_purchase_id = new_lines
        return {}

      @api.model
      def create(self, vals):
        vals['equipment_sequence'] = self.env['ir.sequence'].next_by_code('equipment.seq')
        return super(equipment_request, self).create(vals)

      @api.multi
      def draft_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })

      @api.multi
      def manager_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'manager',
            })
        maintenance=self.env['maintainance.request']
        logistic_sale=self.env['logistic.sale.line']
        logistic_purchase=self.env['logistic.purchase.line']
        logistic_package=self.env['logistic.package.line']

        if self.operation=='deliver':
            for maint in self:
                main_id=maintenance.create({
                    'equipment_sequence':maint.equipment_sequence,
                    'equipment':maint.equipment.id,
                    'request_date':maint.request_date,
                    'operation':'deliver',
                    'sale_order':maint.sale_order.id,
                    'purchase_order':maint.purchase_order.id,
                    'describtion':maint.describtion,
                })
                for line in maint.equipment_line_id:
                    print "<<<<<<<<< ",line.product.name
                    print "<<<<<<<<< ",line.quantity
                    logistic_sale.create({
                        'logistic_sale_id':main_id.id,
                        'product':line.product.id,
                        'quantity':line.quantity,
                    })
                for pack in maint.package_id:
                    print "<<<<<<<<< ",pack.product.name
                    print "<<<<<<<<< ",pack.quantity
                    logistic_package.create({
                        'logistic_package_id':main_id.id,
                        'product':pack.product.id,
                        'quantity':pack.quantity,
                    })

        elif self.operation=='receipt':
            print"<<<<<<<<<<<<<< receipt >>>>>>>>>>>>> "
            for maint in self:
                main_id=maintenance.create({
                    'equipment_sequence':maint.equipment_sequence,
                    'equipment':maint.equipment.id,
                    'request_date':maint.request_date,
                    'operation':'receipt',
                    'purchase_order':maint.purchase_order.id,
                    'describtion':maint.describtion,
                })
            for purchase in maint.equipment_purchase_id:
                    print "<<<<<<<<< ",purchase.product.name
                    print "<<<<<<<<< ",purchase.quantity
                    logistic_purchase.create({
                        'logistic_purchase_id':main_id.id,
                        'product':purchase.product.id,
                        'quantity':purchase.quantity,
                    })
class equipment_request_line(models.Model):
      _name = 'equipment.request.line'

      equipment_id = fields.Many2one(comodel_name="equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      sale_line_id = fields.Many2one('sale.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      sale_order = fields.Many2one('sale.order', related='sale_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _set_additional_fields(self,invoice):
        pass
class purchase_request_line(models.Model):
      _name = 'purchase.request.line'

      purchase_equipment_id = fields.Many2one(comodel_name="equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      purchase_order = fields.Many2one('purchase.order', related='purchase_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _set_additional(self,invoice):
        pass
class package_request_line(models.Model):
      _name = 'package.request.line'

      package_equipment_id = fields.Many2one(comodel_name="equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      pack_line_id = fields.Many2one('package.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      sale_order = fields.Many2one('sale.order', related='pack_line_id.package_line', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _package_additional_fields(self,invoice):
        pass


class warehouse_request(models.Model):
      _name = 'warehouse.request'

      warehouse_line = fields.One2many(comodel_name="warehouse.request.line", inverse_name="warehouse_id",)
      warehouse_sequence = fields.Char(string="warehouse sequence",readonly=True, )
      warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
      warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
      schedule_date = fields.Datetime(string="schedule date", required=False, )
      calendar_id = fields.Many2one('calender.equipment.request', string="calender", required=False, )

      state = fields.Selection(selection=[('draft', 'request sent'),
                                                          ('manager', 'manager'),
                                                          ('waiting', 'waiting bocking'),
                                                          ('bocked', 'bocked'),
                                                          ('done', 'done'), ],default='draft',string='State' )


      @api.model
      def create(self, vals):
        vals['warehouse_sequence'] = self.env['ir.sequence'].next_by_code('warehouse.seq')
        return super(warehouse_request, self).create(vals)

      @api.multi
      def draft_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })

      @api.multi
      def manager_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'manager',
            })
        warehouse=self.env['warehouse.equipment.request']
        warehouse_line=self.env['warehouse.equipment.line']
        for warehouse_data in self:
            warehouse_id=warehouse.create({
                'warehouse_sequence':warehouse_data.warehouse_sequence,
                'warehouse_from':warehouse_data.warehouse_from.id,
                'warehouse_to':warehouse_data.warehouse_to.id,
                'schedule_date':warehouse_data.schedule_date,
            })
            for line in warehouse_data.warehouse_line:
                warehouse_line.create({
                'warehouse_equipment_id':warehouse_id.id,
                'product':line.product.id,
                'quantity':line.quantity,
            })
class warehouse_request_line(models.Model):
      _name = 'warehouse.request.line'

      warehouse_id = fields.Many2one(comodel_name="warehouse.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)


class warehouse_equipment_request(models.Model):
      _name = 'warehouse.equipment.request'

      warehouse_equipment_lines = fields.One2many(comodel_name="warehouse.equipment.line", inverse_name="warehouse_equipment_id",)
      warehouse_equipment_sequence = fields.Char(string="equipment sequence",readonly=True, )
      warehouse_sequence = fields.Char(string="warehouse sequence",readonly=True, )
      warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
      warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
      schedule_date = fields.Datetime(string="schedule date", required=False, )
      state = fields.Selection(selection=[('waiting', 'waiting bocking'),
                                          ('approved', 'approved'),
                                          ('done', 'done'), ],default='waiting',string='State' )


      @api.model
      def create(self, vals):
        vals['warehouse_equipment_sequence'] = self.env['ir.sequence'].next_by_code('warequ.seq')
        return super(warehouse_equipment_request, self).create(vals)

      @api.multi
      def waiting_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'waiting',
        })

        equip_warehouse=self.env['warehouse.request']
        warehouse_id=equip_warehouse.search([('warehouse_sequence', '=',self.warehouse_sequence)])
        if warehouse_id:
            print">>>>>>>>>>>>>>>>>>>>> 1"
            for equip in warehouse_id:
                equip.write({
                    'state': 'waiting',
                })

      @api.multi
      def approved_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'approved',
            })
        print">>>>>>>>>>>>>>>>>>>>> 2"
        equip_warehouse=self.env['warehouse.request']
        warehouse_id=equip_warehouse.search([('warehouse_sequence', '=',self.warehouse_sequence)])
        if warehouse_id:
            for equip in warehouse_id:
                equip.write({
                    'state': 'bocked',
                })

      @api.multi
      def done_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'done',
            })
        print">>>>>>>>>>>>>>>>>>>>> 3"
        equip_warehouse=self.env['warehouse.request']
        warehouse_id=equip_warehouse.search([('warehouse_sequence', '=',self.warehouse_sequence)])
        if warehouse_id:
            for equip in warehouse_id:
                equip.write({
                    'state': 'done',
                })
class warehouse_equipment_line(models.Model):
      _name = 'warehouse.equipment.line'

      warehouse_equipment_id = fields.Many2one(comodel_name="warehouse.equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

class manufacturing_equipment(models.Model):

      _name = 'manufacturing.equipment'

      manufacturing_line = fields.One2many(comodel_name="manufacturing.request.line", inverse_name="manufacturing_id",)

      manufacturing_sequence = fields.Char(string="warehouse sequence",readonly=True, )
      purchase_id = fields.Many2one(comodel_name="mrp.production", string="Manufactoring order", required=False, )
      warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
      warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
      schedule_date = fields.Datetime(string="schedule date", required=False, )
      describtion = fields.Text(string="describtion", required=False, )
      calendar_id = fields.Many2one('calender.equipment.request', string="calender", required=False, )

      state = fields.Selection(selection=[('draft', 'request sent'),
                                          ('manager', 'manager'),
                                          ('waiting', 'waiting bocking'),
                                          ('bocked', 'bocked'),
                                          ('done', 'done'), ],default='draft',string='State' )

      def _prepare_invoice_line_from_po_line(self, line):
        data = {
            'purchase_line_id': line.id,
            'quantity': line.product_uom_qty,
            'product': line.product_id.id,
        }
        return data

      @api.onchange('purchase_id')
      def purchase_order_change(self):
        if not self.purchase_id:
            return {}
        new_lines = self.env['manufacturing.request.line']
        for line in self.purchase_id.move_finished_ids - self.manufacturing_line.mapped('purchase_line_id'):
            data = self._prepare_invoice_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines += new_line
        self.manufacturing_line = new_lines
        return {}




      @api.model
      def create(self, vals):
        vals['manufacturing_sequence'] = self.env['ir.sequence'].next_by_code('manufacturing.seq')
        return super(manufacturing_equipment, self).create(vals)

      @api.multi
      def draft_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })

      @api.multi
      def manager_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'manager',
            })
        manufacturing = self.env['manufacturing.equipment.request']
        manufacturing_lines=self.env['manufacturing.equipment.line']
        for manufacturing_data in self:
            manufacturing_id=manufacturing.create({
                'purchase_id':manufacturing_data.purchase_id.id,
                'manufacturing_sequence':manufacturing_data.manufacturing_sequence,
                'warehouse_from':manufacturing_data.warehouse_from.id,
                'warehouse_to':manufacturing_data.warehouse_to.id,
                'schedule_date':manufacturing_data.schedule_date,
                'describtion':manufacturing_data.describtion,
            })
            for line in manufacturing_data.manufacturing_line:
                manufacturing_lines.create({
                'manufacturing_id':manufacturing_id.id,
                'product':line.product.id,
                'quantity':line.quantity,
            })
class manufacturing_request_line(models.Model):
      _name = 'manufacturing.request.line'

      manufacturing_id = fields.Many2one(comodel_name="manufacturing.equipment",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)

      purchase_line_id = fields.Many2one('stock.move', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
      purchase_id = fields.Many2one('mrp.production', related='purchase_line_id.production_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

      def _set_additional_fields(self,invoice):
         pass

class manufacturing_equipment_request(models.Model):
      _name = 'manufacturing.equipment.request'

      timer_equipment_lines = fields.One2many(comodel_name="timer.equipment.line", inverse_name="timer_id",)
      manufacturing_equipment_lines = fields.One2many(comodel_name="manufacturing.equipment.line", inverse_name="manufacturing_id",)
      manufacturing_equipment_sequence = fields.Char(string="equipment sequence",readonly=True, )
      manufacturing_sequence = fields.Char(string="manufactoring sequence",readonly=True, )
      purchase_id = fields.Many2one(comodel_name="mrp.production", string="Manufactoring order", required=False, )
      warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
      warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
      schedule_date = fields.Datetime(string="schedule date", required=False, )
      state = fields.Selection(selection=[('waiting', 'waiting bocking'),
                                          ('approved', 'approved'),
                                          ('done', 'done'), ],default='waiting',string='State' )
      describtion = fields.Text(string="describtion", required=False, )

      start_date = fields.Datetime(string="Start Date", required=False, )
      end_date = fields.Datetime(string="End Date", required=False, )

      # second = fields.Float(string="second",  required=False,default=0, )
      # minutes = fields.Float(string="minutes",  required=False,default=0,compute='timer_progressbar', )
      # time_start = time.time()


      @api.model
      def create(self, vals):
        vals['manufacturing_equipment_sequence'] = self.env['ir.sequence'].next_by_code('manufacturing.eq')
        return super(manufacturing_equipment_request, self).create(vals)


      x=datetime.datetime.now()
      y=datetime.datetime.now()
      @api.multi
      def start_order(self):
          print">>>>>>>> start"
          global x
          global y
          x=datetime.datetime.now()
          y=datetime.datetime.now()
          for rec in self:
              rec.write({
                  'start_date':x,
              })
              print ">>>>>>>>>>> date ",x.time().strftime('%H:%M:%S')

      totaldays = fields.Char(string="Total", required=False,compute='_amount_all' )

      @api.one
      @api.depends('timer_equipment_lines.duration')
      def _amount_all(self):
        for rec in self:
            # totalday='0:0:0'
            # totaldays=datetime.datetime.strptime(totalday,'%H:%M:%S')
            # for line in rec.timer_equipment_lines:
            #     totaldays += line.duration
            # rec.update({
            #     'totaldays':totaldays,
            # })
            sum = datetime.timedelta()
            cumulative=[]
            d=[]
            for l in rec.timer_equipment_lines:
                (h,m,s)=l.duration.split(':')
                sum+= datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                # for i in l.duration:
                #     # cumulative +=i
                #     print"append",i
                #     d.append(i)
                # print"append :d ",d
                # cumulative.append(i)
                    # for line in cumulative:
                    #     d.append(line)
                        # (h,m,s) = line.split(':')
                        # d = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                print ">>>>>>>>>>> cumulative : ",cumulative
            self.totaldays=str(sum)
            # print ">>>>>>>>>>> d : ",d
                #     (h, m, s) = i.split(':')
                #     d = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                #     sum += d
                # print(str(sum))

      @api.multi
      def pause_order(self):
          print">>>>>>>> pause"
          # x=datetime.datetime.now()
          # print type(x)

          time=self.env['timer.equipment.line']
          for rec in self:
              y=datetime.datetime.now()
              rec.write({
                  'end_date':datetime.datetime.now(),
              })
              z=y.time().strftime('%H:%M:%S')
              zz=datetime.datetime.strptime(z,'%H:%M:%S')
              # print ">>>>>>>>>>> date pause ",z
              # print ">>>>>>>>>>> date y ",type(y)
              # print ">>>>>>>>>>> date z ",type(z)
              # print ">>>>>>>>>>> date start ",type(rec.start_date)
              # print ">>>>>>>>>>> date start ",rec.start_date[11:]
              # print ">>>>>>>>>>> date zz ",datetime.datetime.strptime(z,'%H:%M:%S')
              # print ">>>>>>>>>>> date zzstart ",datetime.datetime.strptime(rec.start_date[11:],'%H:%M:%S')
              c= datetime.datetime.strptime(z,'%H:%M:%S') - (datetime.datetime.strptime(rec.start_date[11:],'%H:%M:%S'))
              print ">>>>>>>>>>>>>>>>>>> c",c
              time.create({
                  'timer_id':self.id,
                  'start_date':rec.start_date,
                  'end_date':y,
                  'duration':c,
              })


      # @api.multi
      # def continue_order(self):
      #     print">>>>>>>> continue"

      @api.multi
      def finish_order(self):
          print">>>>>>>> finish"


      # @api.multi
      # def timer_progressbar(self):
      #   self.ensure_one()
        # time_start = time.time()
        # x = 2
        # # seconds = 0
        # # minutes = 0
        # if x == 2:
        #     timeLoop = True
        #     for rec in self:
        #         while timeLoop:
        #             try:
        #                 # sys.stdout.write("\r{minutes} Minutes {seconds} Seconds".format(minutes=self.minutes, seconds=self.second))
        #                 sys.stdout.flush()
        #                 time.sleep(1)
        #                 rec.second = int(time.time() - time_start) - rec.minutes * 60
        #                 if rec.second >= 60:
        #                     rec.minutes += 1
        #                     rec.second = 0
        #             except KeyboardInterrupt, e:
        #                 break
        # while self.minutes <= 100:
        #     time.sleep(1)
        #     self.second += 1
        #     print ">>>>>>>> second ",self.second


      @api.multi
      def waiting_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'waiting',
        })

        equip_manufacturing=self.env['manufacturing.equipment']
        equip_manufacturing_id=equip_manufacturing.search([('manufacturing_sequence', '=',self.manufacturing_sequence)])
        if equip_manufacturing_id:
            for equip in equip_manufacturing_id:
                equip.write({
                    'state': 'waiting',
                })

      @api.multi
      def approved_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'approved',
            })
        equip_manufacturing=self.env['manufacturing.equipment']
        equip_manufacturing_id=equip_manufacturing.search([('manufacturing_sequence', '=',self.manufacturing_sequence)])
        if equip_manufacturing_id:
            for equip in equip_manufacturing_id:
                equip.write({
                    'state': 'bocked',
                })

      @api.multi
      def done_progressbar(self):
        self.ensure_one()
        self.write({
                'state': 'done',
            })
        equip_manufacturing=self.env['manufacturing.equipment']
        equip_manufacturing_id=equip_manufacturing.search([('manufacturing_sequence', '=',self.manufacturing_sequence)])
        if equip_manufacturing_id:
            for equip in equip_manufacturing_id:
                equip.write({
                    'state': 'done',
                })
class manufacturing_equipment_line(models.Model):
      _name = 'manufacturing.equipment.line'

      manufacturing_id = fields.Many2one(comodel_name="manufacturing.equipment.request",)
      product = fields.Many2one(comodel_name="product.product", string="product",)
      quantity = fields.Float(string="quantity",)
class timer_equipment_line(models.Model):
      _name = 'timer.equipment.line'

      timer_id = fields.Many2one(comodel_name="manufacturing.equipment.request",)
      start_date = fields.Datetime(string="start date", required=False, )
      end_date = fields.Datetime(string="end date", required=False, )
      duration = fields.Char(string="duration", required=False, )
