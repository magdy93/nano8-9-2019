# -*- coding: utf-8 -*-
{
    'name': "Equipment Request",

    'summary': """
        Equipment Request""",

    'description': """
Equipment Request""",

    'author': "Equipment Request",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','purchase','mrp','stock','calender_equipment_request','nano_logistic',],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'equipment.xml',
        'maintenance.xml',
        'warehouse.xml',
        'warehouse_equipment.xml',
        'manufacturing.xml',
        'sequence.xml',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}