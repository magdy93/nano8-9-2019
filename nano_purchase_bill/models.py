from odoo import models, fields, api


# import pymysql
class nano_purchase_bill(models.Model):
    _inherit = 'res.partner'
    account_on_advance = fields.Many2one(comodel_name="account.account", string="account on advance", required=False, )


class NanoPurchaseVendor(models.Model):
    _inherit = 'account.invoice'
    account_on_advance = fields.Many2one(comodel_name="account.account", string="account on advance", required=False, )
    payments = fields.Selection(string="Payments",
                                selection=[('onaccount', 'on account'), ('onadvance', 'on advance'), ('lc', 'lc'), ],
                                default='onadvance', required=False, )
    # number1 = fields.Char(string="number", readonly=True, )
    # x = fields.Integer(string="x", required=True, )

    # @api.model
    # def create(self,vals):
    #    account_move = self.env['account.move']
    #    for move in account_move:
    #       journal = move.journal_id
    #       # vals['number1'] = self.env['ir.sequence'].next_by_code()
    #       vals['number1'] = journal.sequence_id.with_context(ir_sequence_date=move.date).next_by_id()
    #       return super(nano_purchase_vendor, self).create(vals)

    @api.multi
    def goto_journal(self):
        logiistic = self.env['account.move']
        stok_id = logiistic.search([('name', '=', self.number)])
        if stok_id:
            return {
                'type': 'ir.actions.act_window',
                'name': 'account.view_move_form',
                'res_model': 'account.move',
                'res_id': stok_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
            }

    # @api.multi
    # def goto_db(self):
    #   # Connect to the database
    #     connection = pymysql.connect(host='localhost',
    #                                user='root',
    #                                password='',
    #                                db='magdy',
    #                                charset='utf8mb4',
    #                                cursorclass=pymysql.cursors.DictCursor)
    #     with connection.cursor() as cursor:
    #       # Create a new record
    #       sql = "INSERT INTO `dbtable` (`fname`, `lname`) VALUES (%s, %s)"
    #       cursor.execute(sql, (self.x, self.x))
    #       # connection is not autocommit by default. So you must commit to save
    #       # your changes.
    #     connection.commit()

    @api.multi
    def purchase_modify(self):
        print "ooooooooooooooooooooooooooooooooooooooooooooooooooo"

    @api.multi
    @api.onchange('partner_id')
    def change_payment_method(self):
        self.account_on_advance = self.partner_id.account_on_advance


class ReceiveOrder(models.TransientModel):
    _name = 'payment.register.wizerd'
    # default=lambda s:s.env['account.invoice'].browse(s._context.get('active_id',False)).journal_id.id
    journals = fields.Many2one(comodel_name="account.journal", string="journal", required=False)
    payment_amount = fields.Float(string="Payment ammount", required=False,readonly=True, )
    date = fields.Date(string="payment date", required=False, )
    memo = fields.Char(string="Memo", required=False, )
    account_on_advance = fields.Many2one(comodel_name="account.account", string="account on advance", required=False, )

    @api.multi
    def button_cancel(self):
        return True

    @api.multi
    def payment_validate(self):
        inv = self.env['account.invoice'].browse(self._context.get('active_id'))
        inv.state = 'paid'
        # return
        # pay=self.env['account.invoice']
        # attendance_ids = pay.search([])[0]
        # print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> out"
        # for payment in attendance_ids:
        #     jornal=payment.account_on_advance.id
        #     print ">>>>>>>>>>>>>>>>>>>>>>>>>>> jornal : ",jornal

        move_obj = self.env['account.move']
        for emp in self:
            move = move_obj.create({
                'journal_id': emp.account_on_advance.id,
                # 'name':emp.number1,
            })
            inv.move_id = move.id


            if emp.journals.type == 'bank':
                custudy = emp.journals.default_debit_account_id.id
            else:
                custudy = emp.journals.default_debit_account_id.id

            journalline = self.with_context(dict(self._context, check_move_validity=False)).env['account.move.line']
            journalline.create({
                'move_id': move.id,
                'account_id': custudy,
                'name': emp.memo,
                'debit': emp.payment_amount,
                'credit': 0.0,
            })
            journalline.create({
                'move_id': move.id,
                'account_id': custudy,
                'name': emp.memo,
                'debit': 0.0,
                'credit': emp.payment_amount,
            })
            move.post()



            # payment.write({'state':'paid'})
