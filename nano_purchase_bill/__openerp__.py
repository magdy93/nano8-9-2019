# -*- coding: utf-8 -*-
{
    'name': "nano purchase bill",

    'summary': """
        nano purchase bill""",

    'description': """
nano purchase bill""",

    'author': "UNSS",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account',],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'payment_wizard.xml',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}