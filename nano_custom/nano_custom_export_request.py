from odoo import models, fields, api


class createExportRequest(models.Model):
    _inherit = 'sale.order'

    # @api.multi
    # def action_confirm(self):
    #     export_req = self.env['export.request']
    #     export_req_line = self.env['request.order.line']
    #     for line in self:
    #         req_id = export_req.create({
    #             'sales_order_q': line.id,
    #             'incoterm': line.incoterm.id
    #         })
    #         for rec in line.order_line:
    #             export_req_line.create({
    #                 'order_line': req_id.id,
    #                 'product': rec.product_id.id,
    #                 'description': rec.name,
    #                 'quantity': rec.product_uom_qty,
    #                 'taxes': rec.tax_id.amount,
    #                 'uom': rec.product_uom.category_id.name,
    #                 'price': rec.price_unit,
    #                 'subtotal': rec.price_subtotal
    #             })
    #     for order in self:
    #         order.state = 'sale'
    #         order.confirmation_date = fields.Datetime.now()
    #         if self.env.context.get('send_email'):
    #             self.force_quotation_send()
    #         order.order_line._action_procurement_create()
    #     if self.env['ir.values'].get_default('sale.config.settings', 'auto_done_setting'):
    #         self.action_done()
    #
    #     return True


class exportRequest(models.Model):
    _name = 'export.request'

    order_line_rel = fields.One2many('request.order.line', 'order_line')
    req_no = fields.Char(string='Export Request', readonly=True)
    sales_order_q = fields.Many2one('sale.order', string='Sales order', readonly=True)
    request_date = fields.Date(string='Request Date', default=fields.Date.today)
    incoterm = fields.Many2one('stock.incoterms', string='Incoterms', readonly=True)

    # for smart buttons
    state_of_warehouse = fields.Char(string='State Of Warehouse')
    state_of_logistic = fields.Char(string='State Of Logistic')

    # workflow
    state = fields.Selection([
        ('draft', 'Draft'),
        ('todo', 'Mark As To Do'),
    ], default='draft')

    todo_exports = fields.Char(string='Exports')

    @api.multi
    def mark_as_todo(self):
        self.ensure_one()
        self.write({
            'state': 'todo',
        })
        logistic_purchase_state = self.env['nano.logistic'].search([('sale_order', '=', self.sales_order_q.id)])
        logistic_purchase_state.sudo().write({
            'custom_status':'todo',
        })

    @api.multi
    def draft(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })

    # sequence field
    @api.model
    def create(self, vals):
        vals['req_no'] = self.env['ir.sequence'].next_by_code('req.seq')
        return super(exportRequest, self).create(vals)

    @api.multi
    def action_state_of_warehouse(self):
        stok_picking = self.env['stock.picking']
        stok_order_id = stok_picking.search([('sale_id', '=', self.sales_order_q.id)])
        if stok_order_id:
            for line in stok_order_id:
                print ">>>>>>>", line.state
                self.state_of_warehouse = line.state

    @api.multi
    def action_state_of_logistic(self):
        logistic = self.env['nano.logistic']
        logistic_order_id = logistic.search([('sale_order', '=', self.sales_order_q.id)])
        if logistic_order_id:
            for line in logistic_order_id:
                print ">>>>>>>>>", line.stat
                self.state_of_logistic = line.stat

    @api.multi
    def action_exports(self):
        ex = self.env['exports']
        for line in self:
            view_id = ex.create({
                'sales_order': line.sales_order_q.id,
                'ex_no': line.req_no
            })
            return {
                'type': 'ir.actions.act_window',
                'name': 'Exports',
                'res_model': 'exports',
                'res_id': view_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self'
            }


class request_order_line(models.Model):
    _name = 'request.order.line'

    order_line = fields.Many2one('export.request')
    product = fields.Many2one('product.product', string='Product')
    description = fields.Char(string='Description')
    quantity = fields.Float(string='Order qty')
    uom = fields.Char(string='Unit Of Measure')
    taxes = fields.Char(string='Taxes')
    price = fields.Float(string='Unit price')
    subtotal = fields.Float(string='Subtotal')


class exports(models.Model):
    _name = 'exports'

    ex_no = fields.Char(string='Export Request', readonly=True)
    sales_order = fields.Many2one('sale.order', string='Sales order', readonly=True)
    status_of_goods = fields.Selection([
        ('under_check', 'Under check'),
        ('checked', 'Checked'),
        ('delivered_to_logistic', 'Delivered to logistic'),
        ('on_board', 'On board')], string='Status of goods')

    export_logistic_rel = fields.One2many('exports.attachment', 'ex_rel')
    export_custom_rel = fields.One2many('exports.attachment', 'exx_rel')
    export_state = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Done'),
    ], default='draft')

    @api.multi
    def draft_state(self):
        self.ensure_one()
        self.write({
            'export_state': 'draft',
        })

    @api.multi
    def in_progress_state(self):
        self.ensure_one()
        self.write({
            'export_state': 'in_progress',
        })

    @api.multi
    def done_state(self):
        self.ensure_one()
        self.write({
            'export_state': 'done',
        })


class exportsAttachment(models.Model):
    _name = 'exports.attachment'

    ex_rel = fields.Many2one('exports')
    exx_rel = fields.Many2one('exports')
    logistic_file_name = fields.Char(string='File Name')
    logistic_attachment_ids = fields.Many2many('ir.attachment', 'sales_custom_request_ir_attachments_rel',
                                      'sales_custom_request_id', 'attachment_id', string='Attachments')
    custom_file_name = fields.Char(string='File Name')
    custom_attachment_ids = fields.Many2many('ir.attachment', 'sales_custom_request_ir_attachments_rel',
                                      'sales_custom_request_id', 'attachment_id', string='Attachments')
    status = fields.Selection([('in_progress', 'In Progress'), ('finished', 'Finished')], string='Status')
    notes = fields.Text(string='Notes')


class expensesExport(models.Model):
    _name = 'expenses.export'

    description = fields.Char(string='Description Expense')
    product = fields.Many2one('product.product', string='Product')
    unit_price = fields.Float(string='Unit Of Price')
    qty = fields.Float(string='Quantity')
    total = fields.Float(string='Totla', compute='get_total')
    bill_ref = fields.Many2one(comodel_name="sale.order",string='Bill Reference')
    date = fields.Date(string='Date', default=fields.Date.today)
    account = fields.Many2one('account.account', string='Account')
    analytic_account = fields.Many2one('account.analytic.account', string='Analytic Account')
    employee = fields.Many2one('hr.employee', string='Employee')
    payment_by = fields.Selection([('by_custody', 'Custody'),
                                   ('by_employee', 'Employee')],
                                  string='Payment by')
    custody = fields.Many2one('account.account', string='Custody Account')
    journal_ii = fields.Many2one('account.journal', string='Journal Account')
    payable = fields.Many2one('res.partner', string='Employee Account')
    post = fields.Boolean(string='Post')
    documents = fields.Float(string='Documents', compute = '_compute_attachment_number')
    label = fields.Char(string='Label')
    notes = fields.Text(string='Notes')
    expense_state = fields.Selection([('save', 'Save'), ('submit', 'Submit'), ('confirm', 'Confirm')], default='save')
    req_no = fields.Char(string='Expense No.', readonly=True)


    @api.model
    def create(self, vals):
        vals['req_no'] = self.env['ir.sequence'].next_by_code('req.seqq')
        return super(expensesExport, self).create(vals)

    @api.onchange('product')
    def so_domain(self):
        bill_ids = []
        so_ids = self.env['sale.order'].search([('order_line.product_id','=',self.product.id)])
        for so_id in so_ids:
            bill_ids.append(so_id.id)
        domain = [('id', 'in', bill_ids)]
        return {'domain': {'bill_ref': domain}}

    @api.multi
    def save_state(self):
        self.ensure_one()
        self.write({
            'expense_state': 'save'
        })

    @api.multi
    def submit_state(self):
        self.ensure_one()
        self.write({
            'expense_state': 'submit'
        })

    @api.multi
    def confirm_state(self):
        self.ensure_one()
        self.write({
            'expense_state': 'confirm'
        })
        journal = self.env['account.move']
        for rec in self:
            if rec.post == True:
                state = 'posted'
            else:
                state = 'draft'

            if rec.payment_by == 'by_custody':
                acc = rec.custody.id
            else:
                acc = rec.payable.property_account_payable_id.id

            line_id = journal.create({
                'journal_id': rec.journal_ii.id,
                'state': state
            })
            journal_line = self.with_context(dict(self._context, check_move_validity=False)).env['account.move.line']
            journal_line.create({
                'move_id': line_id.id,
                'name': rec.label,
                'analytic_account_id': rec.analytic_account.id,
                'debit': rec.total,
                'credit': 0.0,
                'account_id': rec.account.id
            })
            journal_line.create({
                'move_id': line_id.id,
                'name': rec.label,
                'analytic_account_id': rec.analytic_account.id,
                'debit': 0.0,
                'credit': rec.total,
                'account_id': acc
            })

    @api.multi
    def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'hr.expense'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'hr.expense', 'default_res_id': self.id}
        return res

    @api.multi
    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group([('res_model', '=', 'hr.expense'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.documents = attachment.get(expense.id, 0)


    @api.depends('unit_price', 'qty')
    def get_total(self):
        if self.unit_price or self.qty:
            self.total = self.unit_price * self.qty
