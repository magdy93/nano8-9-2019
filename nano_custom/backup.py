# code to be moved into new module
#         logistic_import = self.sudo().env['logistic.purchase']
#         logistic_import_line = self.sudo().env['logistic.purchase.order.line']
#         logistic_container_line = self.env['nano.purchase.containers']
#         domestic_import = self.env['domistic.purchase']
#         domestic_import_line = self.env['domistic.purchase.order.line']
#         domestic_container_line = self.env['nano.domestic.containers']
#         logistic_purchase_docs_obj = self.sudo().env['nano.logistic.purchase.docs']
#         for rec in self:
#             if self.type == 'international':
#                 # a. Create logistic request -> we will explain it in logistic module
#                 logistic_id = logistic_import.sudo().create({
#                     'purchase_order': rec.id,
#                     'incoterms': rec.incoterm_id.id,
#                     'loading_port': rec.loading_port,
#                     'unloading_port': rec.unloading_port,
#                     'from_street': rec.from_street,
#                     'from_street2': rec.from_street2,
#                     'from_zip': rec.from_zip,
#                     'from_city': rec.from_city,
#                     'from_state_id': rec.from_state_id.id,
#                     'from_country_id': rec.from_country_id.id,
#                     'to_street': rec.to_street,
#                     'to_street2': rec.to_street2,
#                     'to_zip': rec.to_zip,
#                     'to_city': rec.to_city,
#                     'to_state_id': rec.to_state_id.id,
#                     'to_country_id': rec.to_country_id.id,
#                     'vessel_loading_date': rec.vessel_loading_date,
#                     'vessel_unloading_date': rec.vessel_unloading_date,
#                     'wh_incoming_date': rec.wh_incoming_date,
#                     'custom_status': 'draft',
#                     'warehouse_status': 'assigned',
#                 })
#                 for lg in rec.order_line:
#                     logistic_import_line.sudo().create({
#                         'order_line': logistic_id.id,
#                         'product': lg.product_id.id,
#                         'uom': lg.product_uom.id,
#                         'describtion': lg.name,
#                         'subtotal': lg.price_subtotal,
#                         # 'tax': line.taxes_id,
#                         'price': lg.price_unit,
#                         'quantity': lg.product_qty
#                     })
#                 for doc_obj in rec.attach_rel:
#                     logistic_purchase_docs_obj.sudo().create({
#                         'logistic_purchase_id': logistic_id.id,
#                         'document': doc_obj.document,
#                         'attachment_ids': [(6, 0, doc_obj.attachment_ids.ids)]
#                     })
#                     doc_obj.is_send = False
#                     doc_obj.flag = 1
#
#                 for container in rec.container_ids:
#                     logistic_container_line.sudo().create({
#                         'purchase_containers_id': logistic_id.id,
#                         'document': container.document,
#                         'attachment_ids': container.attachment_ids,
#                     })
#                     container.is_send_container = False
#                     container.flag_container = 1
#                 rec.sudo().write({
#                     'state_of_logistic_request': 'draft'
#                 })
#             elif self.type == 'domestics' and self.carrying == 'company':
#                 # a. Create logistic request -> we will explain it in logistic module
#                 logistic_id = domestic_import.sudo().create({
#                     'purchase_order': rec.id,
#                     'incoterms': rec.incoterm_id.id,
#                     'loading_port': rec.loading_port,
#                     'unloading_port': rec.unloading_port,
#                     'from_street': rec.from_street,
#                     'from_street2': rec.from_street2,
#                     'from_zip': rec.from_zip,
#                     'from_city': rec.from_city,
#                     'from_state_id': rec.from_state_id.id,
#                     'from_country_id': rec.from_country_id.id,
#                     'to_street': rec.to_street,
#                     'to_street2': rec.to_street2,
#                     'to_zip': rec.to_zip,
#                     'to_city': rec.to_city,
#                     'to_state_id': rec.to_state_id.id,
#                     'to_country_id': rec.to_country_id.id,
#                     'vessel_loading_date': rec.vessel_loading_date,
#                     'vessel_unloading_date': rec.vessel_unloading_date,
#                     'wh_incoming_date': rec.wh_incoming_date,
#                 })
#                 for lg in rec.order_line:
#                     domestic_import_line.sudo().create({
#                         'order_line': logistic_id.id,
#                         'product': lg.product_id.id,
#                         'describtion': lg.name,
#                         'subtotal': lg.price_subtotal,
#                         # 'tax': line.taxes_id,
#                         'price': lg.price_unit,
#                         'quantity': lg.product_qty
#                     })
#                 for container in rec.container_ids:
#                     domestic_container_line.sudo().create({
#                         'domestic_containers_id': logistic_id.id,
#                         'document': container.document,
#                         'attachment_ids': container.attachment_ids,
#                     })
#                 rec.sudo().write({
#                     'state_of_logistic_request': 'draft'
#                 })
#         # end of code to be moved to new module
