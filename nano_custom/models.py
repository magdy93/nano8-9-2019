from odoo import models, fields, api, _
from werkzeug.routing import ValidationError
from odoo.exceptions import UserError


class nano_custom(models.Model):
    _name = 'nano.custom'

    custom_order = fields.One2many('custom.order.line', 'order_line')
    logistic_docs_order_line = fields.One2many('nano.logistic.docs', 'logistic_docs_order_id')
    custom_docs_order_line = fields.One2many('nano.custom.docs', 'custom_docs_order_id')
    delivery_line_id = fields.One2many('nano.delivery.order', 'delivery_order_id')
    open_certificate_line_id = fields.One2many('open.certificate.order', 'open_certificate_id')
    container_docs_ids = fields.One2many('nano.container.docs', 'container_docs_order_id')

    purchase_logistic_id = fields.Many2one('purchase.order', string='purchase order')
    logistic_id = fields.Many2one('logistic.purchase', string='Logistic Request')
    name = fields.Char(string="Import Request", readonly=True, )
    purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase Quotation", required=False, )
    incoterms = fields.Many2one(comodel_name="stock.incoterms", string="Incoterms", readonly=True, )
    request_date = fields.Date(string="PO Approval Date", required=False, default=fields.Date.today,
                               related='logistic_id.request_date')
    status = fields.Char(string="Warehouse State", required=False, )
    custom_status = fields.Char(string="logistic state", required=False, )
    todo = fields.Char(string="To Do State", required=False, )
    company_id = fields.Many2one(comodel_name='res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)

    loading_port = fields.Char(string='Loading Port')
    unloading_port = fields.Char(string='Unloading Port')
    from_street = fields.Char(string='Street')
    from_street2 = fields.Char(string='Street 2')
    from_zip = fields.Char(string='ZIP', change_default=True)
    from_city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    from_country_id = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    wh_incoming_date = fields.Date(string='WH Incoming')
    purchase_user_id = fields.Many2one('res.users', default=lambda self: self.env.user, string='Purchase User')
    purchase_manager_id = fields.Many2one('res.users', string='Purchase Manager')

    collecting_delivery_order = fields.Boolean(string="collecting delivery order", )
    open_certificate = fields.Boolean(string="open certificate", )

    stat = fields.Selection([
        ('draft', 'import'),
        ('todo', 'To Do'),
        ('collecting', 'collecting delivery order'),
        ('open', 'open certificate'),
        ('goods_receiving', 'Goods Receiving from Vessel'),
        ('warehouse', 'To warehouse'),
        ('done', 'Done'),
        ('temporary', 'Temporary permit'),
    ], default='draft')

    @api.multi
    def draft_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'draft',
        })

    @api.multi
    def collecting_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'collecting',
            'collecting_delivery_order': True,
        })

    @api.multi
    def open_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'open',
            'open_certificate': True,
        })

    @api.multi
    def goods_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'goods_receiving',
        })

    @api.multi
    def warehouse_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'warehouse',
        })

    @api.multi
    def done_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'done',
        })

    @api.multi
    def temporary_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'temporary',
        })

    @api.multi
    def todo_progressbar(self):
        self.ensure_one()
        purchase_order_id = self.env['purchase.order'].search([('id', '=', self.sudo().logistic_id.purchase_order.id)])
        logistic_purchase_state = self.sudo().env['logistic.purchase'].sudo().search(
            [('purchase_order', '=', self.sudo().logistic_id.purchase_order.id)])
        self.sudo().write({
            'stat': 'todo',
        })
        purchase_order_id.sudo().write({
            'state_of_custom_request': 'todo'
        })
        logistic_purchase_state.sudo().write({
            'custom_status': 'todo',
        })

    @api.multi
    def custom_state(self):
        print
        ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Custom state"
        logist = self.sudo().env['logistic.purchase']
        stok_id = logist.sudo().search([('purchase_order', '=', self.sudo().logistic_id.purchase_order.id)])
        if stok_id:
            for attend in stok_id:
                print
                "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", attend.stat
                self.custom_status = attend.sudo().stat

    @api.multi
    def warehouse_state(self):
        stok = self.sudo().env['stock.picking']
        stok_id = stok.sudo().search([('purchase_id', '=', self.sudo().logistic_id.purchase_order.id)])
        if stok_id:
            for attend in stok_id:
                print
                "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", attend.state
                self.status = attend.sudo().state

    @api.multi
    def magdy_salah(self):
        cary = self.sudo().env['custom.import']
        for emp in self:
            cary_id = cary.sudo().create({
                'purchase_order': emp.sudo().logistic_id.purchase_order.id,
                'name': emp.sudo().name,
                'temporary': emp.sudo().logistic_id.purchase_order.is_temporary_permit,
            })

            return {
                'type': 'ir.actions.act_window',
                'name': 'nano_custom.custom_import_view',
                'res_model': 'custom.import',
                'res_id': cary_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
            }

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('ree.seq')
        return super(nano_custom, self).create(vals)


class custom_order_line(models.Model):
    _name = 'custom.order.line'
    order_line = fields.Many2one('nano.custom')
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    describtion = fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty", required=False, )
    delivery = fields.Float(string="Delivery", required=False, )
    invoiced = fields.Float(string="Invoiced", required=False, )
    price = fields.Float(string="unit price", required=False, )
    tax = fields.Many2one(comodel_name="account.tax", string="Tax", required=False, )
    subtotal = fields.Float(string="Subtotal", required=False, )
    uom = fields.Many2one(comodel_name="product.uom", string="UOM", required=False, )


class NanoLogisticDocument(models.Model):
    _name = 'nano.logistic.docs'

    logistic_docs_order_id = fields.Many2one('nano.custom')
    document = fields.Char(string='Document')
    doc_status = fields.Selection([('required', 'required'), ('received', 'received')], string='Status')
    attachment_ids = fields.Many2many('ir.attachment', 'nano_custom_ir_attachments_rel',
                                      'nano_custom_id', 'attachment_id', string='Attachments')


class NanocustomDocument(models.Model):
    _name = 'nano.custom.docs'

    custom_docs_order_id = fields.Many2one('nano.custom')
    document = fields.Char(string='Document')
    attachment_id = fields.Binary(string="Attachments", )
    # attachment_ids = fields.Many2many('ir.attachment', 'nano_custom_ir_attachments_rel',
    #                                   'nano_custom_id', 'attachment_id', string='Attachments')


class NanoContainerDocument(models.Model):
    _name = 'nano.container.docs'

    container_docs_order_id = fields.Many2one('nano.custom')
    document = fields.Char(string='Container Number')
    attachment_ids = fields.Binary(string="Attachments", )
    # is_send_container = fields.Boolean('Is Send ?',)
    # flag_container = fields.Integer(default=0)


class CollectingDeliveryOrder(models.Model):
    _name = 'nano.delivery.order'

    delivery_order_id = fields.Many2one('nano.custom')
    document = fields.Char(string='Document')
    attachment_id = fields.Binary(string="Attachments", )
    # attachment_ids = fields.Many2many('ir.attachment', 'nano_custom_ir_attachments_rel',
    #                                   'nano_custom_id', 'attachment_id', string='Attachments')


class OpenCertificate(models.Model):
    _name = 'open.certificate.order'

    open_certificate_id = fields.Many2one('nano.custom')
    document = fields.Char(string='Document')
    attachment_id = fields.Binary(string="Attachments", )
    # attachment_ids = fields.Many2many('ir.attachment', 'nano_custom_ir_attachments_rel',
    #                                   'nano_custom_id', 'attachment_id', string='Attachments')


class custom_import(models.Model):
    _name = 'custom.import'
    logistic_line = fields.One2many(comodel_name="logistic.import.line", inverse_name="order_id",
                                    string="Logistic line", required=False, )
    custom_line = fields.One2many(comodel_name="custom.import.line", inverse_name="order_id", string="Custom line",
                                  required=False, )
    name = fields.Char(string="Import order", readonly=True, )
    purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", readonly=True,
                                     required=False, )
    status = fields.Selection(string="Status of goods",
                              selection=[('under_check', 'Under check'), ('checked', 'checked'),
                                         ('delivered', 'delivered to logistic'), ('onboard', 'on board'), ],
                              required=False, )
    temporary = fields.Boolean(string="Temporary Permit ", readonly=True, )
    todo = fields.Float(string="temporary", required=False, )
    stat = fields.Selection([
        ('draft', 'Draft'),
        ('inprogress', 'In progress'),
        ('done', 'Done'),
    ], default='draft')

    @api.multi
    def draft_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'draft',
        })

    @api.multi
    def inprogress_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'inprogress',
        })

    @api.multi
    def done_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'done',
        })

    @api.multi
    def magdy_salah(self):
        cary = self.env['temporary.permit']
        caryline = self.env['temporary.permit.item']
        for emp in self:
            cary_id = cary.create({
                'import_order': self.id,
                'purchase_order': emp.purchase_order.id,
            })
            for rec in self.purchase_order:
                for record in rec.order_line:
                    caryline.create({
                        'order_id': cary_id.id,
                        'item': record.product_id.id,
                        'quantity': record.product_qty,
                        'uom': record.product_uom.id,

                    })

            return {
                'type': 'ir.actions.act_window',
                'name': 'nano_custom.temporary_permit_view ',
                'res_model': 'temporary.permit',
                'res_id': cary_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
            }


class logistic_import_line(models.Model):
    _name = 'logistic.import.line'
    order_id = fields.Many2one(comodel_name="custom.import", string="logistic", required=False, )
    name = fields.Char(string="File Name", )
    attachment = fields.Binary(string="Attachment", )


class custom_import_line(models.Model):
    _name = 'custom.import.line'
    order_id = fields.Many2one(comodel_name="custom.import", string="custom", required=False, )
    name = fields.Char(string="File Name", )
    attachment = fields.Binary(string="Attachment", )
    status = fields.Selection(string="Status", selection=[('inprogress', 'in progress'), ('finished', 'Finished'), ],
                              required=False, )
    note = fields.Text(string="Note", required=False, )


class expence_custom(models.Model):
    _name = 'expence.custom'

    seq_no = fields.Char(string='Expense No.', readonly=True)
    expence_describtion = fields.Char(string="Description Expense", required=False, )
    bill_reference = fields.Many2one(comodel_name="purchase.order", string="Bill Reference", required=False, )
    label = fields.Char(string="label", required=False, )
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    unit_price = fields.Float(string="Unite Price", required=False, )
    quantity = fields.Float(string="Quantity", required=False, )
    total = fields.Float(string="Total", required=False, compute='_get_total')
    account = fields.Many2one(comodel_name="account.account", string="Account", required=False, )
    journal = fields.Many2one(comodel_name="account.journal", string="journal", required=False, )
    analytic_account = fields.Many2one(comodel_name="account.analytic.account", string="Analytic Account",
                                       required=False, )
    date = fields.Date(string="Date", required=False, default=fields.Date.today)
    employee = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False, )
    # payment = fields.Selection(string="Payment by", selection=[('employee', 'employee'), ('custody', 'custody'), ], required=False, )
    payment = fields.Char(string="Payment by", default='custody', readonly=True, required=False, )
    custody_account = fields.Many2one(comodel_name="account.account", string="Custody Account", required=False, )
    vendor = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )
    # post = fields.Boolean(string="Post",default=True, )
    attachment_number = fields.Float(string="Document", required=False, compute='_compute_attachment_number', )
    stat = fields.Selection([
        ('save', 'save'),
        ('submit', 'submit'),
        ('confirm', 'confirm'),
    ], default='save', string='Status', )

    @api.model
    def create(self, vals):
        vals['seq_no'] = self.env['ir.sequence'].next_by_code('req.sequencee')
        return super(expence_custom, self).create(vals)

    @api.onchange('product')
    def check_show(self):
        bill_ids = []
        domain = [['id', '=', False]]
        bill_of_mat = self.env['purchase.order'].search([('order_line.product_id', '=', self.product.id)])
        for bill in bill_of_mat:
            bill_ids.append(bill.id)
        domain = [('id', 'in', bill_ids)]
        return {'domain': {'bill_reference': domain}}

    @api.multi
    def action_save_send(self):
        self.ensure_one()
        self.write({
            'stat': 'save',
        })

    @api.multi
    def action_submit_send(self):
        self.ensure_one()
        self.write({
            'stat': 'submit',
        })

    @api.multi
    def action_confirm_send(self):
        self.ensure_one()
        self.write({
            'stat': 'confirm',
        })
        journal = self.env['account.move']
        # journalline = self.env['account.move.line']
        for emp in self:
            print
            ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> custody "
            # if emp.post == True:
            #    name1 = 'posted'
            # else:
            #    name1 = 'draft'

            journal_id = journal.create({
                'journal_id': emp.journal.id,
                # 'state':name1,
            })

            if emp.payment == 'custody':
                custudy = emp.custody_account.id
            else:
                custudy = emp.employee.address_home_id.property_account_payable_id.id

            journalline = self.with_context(dict(self._context, check_move_validity=False)).env['account.move.line']
            journalline.create({
                'move_id': journal_id.id,
                'account_id': emp.account.id,
                'name': emp.label,
                'debit': emp.total,
                'credit': 0.0,
                'analytic_account_id': emp.analytic_account.id,
            })
            journalline.create({
                'move_id': journal_id.id,
                'account_id': custudy,
                'name': emp.label,
                'debit': 0.0,
                'credit': emp.total,
                'analytic_account_id': emp.analytic_account.id,
            })

    @api.multi
    def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'expence.custom'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'expence.custom', 'default_res_id': self.id}
        return res

    @api.multi
    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group(
            [('res_model', '=', 'expence.custom'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.attachment_number = attachment.get(expense.id, 0)

    @api.multi
    @api.depends('unit_price', 'quantity')
    def _get_total(self):
        self.total = self.unit_price * self.quantity


class temporary_permit(models.Model):
    _name = 'temporary.permit'

    item_id = fields.One2many(comodel_name="temporary.permit.item", inverse_name="order_id", string="item",
                              required=False, )
    confirm_id = fields.One2many(comodel_name="temporary.permit.confirm", inverse_name="confirmm_id", string="confirm",
                                 required=False, )
    name = fields.Char(string="Name", required=False, )
    import_order = fields.Many2one(comodel_name="custom.import", string="Import order", readonly=True, required=False, )
    purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", readonly=True,
                                     required=False, )
    receiving_date = fields.Date(string="receiving date", required=False, )
    last_date = fields.Date(string="Last Period of Permission", required=False, )
    date_order = fields.Datetime(string="Date", required=False, related='purchase_id.date_order', )
    purchase_id = fields.Many2one(comodel_name="sale.order", string="sale order", required=False, )
    stat = fields.Selection([
        ('draft', 'Unreconciled'),
        ('todo', 'Reconciled'),
    ], default='draft', )

    # @api.one
    # @api.constrains('confirm_id')
    # def on_change_confirm_id(self):
    #     product_ids=[]
    #     for line in self.confirm_id:
    #         if line.product.id not in product_ids:
    #             product_ids.append(line.product.id)
    #         else:
    #             raise ValidationError('nooooooooooooooooooooooo')
    #

    @api.multi
    def action_save_send(self):
        self.ensure_one()
        self.write({
            'stat': 'draft',
        })

    @api.multi
    def action_submit_send(self):
        self.ensure_one()
        self.write({
            'stat': 'todo',
        })

    @api.constrains('receiving_date')
    def _user_id(self):
        if self.receiving_date > self.last_date:
            raise ValidationError('Last Period of Permission should be greater than receiving date!')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('reqq.seq')
        return super(temporary_permit, self).create(vals)

    # get confirm line automatically
    def _prepare_invoice_line_from_po_line(self, line):
        invoice_line = self.env['temporary.permit.confirm']
        data = {
            'purchase_line_id': line.id,
            'quantity': line.product_uom_qty,
            'product': line.product_id.id,
            'uom': line.product_uom.id,
            'date': self.date_order,
        }
        return data

    purchase_ids = []

    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if not self.purchase_id:
            return {}
        # if not self.partner_id:
        #     self.partner_id = self.purchase_id.partner_id.id

        new_lines = self.env['temporary.permit.confirm']
        for line in self.purchase_id.order_line - self.confirm_id.mapped('purchase_line_id'):
            data = self._prepare_invoice_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines += new_line

        self.confirm_id += new_lines
        self.purchase_ids.append(self.purchase_id.id)
        self.purchase_id = False
        return {
            'domain': {'purchase_id': [('id', 'not in', self.purchase_ids)]}
        }
        # return {}


class temporary_permit_item(models.Model):
    _name = 'temporary.permit.item'
    order_id = fields.Many2one(comodel_name="temporary.permit", string="temporary item", required=False, )
    item = fields.Many2one(comodel_name="product.product", string="Item", readonly=True, required=False, )
    quantity = fields.Float(string="Quantity", required=False, readonly=True, )
    uom = fields.Many2one(comodel_name="product.uom", string="UOM", readonly=True, required=False, )


class temporary_permit_confirm(models.Model):
    _name = 'temporary.permit.confirm'
    confirmm_id = fields.Many2one(comodel_name="temporary.permit", string="temporary confirm", required=False, )
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    quantity = fields.Float(string="Quantity", required=False, )
    uom = fields.Many2one(comodel_name="product.uom", string="UOM", required=False, )
    date = fields.Date(string="Date", required=False, )

    # linked with get confirmed line automatically in temporary.permit
    purchase_line_id = fields.Many2one('sale.order.line', 'Purchase Order Line', ondelete='set null', index=True,
                                       readonly=True)
    purchase_id = fields.Many2one('sale.order', related='purchase_line_id.order_id', string='Purchase Order',
                                  store=False, readonly=True, related_sudo=False,
                                  help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

    def _set_additional_fields(self, invoice):
        pass


class nano_purchase_lines(models.Model):
    _inherit = 'purchase.order.line'


# class nano_stock_lines(models.Model):
#     _inherit = 'stock.immediate.transfer'
#
#     @api.multi
#     def process(self):
#         self.ensure_one()
#         # If still in draft => confirm and assign
#         if self.pick_id.state == 'draft':
#             self.pick_id.action_confirm()
#             if self.pick_id.state != 'assigned':
#                 self.pick_id.action_assign()
#                 if self.pick_id.state != 'assigned':
#                     raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
#         for pack in self.pick_id.pack_operation_ids:
#             if pack.product_qty > 0:
#                 pack.write({'qty_done': pack.product_qty})
#             else:
#                 pack.unlink()
#         self.pick_id.do_transfer()

class nano_stock_lines(models.Model):
    _inherit = 'stock.picking'

    # @api.multi
    # def do_new_transfer(self):
    #     purchase_order_id = self.env['purchase.order'].search([('id', '=', self.purchase_id.id)]) #magdy
    #     logistic_purchase_state = self.env['logistic.purchase'].search([('purchase_order', '=', self.purchase_id.id)]) #magdy
    #     logistic_sale_state = self.env['nano.logistic'].search([('sale_order', '=', self.sale_id.id)]) #magdy
    #
    #     for pick in self:
    #         if pick.state == 'done':
    #             raise UserError(_('The pick is already validated'))
    #             purchase_order_id.write({'ship_data': 'done'}) #magdy
    #             logistic_purchase_state.write({'warehouse_status': 'done'}) #magdy
    #             logistic_sale_state.write({'status': 'done'}) #magdy
    #         pack_operations_delete = self.env['stock.pack.operation']
    #         if not pick.move_lines and not pick.pack_operation_ids:
    #             raise UserError(_('Please create some Initial Demand or Mark as Todo and create some Operations. '))
    #         # In draft or with no pack operations edited yet, ask if we can just do everything
    #         if pick.state == 'draft' or all([x.qty_done == 0.0 for x in pick.pack_operation_ids]):
    #             purchase_order_id.write({'ship_data': 'done'}) #magdy
    #             logistic_purchase_state.write({'warehouse_status': 'done'}) #magdy
    #             logistic_sale_state.write({'status': 'done'}) #magdy
    #             # If no lots when needed, raise error
    #             picking_type = pick.picking_type_id
    #             if (picking_type.use_create_lots or picking_type.use_existing_lots):
    #                 for pack in pick.pack_operation_ids:
    #                     if pack.product_id and pack.product_id.tracking != 'none':
    #                         raise UserError(_('Some products require lots/serial numbers, so you need to specify those first!'))
    #             view = self.env.ref('stock.view_immediate_transfer')
    #             wiz = self.env['stock.immediate.transfer'].create({'pick_id': pick.id})
    #             # TDE FIXME: a return in a loop, what a good idea. Really.
    #             return {
    #                 'name': _('Immediate Transfer?'),
    #                 'type': 'ir.actions.act_window',
    #                 'view_type': 'form',
    #                 'view_mode': 'form',
    #                 'res_model': 'stock.immediate.transfer',
    #                 'views': [(view.id, 'form')],
    #                 'view_id': view.id,
    #                 'target': 'new',
    #                 'res_id': wiz.id,
    #                 'context': self.env.context,
    #             }
    #
    #         # Check backorder should check for other barcodes
    #         if pick.check_backorder():
    #             view = self.env.ref('stock.view_backorder_confirmation')
    #             wiz = self.env['stock.backorder.confirmation'].create({'pick_id': pick.id})
    #             # TDE FIXME: same reamrk as above actually
    #             return {
    #                 'name': _('Create Backorder?'),
    #                 'type': 'ir.actions.act_window',
    #                 'view_type': 'form',
    #                 'view_mode': 'form',
    #                 'res_model': 'stock.backorder.confirmation',
    #                 'views': [(view.id, 'form')],
    #                 'view_id': view.id,
    #                 'target': 'new',
    #                 'res_id': wiz.id,
    #                 'context': self.env.context,
    #             }
    #         for operation in pick.pack_operation_ids:
    #             if operation.qty_done < 0:
    #                 raise UserError(_('No negative quantities allowed'))
    #             if operation.qty_done > 0:
    #                 operation.write({'product_qty': operation.qty_done})
    #             else:
    #                 pack_operations_delete |= operation
    #         if pack_operations_delete:
    #             pack_operations_delete.unlink()
    #     self.do_transfer()
    #     return
    #


class AccountJournal(models.Model):
    _inherit = 'account.journal'
    is_landed_cost = fields.Boolean(string="Landed Cost", )


class nano_purchase_flow(models.Model):
    _inherit = 'purchase.order'
    # ship_data = fields.Char(string="shipping", required=False, )

    # @api.multi
    # def action_view_picking(self):
    #     user = self.env['res.users'].browse(self.env.uid)
    #     if user.has_group('nano_plan.module_category_custom_management'):
    #         print
    #         'This user is a custom'
    #         print
    #         ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> ok"
    #         stok = self.env['stock.picking']
    #         stok_id = stok.search([('purchase_id', '=', self.name)])
    #         if stok_id:
    #             for attend in stok_id:
    #                 print
    #                 "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", attend.state
    #                 self.ship_data = attend.state
    #                 print
    #                 "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", self.ship_data
    #     elif user.has_group('nano_plan.module_category_logistic_management'):
    #         print
    #         'This user is a logistic'
    #         stok = self.env['stock.picking']
    #         stok_id = stok.search([('purchase_id', '=', self.name)])
    #         if stok_id:
    #             for attend in stok_id:
    #                 print
    #                 "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", attend.state
    #                 self.ship_data = attend.state
    #                 print
    #                 "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", self.ship_data
    #     else:
    #         print
    #         'This user is user'
    #         print
    #         ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> ok"
    #         stok = self.env['stock.picking']
    #         stok_id = stok.search([('purchase_id', '=', self.name)])
    #         if stok_id:
    #             for attend in stok_id:
    #                 print
    #                 "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", attend.state
    #                 self.ship_data = attend.state
    #                 print
    #                 "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state", self.ship_data
    #         '''
    #         This function returns an action that display existing picking orders of given purchase order ids.
    #         When only one found, show the picking immediately.
    #         '''
    #         action = self.env.ref('stock.action_picking_tree')
    #         result = action.read()[0]
    #
    #         # override the context to get rid of the default filtering on picking type
    #         result.pop('id', None)
    #         result['context'] = {}
    #         pick_ids = sum([order.picking_ids.ids for order in self], [])
    #         # choose the view_mode accordingly
    #         if len(pick_ids) > 1:
    #             result['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
    #         elif len(pick_ids) == 1:
    #             res = self.env.ref('stock.view_picking_form', False)
    #             result['views'] = [(res and res.id or False, 'form')]
    #             result['res_id'] = pick_ids and pick_ids[0] or False
    #         return result

    @api.multi
    def button_confirm(self):

        for order in self:
            if order.state not in ['draft', 'sent']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step' \
                    or (order.company_id.po_double_validation == 'two_step' \
                                and order.amount_total < self.env.user.company_id.currency_id.compute(
                            order.company_id.po_double_validation_amount, order.currency_id)) \
                    or order.user_has_groups('purchase.group_purchase_manager'):
                order.button_approve()
            else:
                order.write({'state': 'to approve'})
        landed_cost = self.sudo().env['stock.landed.cost']
        landed_cost_lines = self.env['stock.landed.cost.lines']
        if self.is_landed_cost_ok == False:
            land_cost_obj = landed_cost.search([('purchase_order_id', '=', self.purchase_service_id.id)])
            if land_cost_obj:
                for land in self:
                    for l_cost in land.order_line:
                        landed_cost_lines.create({
                            'cost_id': land_cost_obj.id,
                            'product_id': l_cost.product_id.id,
                            'name': l_cost.product_id.name or '',
                            'split_method': l_cost.product_id.split_method or 'equal',
                            'price_unit': self.amount_total or 0.0,
                            'account_id': l_cost.product_id.property_account_expense_id.id or l_cost.product_id.categ_id.property_account_expense_categ_id.id,
                        })
        else:
            stock_picking_id = self.sudo().env['stock.picking'].sudo().search([('purchase_id', '=', self.id)])
            # if stock_picking_id:
            journal = self.sudo().env['account.journal'].search([('is_landed_cost', '=', True)])
            landed_cost.sudo().create({
                'date': fields.Datetime.now(),
                'picking_ids': [(4, stock_picking_id.id) if stock_picking_id else None],
                'account_journal_id': journal.id or None,
                'purchase_order_id': self.id,
            })
            lande_sequence = landed_cost.sudo().search([('picking_ids', '=', stock_picking_id.id)])
            if lande_sequence:
                self.sudo().write({
                    'landed_cost_sequence': lande_sequence.sudo().name
                })
        stok = self.sudo().env['stock.picking']
        stok_id = stok.sudo().search([('purchase_id', '=', self.name)])
        if stok_id:
            for attend in stok_id:
                self.ship_data = attend.sudo().state
        return True
