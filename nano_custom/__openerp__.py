# -*- coding: utf-8 -*-
{
    'name': "Nano custom compine",

    'summary': """
        Nano custom combine""",

    'description': """
Nano custom combine""",

    'author': "Magdy",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','purchase','mail','stock','sale','account', 'hr_expense', 'nano_logistic', 'nano_plan', 'sales_team'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'templates.xml',
        'sequence.xml',
        'sequence2.xml',
        'nano_custom_export_request_view.xml',
        'nano_custom_export_sequence_view.xml',
        # 'security_custom/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}