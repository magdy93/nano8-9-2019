from odoo import models, fields, api


class nano_smart(models.Model):
    _inherit='sale.order'

    # type = fields.Selection(string="Type", selection=[('inside', 'local'), ('outside', 'outside'), ], required=False, )
    custom = fields.Integer(string="custom", required=False,compute='_custom_field' )
    logistic = fields.Integer(string="logistic", required=False,compute='_custom_field' )

    @api.multi
    def action_deliver_move_new(self):
        logiistic = self.env['nano.logistic']
        stok_id=logiistic.search([('sale_order', '=',self.id)])
        if stok_id:
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'nano_logistic.logistic_sale_view',
                    'res_model': 'nano.logistic',
                    'res_id': stok_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
            }

    @api.multi
    def custom_smart(self):
        customm = self.env['export.request']
        stokkk_id=customm.search([('sales_order_q', '=',self.id)])
        if stokkk_id:
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'sales_custom_request.view_form_export_request',
                    'res_model': 'export.request',
                    'res_id': stokkk_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
            }
        else:
            return True

    @api.multi
    @api.depends('type')
    def _custom_field(self):
        for rec in self:
            if self.type == 'local'and self.carrying == 'company':
                rec.logistic+=1
            elif self.type == 'outside':
                rec.custom+=1
                rec.logistic+=1
            else:
                rec.custom=0
                rec.logistic=0
