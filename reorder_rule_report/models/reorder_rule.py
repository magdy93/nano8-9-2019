# -*- coding: utf-8 -*-

from odoo import models, fields, api


class StockWarehouseOrderPoint(models.Model):
    _inherit = 'stock.warehouse.orderpoint'

    product_min_qty = fields.Float(string="Cretical Quantity ")
    nano_product_min_qty = fields.Float(string="Minimum Quantity ")
