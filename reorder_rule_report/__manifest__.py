# -*- coding: utf-8 -*-
{
    'name': "reorder_rule_report",
    'summary': """
        reorder_rule_report""",
    'description': """
        reorder_rule_report
    """,
    'author': "Magdy",
    'website': "http://www.yourcompany.com",
    'category': 'stock',
    'version': '0.1',
    'depends': ['stock'],
    'data': [
        'report/product_report_view.xml',
        'views/reorder_rule.xml',
    ],
}
