# -*- coding: utf-8 -*-

from odoo import fields, models, tools, api


class ProductTemplateReport(models.Model):
    _name = "product.template.report"
    _auto = False
    _rec_name = 'id'

    name = fields.Char(readonly=True)
    qty_available = fields.Float(compute="get_qty_available", readonly=True)
    nano_product_min_qty = fields.Float(compute="get_minimum_quantity", string="Critical Qty", readonly=True)
    product_min_qty = fields.Float(compute="get_minimum_quantity", string="Minimum Qty", readonly=True)
    virtual_available = fields.Float(compute="get_qty_available", readonly=True, string="Forcaste Qty")

    def _select(self):
        return """
        SELECT
        id, name
        """

    def _from(self):
        return """
        FROM product_template
        """

    @api.model_cr
    def init(self):
        """ Init Function"""
        tools.drop_view_if_exists(self._cr, self._table)
        query_str = """
            CREATE OR REPLACE VIEW %s AS (%s %s)
        """ % (self._table, self._select(), self._from())
        self._cr.execute(query_str)

    @api.multi
    def get_qty_available(self):
        for record in self:
            product_ids = self.env['product.template'].search([('name', '=', record.name)])
            if product_ids:
                for i in product_ids:
                    record.qty_available = i.qty_available
                    record.virtual_available = i.virtual_available

    @api.multi
    def get_minimum_quantity(self):
        for record in self:
            product_ids = self.env['stock.warehouse.orderpoint'].search([('product_id.name', '=', record.name)])
            if product_ids:
                for i in product_ids:
                    record.nano_product_min_qty = i.nano_product_min_qty
                    record.product_min_qty = i.product_min_qty
