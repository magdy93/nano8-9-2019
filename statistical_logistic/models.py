from odoo import models, fields, api
from datetime import datetime
#coding: utf-8


class statistical_logistics(models.Model):
      _name='statistical.logistics'

      static_id=fields.One2many('static.invoice','order_line')

      name = fields.Many2one(comodel_name="res.company", string="name", required=False, )
      street = fields.Char(string="address", required=False,related='name.street' )
      website = fields.Char(string="website", required=False, related='name.website' )
      phone = fields.Char(string="phone", required=False, related='name.phone')
      mobile = fields.Char(string="mobile", required=False, )
      fax = fields.Char(string="fax", required=False,related='name.fax' )
      email = fields.Char(string="email", required=False,related='name.email' )
      vat=fields.Char(string="Tax ID", required=False,related='name.vat' )
      country_id = fields.Many2one(comodel_name="res.country", string="Nationality", required=False,related='name.country_id', )
      company_registry = fields.Char(string="Company registry", required=False,related='name.company_registry' )
      registration_date = fields.Date(string="Date", required=False, )
      note_company=fields.Char(string="notes", required=False,)
      registration_import=fields.Char(string="import", required=False,)
      registration_export=fields.Char(string="export", required=False,)
      law=fields.Char(string="law", required=False,)
      registration_date1 = fields.Date(string="Date", required=False, )
      registration_date2 = fields.Date(string="Date", required=False, )
      sale_order=fields.Many2one('sale.order',)


      custom = fields.Char(string="Custom", required=False, )
      custom_system = fields.Char(string="Custom System", required=False, )
      complex = fields.Char(string="Complex", required=False, )
      statment_submitted = fields.Many2one(comodel_name="hr.employee", string="The Statement is Submitted", required=False, )
      year = fields.Selection([(num, str(num)) for num in range((datetime.now().year)-50 , (datetime.now().year)+50 )], 'Year',default=2000)
      series = fields.Char(string="Series", required=False, )
      wordPad = fields.Char(string="WordPad", required=False, )
      notes = fields.Char(string="Notes", required=False, )


      po_number = fields.Char(string="po number ", required=False, )
      num_package = fields.Integer(string="Number of Packages", required=False, )
      shipment_date = fields.Date(string="Date of shipment", required=False, )
      shipment_port = fields.Char(string="Shipment Port", required=False, )
      arrival_port = fields.Char(string="Arrival Port", required=False, )
      arrival_country = fields.Many2one(comodel_name="res.country", string="Arrival Country", required=False, )
      ship_agent = fields.Many2one(comodel_name="res.partner", string="Shipping Agent", required=False, )
      package = fields.Many2one(comodel_name="product.product", string="Packages Type",domain=[('is_package','=',True)], )
      gross_weight = fields.Float(string="Gross Weight",  required=False, )
      net_weight = fields.Float(string="Net Weight",  required=False, )
      uom = fields.Many2one(comodel_name="product.uom", string="UOM of Weight", required=False, )
      transportation = fields.Char(string="Transportation", required=False, )


      release_office = fields.Char(string="Release Office", required=False, )
      certificate_type = fields.Char(string="Certificate Type", required=False, )
      certificate_series = fields.Char(string="Certificate Series", required=False, )
      release_date = fields.Date(string="Release Date", required=False, )

      invoice_umber = fields.Char(string="Invoice Number", required=False, )
      invoice_date = fields.Date(string="Invoice Date", required=False, )
      currency = fields.Many2one(comodel_name="", string="Currency", required=False, )
      contract_type = fields.Char(string="Contract Type", required=False, )
      freight = fields.Char(string="Freight", required=False, )

class static_invoice(models.Model):
    _name='static.invoice'

    order_line=fields.Many2one('statistical.logistics')
    product = fields.Many2one(comodel_name="product.product",required=False, )
    describtion=fields.Char()
    unit = fields.Many2one(comodel_name="product.uom", required=False, )
    quantity = fields.Float( required=False, )
    price_unit = fields.Float(required=False,)
    subtotal = fields.Float( required=False,)


