# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockPicking(models.Model):
    _inherit = 'stock.picking'
    transportation_company_id = fields.Many2one(comodel_name="res.partner", string="Transportation company", required=False, )
    car_number = fields.Char(string="Car Number", required=False, )
    driver_name = fields.Char(string="Driver Name", required=False, )
    mobile_number = fields.Integer(string="Mobile Number", required=False, )
    id_number = fields.Integer(string="ID Number", required=False, )
    passport_number = fields.Integer(string="Passport number", required=False, )
    license_number = fields.Char(string="License Number", required=False, )
    license_document = fields.Binary(string="License Document", )
    check_in = fields.Datetime(string="Check In Time/Date", required=False, )
    check_out = fields.Datetime(string="Check out Time/Date", required=False, )

    @api.multi
    def do_new_transfer(self):
        res = super(StockPicking, self).do_new_transfer()
        if self.origin:
           driver_id = self.env['driver.information.line']
           transportation = self.env['transportation.detail']
           transportation_id = transportation.search([('purchase_id.name', '=', self.origin)])
           if transportation_id:
               for t in self:
                   for l in t.pack_operation_product_ids:
                       driver_id.create({
                           'transportation_id':transportation_id.id,
                           'driver_name':self.driver_name,
                           'check_in':self.check_in,
                           'check_out':self.check_out,
                           'product_id':l.product_id.id,
                           'quantity':l.qty_done,
                           'transportation_company_id':self.transportation_company_id.id,
                           'license_document':self.license_document,
                           'license_number':self.license_number,
                           'passport_number':self.passport_number,
                           'id_number':self.id_number,
                           'mobile_number':self.mobile_number,
                       })
               return res
           else:
               return res