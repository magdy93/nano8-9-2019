# -*- coding: utf-8 -*-

{
    'name': 'Nano Purchase Order',
    'description': 'Nano Purchase Order',
    'summary': 'UNSS-Purchase Module',
    'category': 'Purchase',
    'author': 'Universal Selective Systems',
    'depends': ['base', 'purchase', 'nano_custom', 'nano_logistic'],
    'data': [
        'security/ir.model.access.csv',
        'data/purchase_data.xml',
        'views/nano_purchase_order_view.xml',
        'views/payment.xml',
    ]
}
