# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError

from lxml import etree
import json


class AccountJournal(models.Model):
    _inherit = "account.journal"

    type = fields.Selection(selection_add=[('service', 'service')])


class ProductCategory(models.Model):
    _inherit = "product.category"

    responsible_user = fields.Many2one('res.users')


class PurchaseConfigSettings(models.TransientModel):

    _inherit = 'purchase.config.settings'

    default_total_price_nano = fields.Float(string="Supply chain Total Price", default_model="purchase.order")
    default_total_price_ceo = fields.Float(string="Ceo Total Price", default_model="purchase.order")


class nanoPurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    total_price_nano = fields.Float(string="Maximum Total Price")
    total_price_ceo = fields.Float(string="Ceo Total Price")
    order_line2 = fields.One2many('purchase.order.line', 'order_id', string='Order Lines',
                                  states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True)
    attach_rel = fields.One2many('nano.purchase.docs', 'purchase_order_id')
    container_ids = fields.One2many('nano.purchase.container', 'purchase_container_id')
    type = fields.Selection([('domestics', 'Domestics'), ('international', 'International')], string='Type')
    carrying = fields.Selection([('company', 'On our Company'), ('vendor', 'on the vendor')], string='Carrying')
    # it will effect on the warehouse and custom of purchase modules
    is_temporary_permit = fields.Boolean(string='IS Temporary Permit')
    temporary_stock = fields.Many2one(comodel_name="stock.picking.type", string="temporary stock", )
    # default=lambda self: self.env['stock.picking.type'].search([('is_temporary','=',True)])
    # Every option will effect in the vendor bill and cycle of purchase.
    payment_type = fields.Selection([('account', 'On account'), ('advance', 'On advance'), ('lc', 'LC')],
                                    string='Payment type')
    payment = fields.Many2one(comodel_name="purchase.payment.type", string="Payment", required=False, )

    state_of_logistic_request = fields.Integer('Logistic Request', compute='get_state_of_logistic_request')
    state_of_custom_request = fields.Char('Custom Request', compute='get_state_of_custom_request')
    state_of_lc_request = fields.Char('LC Request', compute='get_state_of_lc_request')

    # shipping date to be remove
    loading_port = fields.Char(string='Loading Port')
    unloading_port = fields.Char(string='Unloading Port')
    from_street = fields.Char(string='Street')
    from_street2 = fields.Char(string='Street 2')
    from_zip = fields.Char(string='ZIP', change_default=True)
    from_city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    from_country_id = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    wh_incoming_date = fields.Date(string='WH Incoming')
    # end of shipping date to be remove
    purchase_user_id = fields.Many2one('res.users', default=lambda self: self.env.user, string='Purchase User')
    purchase_manager_id = fields.Many2one('res.users', string='Purchase Manager')

    service_state = fields.Selection(string="service", selection=[('service', 'service'), ('purchase', 'purchase'), ],
                                     required=True, )
    service_sequence = fields.Char(string="name", readonly=1, required=False, index=True, copy=False, store=True)
    to_bill = fields.Boolean(string="To Bill", )
    is_logistic = fields.Boolean(string="Is Logistic", )
    imp_order = fields.Char(string="Import order", )
    purchase_service_id = fields.Many2one(comodel_name="purchase.order", string="Purchase Order", required=False, )
    state = fields.Selection(selection_add=[('2nd_approve', '2nd Approve'), ('3rd_approve', '3rd Approve')])
    is_approved = fields.Boolean()
    status_of_product = fields.Selection([
        ('manufactured', 'Under manufacture'),
        ('in_stock', 'delivered to warehouses'),
        ('with_logistics', 'Delivered to logistics'),
        ('in_customs_clearance_procedures', 'Delivered to Customs'),
        ('on_vessel', 'Delivered to deck of ship')], steing='Status Of Product')

    # if self.env.user.has_group('nano_plan.module_category_ceo_management'):
    #     if line.product_id.categ_id.responsible_user.has_group('nano_plan.module_category_ceo_management'):
    #         line.status_of_approve = 'approve'
    # elif self.env.user.has_group('group_supply_chain'):
    #     if line.product_id.categ_id.responsible_user.has_group('group_supply_chain'):
    #         line.status_of_approve == 'approve'

    @api.multi
    def first_purchase_approval(self):
        for record in self:
            if record.total_price_nano < record.amount_total and record.amount_total < record.total_price_ceo\
                    and record.state not in ['2nd_approve', '3rd_approve']:
                for line in record.order_line:
                    if line.product_id.categ_id.responsible_user.has_group('nano_purchase_order.group_supply_chain')\
                            and line.status_of_approve != 'approve':
                            raise UserError(_("second approval required"))
                    elif line.product_id.categ_id.responsible_user.has_group('nano_plan.module_category_ceo_management')\
                            and line.status_of_approve != 'approve':
                        raise UserError(_("Third approval required"))
                raise UserError(_("second approval required"))
            elif record.amount_total > record.total_price_ceo\
                    and record.state not in ['2nd_approve', '3rd_approve']:
                for line in record.order_line:
                    if line.product_id.categ_id.responsible_user.has_group('nano_plan.module_category_ceo_management')\
                            and line.status_of_approve == 'no_approve':
                        raise UserError(_("Third approval required"))
                    elif line.product_id.categ_id.responsible_user.has_group('nano_purchase_order.group_supply_chain')\
                            and line.status_of_approve == 'no_approve':
                            raise UserError(_("second approval required"))
                raise UserError(_("Third approval required"))
            else:
                record.write({
                    'is_approved': True,
                    # 'state': 'draft'
                })
                # raise UserError(_("No approval required you can confirm the rfq"))

    @api.multi
    def button_confirm(self):
        for order in self:
            if not order.purchase_manager_id:
                order.purchase_manager_id = self.env.user.id

            if order.state not in ['draft', 'sent', '2nd_approve', '3rd_approve']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step'\
                    or (order.company_id.po_double_validation == 'two_step'\
                        and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id))\
                    or order.user_has_groups('purchase.group_purchase_manager'):
                order.button_approve()
            else:
                order.write({'state': 'to approve'})
            order.write({
                    'is_approved': False,
                })
        return True

    @api.multi
    def second_purchase_approval(self):
        for record in self:
            for line in record.order_line:
                if self.env.user.has_group('nano_purchase_order.group_supply_chain'):
                    if line.product_id.categ_id.responsible_user.has_group('nano_purchase_order.group_supply_chain'):
                        line.write({'status_of_approve': 'approve'})
            record.write({
                'state': '2nd_approve'
            })
    @api.multi
    def third_purchase_approval(self):
        for record in self:
            for line in record.order_line:
                if self.env.user.has_group('nano_plan.module_category_ceo_management'):
                    if line.product_id.categ_id.responsible_user.has_group('nano_plan.module_category_ceo_management'):
                        line.write({'status_of_approve': 'approve'})
            record.write({
                'state': '3rd_approve'
            })

    @api.multi
    @api.onchange('status_of_product')
    def create_journal(self):
        # print "jjjjjjj",self._origin.id
        # print "jjjjjjj",self.incoterm_id.name
        # print "jjjjjjj",self.status_of_product
        stock_picking_id = self.sudo().env['stock.picking'].sudo().search([('purchase_id', '=', self._origin.id)])
        logistic_id = self.sudo().env['logistic.purchase'].sudo().search([('purchase_order', '=', self._origin.id)])
        custom_id = self.sudo().env['nano.custom'].sudo().search([('purchase_logistic_id', '=', self._origin.id)])
        # print "stock_picking_id :",stock_picking_id.name
        # print "purchase_order :",logistic_id.purchase_order.name , logistic_id.stat
        # print "custom_id :",custom_id.purchase_logistic_id.name,custom_id.stat
        if self.incoterm_id.name == 'EX WORKS' and self.status_of_product == 'manufactured' \
                and logistic_id.stat == 'todo' and custom_id.stat == 'todo':
            account_move_id = self.sudo().env['account.move']
            account_move_obj = account_move_id.sudo().search([('ref', '=', stock_picking_id.name)])
            account_move_line_id = self.with_context(dict(self._context, check_move_validity=False)).sudo().env[
                'account.move.line']
            print
            "lll"
            for line in self.order_line:
                if account_move_obj:
                    print
                    "11111111"
                    for ref in account_move_obj:
                        print
                        "2222222"
                        for j in ref.line_ids:
                            print
                            "33333333"
                            if line.product_id.categ_id.property_stock_journal.id == j.journal_id.id:
                                print
                                "4444444"
                                account_move_line_id.write({
                                    'move_id': ref.id,
                                    'partner_id': self.partner_id.id,
                                    'journal_id': line.product_id.categ_id.property_stock_journal.id,
                                    'account_id': line.product_id.categ_id.property_stock_account_input_categ_id.id,
                                    'name': line.product_id.name,
                                    'debit': 0.0,
                                    'credit': line.price_subtotal,
                                })

                                account_move_line_id.write({
                                    'move_id': ref.id,
                                    'partner_id': self.partner_id.id,
                                    'journal_id': line.product_id.categ_id.property_stock_journal.id,
                                    'account_id': line.product_id.categ_id.property_stock_account_output_categ_id.id,
                                    'name': line.product_id.name,
                                    'debit': line.price_subtotal,
                                    'credit': 0.0,
                                })
                else:
                    print
                    "55555555"
                    print
                    "product : ", line.product_id.name
                    journal_id = account_move_id.create({
                        'journal_id': line.product_id.categ_id.property_stock_journal.id,
                        'ref': stock_picking_id.name,
                    })
                    account_move_line_id.create({
                        'move_id': journal_id.id,
                        'partner_id': self.partner_id.id,
                        'journal_id': line.product_id.categ_id.property_stock_journal.id,
                        'account_id': line.product_id.categ_id.property_stock_account_input_categ_id.id,
                        'name': line.product_id.name,
                        'debit': 0.0,
                        'credit': line.price_subtotal,
                    })

                    account_move_line_id.create({
                        'move_id': journal_id.id,
                        'partner_id': self.partner_id.id,
                        'journal_id': line.product_id.categ_id.property_stock_journal.id,
                        'account_id': line.product_id.categ_id.property_stock_account_output_categ_id.id,
                        'name': line.product_id.name,
                        'debit': line.price_subtotal,
                        'credit': 0.0,
                    })


        else:
            print
            "no"

    @api.multi
    def action_view_invoice(self):
        '''
        This function returns an action that display existing vendor bills of given purchase order ids.
        When only one found, show the vendor bill immediately.
        '''
        action = self.env.ref('account.action_invoice_tree2')
        result = action.read()[0]
        ser = False
        journal_id = False
        if self.service_state == 'service':
            ser = True
            journal_id = self.env['account.journal'].search([('type', '=', 'service')], limit=1).id
        else:
            journal_id = self.env['account.journal'].search([('type', '=', 'purchase')], limit=1).id
        # override the context to get rid of the default filtering
        result['context'] = {'type': 'in_invoice', 'default_purchase_id': self.id,
                             'default_is_service': ser,
                             'default_journal_id': journal_id,
                             'default_payment_type': self.payment_type}

        if not self.invoice_ids:
            # Choose a default account journal in the same currency in case a new invoice is created
            journal_domain = [
                ('type', '=', 'purchase'),
                ('company_id', '=', self.company_id.id),
                ('currency_id', '=', self.currency_id.id),
            ]
            default_journal_id = self.env['account.journal'].search(journal_domain, limit=1)
            if default_journal_id:
                result['context']['default_journal_id'] = default_journal_id.id
        else:
            # Use the same account journal than a previous invoice
            result['context']['default_journal_id'] = self.invoice_ids[0].journal_id.id

        # choose the view_mode accordingly
        if len(self.invoice_ids) != 1:
            result['domain'] = "[('id', 'in', " + str(self.invoice_ids.ids) + ")]"
        elif len(self.invoice_ids) == 1:
            res = self.env.ref('account.invoice_supplier_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = self.invoice_ids.id
        # if self.service_state == 'service' and self.to_bill == False and self.is_logistic == True:
        #     raise UserError(_("Service Not Done Yet"))
        return result

    @api.onchange('is_logistic')
    def get_logistic_vendor(self):
        if self.is_logistic == True:
            vendor_ids = []
            domain = [['id', '=', False]]
            vendor_obj = self.env['res.partner'].search([('category_id.name', 'in', ['logistic', 'logistics'])])
            for obj in vendor_obj:
                vendor_ids.append(obj.id)
            domain = [('id', 'in', vendor_ids)]
            return {'domain': {'partner_id': domain}}

    @api.onchange('service_state')
    def check_show(self):
        if self.service_state == 'service':
            print
            "service"
            bill_ids = []
            domain = [['id', '=', False]]
            bill_of_mat = self.env['product.product'].search([('landed_cost_ok', '=', True)])
            for bill in bill_of_mat:
                bill_ids.append(bill.id)
            domain = [('id', 'in', bill_ids)]
            return {'domain': {'order_line.product_id': domain}}

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        context = self._context or {}
        res = super(nanoPurchaseOrder, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                             submenu=False)
        if context.get('service'):
            for field in res['fields']:
                if field == 'state':
                    res['fields'][field]['selection'] = [('draft', 'service'),
                                                         ('sent', 'service Sent'),
                                                         ('to approve', 'To Approve'),
                                                         ('purchase', 'service Order'),
                                                         ('done', 'Locked'),
                                                         ('cancel', 'Cancelled')
                                                         ]
        return res

    @api.model
    def create(self, vals):
        if vals['service_state'] == 'service':
            vals['service_sequence'] = 'Service'

            vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order.service') or '/'
        # else:
        #     if vals.get('name', 'New') == 'New':
        #         vals['service_sequence'] = 'purchase'
        #         vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order') or '/'
        return super(nanoPurchaseOrder, self).create(vals)

    # @api.multi
    # def button_confirm(self):
    #     res = super(nanoPurchaseOrder, self).button_confirm()
    #     for rec in self:
    #         if not rec.purchase_manager_id:
    #             print
    #             "False"
    #             rec.purchase_manager_id = self.env.user.id
    #             return res
    #         else:
    #             return res

    @api.model
    def _prepare_picking(self):
        if not self.group_id:
            self.group_id = self.group_id.create({
                'name': self.name,
                'partner_id': self.partner_id.id
            })
        if not self.partner_id.property_stock_supplier.id:
            raise UserError(_("You must set a Vendor Location for this partner %s") % self.partner_id.name)
        return {
            'picking_type_id': self.temporary_stock.id if self.is_temporary_permit == True else self.picking_type_id.id,
            'partner_id': self.partner_id.id,
            'date': self.date_order,
            'origin': self.name,
            'location_dest_id': self._get_destination_location(),
            'location_id': self.partner_id.property_stock_supplier.id,
            'company_id': self.company_id.id,
        }

    @api.multi
    def get_state_of_logistic_request(self):
        for record in self:
            logistic_ids = self.env['logistic.purchase'].search([('purchase_order', '=', record.id)])
            if logistic_ids:
                record.state_of_logistic_request = len(logistic_ids)
            else:
                record.state_of_logistic_request = 0

    @api.multi
    def get_state_of_custom_request(self):
        for record in self:
            custom_ids = self.env['nano.custom'].search([('purchase_logistic_id', '=', record.id)])
            if custom_ids:
                record.state_of_custom_request = len(custom_ids)
            else:
                record.state_of_custom_request = 0

    @api.multi
    def get_state_of_lc_request(self):
        for record in self:
            lc_ids = self.env['nano.lc'].search([('purchase_order', '=', record.id)])
            if lc_ids:
                record.state_of_lc_request = len(lc_ids)
            else:
                record.state_of_lc_request = 0

    @api.multi
    def action_state_of_logistic_request(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'logistic purchase',
            'res_model': 'logistic.purchase',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain': [('purchase_order', '=', self.id)],
            'target': 'current',
        }

    @api.multi
    def action_state_of_custom_request(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Custom Management',
            'res_model': 'nano.custom',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain': [('purchase_logistic_id', '=', self.id)],
            'target': 'current',
        }

    @api.multi
    def action_state_of_lc_request(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'LC Management',
            'res_model': 'nano.lc',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain': [('purchase_order', '=', self.id)],
            'target': 'current',
        }

    @api.depends('order_line.date_planned', 'order_line2.date_planned')
    def _compute_date_planned(self):
        for order in self:
            min_date = False
            if self.order_line:
                for line in order.order_line:
                    if not min_date or line.date_planned < min_date:
                        min_date = line.date_planned
                if min_date:
                    order.date_planned = min_date
            else:
                for line in order.order_line2:
                    if not min_date or line.date_planned < min_date:
                        min_date = line.date_planned
                if min_date:
                    order.date_planned = min_date


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'
    is_temporary = fields.Boolean(string="Temporary", )


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    payment_type = fields.Selection([('account', 'On account'), ('advance', 'On advance'), ('lc', 'LC')],
                                    string='Payment type')
    number = fields.Char(related='', store=True,compute='get_number', readonly=True, copy=False)
    # service_state = fields.Selection(string="service", selection=[('service', 'service'), ('purchase', 'purchase'), ],
    #                                  required=True,)
    is_service = fields.Boolean()

    @api.onchange('is_service')
    def get_journal(self):
        if self.is_service == True:
            journale_id = self.env['account.journal'].search([('type', '=', 'service')], limit=1)
            if journale_id:
                self.journal_id = journale_id.id

    # @api.multi
    # def _default_journal(self):
    #     if self.is_service == True:
    #         print("1111111111")
    #         self.journal_id = self.env['account.journal'].search([('type', '=', 'service')], limit=1).id
    #     else:
    #         print("22222222")
    #         return super(AccountInvoice, self)._default_journal()


    @api.depends('is_service', 'move_id')
    def get_number(self):
        for record in self:
            if record.is_service == True:
                record.number = self.env['ir.sequence'].next_by_code('account.invoice.sequence')
            else:
                record.number = record.move_id.name


    @api.onchange('partner_id', 'company_id', 'payment_type')
    def _onchange_partner_id(self):
        account_id = False
        payment_term_id = False
        fiscal_position = False
        bank_id = False
        warning = {}
        domain = {}
        company_id = self.company_id.id
        p = self.partner_id if not company_id else self.partner_id.with_context(force_company=company_id)
        type = self.type
        if p:
            rec_account = p.property_account_receivable_id
            pay_account = p.property_account_payable_id
            on_account = p.on_account_vendor  # magdy
            on_advance = p.on_advance  # magdy
            lc_account = p.lc_account  # magdy
            if not rec_account and not pay_account:
                action = self.env.ref('account.action_account_config')
                msg = _(
                    'Cannot find a chart of accounts for this company, You should configure it. \nPlease go to Account Configuration.')
                raise RedirectWarning(msg, action.id, _('Go to the configuration panel'))

            if type in ('out_invoice', 'out_refund'):
                account_id = rec_account.id
                payment_term_id = p.property_payment_term_id.id
            else:
                if self.payment_type == 'account':  # magdy
                    account_id = on_account.id
                    payment_term_id = p.property_supplier_payment_term_id.id
                elif self.payment_type == 'advance':  # magdy
                    account_id = on_advance.id
                    payment_term_id = p.property_supplier_payment_term_id.id
                elif self.payment_type == 'lc':  # magdy
                    account_id = lc_account.id
                    payment_term_id = p.property_supplier_payment_term_id.id
                else:
                    account_id = pay_account.id
                    payment_term_id = p.property_supplier_payment_term_id.id

            delivery_partner_id = self.get_delivery_partner_id()
            fiscal_position = self.env['account.fiscal.position'].get_fiscal_position(self.partner_id.id,
                                                                                      delivery_id=delivery_partner_id)

            # If partner has no warning, check its company
            if p.invoice_warn == 'no-message' and p.parent_id:
                p = p.parent_id
            if p.invoice_warn != 'no-message':
                # Block if partner only has warning but parent company is blocked
                if p.invoice_warn != 'block' and p.parent_id and p.parent_id.invoice_warn == 'block':
                    p = p.parent_id
                warning = {
                    'title': _("Warning for %s") % p.name,
                    'message': p.invoice_warn_msg
                }
                if p.invoice_warn == 'block':
                    self.partner_id = False

        self.account_id = account_id
        self.payment_term_id = payment_term_id
        self.date_due = False
        self.fiscal_position_id = fiscal_position

        if type in ('in_invoice', 'out_refund'):
            bank_ids = p.commercial_partner_id.bank_ids
            bank_id = bank_ids[0].id if bank_ids else False
            self.partner_bank_id = bank_id
            domain = {'partner_bank_id': [('id', 'in', bank_ids.ids)]}

        res = {}
        if warning:
            res['warning'] = warning
        if domain:
            res['domain'] = domain
        return res


class nanoPurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    status_of_product = fields.Selection([
        ('manufactured', 'Under manufacture'),
        ('in_stock', 'delivered to warehouses'),
        ('with_logistics', 'Delivered to logistics'),
        ('in_customs_clearance_procedures', 'Delivered to Customs'),
        ('on_vessel', 'Delivered to deck of ship')], steing='Status Of Product')

    status_of_approve = fields.Selection([('approve', 'approve'), ('no_approve', 'not approved')],
                                         steing='Status Of Approve')


class nanoPurchaseDocument(models.Model):
    _name = 'nano.purchase.docs'

    purchase_order_id = fields.Many2one('purchase.order')
    document = fields.Char(string='Document')
    doc_status = fields.Selection([('required', 'required'), ('received', 'received')], string='Status')
    attachment_ids = fields.Many2many('ir.attachment', 'nano_purchase_order_ir_attachments_rel',
                                      'nano_purchase_order_id', 'attachment_id', string='Attachments')
    is_send = fields.Boolean('Is Send ?')
    flag = fields.Integer(default=0)


class nanoPurchaseContainer(models.Model):
    _name = 'nano.purchase.container'

    purchase_container_id = fields.Many2one('purchase.order')
    document = fields.Char(string='Container Number')
    attachment_ids = fields.Binary(string="Attachments", )
    is_send_container = fields.Boolean('Is Send ?', default=1)
    flag_container = fields.Integer(default=0)
    # attachment_ids = fields.Many2many('ir.attachment', 'nano_purchase_order_ir_attachments_rel',
    #                                   'nano_purchase_order_id', 'attachment_id', string='Attachments')
