from openerp import models, fields, api

class tax_quan_line(models.Model):
    _inherit='stock.picking'

    text = fields.Char(string="text", required=False, )
    state = fields.Selection([
        ('draft', 'Draft'), ('cancel', 'Cancelled'),
        ('waiting', 'Waiting Another Operation'),
        ('confirmed', 'Waiting Availability'),
        ('partially_available', 'Partially Available'),
        ('assigned', 'Available'),
        ('assurance', 'Quality assurance'),
        ('Purchasing', 'Purchasing Approved'),
        ('done', 'Done')], string='Status', compute='_compute_state',
        copy=False, index=True, readonly=True, store=True, track_visibility='onchange',)

    @api.multi
    def assurance_modify(self):
        self.ensure_one()
        self.write({
            'state': 'assurance',
        })
        for record in self:
            groups = self.env['res.groups'].search([('name', '=', 'QualityAssurance')])
            recipient_partners = []
            for group in groups:
                for recipient in group.users:
                    if recipient.partner_id.id not in recipient_partners:
                        recipient_partners.append(recipient.partner_id.id)
            if len(recipient_partners):
                record.message_post(body='Quantity available',force_send=True,subtype='mt_comment',
                                  subject='test',partner_ids=recipient_partners,message_type='notification')

    @api.multi
    def purchase_modify(self):
        self.ensure_one()
        self.write({
            'state': 'Purchasing',
        })
