# -*- coding: utf-8 -*-

from openerp.exceptions import UserError, ValidationError
from openerp import models, fields, api , exceptions , _
from datetime import datetime
import time
# import openerp.addons.decimal_precision as dp


class shift_allowance(models.Model):
    _name = 'shift.allowance'
    _inherit = ['mail.thread']

    employee = fields.Many2one('hr.employee', string="Employee")
    _from = fields.Date('From')
    _to = fields.Date('To')
    days = fields.Float('Experience Duration', compute='_compute_days', readonly="true")
    amount_day = fields.Float('Amount / Day')
    total_value = fields.Float('Total Value', compute='_compute_total_value', readonly="true")
    state = fields.Selection([('Draft', 'Draft'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True )

    @api.depends('_from','_to')
    def _compute_days(self):
        for rec in self:
            if rec._from and rec._to:
                fmt = '%Y-%m-%d'
                _from = datetime.strptime(rec._from, fmt) #start date
                _to = datetime.strptime(rec._to, fmt)   #end date

                days = float((_to - _from).days + 1)
                rec.days = days
            else:
                rec.days = 0

    @api.depends('days')
    def _compute_total_value(self):
        for rec in self:
            if rec.days and rec.amount_day:
                rec.total_value = rec.amount_day * rec.days
            else:
                rec.total_value = 0

    @api.multi
    def action_save_shift_allowance(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New shift allowance saved"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Draft'})

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Hr Manager Approval'})
        body = "Shift Allowance Approved from HR"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New shift allowance refused"
        self.message_post(body=body, message_type='email')
        return {}



class meal_allowance(models.Model):
    _name = 'meal.allowance'
    _inherit = ['mail.thread']

    mode = fields.Selection([('Employee', 'Employee'),
                             ('Employee Tag', 'Employee Tag')],
                              'Mode')
    employee = fields.Many2one('hr.employee', string="Employee")
    employee_tag = fields.Many2one('hr.employee.category', string="Employee Tag")
    _from = fields.Date('From')
    _to = fields.Date('To')
    amount = fields.Float('Amount')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True )

    @api.multi
    def action_save_meal_allowance(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Meal allowance saved"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Draft'})

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Hr Manager Approval'})
        if self.mode == "Employee Tag":
            employees = self.env['hr.employee'].search([('category_ids', 'in', self.employee_tag.id)])
            for emp in employees:
                meal_allowance = self.sudo().env['meal.allowance'].create({
                                                                    'mode': 'Employee',
                                                                    'employee': emp.id,
                                                                    '_from': self._from,
                                                                    '_to': self._to,
                                                                    'amount': self.amount,
                                                                    'state': 'Hr Manager Approval'})
        body = "Meal Allowance Approved from HR"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New Meal allowance refused"
        self.message_post(body=body, message_type='email')
        return {}


class day_or_official_vacations(models.Model):
    _name = 'day.or.official.vacations'
    _inherit = ['mail.thread']

    employee = fields.Many2one('hr.employee', string="Employee")
    emp_code = fields.Char('Employee Code', related="employee.full_emp_nu", readonly="true", store=True)
    _from = fields.Datetime('From')
    _to = fields.Datetime('To')
    hours = fields.Float('Hours', compute='_compute_hours', readonly="true")
    reason = fields.Text('Reason')
    amount = fields.Float('Amount')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True )

    @api.depends('_from','_to')
    def _compute_hours(self):
        for rec in self:
            if rec._from and rec._to:
                fmt = '%Y-%m-%d %H:%M:%S'
                d1 = datetime.strptime(rec._from, fmt)
                d2 = datetime.strptime(rec._to, fmt)
                # convert to unix timestamp
                d1_ts = time.mktime(d1.timetuple())
                d2_ts = time.mktime(d2.timetuple())

                timedelta = d2_ts - d1_ts
                diff_hours = float((timedelta / 60) / 60)
                rec.hours = diff_hours
            else:
                rec.hours = 0

    @api.model
    def create(self, vals):
        res = super(day_or_official_vacations, self).create(vals)
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Vacation allowance saved"
            res.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        res.write({'state': 'Draft'})
        return res

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Hr Manager Approval'})
        body = "New Vacation Allowance Approved from HR"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New Vacation allowance refused"
        self.message_post(body=body, message_type='email')
        return {}


class bonus_subsidy(models.Model):
    _name = 'bonus.subsidy'
    _inherit = ['mail.thread']

    type = fields.Selection([('bonus', 'bonus'),
                             ('subsidy', 'subsidy')],
                              'Type')
    mode = fields.Selection([('Employee', 'Employee'),
                             ('Employee Tag', 'Employee Tag')],
                              'Mode')
    employee = fields.Many2one('hr.employee', string="Employee")
    employee_tag = fields.Many2one('hr.employee.category', string="Employee Tag")
    date = fields.Date('Date')
    amount = fields.Float('Amount')
    reason = fields.Text('Reason')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True )


    @api.multi
    def action_save_bonus_subsidy(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New bonus and subsidy saved"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Draft'})

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Hr Manager Approval'})
        if self.mode == "Employee Tag":
            employees = self.env['hr.employee'].search([('category_ids', 'in', self.employee_tag.id)])
            for emp in employees:
                bonus_subsidy = self.sudo().env['bonus.subsidy'].create({
                                                                    'type': self.type,
                                                                    'mode': 'Employee',
                                                                    'employee': emp.id,
                                                                    'date': self.date,
                                                                    'amount': self.amount,
                                                                    'reason': self.reason,
                                                                    'state': 'Hr Manager Approval'})
        body = "New bonus and subsidy Approved from HR"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New bonus and subsidy refused"
        self.message_post(body=body, message_type='email')
        return {}


class day_or_official_vacations_work_order(models.Model):
    _name = 'day.or.official.vacations.work.order'
    _inherit = ['mail.thread']

    employee = fields.Many2one('hr.employee', _('Employee'))
    emp_code = fields.Char(_('Employee Code'), related="employee.full_emp_nu", readonly="true", store=True)
    emp_department = fields.Many2one('hr.department', _('Department'), related="employee.department_id" , readonly="true", store=True)
    emp_company = fields.Many2one('res.company', _('Company'), related="employee.company" , readonly="true", store=True)
    emp_job = fields.Many2one('hr.job', _('Job'), related="employee.job_id" , readonly="true", store=True)
    type = fields.Selection([('day off', 'day off'),
                             ('official vacation', 'official vacation')],
                              _('Type'))
    date = fields.Date(_('Date'))
    working_from = fields.Float(_('From'))
    working_to = fields.Float(_('To'))
    reason = fields.Text(_('Reason'))
    state = fields.Selection([('Draft', 'Draft'),
                              ('Hr Manager', 'Hr Manager'),
                              ('Approved','Approved')],
                               _('Status'), readonly=True )
    is_department_manager = fields.Boolean(compute='_compute_is_dep_manager')

    @api.model
    def create(self, vals):
        res = super(day_or_official_vacations_work_order, self).create(vals)
        if not res.emp_department.manager_id:
           raise ValidationError('There is no manager for this Department')
        if res.is_department_manager == 0:
           raise ValidationError('You are not manager for this employee')
        recipient_partners = []
        if res.employee.user_id.partner_id.id not in recipient_partners:
            recipient_partners.append(res.employee.user_id.partner_id.id)
        if len(recipient_partners):
            body = "you assigned on the draft day off & official vacation work order"
            res.sudo().message_post(body=body, partner_ids=recipient_partners, message_type='email')
        res.sudo().write({'state': 'Draft'})
        return res

    # @api.multi
    # def action_save_vacations_work_order(self):
    #     if not self.emp_department.manager_id:
    #        raise exceptions.ValidationError("There is no manager for this department")
    #     recipient_partners = []
    #     if self.employee.id not in recipient_partners:
    #         recipient_partners.append(self.employee.id)
    #         if len(recipient_partners):
    #             body = "you assigned on the draft day off & official vacation work order"
    #             self.sudo().message_post(body=body, message_type='email')
    #     self.sudo().write({'state': 'Draft'})
    #     return {}

    @api.multi
    def action_direct_manager_approval(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New vacation work order approved from direct manager"
            self.sudo().message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.sudo().write({'state': 'Hr Manager'})
        return {}

    @api.multi
    def action_hr_approval(self):
        recipient_partners = []
        if self.employee.user_id.partner_id.id not in recipient_partners:
            recipient_partners.append(self.employee.user_id.partner_id.id)
        if not self.emp_department.manager_id:
           raise exceptions.ValidationError("There is no manager for this department")
        else:
            if self.emp_department.manager_id.user_id.partner_id.id not in recipient_partners:
                recipient_partners.append(self.emp_department.manager_id.user_id.partner_id.id)
        if len(recipient_partners):
            body = "Your vacation work order approved from HR manager"
            self.sudo().message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.sudo().write({'state': 'Approved'})
        return {}

    @api.multi
    def _compute_is_dep_manager(self):
        for record in self:
            if(record.emp_department.manager_id.user_id.id == self.env.uid):
                record.is_department_manager = 1
            else:
                record.is_department_manager = 0

    @api.constrains('emp_department')
    def _emp_department(self):
        if not self.emp_department.manager_id:
           raise ValidationError('There is no manager for this Department')
        if self.is_department_manager == 0:
           raise ValidationError('You are not manager for this employee')


class permeation_request(models.Model):
    _name = 'permeation.request'
    _inherit = ['mail.thread']

    employee = fields.Many2one('hr.employee', _('Employee'))
    emp_code = fields.Char(_('Employee Code'), related="employee.full_emp_nu", readonly="true", store=True)
    emp_department = fields.Many2one('hr.department', _('Department'), related="employee.department_id" , readonly="true", store=True)
    emp_company = fields.Many2one('res.company', _('Company'), related="employee.company" , readonly="true", store=True)
    emp_job = fields.Many2one('hr.job', _('Job'), related="employee.job_id" , readonly="true", store=True)
    type = fields.Selection([('sign in', 'sign in'),
                             ('sign out', 'sign out')],
                              _('Type'))
    date = fields.Date(_('Date'))
    working_from = fields.Float(_('From'))
    working_to = fields.Float(_('To'))
    reason = fields.Text(_('Reason'))
    state = fields.Selection([('Draft', 'Draft'),
                              ('Direct Manager', 'Direct Manager'),
                              ('Hr Manager', 'Hr Manager'),
                              ('Approve','Approve'),
                              ('Refuse','Refuse')],
                               _('Status'), readonly=True )
    is_department_manager = fields.Boolean(compute='_compute_is_dep_manager')

    @api.model
    def create(self, vals):
        res = super(permeation_request, self).create(vals)
        body = "New permeation request confirmed"
        res.sudo().message_post(body=body, message_type='email')
        res.sudo().write({'state': 'Draft'})
        return res

    # @api.multi
    # def action_save_permeation_request(self):
    #     body = "New permeation request confirmed"
    #     self.message_post(body=body, message_type='email')
    #     self.write({'state': 'Draft'})
    #     return {}

    @api.multi
    def action_permeation_request_to_manager(self):
        if not self.emp_department.manager_id:
           raise exceptions.ValidationError("There is no manager for this department")
        recipient_partners = []
        if self.emp_department.manager_id.user_id.partner_id.id not in recipient_partners:
            recipient_partners.append(self.emp_department.manager_id.user_id.partner_id.id)
            if len(recipient_partners):
                body = "New permeation request sent to direct manager"
                self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Direct Manager'})
        return {}

    @api.multi
    def action_direct_manager_approval(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New permeation request approved from direct manager"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Hr Manager'})
        return {}

    @api.multi
    def action_hr_approval(self):
        body = "permeation request approved from HR manager"
        self.message_post(body=body, message_type='email')
        self.write({'state': 'Approve'})
        return {}

    @api.multi
    def action_hr_refuse(self):
        body = "permeation request refused from HR manager"
        self.message_post(body=body, message_type='email')
        self.write({'state': 'Refuse'})
        return {}

    @api.multi
    def _compute_is_dep_manager(self):
        for record in self:
            if(record.emp_department.manager_id.user_id.id == self.env.uid):
                record.is_department_manager = 1
            else:
                record.is_department_manager = 0


class allowance_variable(models.Model):
    _name = 'allowance.variable'
    _inherit = ['mail.thread']

    mode = fields.Selection([('Employee', 'Employee'),
                             ('Employee Tag', 'Employee Tag')],
                              'Mode')
    employee = fields.Many2one('hr.employee', string="Employee")
    employee_tag = fields.Many2one('hr.employee.category', string="Employee Tag")
    date = fields.Date('Date')
    variable = fields.Float('المتغير')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True )

    @api.model
    def create(self, vals):
        res = super(allowance_variable, self).create(vals)
        groups = res.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New allowance variable saved"
            res.sudo().message_post(body=body, partner_ids=recipient_partners, message_type='email')
        res.sudo().write({'state': 'Draft'})
        return res

    # @api.multi
    # def action_save_allowance_variable(self):
    #     groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
    #     recipient_partners = []
    #     for group in groups:
    #         for recipient in group.users:
    #             if recipient.partner_id.id not in recipient_partners:
    #                 recipient_partners.append(recipient.partner_id.id)
    #     if len(recipient_partners):
    #         body = "New allowance variable saved"
    #         self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
    #     self.write({'state': 'Draft'})
    #     return {}

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Hr Manager Approval'})
        if self.mode == "Employee Tag":
            employees = self.env['hr.employee'].search([('category_ids', 'in', self.employee_tag.id)])
            for emp in employees:
                insurance_application = self.env['allowance.insurance.application'].search([('employee', '=', emp.id)])
                if insurance_application:
                    insurance_application.sudo().write({'variable': self.variable })
        else:
            if self.mode == "Employee":
                insurance_application = self.env['allowance.insurance.application'].search([('employee', '=', self.employee.id)])
                if insurance_application:
                    insurance_application.sudo().write({'variable': self.variable })

        body = "Allowance Variable Approved from HR"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "allowance variable refused"
        self.message_post(body=body, message_type='email')
        return {}


class allowance_insurance_application(models.Model):
    _name = 'allowance.insurance.application'
    _inherit = ['mail.thread']

    employee = fields.Many2one('hr.employee', string="Employee")
    gender = fields.Selection('Gender', related="employee.gender", readonly="true", store=True)
    insurance_no = fields.Char('Insurance number', related="employee.insurance_no", readonly="true", store=True)
    birth = fields.Date('Date of birth', related="employee.birthday", readonly="true", store=True)
    join = fields.Date('Join date', related="employee.join_date", readonly="true", store=True)
    basic_salary = fields.Float('Basic salary', compute="_compute_emp_salary", readonly="true", store=True)
    variable = fields.Float('المتغير')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True )

    @api.depends('employee')
    def _compute_emp_salary(self):
        for rec in self:
            contract = self.env['hr.contract'].search([('employee_id', '=', rec.employee.id)])
            if contract:
                for cont in contract:
                    rec.basic_salary = cont.wage

    @api.model
    def create(self, vals):
        res = super(allowance_insurance_application, self).create(vals)
        groups = res.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Vacation allowance saved"
            res.sudo().message_post(body=body, partner_ids=recipient_partners, message_type='email')
        res.sudo().write({'state': 'Draft'})
        return res

    # @api.multi
    # def action_save_insurance_application(self):
    #     groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
    #     recipient_partners = []
    #     for group in groups:
    #         for recipient in group.users:
    #             if recipient.partner_id.id not in recipient_partners:
    #                 recipient_partners.append(recipient.partner_id.id)
    #     if len(recipient_partners):
    #         body = "New Vacation allowance saved"
    #         self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
    #     self.write({'state': 'Draft'})
    #     return {}

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Hr Manager Approval'})
        body = "New Vacation Allowance Approved from HR"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New Vacation allowance refused"
        self.message_post(body=body, message_type='email')
        return {}


class transfer_promotion(models.Model):
    _name = 'transfer.promotion'
    _inherit = ['mail.thread']

    type = fields.Selection([('transfer', 'transfer'),
                             ('promotion', 'promotion')],
                              'Type')
    employee = fields.Many2one('hr.employee', _("Employee"))
    department_from = fields.Many2one('hr.department', _('Department From'))
    company_from = fields.Many2one('res.company', _('Company From'))
    department_to = fields.Many2one('hr.department', _('Department To'))
    company_to = fields.Many2one('res.company', _('Company To'))
    job_from = fields.Many2one('hr.job', _('Job From'))
    job_to = fields.Many2one('hr.job', _('Job To'))
    date = fields.Date('Date')
    current_salary = fields.Float('Current salary', compute="_compute_emp_salary", readonly="true", store=True)
    new_salary = fields.Float('New salary')
    notes = fields.Text('Notes')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Submit', 'Submit'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('CEO Approval', 'CEO Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True)
    is_department_manager = fields.Boolean(compute='_compute_is_dep_manager')


    @api.onchange('employee')
    def _onchange_employee(self):
        self.department_from = self.employee.department_id
        self.job_from = self.employee.job_id
        return {}

    @api.depends('employee')
    def _compute_emp_salary(self):
        for rec in self:
            contract = self.env['hr.contract'].search([('employee_id', '=', rec.employee.id)])
            if contract:
                for cont in contract:
                    rec.current_salary = cont.wage
        return {}

    @api.model
    def create(self, vals):
        res = super(transfer_promotion, self).create(vals)
        body = "Transfer and Promotion confirmed"
        res.sudo().message_post(body=body, message_type='email')
        res.sudo().write({'state': 'Draft'})
        return res

    # @api.multi
    # def action_save_transfer_promotion(self):
    #     self.write({'state': 'Draft'})
    #     body = "Transfer and Promotion confirmed"
    #     self.message_post(body=body, message_type='email')
    #     return {}

    @api.multi
    def action_submit_transfer_promotion(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Transfer and Promotion saved"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Submit'})
        return {}

    @api.multi
    def action_hr_approval(self):
        groups = self.env['res.groups'].search([('name','=','CEO')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Transfer and Promotion Approved from HR"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Hr Manager Approval'})
        return {}

    @api.multi
    def action_ceo_approval(self):
        for rec in self:
            employee = self.env['hr.employee'].search([('id', '=', rec.employee.id)])
            if employee:
                employee.sudo().write({'department_id': rec.department_to.id, 'job_id': rec.job_to.id, 'company': rec.company_to.id })
                contract = self.env['hr.contract'].search([('employee_id', '=', rec.employee.id)])
                if contract:
                    contract.sudo().write({'wage': rec.new_salary})

                salary_history = self.sudo().env['hr.employee.salaries.history'].create({
                                                                                'salary_id': rec.employee.id,
                                                                                'salary_before_increasing': rec.current_salary,
                                                                                'increasing': (rec.new_salary - rec.current_salary),
                                                                                'salary_after_increasing': rec.new_salary,
                                                                                'date': rec.date,
                                                                                'notes': rec.notes})
        self.write({'state': 'CEO Approval'})
        body = "New Transfer and Promotion Approved from CEO"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New Transfer and Promotion refused"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def _compute_is_dep_manager(self):
        for record in self:
            if(record.department_from.manager_id.user_id.id == self.env.uid):
                record.is_department_manager = 1
            else:
                record.is_department_manager = 0


class over_time(models.Model):
    _name = 'over.time'
    _inherit = ['mail.thread']

    employee = fields.Many2one('hr.employee', _('Employee'))
    emp_code = fields.Char(_('Employee Code'), related="employee.full_emp_nu", readonly="true", store=True)
    emp_department = fields.Many2one('hr.department', _('Department'), related="employee.department_id", readonly="true", store=True)
    emp_job = fields.Many2one('hr.job', _('Job'), related="employee.job_id" , readonly="true", store=True)
    date_1 = fields.Date('Date of work order', default=datetime.today())
    _from = fields.Datetime('From')
    _to = fields.Datetime('To')
    actual_from = fields.Datetime('Actual from')
    actual_to = fields.Datetime('Actual to')
    hours = fields.Float('Hours', compute='_compute_hours', readonly="true")
    type = fields.Selection([('Morning', 'Morning'),
                             ('Night', 'Night')],
                              'Type')
    percentage = fields.Float('Percentage')
    total_salary = fields.Float('Total salary')
    amount = fields.Float('Amount', compute='_compute_amount', readonly="true")
    emp_company = fields.Many2one('res.company', _('Company'), related="employee.company" , readonly="true", store=True)
    date_2 = fields.Date('Date')
    journal = fields.Many2one('account.journal', _('Journal'))
    account = fields.Many2one('account.account', _('Account'))
    journal_entry = fields.Many2one('account.move', 'Journal Entry', readonly=True)
    journal_reconciliation = fields.Many2one('account.move', 'Journal Entry for Reconciliation', readonly=True)
    state = fields.Selection([('Draft', 'Draft'),
                              ('submitted', 'submitted'),
                              ('Hr Manager Approval', 'Hr Manager Approval'),
                              ('Achievement', 'Achievement'),
                              ('Accounting Approval', 'Accounting Approval'),
                              ('Paid','Paid'),
                              ('Refused','Refused')],
                               'Status', readonly=True)
    is_department_manager = fields.Boolean(compute='_compute_is_dep_manager')
    journal_id = fields.Many2one('account.journal', 'Payment Method', domain=[('type', 'in', ('bank', 'cash'))])
    payment_method_entry = fields.Many2one('account.account', _('Payment Method Entry'))

    @api.depends('actual_to')
    def _compute_hours(self):
        for rec in self:
            if rec.actual_from and rec.actual_to:
                fmt = '%Y-%m-%d %H:%M:%S'
                d1 = datetime.strptime(rec.actual_from, fmt)
                d2 = datetime.strptime(rec.actual_to, fmt)
                # convert to unix timestamp
                d1_ts = time.mktime(d1.timetuple())
                d2_ts = time.mktime(d2.timetuple())

                timedelta = d2_ts - d1_ts
                diff_hours = float((timedelta / 60) / 60)
                rec.hours = diff_hours
            else:
                rec.hours = 0

    @api.depends('percentage','total_salary')
    def _compute_amount(self):
        for rec in self:
            if rec.percentage and rec.total_salary:
                percentage = rec.percentage / 100
                rec.amount = percentage * rec.total_salary
            else:
                rec.amount = 0

    @api.model
    def create(self, vals):
        res = super(over_time, self).create(vals)
        if res.is_department_manager == 0:
           raise exceptions.ValidationError("You are not manager for this employee")
        res.sudo().write({'state': 'Draft'})
        return res

    @api.multi
    def action_confirm_over_time(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Manager')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New over time confirmed from direct manager"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'submitted'})
        return {}

    @api.multi
    def action_hr_approval(self):
        body = "New over time Approved from HR"
        self.message_post(body=body, message_type='email')
        self.write({'state': 'Hr Manager Approval'})
        return {}

    @api.multi
    def action_hr_achievement_approval(self):
        accountants = self.env['res.groups'].search([('category_id.name', '=', 'Accounting & Finance'),('name','=','Accountant')])
        recipient_partners = []
        for accountant in accountants:
            for recipient in accountant.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        Advisers = self.env['res.groups'].search([('category_id.name', '=', 'Accounting & Finance'),('name','=','Adviser')])
        for Adviser in Advisers:
            for recipient in Adviser.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New over time achievement approved from HR"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Achievement'})
        return {}

    @api.multi
    def action_hr_accounting_approval(self):
        if not self.journal:
            raise exceptions.ValidationError("You should select a journal")
        if not self.account:
            raise exceptions.ValidationError("You should select an account")
        for rec in self:
            payable_account = rec.employee.address_home_id.property_account_payable_id.id
            if not payable_account:
                raise exceptions.ValidationError("There is no Payable Account at this employee")
            # Create Journal Entry for line
            journal_entry_vals = {
                'journal_id': rec.journal.id,
                'ref': "Over Time " + rec.employee.name,
             }
            journal_entry_lines = []
            journal_entry_lines.append(
                (0,0,{
                'journal_id': rec.journal.id,
                'name': "Over Time " + rec.type,
                'account_id': rec.account.id,
                'debit': rec.amount
                })
             )
            journal_entry_lines.append(
                (0,0,{
                'journal_id': rec.journal.id,
                'name': "/",
                'account_id': payable_account,
                'credit': rec.amount
                })
             )
            journal_entry_vals['line_ids']= journal_entry_lines
            journal_entry_id = self.env['account.move'].create(journal_entry_vals)
            rec.journal_entry = journal_entry_id.id

            body = "New over time Approved and new journal created from Accounting"
            self.message_post(body=body, message_type='email')
            self.write({'state': 'Accounting Approval'})
        return {}

    @api.multi
    def action_register_paid(self):
        # Create the journal entry
        if not self.journal_id:
            raise exceptions.ValidationError("You should select a payment method")
        for rec in self:
            payable_account = rec.employee.address_home_id.property_account_payable_id.id
            if not payable_account:
                raise exceptions.ValidationError("There is no Payable Account at this employee")
            # Create Journal Entry for line
            journal_entry_vals = {
                'journal_id': rec.journal.id,
                'ref': "Over Time " + rec.employee.name,
             }
            journal_entry_lines = []
            journal_entry_lines.append(
                (0,0,{
                'journal_id': rec.journal.id,
                'name': "/",
                'account_id': payable_account,
                'debit': rec.amount
                })
             )
            journal_entry_lines.append(
                (0,0,{
                'journal_id': rec.journal.id,
                'name': "Over Time " + rec.type,
                'account_id': rec.journal_id.default_credit_account_id.id,
                'credit': rec.amount
                })
             )
            journal_entry_vals['line_ids']= journal_entry_lines
            journal_entry_id = self.env['account.move'].create(journal_entry_vals)
            rec.journal_reconciliation = journal_entry_id.id

        self.write({'state': 'Paid'})
        body = "Over Time Registered 'Paid' "
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "Over Time refused"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def _compute_is_dep_manager(self):
        for record in self:
            if(record.emp_department.manager_id.user_id.id == self.env.uid):
                record.is_department_manager = 1
            else:
                record.is_department_manager = 0

    @api.constrains('emp_department')
    def _emp_department(self):
        if not self.emp_department.manager_id:
           raise ValidationError('There is no manager for this Department')
        if self.is_department_manager == 0:
           raise ValidationError('You are not manager for this employee')