# -*- coding: utf-8 -*-
from odoo import fields, models, api


class SplitPurchaseOrderLineWizard(models.TransientModel):
    _name = 'split.purchase.order.line.wizard'

    order_id = fields.Many2one('split.purchase.order.wizard')
    purchase_order_line_id = fields.Many2one('purchase.order.line')
    product_id = fields.Many2one('product.product', string='Product')
    description = fields.Text()
    quantity = fields.Float(string='Quantity')
    remaining = fields.Float(string='Remaining')
    incoming = fields.Float(string='Incoming')
    date = fields.Date(string='Date')
    status_of_product = fields.Selection([
        ('manufactured', 'Under manufacture'),
        ('in_stock', 'delivered to warehouses'),
        ('with_logistics', 'Delivered to logistics'),
        ('in_customs_clearance_procedures', 'Delivered to Customs'),
        ('on_vessel', 'Delivered to deck of ship')], steing='Status Of Product')

    @api.multi
    def _prepare_stock_moves(self, picking):
        """ Prepare the stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        self.ensure_one()
        # active_ids = self.env.context.get('active_ids')
        # purchase_order = self.env['purchase.order'].browse(active_ids)
        res = []
        if self.purchase_order_line_id.product_id.type not in ['product', 'consu']:
            return res
        qty = 0.0
        price_unit = self.purchase_order_line_id._get_stock_move_price_unit()
        for move in self.purchase_order_line_id.move_ids.filtered(lambda x: x.state != 'cancel'):
            qty += move.product_qty
        template = {
            'name': self.purchase_order_line_id.name or '',
            'product_id': self.purchase_order_line_id.product_id.id,
            'product_uom': self.purchase_order_line_id.product_uom.id,
            'date': self.purchase_order_line_id.order_id.date_order,
            'date_expected': self.order_id.shipment_date,
            'location_id': self.purchase_order_line_id.order_id.partner_id.property_stock_supplier.id,
            'location_dest_id': self.purchase_order_line_id.order_id._get_destination_location(),
            'picking_id': picking.id,
            'partner_id': self.purchase_order_line_id.order_id.dest_address_id.id,
            'move_dest_id': False,
            'state': 'draft',
            'purchase_line_id': self.purchase_order_line_id.id,
            'company_id': self.purchase_order_line_id.order_id.company_id.id,
            'price_unit': price_unit,
            'picking_type_id': self.purchase_order_line_id.order_id.picking_type_id.id,
            'group_id': self.purchase_order_line_id.order_id.group_id.id,
            'procurement_id': False,
            'origin': self.purchase_order_line_id.order_id.name,
            'route_ids': self.purchase_order_line_id.order_id.picking_type_id.warehouse_id and [
                (6, 0, [x.id for x in self.purchase_order_line_id.order_id.picking_type_id.warehouse_id.route_ids])] or [],
            'warehouse_id': self.purchase_order_line_id.order_id.picking_type_id.warehouse_id.id,
        }
        # Fullfill all related procurements with this po line
        diff_quantity = self.quantity - qty
        for procurement in self.purchase_order_line_id.procurement_ids.filtered(lambda p: p.state != 'cancel'):
            # If the procurement has some moves already, we should deduct their quantity
            # sum_existing_moves = sum(x.product_qty for x in procurement.move_ids if x.state != 'cancel')
            existing_proc_qty = procurement.product_id.uom_id._compute_quantity(sum_existing_moves,
                                                                            procurement.product_uom)
            procurement_qty = procurement.product_uom._compute_quantity(procurement.product_qty,
                                                                    self.purchase_order_line_id.product_uom) - existing_proc_qty
            if float_compare(procurement_qty, 0.0,
                             precision_rounding=procurement.product_uom.rounding) > 0 and float_compare(diff_quantity,
                                                                                                        0.0,
                                                                                                        precision_rounding=self.purchase_order_line_id.product_uom.rounding) > 0:
                tmp = template.copy()
                tmp.update({
                    # 'product_uom_qty': min(procurement_qty, diff_quantity),
                    'product_uom_qty': self.incoming,
                    'move_dest_id': procurement.move_dest_id.id,  # move destination is same as procurement destination
                    'procurement_id': procurement.id,
                    'propagate': procurement.rule_id.propagate,
                })
                res.append(tmp)
                diff_quantity -= min(procurement_qty, diff_quantity)
        # if float_compare(diff_quantity, 0.0, precision_rounding=self..product_uom.rounding) > 0:
        #     template['product_uom_qty'] = diff_quantity
        #     res.append(template)
        return res

    @api.multi
    def _create_stock_moves(self, picking):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        for line in self:
            for val in line._prepare_stock_moves(picking):
                done += moves.create(val)
        return done