# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.exceptions import ValidationError
from datetime import datetime


class SplitPurchaseOrderWizard(models.TransientModel):
    _name = 'split.purchase.order.wizard'

    shipment_date = fields.Datetime(string='Shipment Date')
    line_ids = fields.One2many('split.purchase.order.line.wizard', 'order_id', default=lambda self: self.get_lines())

    @api.model
    def _prepare_picking(self):
        active_ids = self.env.context.get('active_ids')
        purchase_order = self.env['purchase.order'].browse(active_ids)
        if not purchase_order.group_id:
            purchase_order.group_id = purchase_order.group_id.create({
                'name': purchase_order.name,
                'partner_id': purchase_order.partner_id.id
            })
            purchase_order.write({'group_id': purchase_order.group_id.id})
        if not purchase_order.partner_id.property_stock_supplier.id:
            raise UserError(_("You must set a Vendor Location for this partner %s") % purchase_order.partner_id.name)
        return {
            'picking_type_id': purchase_order.picking_type_id.id,
            'partner_id': purchase_order.partner_id.id,
            'date': purchase_order.date_order,
            'min_date': self.shipment_date,
            'origin': purchase_order.name,
            'location_dest_id': purchase_order._get_destination_location(),
            'location_id': purchase_order.partner_id.property_stock_supplier.id,
            'company_id': purchase_order.company_id.id,
            'purchase_id': purchase_order.id,
            'group_id': purchase_order.group_id.id,
            # 'move_lines': res0,
        }

    @api.multi
    def _create_picking(self):
        active_ids = self.env.context.get('active_ids')
        purchase_order = self.env['purchase.order'].browse(active_ids)
        StockPicking = self.env['stock.picking']
        for order in purchase_order:
            if any([ptype in ['product', 'consu'] for ptype in order.order_line.mapped('product_id.type')]):
                pickings = order.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
                # pickings = order.picking_ids
                # if not pickings:
                res = self._prepare_picking()
                picking = StockPicking.create(res)
                res0 = []
                for line in self.line_ids:
                        # if line.purchase_order_line_id.product_id.type not in ['product', 'consu']:
                        #     return res0
                    qty = 0.0
                    price_unit = line.purchase_order_line_id._get_stock_move_price_unit()
                    for move in line.purchase_order_line_id.move_ids.filtered(lambda x: x.state != 'cancel'):
                        qty += move.product_qty
                    template = {
                        'name': line.purchase_order_line_id.name or '',
                        'product_id': line.purchase_order_line_id.product_id.id,
                        'product_uom': line.purchase_order_line_id.product_uom.id,
                        'date': line.purchase_order_line_id.order_id.date_order,
                        'date_expected': line.order_id.shipment_date,
                        'location_id': line.purchase_order_line_id.order_id.partner_id.property_stock_supplier.id,
                        'location_dest_id': line.purchase_order_line_id.order_id._get_destination_location(),
                        'picking_id': picking.id,
                        'partner_id': line.purchase_order_line_id.order_id.dest_address_id.id,
                        'move_dest_id': False,
                        'state': 'draft',
                        'purchase_line_id': line.purchase_order_line_id.id,
                        'company_id': line.purchase_order_line_id.order_id.company_id.id,
                        'price_unit': price_unit,
                        'picking_type_id': line.purchase_order_line_id.order_id.picking_type_id.id,
                        'group_id': line.purchase_order_line_id.order_id.group_id.id,
                        'procurement_id': False,
                        'origin': line.purchase_order_line_id.order_id.name,
                        'route_ids': line.purchase_order_line_id.order_id.picking_type_id.warehouse_id and [
                            (6, 0, [x.id for x in
                                    line.purchase_order_line_id.order_id.picking_type_id.warehouse_id.route_ids])] or [],
                        'warehouse_id': line.purchase_order_line_id.order_id.picking_type_id.warehouse_id.id,
                        'product_uom_qty': line.incoming,
                    }
                    res0.append(template)
                    stockmove = self.env['stock.move']
                    stockmove.create(template)
                # else:
                #     picking = pickings[0]

                moves = self.env['stock.picking'].search([('id', '=', picking.id )])
                moves0 = moves.filtered(lambda x: x.state not in ('done', 'cancel')).action_confirm()
                seq = 0
                # for move in sorted(moves, key=lambda move: move.date_expected):
                #     seq += 5
                #     move.sequence = seq
                moves.force_assign()
                picking.message_post_with_view('mail.message_origin_link',
                                               values={'self': picking, 'origin': order},
                                               subtype_id=self.env.ref('mail.mt_note').id)
        return True

    @api.multi
    def confirm_action(self):
        self._create_picking()
        active_ids = self.env.context.get('active_ids')
        purchase_order = self.env['purchase.order'].browse(active_ids)
        if self.shipment_date < str(datetime.now()):
            raise ValidationError('Shipment date can not be later than current date')
        self.sudo().env['incoming.shipment'].create({
            'purchase_order_id': purchase_order.id,
            'shipment_date': self.shipment_date,
            'product_line_ids': self.get_product_lines(self.line_ids),
        })

        if purchase_order.is_landed_cost_ok:
            stock_picking_id = self.env['stock.picking'].search([('purchase_id', '=', purchase_order.id)], limit=1)
            landed_cost = self.env['stock.landed.cost']
            landed_cost.create({
                'date': fields.Datetime.now(),
                'picking_ids': [(4, stock_picking_id.id) if stock_picking_id else None],
                'account_journal_id': 1,
                'product_line_ids': self.get_product_lines_landed_cost(self.line_ids),
            })

        return True

    def get_product_lines_landed_cost(self, lines):
        product_lines_landed_cost = []
        for line in lines:
            if line.incoming <= line.remaining:
                if line.incoming > 0:
                    product_lines_landed_cost.append((0, 0, {
                        'product_id': line.product_id.id,
                        'name': line.description,
                        'split_method': 'by_quantity',
                        'price_unit': 0.0,
                    }))
            else:
                raise ValidationError('You can not add Landed Cost more than remaining quantity')
        if not product_lines_landed_cost:
            raise ValidationError('You have to add quantity to one product at least')
        return product_lines_landed_cost

    def get_product_lines(self, lines):
        product_lines = []
        for line in lines:
            if line.incoming <= line.remaining:
                if line.incoming > 0:
                    product_lines.append((0, 0, {
                        'product_id': line.product_id.id,
                        'description': line.description,
                        'quantity': line.incoming,
                        'status_of_product': line.status_of_product,
                        'purchase_order_line_id': line.purchase_order_line_id.id,
                    }))
                    line.purchase_order_line_id.used_for_shipping_qty += line.incoming
            else:
                raise ValidationError('You can not add incoming more than remaining quantity')
        if not product_lines:
            raise ValidationError('You have to add quantity to one product at least')
        return product_lines

    def get_lines(self):
        active_ids = self.env.context.get('active_ids')
        purchase_order = self.env['purchase.order'].browse(active_ids)
        date = purchase_order.date_order
        lines = []
        for rec in purchase_order.order_line:
            lines.append((0, 0, {
                'product_id': rec.product_id.id,
                'description': rec.name,
                'quantity': rec.product_qty,
                'remaining': rec.product_qty - rec.used_for_shipping_qty,
                'status_of_product': rec.status_of_product,
                'date': date,
                'purchase_order_line_id': rec.id
            }))
        return lines
