{
    'name': 'Shipping WorkFlow',
    'version': '1.0',
    'category': 'Purchase',
    'author': 'Shady & Mohamed El-Fakrany',
    'website': '',
    'depends': ['nano_purchase_order', 'nano_logistic', 'purchase'],
    'data': [
        'security/ir.model.access.csv',
        'views/incoming_shipment_view.xml',
        'views/purchase_order_view.xml',
        'wizard/split_purchase_order_wizard_view.xml',
        'views/menuitems.xml',
    ]
}
