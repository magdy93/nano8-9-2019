from odoo import models, api, fields


class NanoPurchaseDocs(models.Model):
    _inherit = 'nano.purchase.docs'

    incoming_shipment_id = fields.Many2one('incoming.shipment')
