from odoo import models, api, fields


class LogisticPurchaseInherit(models.Model):
    _inherit = 'logistic.purchase'

    incoming_shipment_id = fields.Many2one('incoming.shipment')
