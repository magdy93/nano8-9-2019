from odoo import models, api, fields


class IncomingShipmentProductLine(models.Model):
    _name = 'incoming.shipment.product.line'

    product_id = fields.Many2one('product.product', string='Product')
    description = fields.Text(string='Description')
    quantity = fields.Float(string='Quantity')
    status_of_product = fields.Selection([
        ('manufactured', 'Under manufacture'),
        ('in_stock', 'delivered to warehouses'),
        ('with_logistics', 'Delivered to logistics'),
        ('in_customs_clearance_procedures', 'Delivered to Customs'),
        ('on_vessel', 'Delivered to deck of ship')], steing='Status Of Product')
    incoming_shipment_id = fields.Many2one('incoming.shipment')
    purchase_order_line_id = fields.Many2one('purchase.order.line')
