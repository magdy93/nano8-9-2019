from odoo import models, api, fields


class NanoPurchaseContainer(models.Model):
    _inherit = 'nano.purchase.container'

    incoming_shipment_id = fields.Many2one('incoming.shipment')
