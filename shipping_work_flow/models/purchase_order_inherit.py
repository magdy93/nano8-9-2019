from odoo import models, api, fields


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    incoming_shipment_counter = fields.Integer(compute='incoming_shipment_counter_function')

    @api.multi
    def incoming_shipment_counter_function(self):
        for rec in self:
            incoming_shipment = self.env['incoming.shipment'].search([('purchase_order_id', '=', rec.id)])
            if incoming_shipment:
                rec.incoming_shipment_counter = len(incoming_shipment)
            else:
                rec.incoming_shipment_counter = 0

    @api.multi
    def split_order_action(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Split Order',
            'res_model': 'split.purchase.order.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }

    @api.multi
    def related_incoming_shipment(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Incoming Shipment',
            'res_model': 'incoming.shipment',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'target': 'current',
            'domain': [('purchase_order_id', '=', self.id)]
        }
