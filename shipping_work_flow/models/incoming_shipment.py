from odoo import models, api, fields
from odoo.exceptions import ValidationError


class IncomingShipment(models.Model):
    _name = 'incoming.shipment'
    _rec_name = 'partner_id'
    _inherit = ['mail.thread']

    state = fields.Selection(selection=[('draft', 'Draft'), ('update_data', 'Update Data'), ('confirmed', 'Confirmed'),
                                        ('linked_wh', 'Linked W/H Order'), ('done', 'Done')], string='State',
                             default='draft')
    purchase_order_id = fields.Many2one('purchase.order')
    purchase_order_name = fields.Char(related='purchase_order_id.name')
    partner_id = fields.Many2one(related='purchase_order_id.partner_id', string='Vendor')
    vendor_ref = fields.Char(related='purchase_order_id.partner_ref', string='Vendor Reference')
    shipment_date = fields.Datetime(string='Shipment Date')
    confirmation_date = fields.Date(string='Confirmation Date')
    date_order = fields.Datetime(related='purchase_order_id.date_order', string='Order Date')
    type = fields.Selection(related='purchase_order_id.type',
                            string='Type')
    carrying = fields.Selection(related='purchase_order_id.carrying',
                                string='Carrying')
    is_temporary_permit = fields.Boolean(related='purchase_order_id.is_temporary_permit', string='IS Temporary Permit')
    purchase_service_id = fields.Many2one(related='purchase_order_id.purchase_service_id', string="Purchase Order", )
    status_of_product = fields.Selection(related='purchase_order_id.status_of_product', steing='Status Of Product')
    state_of_logistic_request = fields.Char('Status Of Logistic Request')
    state_of_custom_request = fields.Char('Status Of Custom Request')
    state_of_lc_request = fields.Char('Status Of LC Request')
    product_line_ids = fields.One2many('incoming.shipment.product.line', 'incoming_shipment_id')
    attach_rel = fields.One2many('nano.purchase.docs', 'incoming_shipment_id')
    warehouse_line_ids = fields.One2many('incoming.shipment.warehouse.line', 'incoming_shipment_id')
    # shipping information
    loading_port = fields.Char(string='Loading Port')
    unloading_port = fields.Char(string='Unloading Port')
    from_street = fields.Char(string='Street')
    from_street2 = fields.Char(string='Street 2')
    from_zip = fields.Char(string='ZIP', change_default=True)
    from_city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    from_country_id = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    container_ids = fields.One2many('nano.purchase.container', 'incoming_shipment_id')
    wh_incoming_date = fields.Date(string='WH Incoming')
    booking_number = fields.Char(string="Booking Number", required=False, )
    delivery_date = fields.Date(string="Delivery Date", required=False, )
    voy_no = fields.Char(string="Voy NO", required=False, )
    vessel_name = fields.Char(string="Vessel Name", required=False, )

    @api.multi
    def active_action(self):
        for rec in self:
            rec.state = 'update_data'

    @api.multi
    def confirm_data_action(self):
        for rec in self:
            rec.state = 'confirmed'

    @api.multi
    def confirm_shipment_action(self):
        for rec in self:
            rec.state = 'linked_wh'
            # code to create logistic
            logistic_import = self.sudo().env['logistic.purchase']
            logistic_import_line = self.sudo().env['logistic.purchase.order.line']
            logistic_container_line = self.env['nano.purchase.containers']
            domestic_import = self.env['domistic.purchase']
            domestic_import_line = self.env['domistic.purchase.order.line']
            domestic_container_line = self.env['nano.domestic.containers']
            logistic_purchase_docs_obj = self.sudo().env['nano.logistic.purchase.docs']
            if rec.type == 'international':
                # a. Create logistic request -> we will explain it in logistic module
                logistic_id = logistic_import.sudo().create({
                    'purchase_order': rec.purchase_order_id.id,
                    'incoming_shipment_id': rec.id,
                    'booking_number': rec.booking_number,
                    'delivery_date': rec.delivery_date,
                    'voy_no': rec.voy_no,
                    'vessel_name': rec.vessel_name,
                    'incoterms': rec.purchase_order_id.incoterm_id.id,
                    'loading_port': rec.loading_port,
                    'unloading_port': rec.unloading_port,
                    'from_street': rec.from_street,
                    'from_street2': rec.from_street2,
                    'from_zip': rec.from_zip,
                    'from_city': rec.from_city,
                    'from_state_id': rec.from_state_id.id,
                    'from_country_id': rec.from_country_id.id,
                    'to_street': rec.to_street,
                    'to_street2': rec.to_street2,
                    'to_zip': rec.to_zip,
                    'to_city': rec.to_city,
                    'to_state_id': rec.to_state_id.id,
                    'to_country_id': rec.to_country_id.id,
                    'vessel_loading_date': rec.vessel_loading_date,
                    'vessel_unloading_date': rec.vessel_unloading_date,
                    'wh_incoming_date': rec.wh_incoming_date,
                    'custom_status': 'draft',
                    'warehouse_status': 'assigned',
                })
                for lg in rec.product_line_ids:
                    logistic_import_line.sudo().create({
                        'order_line': logistic_id.id,
                        'product': lg.purchase_order_line_id.product_id.id,
                        'uom': lg.purchase_order_line_id.product_uom.id,
                        'describtion': lg.purchase_order_line_id.name,
                        'subtotal': lg.quantity * lg.purchase_order_line_id.price_unit,
                        # 'tax': line.taxes_id,
                        'price': lg.purchase_order_line_id.price_unit,
                        'quantity': lg.quantity
                    })
                for doc_obj in rec.attach_rel:
                    logistic_purchase_docs_obj.sudo().create({
                        'logistic_purchase_id': logistic_id.id,
                        'document': doc_obj.document,
                        'attachment_ids': [(6, 0, doc_obj.attachment_ids.ids)]
                    })
                    doc_obj.is_send = False
                    doc_obj.flag = 1

                for container in rec.container_ids:
                    logistic_container_line.sudo().create({
                        'purchase_containers_id': logistic_id.id,
                        'document': container.document,
                        'attachment_ids': container.attachment_ids,
                    })
                    container.is_send_container = False
                    container.flag_container = 1
                rec.sudo().write({
                    'state_of_logistic_request': 'draft'
                })
            elif rec.type == 'domestics' and rec.carrying == 'company':
                # a. Create logistic request -> we will explain it in logistic module
                logistic_id = domestic_import.sudo().create({
                    'purchase_order': rec.purchase_order_id.id,
                    'incoming_shipment_id': rec.id,
                    'incoterms': rec.purchase_order_id.incoterm_id.id,
                    'loading_port': rec.loading_port,
                    'unloading_port': rec.unloading_port,
                    'from_street': rec.from_street,
                    'from_street2': rec.from_street2,
                    'from_zip': rec.from_zip,
                    'from_city': rec.from_city,
                    'from_state_id': rec.from_state_id.id,
                    'from_country_id': rec.from_country_id.id,
                    'to_street': rec.to_street,
                    'to_street2': rec.to_street2,
                    'to_zip': rec.to_zip,
                    'to_city': rec.to_city,
                    'to_state_id': rec.to_state_id.id,
                    'to_country_id': rec.to_country_id.id,
                    'vessel_loading_date': rec.vessel_loading_date,
                    'vessel_unloading_date': rec.vessel_unloading_date,
                    'wh_incoming_date': rec.wh_incoming_date,
                })
                for lg in rec.product_line_ids:
                    domestic_import_line.sudo().create({
                        'order_line': logistic_id.id,
                        'product': lg.purchase_order_line_id.product_id.id,
                        'uom': lg.purchase_order_line_id.product_uom.id,
                        'describtion': lg.purchase_order_line_id.name,
                        'subtotal': lg.quantity * lg.purchase_order_line_id.price_unit,
                        # 'tax': line.taxes_id,
                        'price': lg.purchase_order_line_id.price_unit,
                        'quantity': lg.quantity
                    })
                for container in rec.container_ids:
                    domestic_container_line.sudo().create({
                        'domestic_containers_id': logistic_id.id,
                        'document': container.document,
                        'attachment_ids': container.attachment_ids,
                    })
                rec.sudo().write({
                    'state_of_logistic_request': 'draft'
                })

    @api.multi
    def finish_action(self):
        for rec in self:
            for product_line in rec.product_line_ids:
                product_qty = product_line.quantity
                product_id = product_line.product_id.id
                received_qty = 0
                for warehouse_line in rec.warehouse_line_ids:
                    for line in warehouse_line.stock_picking_id.pack_operation_product_ids.filtered(
                            lambda self: self.product_id.id == product_id):
                        received_qty += line.qty_done
                if product_qty > received_qty:
                    raise ValidationError('Sorry you have to receive all required products before finish')
            rec.state = 'done'

    @api.multi
    def action_document_send(self):
        for rec in self:
            if rec.type == 'international':
                logistic_purchase_obj = self.sudo().env['logistic.purchase'].search(
                    [('incoming_shipment_id', '=', rec.id)])
                logistic_purchase_docs_obj = self.sudo().env['nano.logistic.purchase.docs']
                if logistic_purchase_obj:
                    logistic_purchase_obj.write({
                        'loading_port': rec.loading_port,
                        'unloading_port': rec.unloading_port,
                        'from_street': rec.from_street,
                        'from_street2': rec.from_street2,
                        'from_zip': rec.from_zip,
                        'from_city': rec.from_city,
                        'from_state_id': rec.from_state_id.id,
                        'from_country_id': rec.from_country_id.id,
                        'to_street': rec.to_street,
                        'to_street2': rec.to_street2,
                        'to_zip': rec.to_zip,
                        'to_city': rec.to_city,
                        'to_state_id': rec.to_state_id.id,
                        'to_country_id': rec.to_country_id.id,
                        'vessel_loading_date': rec.vessel_loading_date,
                        'vessel_unloading_date': rec.vessel_unloading_date,
                        'wh_incoming_date': rec.wh_incoming_date,
                    })
                    for obj in rec.attach_rel:
                        if obj.is_send:
                            logistic_purchase_docs_obj.sudo().create({
                                'logistic_purchase_id': logistic_purchase_obj.id,
                                'document': obj.document,
                                'doc_status': obj.doc_status,
                                'is_send': obj.is_send,
                                'attachment_ids': [(6, 0, obj.attachment_ids.ids)]
                            })
                            obj.is_send = False
                            obj.flag = 1
            elif rec.type == 'domestics':
                logistic_purchase_obj = self.sudo().env['domistic.purchase'].search(
                    [('incoming_shipment_id', '=', rec.id)])
                logistic_purchase_docs_obj = self.sudo().env['nano.domistic.purchase.docs']
                if logistic_purchase_obj:
                    logistic_purchase_obj.write({
                        'loading_port': rec.loading_port,
                        'unloading_port': rec.unloading_port,
                        'from_street': rec.from_street,
                        'from_street2': rec.from_street2,
                        'from_zip': rec.from_zip,
                        'from_city': rec.from_city,
                        'from_state_id': rec.from_state_id.id,
                        'from_country_id': rec.from_country_id.id,
                        'to_street': rec.to_street,
                        'to_street2': rec.to_street2,
                        'to_zip': rec.to_zip,
                        'to_city': rec.to_city,
                        'to_state_id': rec.to_state_id.id,
                        'to_country_id': rec.to_country_id.id,
                        'vessel_loading_date': rec.vessel_loading_date,
                        'vessel_unloading_date': rec.vessel_unloading_date,
                        'wh_incoming_date': rec.wh_incoming_date,
                    })
                    for obj in rec.attach_rel:
                        if obj.is_send:
                            logistic_purchase_docs_obj.sudo().create({
                                'logistic_purchase_id': logistic_purchase_obj.id,
                                'document': obj.document,
                                'doc_status': obj.doc_status,
                                'is_send': obj.is_send,
                                'attachment_ids': [(6, 0, obj.attachment_ids.ids)]
                            })
                            obj.is_send = False
                            obj.flag = 1
