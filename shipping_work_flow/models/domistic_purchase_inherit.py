from odoo import models, api, fields


class DomisticPurchaseInherit(models.Model):
    _inherit = 'domistic.purchase'

    incoming_shipment_id = fields.Many2one('incoming.shipment')
