from odoo import models, api, fields
from odoo.exceptions import ValidationError


class IncomingShipmentWarehouseLine(models.Model):
    _name = 'incoming.shipment.warehouse.line'

    stock_picking_id = fields.Many2one('stock.picking', string='Incoming Shipment')
    date = fields.Date(string='Date')
    incoming_shipment_id = fields.Many2one('incoming.shipment')

    @api.onchange('stock_picking_id')
    def check_stock_picking_unique(self):
        is_used = self.search([('stock_picking_id', '=', self.stock_picking_id.id)])
        if is_used:
            raise ValidationError(self.stock_picking_id.name + ' is already used')

    _sql_constraints = [
        ('name_stock_picking', 'unique (stock_picking_id)', "You already choose this incoming shipment more than once")]
