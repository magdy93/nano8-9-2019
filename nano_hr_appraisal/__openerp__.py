# -*- coding: utf-8 -*-
{
    'name': "Nano Appraisal",

    'summary': """ Nano Appraisal """,

    'description': """
        Nano Appraisal
    """,

    'author': "Universal Selective Systems",
    'website': "http://www.un-ss.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Appraisal',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail','survey','hr_appraisal','nano_hr_recruitment'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
}