# -*- coding: utf-8 -*-

from openerp.tools.translate import _
from openerp import api, fields, models, tools
import datetime
from operator import attrgetter
import time
import math
from openerp.exceptions import UserError, AccessError
from openerp import tools


class nano_hr_survey(models.Model):
    _inherit = 'survey.page'

    # _columns = {
    #     'responsibility': fields.selection([('Direct manager', 'Direct manager'),
    #                                        ('HR', 'HR'),
    #                                        ('Safety', 'Safety'),
    #                                        ('Sector manager', 'Sector manager')],
    #                                         _('Responsibility'), store=True)
    # }
    responsibility = fields.Selection(string="", selection=[('Direct manager', 'Direct manager'), ('HR', 'HR'),
                                           ('Safety', 'Safety'),
                                           ('Sector manager', 'Sector manager'), ], required=False,store=True )

class nano_hr_survey_question(models.Model):
    _inherit = 'survey.question'

    # _columns = {
    #     'grade': fields.float(_('Grade'), store=True)
    # }
    grade = fields.Float(string="Grade",store=True, )


class nano_hr_survey_user_input(models.Model):
    _inherit = 'survey.user_input'

    def action_survey_results(self):
        # Only change the state
        self.write({'state': 'skip'})

    def action_complete_survey(self):
        grade = 0
        for rec in self.browse():
            for line in rec.user_input_line_ids:
                grade += line.grade
            # Get contract to update productive incentive fields
            employee_id = rec.appraisal_id.employee_id.id
            contract_id = self.pool.get('hr.contract').search([('employee_id', '=', employee_id)])
            contract_obj = self.pool.get('hr.contract').browse(contract_id)
            if grade < 50:
                contract_obj.sudo().write({'production_incentive_type': 'Percentage', 'production_incentive_amount': 0})
            else:
                if 66 > grade >= 50:
                    contract_obj.sudo().write({'production_incentive_type': 'Percentage', 'production_incentive_amount': 75.0})
                else:
                    contract_obj.sudo().write({'production_incentive_type': 'Percentage', 'production_incentive_amount': 100.0})
            self.write({'state': 'done'})


class nano_hr_survey_user_input_line(models.Model):
    _inherit = 'survey.user_input_line'

    # _columns = {
    #     'responsibility': fields.selection([('Direct manager', 'Direct manager'),
    #                                        ('HR', 'HR'),
    #                                        ('Safety', 'Safety'),
    #                                        ('Sector manager', 'Sector manager')],
    #                                         _('Responsibility'), readonly=True, store=True),
    #     'grade': fields.float(_('Grade'), readonly=True, store=True)
    # }
    responsibility = fields.Selection(string="", selection=[('Direct manager', 'Direct manager'),
                                           ('HR', 'HR'),
                                           ('Safety', 'Safety'),
                                           ('Sector manager', 'Sector manager') ],readonly=True, store=True )
    grade = fields.Float(string="Grade",readonly=True, store=True)














