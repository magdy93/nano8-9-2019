# -*- coding: utf-8 -*-

from openerp import models, fields, api , exceptions, _
import datetime
from openerp.exceptions import UserError, ValidationError
# import openerp.addons.decimal_precision as dp

class nano_hr_appraisal(models.Model):
    _inherit = 'hr.appraisal'

    emp_code = fields.Char(_('Employee Code'), related="employee_id.full_emp_nu", readonly="true", store=True)
    department = fields.Many2one('hr.department', _('Department'), related="employee_id.department_id", readonly="true", store=True)
    job = fields.Many2one('hr.job', _('Job'), related="employee_id.job_id", readonly="true", store=True)
    type = fields.Selection([('Monthly', 'Monthly'),
                             ('Yearly', 'Yearly')],
                              'Type')
    # evaluations = fields.Float(_('Total Evaluation'), compute="_compute_total_evaluation", readonly=True)
    absent_days = fields.Float(_('Absent days'), compute="_compute_absent_days", readonly=True)
    late = fields.Float(_('Late'), compute="_compute_late", readonly=True)
    deduction = fields.Float(_('Deduction'), compute="_compute_deduction", readonly=True)
    leaves_types = fields.One2many('nano.hr.appraisal.leave.types', 'leave_type_id', _('Leaves Types'), store=True)

    # @api.multi
    # def _compute_total_evaluation(self):
    #     year = datetime.date.today().strftime("%Y")
    #     month = datetime.date.today().strftime("%m")
    #     date_from = '1/'+month +'/'+'/'+ year
    #     date_to = '03/'+month +'/'+'/'+ year
    #     completed_survey = self.env['survey.user_input'].read_group([('appraisal_id', 'in', self.ids), ('state', '=', 'done'),('date_create','>', date_from),('date_create','<', date_to)], ['appraisal_id'], ['appraisal_id'])
    #     result = dict((data['appraisal_id'][0], data['appraisal_id_count']) for data in completed_survey)
    #     print(result)
    #     for appraisal in self:
    #         appraisal.evaluations = result.get(appraisal.id, 0)

    @api.depends('employee_id')
    def _compute_absent_days(self):
        return {}

    @api.depends('employee_id')
    def _compute_late(self):
        return {}

    @api.depends('employee_id')
    def _compute_deduction(self):
        return {}

    def action_get_evaluations(self):
        return {
            'domain': "[('appraisal_id','in','active_id')]",
            'name': ('Evaluations'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'survey.user_input',
            'type': 'ir.actions.act_window',
            'target': 'current',
        }

    @api.multi
    def button_send_appraisal(self):
        if not self.manager_survey_id:
            raise UserError(_('You must set Appraisal Form first.'))
        # Create survey
        survey_user_input = self.sudo().env['survey.user_input'].create({
                                                   'survey_id': self.manager_survey_id.id,
                                                   'appraisal_id': self.id,
                                                   'partner_id': self.employee_id.id})
        current_survey = self.sudo().env['survey.survey'].search([('id', '=', self.manager_survey_id.id)])
        for pages in current_survey.page_ids:
            for question in pages.question_ids:
                # Create survey lines
                survey_user_input_line = self.sudo().env['survey.user_input_line'].create({
                                                               'survey_id': self.manager_survey_id.id,
                                                               'user_input_id': survey_user_input.id,
                                                               'page_id': question.page_id.id,
                                                               'question_id': question.id,
                                                               'responsibility': pages.responsibility,
                                                               'grade':question.grade,
                                                               'skipped': True})
        self.write({'state': 'pending'})

    @api.multi
    def action_get_users_input(self):
        """ Link to open sent appraisal"""
        self.ensure_one()
        action = self.env.ref('survey.action_survey_user_input').read()[0]
        if self.env.context.get('answers'):
            users_input = self.survey_completed_ids
        else:
            users_input = self.survey_sent_ids
        action['domain'] = str([('id', 'in', users_input.ids)])
        action['context'] = {}  # Remove default group_by for surveys.
        return action


class nano_hr_appraisal_leave_types(models.Model):
    _name = 'nano.hr.appraisal.leave.types'

    leave_type_id = fields.Many2one('hr.appraisal', _('Leave Type'))
    leave_type = fields.Many2one('hr.holidays.status', _('Leave Type'), store=True)
    days = fields.Float(_('Number of days'), compute="_compute_days", readonly=True)

    @api.onchange('leave_type')
    def _get_employee_leave_types(self):
        leave_types = []
        holiday_ids = self.env['hr.holidays'].search([('employee_id', '=', self.leave_type_id.employee_id.id),
                                                      ('state', 'in', ['confirm', 'validate1', 'validate']),
                                                      ('type', '=', 'add')])
        for holiday in holiday_ids:
            if holiday.holiday_status_id.id not in leave_types:
                 leave_types.append(holiday.holiday_status_id.id)
        res = {'domain': {'leave_type': [('id', 'in', leave_types)]}}
        return res


    @api.depends('leave_type')
    def _compute_days(self):
        for rec in self:
            days = 0
            holiday_ids = self.env['hr.holidays'].search([('employee_id', '=', rec.leave_type_id.employee_id.id),
                                                          ('state', '=', 'validate'),
                                                          ('type', '=', 'remove'),
                                                          ('holiday_status_id', '=', rec.leave_type.id)])
            for holiday in holiday_ids:
                days += holiday.number_of_days_temp
            rec.days = days
