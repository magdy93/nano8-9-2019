# -*- coding: utf-8 -*-
{
    'name': "bbholding account type",
    'summary': """
        bbholding account type""",

    'description': """
        bbholding account type
    """,

    'author': "USS",
    'website': "http://www.yourcompany.com",
    'category': 'account',
    'version': '0.1',
    'depends': ['base','account',],
    'data': [
        # 'security/ir.model.access.csv',
        'views/account_meni_item.xml',
        # 'views/views.xml',
        # 'views/templates.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}