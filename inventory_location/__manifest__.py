{
    'name': 'Inventory Location',
    'version': '10.0',
    'description': 'Inventory Location',
    'author': 'USS',
    'website': 'www.iti.gov.eg',
    'depends': ['stock'],
    'data': [
        'views/stock_picking.xml',
        # 'views/gallery_template.xml',
        'security/product_security.xml',
        'views/product_template.xml',
        # 'security/ir.model.access.csv',
    ]
}
