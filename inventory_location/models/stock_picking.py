from odoo import models, fields, api,_
from odoo.exceptions import UserError


class Stocklocation(models.Model):
    _inherit = 'stock.location'
    manager_id = fields.Many2one(comodel_name="res.users", string="Manager", required=False, )

class UserLocation(models.Model):
    _name = 'res.users'
    _inherit = 'res.users'
    is_manager = fields.Boolean(string="Is Manager",)
    location_id = fields.Many2many(comodel_name="stock.location", string="Inventory Location", required=False, )

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def write(self, vals):
        res = super(StockPicking, self).write(vals)
        user_id = self.env['res.users'].search([('id','=',self.env.user.id)])
        if user_id.is_manager == True:
            if self.picking_type_code == 'incoming' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to Edit"))
            elif self.picking_type_code == 'outgoing' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to Edit"))
            elif self.picking_type_code == 'internal' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to Edit"))
        else:
            return res

    @api.multi
    def action_confirm(self):
        user_id = self.env['res.users'].search([('id','=',self.env.user.id)])
        if user_id.is_manager == True:
            if self.picking_type_code == 'incoming' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to make mark as todo"))
            elif self.picking_type_code == 'outgoing' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to mark as todo"))
            elif self.picking_type_code == 'internal' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to mark as todo"))
        else:
            self.filtered(lambda picking: not picking.move_lines).write({'launch_pack_operations': True})
            # TDE CLEANME: use of launch pack operation, really useful ?
            self.mapped('move_lines').filtered(lambda move: move.state == 'draft').action_confirm()
            self.filtered(lambda picking: picking.location_id.usage in ('supplier', 'inventory', 'production')).force_assign()
            return True
    @api.multi
    def action_cancel(self):
        user_id = self.env['res.users'].search([('id','=',self.env.user.id)])
        if user_id.is_manager == True:
            if self.picking_type_code == 'incoming' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to cancel"))
            elif self.picking_type_code == 'outgoing' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to cancel"))
            elif self.picking_type_code == 'internal' and not self.location_id.id in user_id.location_id.ids:
                raise UserError(_("You must be amanager of the location to cancel"))
        else:
            self.mapped('move_lines').action_cancel()
            return True

    @api.multi
    def do_new_transfer(self):
        purchase_order_id = self.env['purchase.order'].search([('id', '=', self.purchase_id.id)]) #magdy
        logistic_purchase_state = self.env['logistic.purchase'].search([('purchase_order', '=', self.purchase_id.id)]) #magdy
        logistic_sale_state = self.env['nano.logistic'].search([('sale_order', '=', self.sale_id.id)]) #magdy

        for pick in self:
            user_id = self.env['res.users'].search([('id','=',self.env.user.id)])
            if user_id.is_manager == True:
                if pick.picking_type_code == 'incoming' and not pick.location_id.id in user_id.location_id.ids:
                   raise UserError(_("You must be amanager of the location to validate"))
                elif pick.picking_type_code == 'outgoing' and not pick.location_id.id in user_id.location_id.ids:
                   raise UserError(_("You must be amanager of the location to validate"))
                elif pick.picking_type_code == 'internal' and not pick.location_id.id in user_id.location_id.ids:
                    raise UserError(_("You must be amanager of the location to validate"))
            else:
                if pick.state == 'done':
                    raise UserError(_('The pick is already validated'))
                    purchase_order_id.write({'ship_data': 'done'}) #magdy
                    logistic_purchase_state.write({'warehouse_status': 'done'}) #magdy
                    logistic_sale_state.write({'status': 'done'}) #magdy
                pack_operations_delete = self.env['stock.pack.operation']
                if not pick.move_lines and not pick.pack_operation_ids:
                    raise UserError(_('Please create some Initial Demand or Mark as Todo and create some Operations. '))
                # In draft or with no pack operations edited yet, ask if we can just do everything
                if pick.state == 'draft' or all([x.qty_done == 0.0 for x in pick.pack_operation_ids]):
                    purchase_order_id.write({'ship_data': 'done'}) #magdy
                    logistic_purchase_state.write({'warehouse_status': 'done'}) #magdy
                    logistic_sale_state.write({'status': 'done'}) #magdy
                    # If no lots when needed, raise error
                    picking_type = pick.picking_type_id
                    if (picking_type.use_create_lots or picking_type.use_existing_lots):
                        for pack in pick.pack_operation_ids:
                            if pack.product_id and pack.product_id.tracking != 'none':
                                raise UserError(_('Some products require lots/serial numbers, so you need to specify those first!'))
                    view = self.env.ref('stock.view_immediate_transfer')
                    wiz = self.env['stock.immediate.transfer'].create({'pick_id': pick.id})
                    # TDE FIXME: a return in a loop, what a good idea. Really.
                    return {
                        'name': _('Immediate Transfer?'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'stock.immediate.transfer',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': self.env.context,
                    }

                # Check backorder should check for other barcodes
                if pick.check_backorder():
                    view = self.env.ref('stock.view_backorder_confirmation')
                    wiz = self.env['stock.backorder.confirmation'].create({'pick_id': pick.id})
                    # TDE FIXME: same reamrk as above actually
                    return {
                        'name': _('Create Backorder?'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'stock.backorder.confirmation',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': self.env.context,
                    }
                for operation in pick.pack_operation_ids:
                    if operation.qty_done < 0:
                        raise UserError(_('No negative quantities allowed'))
                    if operation.qty_done > 0:
                        operation.write({'product_qty': operation.qty_done})
                    else:
                        pack_operations_delete |= operation
                if pack_operations_delete:
                    pack_operations_delete.unlink()
                self.do_transfer()
        return