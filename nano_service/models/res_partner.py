# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.addons.base.res.res_partner import WARNING_MESSAGE, WARNING_HELP


class ResPartnerInherit(models.Model):
    _inherit = "res.partner"

    is_svc = fields.Boolean(string="Is a Service Provider", default=False)

    # property_account_service_id = fields.Many2one('account.account',
    #  company_dependent=True, string="Account Receivable", required=True)
    # service_warn = fields.Selection(WARNING_MESSAGE, 'Service Order', help=WARNING_HELP, required=True,
    #                                  default="no-message")
    # service_warn_msg = fields.Text('Message for Service Order')
    property_service_currency_id = fields.Many2one('res.currency', string="Supplier Currency", company_dependent=True)
