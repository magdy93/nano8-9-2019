# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.addons.base.res.res_partner import WARNING_MESSAGE, WARNING_HELP

class ProductProductInherit(models.Model):
    _inherit = 'product.product'

    flag = fields.Boolean()

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    description_service = fields.Text(
        'Service Description', translate=True,
        help="A description of the Product that you want to communicate to your service providers. "
             "This description will be copied to every Service Order, Receipt and Vendor Bill/Refund.")
    service_line_warn = fields.Selection(WARNING_MESSAGE, 'Service Order Line', help=WARNING_HELP, required=True, default="no-message")
    service_line_warn_msg = fields.Text('Message for Service Order Line')

