# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, SUPERUSER_ID
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools.misc import formatLang
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT



class ServiceOrder(models.Model):
    _name = 'service.order'
    _inherit = ['mail.thread']

    READONLY_STATES = {
        'service': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }
    service_line_ids = fields.One2many('service.order.line', 'order_id')
    name = fields.Char('Order Reference', required=True, index=True, copy=False, default='New')
    partner_id = fields.Many2one('res.partner', string='Service Provider', required=True, change_default=True,
                                 states=READONLY_STATES,
                                 track_visibility='always', domain="[('is_svc','=',True)]")
    partner_ref = fields.Char('Service Provider Reference', copy=False)
    date_order = fields.Datetime('Order Date', required=True, index=True, copy=False, default=fields.Datetime.now,
                                 states=READONLY_STATES)

    date_approve = fields.Date('Approval Date', readonly=1, index=True, copy=False)
    payment_term_id = fields.Many2one('account.payment.term', 'Payment Terms')
    notes = fields.Text('Terms and Conditions')
    invoice_count = fields.Integer(compute="_compute_invoice", string='# of Bills', copy=False, default=0)
    invoice_ids = fields.Many2many('account.invoice', compute="_compute_invoice", string='Bills', copy=False)
    invoice_status = fields.Selection([
        ('no', 'Nothing to Bill'),
        ('to invoice', 'Waiting Bills'),
        ('invoiced', 'Bills Received'),
    ], string='Billing Status', readonly=True, copy=False, default='no')
    fiscal_position_id = fields.Many2one('account.fiscal.position', string='Fiscal Position')
    state = fields.Selection([
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),
        ('to_approve', 'To Approve'),
        ('service', 'Service Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True,
                                 default=lambda self: self.env.user.company_id.id)
    currency_id = fields.Many2one('res.currency', 'Currency', required=True, states=READONLY_STATES,
                                  default=lambda self: self.env.user.company_id.currency_id.id)
    amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_amount_all',
                                     track_visibility='always')
    amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_amount_all')
    amount_total = fields.Monetary(string='Total', store=True, readonly=True, compute='_amount_all')

    date_planned = fields.Datetime(string='Scheduled Date', compute='_compute_date_planned', store=True, index=True, oldname='minimum_planned_date')

    shipping_sequence=fields.Char(string='Shipping Sequence',readonly=1)
    roytes_from=fields.Char(string='From')
    roytes_to=fields.Char(string='To')
    is_logistic = fields.Boolean(string="is logistic",)
    logistic_id = fields.Many2one(comodel_name="logistic.purchase", string="logistic",)
    is_landed_cost_ok = fields.Boolean(help="Specify whether the product can be selected in an HR expense.", string="landed cost ok")



    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('partner_ref', operator, name)]
        sos = self.search(domain + args, limit=limit)
        return sos.name_get()

    @api.multi
    @api.depends('name', 'partner_ref')
    def name_get(self):
        result = []
        for so in self:
            name = so.name
            if so.partner_ref:
                name += ' (' + so.partner_ref + ')'
            if so.amount_total:
                name += ': ' + formatLang(self.env, so.amount_total, currency_obj=so.currency_id)
            result.append((so.id, name))
        return result

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('service.order') or '/'
        return super(ServiceOrder, self).create(vals)

    @api.multi
    def unlink(self):
        for order in self:
            if not order.state == 'cancel':
                raise UserError(_('In order to delete a service order, you must cancel it first.'))
        return super(ServiceOrder, self).unlink()

    @api.multi
    def copy(self, default=None):
        new_po = super(ServiceOrder, self).copy(default=default)
        for line in new_po.order_line:
            seller = line.product_id._select_seller(
                partner_id=line.partner_id, quantity=line.product_qty,
                date=line.order_id.date_order and line.order_id.date_order[:10])
                # date=line.order_id.date_order and line.order_id.date_order[:10], uom_id=line.product_uom)
            line.date_planned = line._get_date_planned(seller)
        return new_po

    @api.depends('service_line_ids.invoice_lines.invoice_id.state')
    def _compute_invoice(self):
        for order in self:
            invoices = self.env['account.invoice']
            for line in order.service_line_ids:
                invoices |= line.invoice_lines.mapped('invoice_id')
            order.invoice_ids = invoices
            order.invoice_count = len(invoices)

    @api.depends('service_line_ids.price_total')
    def _amount_all(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.service_line_ids:
                amount_untaxed += line.price_subtotal
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty,
                                                      product=line.product_id, partner=line.order_id.partner_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.currency_id.round(amount_untaxed),
                'amount_tax': order.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
            })

    @api.onchange('partner_id', 'company_id')
    def onchange_partner_id(self):
        if not self.partner_id:
            self.fiscal_position_id = False
            self.payment_term_id = False
            self.currency_id = False
        else:
            self.fiscal_position_id = self.env['account.fiscal.position'].with_context(company_id=self.company_id.id).get_fiscal_position(self.partner_id.id)
            self.payment_term_id = self.partner_id.property_supplier_payment_term_id.id
            self.currency_id = self.partner_id.property_service_currency_id.id or self.env.user.company_id.currency_id.id
        return {}

    @api.multi
    def action_view_invoice(self):
        '''
        This function returns an action that display existing vendor bills of given purchase order ids.
        When only one found, show the vendor bill immediately.
        '''
        purchase_shipment_obj = self.env['routes.shipment.line'].search([('product_id','=',self.id)])
        for obj in purchase_shipment_obj:
            if purchase_shipment_obj.to_bill == False:
                raise ValidationError(_("to bill In Service Must be checked !"))
        action = self.env.ref('nano_service.action_invoice_tree23')
        result = action.read()[0]

        # override the context to get rid of the default filtering
        result['context'] = {'type': 'in_invoice', 'default_service_id': self.id}

        if not self.invoice_ids:
            # Choose a default account journal in the same currency in case a new invoice is created
            journal_domain = [
                ('type', '=', 'service'),
                ('company_id', '=', self.company_id.id),
                ('currency_id', '=', self.currency_id.id),
            ]
            default_journal_id = self.env['account.journal'].search(journal_domain, limit=1)
            if default_journal_id:
                result['context']['default_journal_id'] = default_journal_id.id
        else:
            # Use the same account journal than a previous invoice
            result['context']['default_journal_id'] = self.invoice_ids[0].journal_id.id

        # choose the view_mode accordingly
        if len(self.invoice_ids) != 1:
            result['domain'] = "[('id', 'in', " + str(self.invoice_ids.ids) + ")]"
        elif len(self.invoice_ids) == 1:
            res = self.env.ref('account.invoice_supplier_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = self.invoice_ids.id
        return result

    @api.multi
    def action_rfq_send(self):
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            if self.env.context.get('send_rfq', False):
                template_id = ir_model_data.get_object_reference('nano_service', 'email_template_edi_service')[1]
            else:
                template_id = ir_model_data.get_object_reference('nano_service', 'email_template_edi_service_done')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'service.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            # if order.user_has_groups('purchase.group_purchase_manager'):
            order.button_approve()
            # else:
            #     order.write({'state': 'to_approve'})
            if order.is_landed_cost_ok == True:
               print "purchase order id : ",order.logistic_id.purchase_order.id
               print "purchase order id : ",order.logistic_id.purchase_order.name
               for service_obj in order.service_line_ids:
                    print "lineeeeeeeee",service_obj.product_id.id
                    print "lineeeeeeeee name",service_obj.product_id.name
                    stock_picking_id=self.sudo().env['stock.picking'].sudo().search([('purchase_id','=',order.sudo().logistic_id.purchase_order.id)])
                    print "stock_picking_id :",stock_picking_id.id
                    if stock_picking_id:
                        landed_cost_id=self.sudo().env['stock.landed.cost'].sudo().search([])
                        for raw in landed_cost_id:
                            for r in raw.picking_ids:
                                 print"landed_cost_id :",r.name
                                 print"raw_id :",raw.id
                                 if stock_picking_id.id == r.id:
                                    landed_cost_line=self.sudo().env['stock.landed.cost.lines']
                                    landed_cost_line.sudo().create({
                                        'cost_id':raw.id,
                                        'split_method':'equal',
                                        'price_unit':service_obj.price_unit,
                                        'product_id':service_obj.product_id.id,
                                        'name':service_obj.product_id.name,
                                        # 'account_id':line.account_id.id,
                                    })
        return True

    @api.multi
    def button_approve(self, force=False):
        self.write({'state': 'service'})
        # if self.company_id.po_lock == 'lock':
        #     self.write({'state': 'done'})
        return {}

    @api.multi
    def button_draft(self):
        self.write({'state': 'draft'})
        return {}

    @api.multi
    def button_unlock(self):
        self.write({'state': 'service'})

    @api.multi
    def button_done(self):
        self.write({'state': 'done'})

    @api.multi
    def button_cancel(self):
        # for order in self:
            # for inv in order.invoice_ids:
            #     if inv and inv.state not in ('cancel', 'draft'):
            #         raise UserError(
            #             _("Unable to cancel this purchase order. You must first cancel related vendor bills."))
        self.write({'state': 'cancel'})

    @api.multi
    def _add_supplier_to_product(self):
        # Add the partner in the supplier list of the product if the supplier is not registered for
        # this product. We limit to 10 the number of suppliers for a product to avoid the mess that
        # could be caused for some generic products ("Miscellaneous").
        for line in self.service_line_ids:
            # Do not add a contact as a supplier
            partner = self.partner_id if not self.partner_id.parent_id else self.partner_id.parent_id
            if partner not in line.product_id.seller_ids.mapped('name') and len(line.product_id.seller_ids) <= 10:
                currency = partner.property_service_currency_id or self.env.user.company_id.currency_id
                supplierinfo = {
                    'name': partner.id,
                    'sequence': max(
                        line.product_id.seller_ids.mapped('sequence')) + 1 if line.product_id.seller_ids else 1,
                    # 'product_uom': line.product_uom.id,
                    'min_qty': 0.0,
                    'price': self.currency_id.compute(line.price_unit, currency),
                    'currency_id': currency.id,
                    'delay': 0,
                }
                vals = {
                    'seller_ids': [(0, 0, supplierinfo)],
                }
                try:
                    line.product_id.write(vals)
                except AccessError:  # no write access rights -> just ignore
                    break

    @api.depends('service_line_ids.date_planned')
    def _compute_date_planned(self):
        for order in self:
            min_date = False
            for line in order.service_line_ids:
                if not min_date or line.date_planned < min_date:
                    min_date = line.date_planned
            if min_date:
                order.date_planned = min_date

    @api.multi
    def action_set_date_planned(self):
        for order in self:
            order.service_line_ids.update({'date_planned': order.date_planned})

    @api.multi
    def print_quotation(self):
        self.write({'state': "sent"})
        return self.env['report'].get_action(self, 'nano_service.report_servicequotation')

class ServiceOrderLine(models.Model):
    _name = 'service.order.line'

    order_id = fields.Many2one('service.order')
    name = fields.Text(string='Description', required=True)
    sequence = fields.Integer(string='Sequence', default=10)
    product_qty = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'), required=True,
                               default='1.00')
    taxes_id = fields.Many2many('account.tax', string='Taxes')
    product_id = fields.Many2one('product.product', string='Product', change_default=True, required=True)
    price_unit = fields.Float(string='Unit Price', required=True, digits=dp.get_precision('Product Price'),
                              related="product_id.lst_price")
    price_subtotal = fields.Monetary(compute='_compute_amount', string='Subtotal', store=True)
    price_total = fields.Monetary(compute='_compute_amount', string='Total', store=True)
    price_tax = fields.Monetary(compute='_compute_amount', string='Tax', store=True)
    currency_id = fields.Many2one(related='order_id.currency_id', store=True, string='Currency', readonly=True)
    date_order = fields.Datetime(related='order_id.date_order', string='Order Date', readonly=True)
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic Account')
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')
    company_id = fields.Many2one('res.company', related='order_id.company_id', string='Company', store=True, readonly=True)
    invoice_lines = fields.One2many('account.invoice.line', 'service_line_id', string="Bill Lines", readonly=True, copy=False)
    qty_received = fields.Float(compute='_compute_qty_received', string="Received Qty", digits=dp.get_precision('Product Unit of Measure'), store=True)
    date_planned = fields.Datetime(string='Scheduled Date', required=True, index=True)
    state = fields.Selection(related='order_id.state', store=True)
    partner_id = fields.Many2one('res.partner', related='order_id.partner_id', string='Partner', readonly=True, store=True)

    @api.depends('order_id.state')
    def _compute_qty_received(self):
        for line in self:
            if line.order_id.state not in ['service', 'done']:
                line.qty_received = 0.0
                continue
            if line.product_id.type not in ['consu', 'product']:
                line.qty_received = line.product_qty

    @api.depends('product_qty', 'price_unit', 'taxes_id')
    def _compute_amount(self):
        for line in self:
            taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty,
                                              product=line.product_id, partner=line.order_id.partner_id)
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })


    @api.model
    def _get_date_planned(self, seller, po=False):
        """Return the datetime value to use as Schedule Date (``date_planned``) for
           PO Lines that correspond to the given product.seller_ids,
           when ordered at `date_order_str`.

           :param browse_record | False product: product.product, used to
               determine delivery delay thanks to the selected seller field (if False, default delay = 0)
           :param browse_record | False po: purchase.order, necessary only if
               the PO line is not yet attached to a PO.
           :rtype: datetime
           :return: desired Schedule Date for the PO line
        """
        date_order = po.date_order if po else self.order_id.date_order
        if date_order:
            return datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(
                days=seller.delay if seller else 0)
        else:
            return datetime.today() + relativedelta(days=seller.delay if seller else 0)

    @api.multi
    def unlink(self):
        for line in self:
            if line.order_id.state in ['purchase', 'done']:
                raise UserError(_('Cannot delete a purchase order line which is in state \'%s\'.') % (line.state,))
        return super(ServiceOrderLine, self).unlink()

    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result

        # Reset date, price and quantity since _onchange_quantity will provide default values
        self.date_planned = datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        # self.price_unit = self.product_qty = 0.0
        # self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        # result['domain'] = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}

        product_lang = self.product_id.with_context({
            'lang': self.partner_id.lang,
            'partner_id': self.partner_id.id,
        })
        self.name = product_lang.display_name
        if product_lang.description_service:
            self.name += '\n' + product_lang.description_service

        fpos = self.order_id.fiscal_position_id
        if self.env.uid == SUPERUSER_ID:
            company_id = self.env.user.company_id.id
            self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id))
        else:
            self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id)

        self._suggest_quantity()
        self._onchange_quantity()

        return result

    @api.onchange('product_id')
    def onchange_product_id_warning(self):
        if not self.product_id:
            return
        warning = {}
        title = False
        message = False

        product_info = self.product_id

        if product_info.service_line_warn != 'no-message':
            title = _("Warning for %s") % product_info.name
            message = product_info.service_line_warn_msg
            warning['title'] = title
            warning['message'] = message
            if product_info.service_line_warn == 'block':
                self.product_id = False
            return {'warning': warning}
        return {}

    # @api.onchange('product_qty', 'product_uom')
    @api.onchange('product_qty')
    def _onchange_quantity(self):
        if not self.product_id:
            return

        seller = self.product_id._select_seller(
            partner_id=self.partner_id,
            quantity=self.product_qty,
            date=self.order_id.date_order and self.order_id.date_order[:10])

        if seller or not self.date_planned:
            self.date_planned = self._get_date_planned(seller).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        if not seller:
            return

        price_unit = self.env['account.tax']._fix_tax_included_price(seller.price, self.product_id.supplier_taxes_id,
                                                                     self.taxes_id) if seller else 0.0
        if price_unit and seller and self.order_id.currency_id and seller.currency_id != self.order_id.currency_id:
            price_unit = seller.currency_id.compute(price_unit, self.order_id.currency_id)

        # if seller and self.product_uom and seller.product_uom != self.product_uom:
        # if seller:
        #     price_unit = seller.product_uom._compute_price(price_unit, self.product_uom)

        self.price_unit = price_unit

    @api.onchange('product_qty')
    def _onchange_product_qty(self):
        if (self.state == 'service' or self.state == 'to_approve') and self.product_id.type in ['product',
                                                                                                 'consu'] and self.product_qty < self._origin.product_qty:
            warning_mess = {
                'title': _('Ordered quantity decreased!'),
                'message': _(
                    'You are decreasing the ordered quantity!\nYou must update the quantities on the reception and/or bills.'),
            }
            return {'warning': warning_mess}

    def _suggest_quantity(self):
        '''
        Suggest a minimal quantity based on the seller
        '''
        if not self.product_id:
            return

        seller_min_qty = self.product_id.seller_ids \
            .filtered(lambda r: r.name == self.order_id.partner_id) \
            .sorted(key=lambda r: r.min_qty)
        if seller_min_qty:
            self.product_qty = seller_min_qty[0].min_qty or 1.0
            # self.product_uom = seller_min_qty[0].product_uom
        else:
            self.product_qty = 1.0

class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self, auto_commit=False):
        if self._context.get('default_model') == 'service.order' and self._context.get('default_res_id'):
            if not self.filtered('subtype_id.internal'):
                order = self.env['service.order'].browse([self._context['default_res_id']])
                if order.state == 'draft':
                    order.state = 'sent'
        return super(MailComposeMessage, self.with_context(mail_post_autofollow=True)).send_mail(auto_commit=auto_commit)

