# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    service_id = fields.Many2one('service.order', string='Add Service Order')
    flag = fields.Boolean()

    @api.onchange('state', 'partner_id', 'invoice_line_ids')
    def _onchange_allowed_service_ids(self):
        result = {}
        service_line_ids = self.invoice_line_ids.mapped('service_line_id')
        service_ids = self.invoice_line_ids.mapped('service_id').filtered(lambda r: r.service_line_ids <= service_line_ids)

        result['domain'] = {'service_id': [
            ('invoice_status', '=', 'to invoice'),
            ('partner_id', 'child_of', self.partner_id.id),
            ('id', 'not in', service_ids.ids),
        ]}
        return result

    def _prepare_invoice_line_from_svc_line(self, line):
        qty = line.product_qty
        taxes = line.taxes_id
        invoice_line_tax_ids = line.order_id.fiscal_position_id.map_tax(taxes)
        invoice_line = self.env['account.invoice.line']
        data = {
            'service_line_id': line.id,
            'name': line.order_id.name+': '+line.name,
            'product_id': line.product_id.id,
            'account_id': invoice_line.with_context({'journal_id': self.journal_id.id, 'type': 'in_invoice'})._default_account(),
            'price_unit': line.order_id.currency_id.compute(line.price_unit, self.currency_id, round=False),
            'quantity': qty,
            'discount': 0.0,
            'account_analytic_id': line.account_analytic_id.id,
            'analytic_tag_ids': line.analytic_tag_ids.ids,
            'invoice_line_tax_ids': invoice_line_tax_ids.ids
        }
        account = invoice_line.get_invoice_line_account('in_invoice', line.product_id, line.order_id.fiscal_position_id, self.env.user.company_id)
        if account:
            data['account_id'] = account.id
        return data

    # Load all unsold PO lines
    @api.onchange('service_id')
    def service_order_change(self):
        if not self.service_id:
            return {}
        if not self.partner_id:
            self.partner_id = self.service_id.partner_id.id

        new_lines = self.env['account.invoice.line']
        for line in self.service_id.service_line_ids:
            if line in self.invoice_line_ids.mapped('service_line_id'):
                continue
            data = self._prepare_invoice_line_from_svc_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines += new_line

        self.invoice_line_ids += new_lines
        self.service_id = False
        return {}

    @api.onchange('currency_id')
    def _onchange_currency_id(self):
        if self.currency_id:
            for line in self.invoice_line_ids.filtered(lambda r: r.service_line_id):
                line.price_unit = line.service_id.currency_id.compute(line.service_line_id.price_unit, self.currency_id, round=False)

    @api.onchange('invoice_line_ids')
    def _onchange_origin(self):
        service_ids = self.invoice_line_ids.mapped('service_id')
        if service_ids:
            self.origin = ', '.join(service_ids.mapped('name'))

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        res = super(AccountInvoice, self)._onchange_partner_id()
        if not self.env.context.get('default_journal_id') and self.partner_id and self.currency_id and \
                        self.type in ['in_invoice', 'in_refund'] and \
                        self.currency_id != self.partner_id.property_service_currency_id:
            journal_domain = [
                ('type', '=', 'service'),
                ('company_id', '=', self.company_id.id),
                ('currency_id', '=', self.partner_id.property_service_currency_id.id),
            ]
            default_journal_id = self.env['account.journal'].search(journal_domain, limit=1)
            if default_journal_id:
                self.journal_id = default_journal_id
        return res

    @api.model
    def create(self, vals):
        invoice = super(AccountInvoice, self).create(vals)
        service = invoice.invoice_line_ids.mapped('service_line_id.order_id')
        if service and not invoice.refund_invoice_id:
            message = _("This service provider bill has been created from: %s") % (",".join(
                ["<a href=# data-oe-model=service.order data-oe-id=" + str(order.id) + ">" + order.name + "</a>" for
                 order in service]))
            invoice.message_post(body=message)
        return invoice

    @api.multi
    def write(self, vals):
        result = True
        for invoice in self:
            service_old = invoice.invoice_line_ids.mapped('service_line_id.order_id')
            result = result and super(AccountInvoice, invoice).write(vals)
            service_new = invoice.invoice_line_ids.mapped('service_line_id.order_id')
            # To get all po reference when updating invoice line or adding purchase order reference from vendor bill.
            service = (service_old | service_new) - (service_old & service_new)
            if service:
                message = _("This service provider bill has been modified from: %s") % (",".join(
                    ["<a href=# data-oe-model=service.order data-oe-id=" + str(order.id) + ">" + order.name + "</a>"
                     for order in service]))
                invoice.message_post(body=message)
        return result


class AccountInvoiceLineInherit(models.Model):
    _inherit = 'account.invoice.line'

    service_line_id = fields.Many2one('service.order.line', 'Service Order Line', ondelete='set null', index=True,
                                      readonly=True)
    service_id = fields.Many2one('service.order', related='service_line_id.order_id', string='Service Order',
                                 store=False, readonly=True)
