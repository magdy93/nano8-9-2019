# -*- coding: utf-8 -*-

from . import service_order
from . import product_product
from . import res_partner
from . import res_config
from . import account_invoice
