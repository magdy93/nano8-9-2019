# -*- coding: utf-8 -*-

{
    'name': 'Nano Service Order',
    'description': 'Nano Service Order',
    'summary': 'UNSS-Service Module',
    'category': '',
    'author': 'Universal Selective Systems',
    'depends': ['base','product','account','report',],
    'data': [
        'security/service_security.xml',
        'views/product.xml',
        'views/res_config_views.xml',
        'views/res_partner.xml',
        'views/service_order_views.xml',
        'views/account_invoice_views.xml',
        'reports/service_quotation_report.xml',
        'reports/service_order_report.xml',
        'data/service_data.xml',
        'data/mail_template_data.xml',
    ]
}