# -*- coding: utf-8 -*-
{
    'name': "Transportation detail",

    'summary': """
        Transportation detail""",

    'description': """
        Transportation detail
    """,
    'author': "USS",
    'website': "http://www.yourcompany.com",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base','product','nano_logistic'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}