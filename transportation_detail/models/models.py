# -*- coding: utf-8 -*-

from odoo import models, fields, api

class TransportationDetail(models.Model):
    _name = 'transportation.detail'
    _inherit = ['mail.thread']
    cargo_ids = fields.One2many(comodel_name="cargo.order.line", inverse_name="transportation_id",)
    date_ids = fields.One2many(comodel_name="date.order.line", inverse_name="date_id",)
    vehicle_ids = fields.One2many(comodel_name="vehicle.order.lineee", inverse_name="vehiclee_id",)
    container_ids = fields.One2many(comodel_name="container.order.lineee", inverse_name="containerr_id",)
    driver_information_ids = fields.One2many(comodel_name="driver.information.line", inverse_name="transportation_id",)

    route = fields.Selection(string="Route", selection=[('dispatch', 'dispatch'), ('receipt', 'receipt'), ], required=False, )
    type = fields.Selection(string="Type", selection=[('local', 'domestic'), ('outside', 'international'), ], required=False, )
    license_plate = fields.Char(string="License plate", required=False, )
    logistic_domestic_state = fields.Selection(string="logistic state", selection=[('international', 'international'), ('domestic','domestic'), ], required=False, )
    logistic_id = fields.Many2one(comodel_name="logistic.purchase", string="Source", required=False,)
    demestic_id = fields.Many2one(comodel_name="domistic.purchase", string="Source", required=False,)
    purchase_id = fields.Many2one(comodel_name="purchase.order", string="Purchase Order", required=False,)
    sale_id = fields.Many2one(comodel_name="sale.order", string="Sale Order", required=False,)
    company_id = fields.Many2one(comodel_name='res.company',string='Company',required=True,
                                 default=lambda self: self.env.user.company_id)

    state = fields.Selection([
            ('draft', 'Draft'),
            ('progress', 'In progress'),
            ('done', 'Done'),
            ],default='draft')

    @api.multi
    def draft_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })
        self.message_post(body='state is draft',subtype='mt_comment',
                                              subject='state',message_type='notification')
    @api.multi
    def inprogress_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'progress',
        })
        if self.logistic_domestic_state == 'domestic':
           domestic_obj=self.env['domistic.purchase'].search([('id','=',self.demestic_id.id)])
           if domestic_obj:
              domestic_obj.write({
                        'transportation_status': 'In progress',
                        })
        else:
           logistic_obj=self.env['logistic.purchase'].search([('id','=',self.logistic_id.id)])
           if logistic_obj:
              logistic_obj.write({
                        'transportation_status': 'In progress',
                        })

        self.message_post(body='state is progress',subtype='mt_comment',
                                              subject='state',message_type='notification')
    @api.multi
    def done_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'state': 'done',
        })
        if self.logistic_domestic_state == 'domestic':
           domestic_obj=self.env['domistic.purchase'].search([('id','=',self.demestic_id.id)])
           if domestic_obj:
              domestic_obj.sudo().write({
                        'transportation_status': 'done',
                        })
        else:
           logistic_obj=self.env['logistic.purchase'].search([('id','=',self.logistic_id.id)])
           if logistic_obj:
              logistic_obj.sudo().write({
                        'transportation_status': 'done',
                        })
        self.sudo().message_post(body='state is done',subtype='mt_comment',
                                              subject='state',message_type='notification')

    def _prepare_logistic_line(self, line):
        data = {
            'sale_line_id': line.id,
            'product_id': line.product.id,
            'quantity':line.quantity,
            'uom':line.uom.id,
        }
        return data

    @api.onchange('logistic_id')
    def sale_order_change(self):
        if not self.logistic_id:
            return {}
        new_lines = self.env['cargo.order.line']
        for line in self.logistic_id.logistic_order - self.cargo_ids.mapped('sale_line_id'):
            data = self._prepare_logistic_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional(self)
            new_lines += new_line

        self.cargo_ids = new_lines
        return {}
class CargoOrderLine(models.Model):
    _name = 'cargo.order.line'
    transportation_id = fields.Many2one(comodel_name="transportation.detail", string="transportation", required=False, )
    product_id = fields.Many2one(comodel_name="product.product", string="Cargo", required=False, )
    quantity = fields.Float(string="quantity",  required=False, )
    uom = fields.Many2one(comodel_name="product.uom", string="unit of measure", required=False, )

    sale_line_id = fields.Many2one('logistic.purchase.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
    sale_order = fields.Many2one('logistic.purchase', related='sale_line_id.order_line', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')
    def _set_additional(self,invoice):
        pass
class DateOrderLine(models.Model):
    _name = 'date.order.line'
    date_id = fields.Many2one(comodel_name="transportation.detail", string="transportation", required=False, )
    # departure = fields.Char(string="Departure", required=False, )
    departure = fields.Selection(string="Departure", selection=[('vendor', 'Vendor'), ('warehouse', 'Warehouse'),('port','Port'),], required=False, )
    arrival = fields.Selection(string="Arrival", selection=[('vendor', 'Vendor'), ('warehouse', 'Warehouse'),('port','Port'),], required=False, )
    # arrival = fields.Char(string="Arrival", required=False, )
    D_date = fields.Date(string="Date", required=False, )
    A_date = fields.Date(string="Date", required=False, )
class VehicleOrderLine(models.Model):
    _name = 'vehicle.order.lineee'
    vehiclee_id = fields.Many2one(comodel_name="transportation.detail", string="transportation", required=False, )
    vehicle_name = fields.Char(string="Vehicle Name", required=False, )
    plate_no = fields.Char(string="Plate No", required=False, )
    driver_name = fields.Char(string="Driver Name", required=False, )
    driver_id = fields.Integer(string="ID Number", required=False, )
    driver_attachments = fields.Binary(string="Attachments",)
class ContainerOrderLine(models.Model):
    _name = 'container.order.lineee'
    containerr_id = fields.Many2one(comodel_name="transportation.detail", string="transportation", required=False, )
    container_number = fields.Char(string="Number", required=False, )
    container_attachments = fields.Binary(string="Attachments",)
    # container_attachments = fields.Many2many('ir.attachment', 'transportation_detail_ir_attachments_rel',
    #                             'container_order_line_id', 'attachment_id', string='Attachments')
class DriverInformationLine(models.Model):
    _name = 'driver.information.line'
    transportation_id = fields.Many2one(comodel_name="transportation.detail", string="transportation", required=False, )
    driver_name = fields.Char(string="Driver Name", required=False, )
    product_id = fields.Many2one(comodel_name="product.product", string="Product", required=False, )
    quantity = fields.Float(string="Quantity",  required=False, )
    check_in = fields.Datetime(string="Check In Time/Date", required=False, )
    check_out = fields.Datetime(string="Check out Time/Date", required=False, )
    transportation_company_id = fields.Many2one(comodel_name="res.partner", string="Transportation company", required=False, )
    car_number = fields.Char(string="Car Number", required=False, )
    mobile_number = fields.Integer(string="Mobile Number", required=False, )
    id_number = fields.Integer(string="ID Number", required=False, )
    passport_number = fields.Integer(string="Passport number", required=False, )
    license_number = fields.Char(string="License Number", required=False, )
    license_document = fields.Binary(string="License Document", )
