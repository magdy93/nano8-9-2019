# -*- coding: utf-8 -*-

from odoo import models, fields, api

class PettyCashRequest(models.Model):
    _name = 'petty.cash.request'
    _inherit = ['mail.thread']
    expense_ids = fields.One2many(comodel_name="hr.expense", inverse_name="petty_expense_id", string="Petty Cash", required=False, )
    payment_ids = fields.One2many(comodel_name="account.payment", inverse_name="petty_payment_id", string="Petty Cash", required=False, )
    journal_ids = fields.One2many(comodel_name="account.move.line", inverse_name="petty_id", string="Petty Cash", required=False, )
    user_id = fields.Many2one(comodel_name="res.users", string="Requested By",default=lambda self: self.env.user )
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False, )
    department_id = fields.Many2one(comodel_name="hr.department", string="Department", required=False, )
    job_id = fields.Many2one(comodel_name="hr.job", string="Job Title", required=False, )
    date_received = fields.Date(string="Date Received", required=False, )
    date_requested = fields.Date(string="Date Of Request" , readonly=True , default=fields.date.today(),)
    amount = fields.Float(string="Amount Received",required=True )
    journal_id = fields.Many2one(comodel_name="account.journal", string="Petty Cash Journal", required=True, )
    journal_transfer_id = fields.Many2one(comodel_name="account.journal", string="Transfer To", required=True, )
    account_id = fields.Many2one(comodel_name="account.account", string="Petty Cash Credit Account", required=True, )
    state = fields.Selection(string="State", selection=[('draft', 'draft'),
                                                        ('requested', 'Submit To Manager'),
                                                        ('cash_dispatched', 'Cash Dispatched'),
                                                        ('cash_sent', 'Cash Sent'),
                                                        ('to_reconcile', 'To Reconcile'),
                                                        ('reconciled', 'Reconciled'),
                                                        ('cancel', 'Cancel'),
                                                        ],default='draft', required=False, )
    def action_draft(self):
        self.write({
            'state':'draft'
        })
    def action_approve_requested(self):
        self.write({
            'state': 'requested'
        })
        self.message_post(body="Petty Cash Requested",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')
    def action_cancel_requested(self):
        self.write({
            'state': 'cancel'
        })
        self.message_post(body="Petty Cash Cancelled",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')
    def action_approve_cash_dispatch(self):
        account_payment = self.env['account.payment']
        acc=account_payment.create({
            'petty_payment_id': self.id,
            'payment_type': 'transfer',
            'journal_id': self.journal_id.id,
            'destination_journal_id': self.journal_transfer_id.id,
            'amount': self.amount,
            'payment_date': self.date_received,
            'payment_method_id': 1,
        })
        self.write({
            'state':'cash_dispatched'
        })
        self.message_post(body="Petty Cash Dispatched",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')
    def action_cancel_cash_dispatch(self):
        self.write({
            'state':'cancel'
        })
        self.message_post(body="Petty Cash Cancelled",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')
    def action_to_approve_sent(self):
        self.write({
            'state':'cash_sent'
        })
        self.message_post(body="Petty Cash Sent",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')
    def action_to_cancel_sent(self):
        self.write({
            'state':'cancel'
        })
        self.message_post(body="Petty Cash Cancelled",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')
    def action_to_reconciled(self):
        self.write({
            'state':'to_reconcile'
        })
        self.message_post(body="Petty Cash To Reconciled",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')
    def action_reconciled(self):
        self.write({
            'state':'reconciled'
        })
        self.message_post(body="Petty Cash Reconciled",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')

    @api.multi
    def action_sent_to_manager(self):
        for record in self:
            groups = self.env['res.groups'].search([('name', '=', 'Manager')])
            recipient_partners = []
            for group in groups:
                for recipient in group.users:
                    if recipient.partner_id.id not in recipient_partners:
                        recipient_partners.append(recipient.partner_id.id)
            if len(recipient_partners):
                record.message_post(body='Petty Cash Action Needed',
                                    force_send=True,
                                    subtype='mt_comment',
                                    subject='Petty Cash Notification',
                                    partner_ids=recipient_partners,
                                    message_type='notification'
                                    )
        self.message_post(body="Petty Cash Notification Sent",
                          subtype='mt_comment',
                          subject='Petty Cash',
                          message_type='notification')

    @api.onchange('user_id')
    def get_employee_data(self):
        employee_ids = self.env['hr.employee'].search([('user_id','=',self.user_id.id)])
        if employee_ids:
           for employee in employee_ids :
               self.employee_id =  employee.id
               self.department_id =  employee.department_id.id
               self.job_id =  employee.job_id.id
