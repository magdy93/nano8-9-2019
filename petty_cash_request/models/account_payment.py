# -*- coding: utf-8 -*-

from odoo import models, fields, api

class AccountPayment(models.Model):
    _inherit = 'account.payment'
    petty_payment_id = fields.Many2one(comodel_name="petty.cash.request", string="Petty Cash", required=False, )