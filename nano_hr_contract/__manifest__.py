{
    'name': 'Nano HR Contract',
    'version': '10.0',
    'description': 'HR Contract',
    'author': 'USS',
    'website': 'www.iti.gov.eg',
    'depends': ['hr'],
    'data': [
        'views/hr_contract.xml',
        'views/hr_contract_first_test.xml',
        'views/hr_contract_employee.xml',
        # 'security/hr_security.xml',
        # 'security/ir.model.access.csv',
    ]
}
