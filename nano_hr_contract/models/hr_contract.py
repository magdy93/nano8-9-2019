# -*- coding: utf-8 -*-
from odoo import models, fields, api
class HrContract(models.Model):
    _name = 'nano.hr.contract'
    name = fields.Char(string="الاسم", required=False, )
    date = fields.Datetime(string="تاريخ الميلاد",)
    job_id = fields.Many2one(comodel_name="hr.job", string="الوظيفه المرشح لها",)
    department_id = fields.Many2one(comodel_name="hr.department", string="الاداره", required=False, )
    result = fields.Char(string="نتيجه الاختبار", required=False, )
    test = fields.Char(string="اختبار الصلاحيه", required=False, )