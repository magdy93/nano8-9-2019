# import json
from odoo import http
# from odoo.exceptions import MissingError


def get_error_response(error_msg, status_code):
    data = json.dumps({
        'error': True,
        'error_msg': str(error_msg)
    })
    response = http.request.make_response(data)
    response.status = status_code
    return response


class TagController(http.Controller):
    @http.route('/api/v1/tags', type='http', auth='none', methods=['GET'], cors='*')
    def get_tags(self, **kw):
        company_id = kw.get('company_id', False)

        # Try to cast company_id parameter
        try:
            company_id = int(company_id)
        except ValueError as e:
            return get_error_response(error_msg=e, status_code='400')

        domain = []
        if company_id:
            company = http.request.env['res.company'].sudo().search([('id', '=', company_id)])
            if not company:
                return get_error_response(error_msg='company id not found', status_code='404')
            domain = [('company_id', '=', company_id)]

        all_tags = http.request.env['partner.tag'].sudo().search(domain)
        tags = []
        for tag in all_tags:
            tag_dict = {
                'name': tag.name or None,
                'company_id': tag.company_id.id or None,
                'country_id': tag.country_id.id or None,
                'short_name': tag.short_name or None,
                'tag_id': tag.id or None,
                'department_id': tag.department_id.id or None
            }
            tags.append(tag_dict)
        return json.dumps(tags)

    @http.route('/api/v1/tags/<int:tag_id>', type='http', auth='none', methods=['GET'], cors='*')
    def get_tag_by_id(self, tag_id, **kw):
        try:
            tag = http.request.env['partner.tag'].sudo().browse(tag_id)
            tag_dict = {
                'name': tag.name or None,
                'company_id': tag.company_id.id or None,
                'country_id': tag.country_id.id or None,
                'short_name': tag.short_name or None,
                'tag_id': tag.id,
                'industry_id': tag.industry_id.id or None,
                'business_line_id': tag.station_client_type or None,
                'sales_person_id': tag.sales_person_id.id or None,
                'phone': tag.phone or None,
                'website': tag.website or None,
                'department_id': tag.department_id.id or None,
                'sector_id': tag.sector_id.id or None
            }
        except MissingError as e:
            return get_error_response(error_msg=str(e), status_code='404')

        return json.dumps(tag_dict)
