import json
from odoo import http


class MyController(http.Controller):
    @http.route("/api/v1/employee", methods=['GET', 'POST'], cors='*', csrf=False, auth='none')
    def get_tags(self, **kwargs):
        employees = http.request.env['hr.employee'].sudo().search([], limit=3)
        context = {"employees": employees}
        return http.request.render('my_hr.gallery', context)
