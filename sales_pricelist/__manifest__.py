# -*- coding: utf-8 -*-
{
    'name': "Sales Pricelist",
    'summary': """Sales Pricelist""",
    'author': "",
    'website': "",
    'category': 'sales',

    'description': """

    1- This module add page in sales/customers called sales pricelist \n
    2- add field boolean in product pricelist called default \n
    3- domain on pricelist and incoterms in quotations \n
    4- validation error on incoterms , default pricelist and pricelist valid date
        """,
    'version': '0.1',
    'depends': [
        'base','sale','stock'
    ],

    'data': [
        'views/customer_sales_pricelist.xml',

    ],

    'installable': True,
    'auto_install': False,
}
