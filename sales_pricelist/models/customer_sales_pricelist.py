# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import ValidationError


class ResPartner(models.Model):
    _inherit = "res.partner"

    sales_pricelist_ids = fields.One2many('product.pricelist.item', 'sale_pricelist_id', string="Sales Pricelist")


class SalesPriceList(models.Model):
    _inherit = "product.pricelist.item"

    sale_pricelist_id = fields.Many2one('sale.pricelist', string="Sales Pricelist")
    incoterm_id = fields.Many2one('stock.incoterms', string="Incoterms")
    package_type = fields.Char(string="Package Type", )
    from_port = fields.Char(string="From Port")
    to_port = fields.Char(string="To Port")
    valid_date = fields.Date(string="Valid Until")


class ProductPricelist(models.Model):
    _inherit = "product.pricelist"

    pricelist_checked = fields.Boolean(string="Default")


class SaleOredr(models.Model):
    _inherit = "sale.order"

    @api.multi
    @api.onchange('partner_id')
    def get_customer_pricelist(self):
        pricelist_ids = []
        for rec in self:
            if rec.partner_id:
                customer_price_list_obj = self.env['res.partner'].search([('id', '=', rec.partner_id.id)])
                for pricelist in customer_price_list_obj:
                    if pricelist.sales_pricelist_ids:
                        for price in pricelist.sales_pricelist_ids:
                            if price.pricelist_id:
                                pricelist_ids.append(price.pricelist_id.id)
                    if pricelist.property_product_pricelist:
                        pricelist_ids.append(pricelist.property_product_pricelist.id)
                return {'domain': {'pricelist_id': [('id', 'in', pricelist_ids)]}}

    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', required=True, readonly=True,
                                   states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                                   help="Pricelist for current sales order.", domain=get_customer_pricelist)

    @api.multi
    @api.onchange('pricelist_id')
    def get_customer_incoterms(self):
        for rec in self:
            if rec.pricelist_id:
                price_list_obj = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
                if price_list_obj.sales_pricelist_ids:
                    for list in price_list_obj.sales_pricelist_ids:
                        if rec.pricelist_id.id == list.pricelist_id.id:
                            if list.incoterm_id:
                                rec.incoterm = list.incoterm_id
                            if not list.incoterm_id:
                                rec.incoterm =False


    @api.multi
    def action_quotation_send(self):
        self.ensure_one()
        if self.pricelist_id:
            price_list_obj = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
            if price_list_obj.sales_pricelist_ids:
                for list in price_list_obj.sales_pricelist_ids:
                    if self.pricelist_id.id == list.pricelist_id.id:
                        if list.valid_date:
                            if fields.Date.from_string(fields.Date.today()) >= fields.Date.from_string(list.valid_date):
                              raise ValidationError(_('Sorry . You Must Check The Pricelist Expiry Date !'))
                        if self.incoterm.id != list.incoterm_id.id:
                            raise ValidationError(_('Sorry . The Incoterms Should Be The Same In Pricelist!'))
        if self.pricelist_id.pricelist_checked == True:
            raise ValidationError(_('Sorry .This Pricelist Is A Default Pricelist!'))

        else:
            ir_model_data = self.env['ir.model.data']
            try:
                template_id = ir_model_data.get_object_reference('sale', 'email_template_edi_sale')[1]
            except ValueError:
                template_id = False
            try:
                compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
            except ValueError:
                compose_form_id = False
            ctx = {
                'default_model': 'sale.order',
                'default_res_id': self.ids[0],
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'mark_so_as_sent': True,
                'custom_layout': "sale.mail_template_data_notification_email_sale_order",
                'proforma': self.env.context.get('proforma', False),
                'force_email': True
            }
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'views': [(compose_form_id, 'form')],
                'view_id': compose_form_id,
                'target': 'new',
                'context': ctx,
            }

    # @api.multi
    # def action_confirm(self):
    #     for order in self:
    #         if order.pricelist_id:
    #             price_list_obj = self.env['res.partner'].search([('id', '=', order.partner_id.id)])
    #             if price_list_obj.sales_pricelist_ids:
    #                 for list in price_list_obj.sales_pricelist_ids:
    #                     if order.pricelist_id.id == list.pricelist_id.id:
    #                         if list.valid_date:
    #                             if fields.Date.from_string(fields.Date.today()) >= fields.Date.from_string(list.valid_date):
    #                                 raise ValidationError(_('Sorry . You Must Check The Pricelist Expiry Date !'))
    #                         if order.incoterm.id != list.incoterm_id.id:
    #                             raise ValidationError(_('Sorry . The Incoterms Should Be The Same In Pricelist!'))
    #         if order.pricelist_id.pricelist_checked == True:
    #             raise ValidationError(_('Sorry .This Pricelist Is A Default Pricelist!'))
    #         order.state = 'sale'
    #         order.confirmation_date = fields.Datetime.now()
    #         if self.env.context.get('send_email'):
    #             self.force_quotation_send()
    #         order.order_line._action_procurement_create()
    #     if self.env['ir.values'].get_default('sale.config.settings', 'auto_done_setting'):
    #         self.action_done()
    #     return True
