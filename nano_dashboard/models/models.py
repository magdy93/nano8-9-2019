# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CustomSalesDashboard(models.Model):
    _name = "custom.sales.dashboard"
    # _inherit = 'sale.order'
    is_ok = fields.Boolean(string="ok", )

    @api.multi
    @api.depends('is_ok')
    def _get_count(self):
        if self.is_ok == True:
            print "kkkkkkkkkk"
            quotations_count = self.env['sale.order'].search([('state', '=', 'draft')])
            orders_count = self.env['sale.order'].search([('state', '=', 'sale')])
            orders_done_count = self.env['sale.order'].search([('state', '=', 'done')])

            self.orders_count = len(orders_count)
            self.quotations_count = len(quotations_count)
            self.orders_done_count = len(orders_done_count)

        else:
            print "mmmmmmmmmm"
        quotations_count = self.env['sale.order'].search([('state', '=', 'draft')])
        orders_count = self.env['sale.order'].search([('state', '=', 'sale')])
        orders_done_count = self.env['sale.order'].search([('state', '=', 'done')])

        self.orders_count = len(orders_count)
        self.quotations_count = len(quotations_count)
        self.orders_done_count = len(orders_done_count)

    color = fields.Integer(string='Color Index')
    name = fields.Char(string="Name")
    orders_count = fields.Integer(compute = '_get_count',store=True)
    quotations_count = fields.Integer(compute= '_get_count',store = True)
    orders_done_count = fields.Integer(compute= '_get_count',store =True)