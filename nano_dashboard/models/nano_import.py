# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CustomSalesDashboard(models.Model):
    _name = "custom.import.dashboard"
    inherit = 'logistic.purchase'
    color = fields.Integer(string='Color Index')
    name = fields.Char(string="Name")
    orders_count = fields.Integer(compute='_get_count', store=True)
    # quotations_count = fields.Integer(compute= '_get_count',store = True)
    # orders_done_count = fields.Integer(compute= '_get_count',store =True)

    @api.multi
    def _get_count(self):
        orders_count = self.env['logistic.purchase'].search([])
        self.orders_count = len(orders_count)
        print "count : ",self.orders_count
