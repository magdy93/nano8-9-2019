# -*- coding: utf-8 -*-

from openerp.exceptions import UserError, ValidationError
from openerp import models, fields, api , exceptions, _
from datetime import datetime, time
from dateutil.relativedelta import relativedelta
import dateutil.parser as parser
# import datetime
# import openerp.addons.decimal_precision as dp

class recruitment_plan(models.Model):
    _name = 'recruitment.plan'
    _inherit = ['mail.thread']
#     AVAILABLE_PRIORITIES = [
#     ('0', 'Normal'),
#     ('1', 'Good'),
#     ('2', 'Very Good'),
#     ('3', 'Excellent')
# ]

    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id, readonly="true")
    department_id = fields.Many2one('hr.department', 'Department', required=True)
    manager_id = fields.Many2one(comodel_name="res.partner", string="Responsible Manager", required=True, )
    nature_of_department = fields.Selection([('Production', 'Production'),
                                             ('Non-production', 'Non-production')],
                                              'Nature of Department')
    year = fields.Selection([(num, str(num)) for num in range((datetime.now().year)-50 , (datetime.now().year)+50 )], 'Year', required=True)
    # priority = fields.Selection(AVAILABLE_PRIORITIES, "Appreciation", default='0')
    is_any_manager = fields.Boolean('is any manager', compute='_compute_is_any_manager')
    shifts = fields.One2many('recruitment.plan.shifts', 'shift_id', 'Shifts', required=True)
    state = fields.Selection([('Draft', 'Draft'),
                              ('department_approval', 'Department Approval'),
                              ('department_refused', 'Department Refused'),
                              ('Human resource Approval', 'Human resource Approval'),
                              ('CEO Approval','CEO Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True)
    start_button = fields.Boolean('Start', readonly=True)

    # @api.onchange('department_id')
    # def check_department_duplicated(self):
    #     department = self.env['recruitment.plan'].search([('department_id', '=', self.department_id.id)])
    #     if len(department):
    #         raise exceptions.ValidationError("This Department already has a Plan")


    @api.one
    @api.constrains('year')
    def get_current_year(self):
        now = datetime.now()
        year_now = now.year
        if self.year < year_now:
             raise UserError(_("Year must be greater than current year !"))

    @api.onchange('shifts')
    def _shift_production_line_shift_behaviour(self):
        for shift in self.shifts:
            shift.shift_id.nature = self.nature_of_department

    @api.onchange('nature_of_department')
    def _shift_production_line_behaviour(self):
        for shift in self.shifts:
            shift.nature = self.nature_of_department

    @api.model
    def create(self, vals):
        res = super(recruitment_plan, self).create(vals)
        res.start_button = True
        return res

    @api.multi
    def action_save_recruitment_plan(self):
        recipient_partners = []
        for record in self:
            body = "New Recruitment Plan created and needs Department approval"
            recipient_partners.append((4,self.manager_id.id))
        record.message_post(body=body,force_send=True,subtype='mt_comment',
                                  subject='message',partner_ids=recipient_partners,message_type='notification')

        # groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Employee')])
        # recipient_partners = []
        # for group in groups:
        #     for recipient in group.users:
        #         if recipient.partner_id.id not in recipient_partners:
        #             recipient_partners.append(recipient.partner_id.id)
        # if len(recipient_partners):
        #     body = "New Recruitment Plan created and needs HR approval"
        #     self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Draft'})
        self.start_button = False

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Human resource Approval'})
        groups = self.env['res.groups'].search([('name','=','CEO')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Recruitment Plan approved from HR and needs CEO approval"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        for rec in self:
            for line in rec.shifts:
                job=self.env['hr.job']
                job_obj=job.search([('name','=',line.job.name)])
                if job_obj:
                    print "found",job_obj.no_of_recruitment
                    job_obj.write({
                        'no_of_recruitment': job_obj.no_of_recruitment + line.employees_deficient_nu,
                    })
                else:
                    print "new job"
                    job.create({
                        'name':line.job.name,
                        'department_id':line.department_id.id,
                        'no_of_recruitment':line.employees_deficient_nu,
                    })
        return {}

    @api.multi
    def action_department_approval(self):
        print "1",self.manager_id.partner_id.id
        print "1",self.manager_id.partner_id.name
        print "2",self.env.user.id
        if self.manager_id.user_id.id == self.env.user.id :
            self.write({'state': 'department_approval'})
            body = "New Recruitment Plan approved from department"
            self.message_post(body=body, message_type='email')
        else:
             raise UserError(_("You aren't allow to approve"))
    @api.multi
    def action_department_refused(self):
        self.write({'state': 'department_refused'})
        body = "New Recruitment Plan Refused from department"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_ceo_approval(self):
        self.write({'state': 'CEO Approval'})
        body = "New Recruitment Plan approved from CEO"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New Recruitment Plan refused"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def _compute_is_any_manager(self):
        for record in self:
            manager = self.env['hr.department'].search([('manager_id.user_id.id', '=', self.env.uid)])
            if len(manager):
               record.is_any_manager = 1


class recruitment_plan_shifts(models.Model):
    _name = 'recruitment.plan.shifts'

    shift_id = fields.Many2one('recruitment.plan', 'Shifts')
    shift = fields.Many2one('resource.calendar','Shift', required=True)
    department_id = fields.Many2one('hr.department', 'Department', required=True)
    job = fields.Many2one('hr.job', 'Job', required=True)
    nature = fields.Selection([('Production', 'Production'),
                               ('Non-production', 'Non-production')],
                                'Nature')
    production_line = fields.Many2one('mrp.workcenter', 'Production Line')
    employees_act_nu = fields.Integer('Actual NO.',compute="get_actual_number",store=True)
    employees_nu_needed = fields.Integer('Needs', required=True)
    employees_deficient_nu = fields.Integer('Deficient', compute='_compute_employees_deficient_nu', readonly=True)

    @api.onchange('department_id')
    def check_show(self):
        print "service"
        bill_ids = []
        job_ids = []
        domain = [['id', '=', False]]
        job_domain = [['id', '=', False]]
        bill_of_mat = self.env['resource.calendar'].search([('hr_department','=',self.department_id.id)])
        hr_job = self.env['hr.job'].search([('department_id','=',self.department_id.id)])
        for bill in bill_of_mat:
            bill_ids.append(bill.id)
        # domain = [('id', 'in', bill_ids)]
        for job in hr_job:
            job_ids.append(job.id)
        domain = [('id', 'in', bill_ids)]
        job_domain = [('id', 'in', job_ids)]
        return {'domain': {'shift': domain,'job': job_domain}}

    @api.multi
    @api.depends('job')
    def get_actual_number(self):
        employee_number=[]
        for rec in self:
            employee_obj=self.env['hr.employee'].search([('job_id','=',rec.job.id)])
            if employee_obj:
                for emp_id in employee_obj:
                    employee_number.append(emp_id.id)
            rec.employees_act_nu = len(employee_number)
            del employee_number[:]


    @api.constrains('employees_nu_needed')
    def _compute_employees_deficient_nu(self):
        for shift in self:
            if shift.employees_nu_needed < shift.employees_act_nu:
                raise exceptions.ValidationError("Number of employees Needed must be greater than Actual Number of employees")
            else:
                shift.employees_deficient_nu = shift.employees_nu_needed - shift.employees_act_nu



class recruitment_job_application(models.Model):
    _name = 'recruitment.job.application'
    _inherit = ['mail.thread']

    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id, readonly="true")
    department_id = fields.Many2one('hr.department', 'Department', required=True)
    nature_of_department = fields.Selection([('Production', 'Production'),
                                             ('Non-production', 'Non-production')],
                                              'Type')
    needed_no = fields.Integer('Total Needed', required=True)
    job = fields.Many2one('hr.job', 'Job', required=True)
    date = fields.Date('Date', default=datetime.today())
    shifts = fields.One2many('recruitment.job.application.shifts', 'shift_id', 'Shifts', store=True)
    state = fields.Selection([('Draft', 'Draft'),
                              ('Human resource Approval', 'Human resource Approval'),
                              ('CEO Approval','CEO Approval'),
                              ('Refused','Refused')],
                               'Status', readonly=True)
    start_button = fields.Boolean('Start', readonly=True)
    total = fields.Integer(string="Total", required=False,compute='get_total_needs', )

    # @api.onchange('department_id')
    # def _fill_lines(self):
    #     plan = self.env['recruitment.plan'].search([('department_id', '=', self.department_id.id)])
    #     result = []
    #     for line in plan:
    #         for shift in line.shifts:
    #             data = {
    #                 'shift_id': self.id,
    #                 'shift': shift.shift,
    #                 'job': shift.job,
    #                 'nature': shift.nature,
    #                 'production_line': shift.production_line,
    #                 'employees_act_nu': shift.employees_act_nu,
    #                 'employees_nu_needed': shift.employees_nu_needed,
    #                 'employees_deficient_nu': shift.employees_deficient_nu,
    #             }
    #             result.append(data)
    #     self.shifts = result

    @api.model
    def create(self, vals):
        res = super(recruitment_job_application, self).create(vals)
        res.start_button = True
        return res

    @api.multi
    def action_save_recruitment_request(self):
        groups = self.env['res.groups'].search([('category_id.name', '=', 'Human Resources'),('name','=','Employee')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Recruitment Job Application created and needs HR approval"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        self.write({'state': 'Draft'})
        self.start_button = False

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'Human resource Approval'})
        groups = self.env['res.groups'].search([('name','=','CEO')])
        recipient_partners = []
        for group in groups:
            for recipient in group.users:
                if recipient.partner_id.id not in recipient_partners:
                    recipient_partners.append(recipient.partner_id.id)
        if len(recipient_partners):
            body = "New Recruitment Plan approved from HR and needs CEO approval"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
        return {}

    @api.multi
    def action_ceo_approval(self):
        self.sudo().write({'state': 'CEO Approval'})
        body = "New Recruitment Plan approved from CEO"
        self.sudo().message_post(body=body, message_type='email')
        self.sudo().job.no_of_recruitment = self.needed_no
        for rec in self:
            for line in rec.shifts:
                job=self.env['hr.job']
                job_obj=job.search([('name','=',line.job.name)])
                if job_obj:
                    print "found",job_obj.no_of_recruitment
                    job_obj.write({
                        'no_of_recruitment': job_obj.no_of_recruitment + line.employees_deficient_nu,
                    })
                else:
                    print "new job"
                    job.create({
                        'name':line.job.name,
                        'department_id':line.department_id.id,
                        'no_of_recruitment':line.employees_deficient_nu,
                    })
        return {}

    @api.multi
    def action_refused(self):
        self.write({'state': 'Refused'})
        body = "New Recruitment Plan refused"
        self.message_post(body=body, message_type='email')
        return {}

    @api.depends('shifts.employees_deficient_nu')
    def get_total_needs(self):
        for rec in self:
            subtotal = 0.0
            for line in rec.shifts:
                subtotal += line.employees_nu_needed
            rec.update({
                'total':subtotal,
            })

    @api.constrains('needed_no')
    def compare_total_needed(self):
        if self.needed_no < self.total:
           raise exceptions.ValidationError("Total needed must be greater than needs !")
        else:
            return {}
class recruitment_job_application_shifts(models.Model):
    _name = 'recruitment.job.application.shifts'

    shift_id = fields.Many2one('recruitment.job.application', 'Shifts', store="true")
    shift = fields.Many2one('resource.calendar','Shift', required=True)
    department_id = fields.Many2one('hr.department', 'Department', required=True)
    job = fields.Many2one('hr.job', 'Job', store="true",)
    nature = fields.Selection([('Production', 'Production'),
                               ('Non-production', 'Non-production')],
                                'Nature', store="true",)
    production_line = fields.Many2one('mrp.workcenter', 'Production Line')
    employees_act_nu = fields.Integer('Actual NO.',compute="get_actual_number",store=True)
    employees_nu_needed = fields.Integer('Needs', required=True)
    employees_deficient_nu = fields.Integer('Deficient', compute='_compute_employees_deficient_nu', readonly=True)

    @api.onchange('department_id')
    def check_show(self):
        print "service"
        bill_ids = []
        job_ids = []
        domain = [['id', '=', False]]
        job_domain = [['id', '=', False]]
        bill_of_mat = self.env['resource.calendar'].search([('hr_department','=',self.department_id.id)])
        hr_job = self.env['hr.job'].search([('department_id','=',self.department_id.id)])
        for bill in bill_of_mat:
            bill_ids.append(bill.id)
        # domain = [('id', 'in', bill_ids)]
        for job in hr_job:
            job_ids.append(job.id)
        domain = [('id', 'in', bill_ids)]
        job_domain = [('id', 'in', job_ids)]
        return {'domain': {'shift': domain,'job': job_domain}}

    @api.multi
    @api.depends('job')
    def get_actual_number(self):
        employee_number=[]
        for rec in self:
            employee_obj=self.env['hr.employee'].search([('job_id','=',rec.job.id)])
            if employee_obj:
                for emp_id in employee_obj:
                    employee_number.append(emp_id.id)
            rec.employees_act_nu = len(employee_number)
            del employee_number[:]

    @api.constrains('employees_nu_needed')
    def _compute_employees_deficient_nu(self):
        for shift in self:
            if shift.employees_nu_needed < shift.employees_act_nu:
                raise exceptions.ValidationError("Number of employees Needed must be greater than Actual Number of employees")
            else:
                shift.employees_deficient_nu = shift.employees_nu_needed - shift.employees_act_nu



class recruitment_application(models.Model):
    _inherit = 'hr.applicant'

    country = fields.Many2one('res.country', string='Country')
    address = fields.Text(string="Address")
    dep_manager = fields.Many2one('hr.employee', related="department_id.manager_id", string="dep_manager")
    is_department_manager = fields.Boolean(compute='_compute_is_dep_manager')
    city = fields.Many2one('res.country.state', string="City")
    birth_date = fields.Date(string="Date of Birth")
    today = fields.Date(string="today", default=datetime.today())
    age = fields.Char(string="Age", compute='_compute_age', readonly="true")
    birth_place = fields.Char(string="Place of Birth")
    nationality = fields.Many2one('res.country', string='Nationality')
    religion = fields.Selection([('Muslim', 'Muslim'),
                                 ('Christian', 'Christian'),
                                 ('Other', 'Other')],
                                  'Religion')
    id_no = fields.Char(string="ID")
    id_place = fields.Many2one('res.country.state',string="Place of Issue ID")
    id_date = fields.Date(string="Date of Issue ID")
    id_expiry= fields.Date(string="Expiry Date of ID")
    id_military_no = fields.Char(string="Number of Military ID")
    military_status = fields.Selection([('Exemption', 'Exemption'),
                                        ('Complete the service Military', 'Complete the service Military'),
                                        ('Postponed', 'Postponed'),
                                        ('Currently serving', 'Currently serving'),
                                        ('Does not apply', 'Does not apply')],
                                         'Military Status')
    service_degree = fields.Char(string="service Degree")
    marital_status = fields.Selection([('Single', 'Single'),
                                        ('Married', 'Married'),
                                        ('Devoured', 'Devoured'),
                                        ('widower', 'widower')],
                                         'Marital Status')
    children = fields.Integer(string="Number of children")
    educational_qualification = fields.Char(string="Educational Qualification")
    relatives_working = fields.Boolean(string="You have relatives working in company?")
    relatives = fields.Many2many('hr.employee',string="Name of relatives")
    languages = fields.One2many('recruitment.application.language', 'language_id', 'Languages')
    experiences = fields.One2many('recruitment.application.experience', 'experience_id', 'Experiences')
    years = fields.Integer('Years', compute='_compute_external_experience')
    months = fields.Integer('Months', compute='_compute_external_experience')
    external_experience = fields.Char('External Experience', compute='_compute_external_experience', readonly="true")
    references = fields.One2many('recruitment.application.reference.persons', 'reference_id', 'References Persons')
    state = fields.Selection([('Draft', 'Draft'),
                              ('interview', 'interview'),
                              ('first_test', 'First Test'),
                              ('probation_period', 'Probation Period'),
                              ('second_test', 'Second Test'),
                              ('Create Employee','Create Employee'),
                              ('CEO Approval','CEO Approval'),
                              ('contract_signed','Contract Signed'),
                              # ('Department manager Evaluation', 'Department manager Evaluation'),
                              ('Refused','Refused')],
                               'Status', readonly=True)
    start_button = fields.Boolean('Start', readonly=True)
    result = fields.Float(string="Result",)

    @api.multi
    def action_department_manager_approve(self):
        if self.state == 'Draft':
            self.write({'state':'interview'})
        elif self.state == 'interview':
            first=self.env['recruitment.applicant.first.test'].search([('applicant','=',self.id)])[-1]
            if first:
                for rec in first:
                    if rec.total_score > self.result :
                        self.write({'state':'first_test'})
                    else:
                        raise UserError(_('The percentage is less than result'))
            else:
                raise UserError(_('You must take the first exam'))
        elif self.state == 'first_test':
            self.write({'state':'probation_period'})
        else:
            second=self.env['recruitment.applicant.second.test'].search([('applicant','=',self.id)])[-1]
            if second:
                for rec in second:
                    if rec.total_score > self.result :
                        self.write({'state':'second_test'})
                    else:
                        raise UserError(_('The percentage is less than result'))

    @api.multi
    def _compute_is_dep_manager(self):
        for record in self:
            if(record.department_id.manager_id.user_id.id == self.env.uid):
                record.is_department_manager = 1
            else:
                record.is_department_manager = 0

    # compute external experience
    @api.depends('experiences')
    def _compute_external_experience(self):
        for experience in self.experiences:
            if experience._from and experience._to:
                fmt = '%Y-%m-%d'
                _from = datetime.strptime(experience._from, fmt) #start date
                _to = datetime.strptime(experience._to, fmt)   #end date

                duration = relativedelta(_to, _from)
                self.years = self.years + duration.years
                self.months = self.months + duration.months
                if self.months > 12:
                    self.months = self.months - 12
                    self.years = self.years + 1
                    self.external_experience = "%s years ,%s months" %(self.years,self.months)
                else:
                    self.external_experience = "%s years ,%s months" %(self.years,self.months)


    def action_first_test_result(self):
        return {
            'domain': "[('applicant','in','active_id')]",
            'name': ('First test result'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'recruitment.applicant.first.test',
            'type': 'ir.actions.act_window',
            'target': 'self',
        }

    def action_second_test_result(self):
        return {
            'domain': "[('applicant','in','active_id')]",
            'name': ('Second test result'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'recruitment.applicant.second.test',
            'type': 'ir.actions.act_window',
            'target': 'self',
        }


    @api.depends('birth_date')
    def _compute_age(self):
        for rec in self:
            if rec.birth_date:
                fmt = '%Y-%m-%d'
                birth_date = datetime.strptime(rec.birth_date, fmt) #start date
                today = datetime.now()   #end date

                duration = relativedelta(today, birth_date)
                current_age = "%s years ,%s months" %(duration.years,duration.months)
                rec.age = current_age
            else:
                rec.age = 0


    @api.multi
    def action_hr_save_application(self):
        if self.department_id:
            if self.dep_manager:
                manager_id = self.dep_manager
                recipient_partners = []
                if manager_id.id not in recipient_partners:
                    recipient_partners.append(manager_id.id)
                if len(recipient_partners):
                    body = "New Application created and needs HR approval"
                    self.sudo().message_post(body=body, partner_ids=recipient_partners, message_type='email')
                    self.sudo().write({'state': 'Draft'})
                    self.sudo().start_button = True
            else:
                raise UserError(_('Set Manager to selected Department.'))

    @api.multi
    def action_department_manager_evaluation(self):
        for rec in self:
            self.browse(rec.id).sudo().write({'state': 'Department manager Evaluation'})
            groups = rec.sudo().env['res.groups'].search([('name','=','CEO')])
            recipient_partners = []
            for group in groups:
                for recipient in group.users:
                    if recipient.partner_id.id not in recipient_partners:
                        recipient_partners.append(recipient.partner_id.id)
            if len(recipient_partners):
                body = "New Application approved from Department manager"
                rec.sudo().message_post(body=body, partner_ids=recipient_partners, message_type='email')
        return {}

    @api.multi
    def action_ceo_approval(self):
        self.write({'state': 'CEO Approval'})
        body = "New Application approved from CEO"
        self.message_post(body=body, message_type='email')
        return {}

    @api.multi
    def create_employee_from_applicant(self):
        """ Create an hr.employee from the hr.applicants """
        employee = False
        for applicant in self:
            address_id = contact_name = False
            if applicant.partner_id:
                address_id = applicant.partner_id.address_get(['contact'])['contact']
                contact_name = applicant.partner_id.name_get()[0][1]
            if applicant.job_id and (applicant.partner_name or contact_name):
                applicant.job_id.write({'no_of_hired_employee': applicant.job_id.no_of_hired_employee + 1})
                employee = self.sudo().env['hr.employee'].create({'name': applicant.partner_name or contact_name,
                                               'job_id': applicant.job_id.id,
                                               'address_home_id': address_id,
                                               'department_id': applicant.department_id.id or False,
                                               'company': applicant.company_id.id,
                                               'address_id': applicant.company_id and applicant.company_id.partner_id and applicant.company_id.partner_id.id or False,
                                               'work_email': applicant.department_id and applicant.department_id.company_id and applicant.department_id.company_id.email or False,
                                               'work_phone': applicant.department_id and applicant.department_id.company_id and applicant.department_id.company_id.phone or False,
                                               'country': applicant.country.id,
                                               'address': applicant.address,
                                               'city': applicant.city.id,
                                               'birth_date': applicant.birth_date,
                                               'age': applicant.age,
                                               'birth_place': applicant.birth_place,
                                               'nationality': applicant.nationality.id,
                                               'religion': applicant.religion,
                                               'id_no': applicant.id_no,
                                               'id_place': applicant.id_place.id,
                                               'id_date': applicant.id_date,
                                               'id_expiry': applicant.id_expiry,
                                               'id_military_no': applicant.id_military_no,
                                               'military_status': applicant.military_status,
                                               'service_degree': applicant.service_degree,
                                               'marital_status': applicant.marital_status,
                                               'children': applicant.children,
                                               'educational_qualification': applicant.educational_qualification,
                                               'relatives_working': applicant.relatives_working,
                                               'external_experience': applicant.external_experience,
                                               'external_years': applicant.years,
                                               'external_months': applicant.months})

                applicant.write({'emp_id': employee.id})
                employee.relatives = (applicant.relatives.ids)
                applicant.job_id.message_post(
                    body=_('New Employee %s Hired') % applicant.partner_name if applicant.partner_name else applicant.name,
                    subtype="hr_recruitment.mt_job_applicant_hired")
                employee._broadcast_welcome()
                self.sudo().write({'state': 'Create Employee'})
            else:
                raise UserError(_('You must define an Applied Job and a Contact Name for this applicant.'))

        employee_action = self.sudo().env.ref('hr.open_view_employee_list')
        dict_act_window = employee_action.read([])[0]
        if employee:
            dict_act_window['res_id'] = employee.id
        dict_act_window['view_mode'] = 'form,tree'
        return dict_act_window

    @api.multi
    def archive_applicant(self):
        self.write({'active': False})
        self.write({'state': 'Refused'})
        body = "New Recruitment Plan refused"
        self.message_post(body=body, message_type='email')
        return {}


class recruitment_application_language(models.Model):
    _name = 'recruitment.application.language'

    language_id = fields.Many2one('hr.applicant', 'Languages')
    name = fields.Char('Language Name')
    speaking = fields.Selection([('good', 'good'),
                                 ('very good', 'very good'),
                                 ('Fluent', 'Fluent')],
                                  'Speaking')
    reading = fields.Selection([('good', 'good'),
                                 ('very good', 'very good'),
                                 ('Fluent', 'Fluent')],
                                  'Reading')
    writing = fields.Selection([('good', 'good'),
                                 ('very good', 'very good'),
                                 ('Fluent', 'Fluent')],
                                  'Writing')


class recruitment_application_experience(models.Model):
    _name = 'recruitment.application.experience'

    experience_id = fields.Many2one('hr.applicant', 'Experiences')
    name = fields.Char('Company Name')
    job = fields.Char('Job')
    _from = fields.Date('From')
    _to = fields.Date('To')
    duration = fields.Char(compute='_compute_experience_duration', readonly="true"'Experience Duration')
    total_salary = fields.Float('Total Salary')
    leave_reason = fields.Text('Reason of leave your job ')


    @api.depends('_from','_to','duration')
    def _compute_experience_duration(self):
        for rec in self:
            if rec._from and rec._to:
                fmt = '%Y-%m-%d'
                _from = datetime.strptime(rec._from, fmt) #start date
                _to = datetime.strptime(rec._to, fmt)   #end date

                duration = relativedelta(_to, _from)
                current_duration = "%s years ,%s months" %(duration.years,duration.months)
                rec.duration = current_duration
            else:
                rec.duration = 0


class recruitment_application_reference_persons(models.Model):
    _name = 'recruitment.application.reference.persons'

    reference_id = fields.Many2one('hr.applicant', 'Reference Persons')
    name = fields.Char('Name')
    job = fields.Char('Job')
    address = fields.Text('Address')

class recruitment_applicant_first_test(models.Model):
    _name = 'recruitment.applicant.first.test'
    _inherit = ['mail.thread']

    applicant = fields.Many2one('hr.applicant', 'Name of applicant', default=lambda self: self.env.context.get('active_id', False), readonly="true",required=True)
    job = fields.Many2one('hr.job', 'Job', related="applicant.job_id", readonly="true")
    birth_date = fields.Date(string="Date of Birth", related="applicant.birth_date", readonly="true")
    department = fields.Many2one('hr.department', related="applicant.department_id", string="Department", readonly="true")
    exam_result = fields.Float('The result of Exam')
    total_score = fields.Float(string="Total Score",  required=False,compute='get_total_score' )
    date = fields.Date(string="Date", required=False,default=fields.date.today(), )
    exam_validity = fields.Selection([('Valid', 'Valid'),
                                      ('not Valid', 'not Valid')],
                                       'Exam Result')
    validity_percentage = fields.Float('Validity Estimated percentage')
    committee_members = fields.One2many('first.test.committee.members', 'committee_member_id', 'Committee Members')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Notification Sent', 'Notification Sent'),
                              ('hr_approval', 'Hr Approval'),
                              ('hr_refused', 'Hr Refused')],
                               'Status', readonly=True, default='Draft')

    @api.depends('committee_members.score')
    def get_total_score(self):
        for rec in self:
            subtotal = 0.0
            i=0
            for line in rec.committee_members:
                subtotal += line.score
                i+=1
            rec.update({
                'total_score':subtotal/ (i if i else 1),
            })
    @api.multi
    def action_send_notification(self):
        recipient_partners = []
        for members in self.committee_members:
            for member in members.emp_name:
                if member.id not in recipient_partners:
                    recipient_partners.append(member.id)
        if len(recipient_partners):
            body = "Please approve on this recruitment applicant test"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
            self.write({'state': 'Notification Sent'})
        else:
            raise exceptions.ValidationError("There is no Employees to send notification")
        return {}

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'hr_approval'})
    @api.multi
    def action_hr_refused(self):
        self.write({'state': 'hr_refused'})

class first_test_committee_members(models.Model):
    _name = 'first.test.committee.members'

    committee_member_id = fields.Many2one('recruitment.applicant.first.test', 'Committee Members')
    emp_name = fields.Many2one('hr.employee', string="Employee Name")
    job = fields.Many2one('hr.job', 'Job', related="emp_name.job_id", readonly="true")
    department_id = fields.Many2one('hr.department', 'department')
    approval = fields.Boolean(string="Approval")
    score = fields.Float(string="Score",  required=False, )
    is_committee_employee = fields.Boolean(compute='_compute_is_committee_employee')

    @api.onchange('department_id')
    def check_show(self):
        print "job_id"
        job_ids = []
        job_domain = [['id', '=', False]]
        hr_job = self.env['hr.employee'].search([('department_id','=',self.department_id.id)])
        for job in hr_job:
            job_ids.append(job.id)
        job_domain = [('id', 'in', job_ids)]
        return {'domain': {'emp_name': job_domain}}

    @api.multi
    def _compute_is_committee_employee(self):
        for record in self:
            if(record.emp_name.user_id.id == self.env.uid):
                record.is_committee_employee = 1
            else:
                record.is_committee_employee = 0


class recruitment_applicant_second_test(models.Model):
    _name = 'recruitment.applicant.second.test'
    _inherit = ['mail.thread']

    applicant = fields.Many2one('hr.applicant', 'Name of applicant', default=lambda self: self.env.context.get('active_id', False), readonly="true",required=True)
    job = fields.Many2one('hr.job', 'Job', related="applicant.job_id", readonly="true")
    birth_date = fields.Date(string="Date of Birth", related="applicant.birth_date", readonly="true")
    department = fields.Many2one('hr.department', related="applicant.department_id", string="Department", readonly="true")
    exam_result = fields.Float('The result of Exam')
    total_score = fields.Float(string="Total Score",  required=False,compute='get_total_score' )
    date = fields.Date(string="Date", required=False,default=fields.date.today(), )
    exam_validity = fields.Selection([('Valid', 'Valid'),
                                      ('not Valid', 'not Valid')],
                                       'Exam Result')
    validity_percentage = fields.Float('Validity Estimated percentage')
    committee_members = fields.One2many('second.test.committee.members', 'committee_member_id', 'Committee Members')
    state = fields.Selection([('Draft', 'Draft'),
                              ('Notification Sent', 'Notification Sent'),
                              ('hr_approval', 'Hr Approval'),
                              ('hr_refused', 'Hr Refused')],
                               'Status', readonly=True, default='Draft')

    @api.depends('committee_members.score')
    def get_total_score(self):
        for rec in self:
            subtotal = 0.0
            i=0
            for line in rec.committee_members:
                subtotal += line.score
                i+=1
            rec.update({
                'total_score':subtotal/ (i if i else 1),
            })
    @api.multi
    def action_send_notification(self):
        recipient_partners = []
        for members in self.committee_members:
            for member in members.emp_name:
                if member.id not in recipient_partners:
                    recipient_partners.append(member.id)
        if len(recipient_partners):
            body = "Please approve on this recruitment applicant test"
            self.message_post(body=body, partner_ids=recipient_partners, message_type='email')
            self.write({'state': 'Notification Sent'})
        else:
            raise exceptions.ValidationError("There is no Employees to send notification")
        return {}

    @api.multi
    def action_hr_approval(self):
        self.write({'state': 'hr_approval'})
    @api.multi
    def action_hr_refused(self):
        self.write({'state': 'hr_refused'})

class second_test_committee_members(models.Model):
    _name = 'second.test.committee.members'

    committee_member_id = fields.Many2one('recruitment.applicant.second.test', 'Committee Members')
    emp_name = fields.Many2one('hr.employee', string="Employee Name")
    job = fields.Many2one('hr.job', 'Job', related="emp_name.job_id", readonly="true")
    department_id = fields.Many2one('hr.department', 'department')
    approval = fields.Boolean(string="Approval")
    score = fields.Float(string="Score",  required=False, )
    is_committee_employee = fields.Boolean(compute='_compute_is_committee_employee')

    @api.onchange('department_id')
    def check_show(self):
        print "job_id"
        job_ids = []
        job_domain = [['id', '=', False]]
        hr_job = self.env['hr.employee'].search([('department_id','=',self.department_id.id)])
        for job in hr_job:
            job_ids.append(job.id)
        job_domain = [('id', 'in', job_ids)]
        return {'domain': {'emp_name': job_domain}}

    @api.multi
    def _compute_is_committee_employee(self):
        for record in self:
            if(record.emp_name.user_id.id == self.env.uid):
                record.is_committee_employee = 1
            else:
                record.is_committee_employee = 0



class recruitment_employee(models.Model):
    _inherit = 'hr.employee'

    insurance_no = fields.Char('Insurance Number')
    salaries = fields.One2many('hr.employee.salaries.history', 'salary_id', 'Salaries')
    deductions = fields.One2many('hr.employee.deductions.history', 'deduction_id', 'deductions')
    country = fields.Many2one('res.country', string='Country')
    address = fields.Text(string="Address")
    city = fields.Many2one('res.country.state', string="City")
    birth_date = fields.Date(string="Date of Birth")
    today = fields.Date(string="today", default=datetime.today())
    age = fields.Char(string="Age", compute='_compute_age', readonly="true")
    birth_place = fields.Char(string="Place of Birth")
    nationality = fields.Many2one('res.country', string='Nationality')
    religion = fields.Selection([('Muslim', 'Muslim'),
                                 ('Christian', 'Christian'),
                                 ('Other', 'Other')],
                                  'Religion')
    id_no = fields.Char(string="ID")
    id_place = fields.Many2one('res.country.state',string="Place of Issue ID")
    id_date = fields.Date(string="Date of Issue ID")
    id_expiry = fields.Date(string="Expiry Date of ID")
    id_military_no = fields.Char(string="Number of Military ID")
    military_status = fields.Selection([('Exemption', 'Exemption'),
                                        ('Complete the service Military', 'Complete the service Military'),
                                        ('Postponed', 'Postponed'),
                                        ('Currently serving', 'Currently serving'),
                                        ('Does not apply', 'Does not apply')],
                                         'Military Status')
    service_degree = fields.Char(string="service Degree")
    marital_status = fields.Selection([('Single', 'Single'),
                                        ('Married', 'Married'),
                                        ('Devoured', 'Devoured'),
                                        ('widower', 'widower')],
                                         'Marital Status')
    children = fields.Integer(string="Number of children")
    educational_qualification = fields.Char(string="Educational Qualification")
    relatives_working = fields.Boolean(string="You have relatives working in company?")
    relatives = fields.Many2many('hr.employee', 'employee_relatives', 'emp_1', 'emp_2', string="Name of relatives")

    insurance_nu = fields.Char('Insurance Number')
    restriction_date = fields.Date('تاريخ القيد')
    restriction_nu = fields.Char('رقم القيد')
    reply_nu = fields.Char('تاريخ الرد')
    license_nu = fields.Char('License Number of Driver')
    certificate_nu = fields.Char('Certificate Number of Measuring Skill')
    practicing_license_nu = fields.Char('رقم رخصة مزاولة النشاط‬')
    external_experience = fields.Char('External Experience', readonly="true")
    external_years = fields.Integer('Years')
    external_months = fields.Integer('Months')
    internal_Experience = fields.Char('Internal Experience', compute='_compute_internal_Experience', readonly="true")
    internal_years = fields.Integer('Internal Years', compute='_compute_internal_Experience')
    internal_months = fields.Integer('Internal Months', compute='_compute_internal_Experience')
    total_Experience = fields.Char('Total Experience', compute='_compute_internal_Experience', readonly="true")
    total_years = fields.Integer('Total Years', compute='_compute_internal_Experience')
    total_months = fields.Integer('Total Months', compute='_compute_internal_Experience')
    file = fields.Binary('Upload File')
    file_filename = fields.Char("File Name")
    join_date = fields.Date('Join date')
    join_code = fields.Char('Join Code', compute='_compute_internal_Experience', readonly="true")
    end_service = fields.Date(string="End of Service", readonly="true")
    company = fields.Many2one('res.company', 'Company')
    company_code = fields.Char('Company code', related="company.company_code", readonly="true")
    department_code = fields.Char('Department code', related="department_id.department_code", readonly="true")
    job_code = fields.Char('Job code', related="job_id.job_code", readonly="true")
    emp_nu = fields.Char('Employee Code', readonly="true")
    full_emp_nu = fields.Char('Employee Number', compute='_compute_full_emp_nu', readonly="true")

    field1 = fields.Boolean('طلب التوظيف')
    field2 = fields.Boolean('طلب الألتحاق بالوظيفة')
    field3 = fields.Boolean('نتيجة الأختبار')
    field4 = fields.Boolean('أخطار التعيين')
    field5 = fields.Boolean('بيانات العامل')
    field6 = fields.Boolean('صحيفة الحالة الأجتماعية')
    field7 = fields.Boolean('شهاده طبية تثبت صحة العامل')
    field8 = fields.Boolean('شهادة قياس المهارة - المهني')
    field9 = fields.Boolean('رخصة مزاولة المهنة - المهني')
    field10 = fields.Boolean('بطاقة وصف وظيفة')
    field11 = fields.Boolean('شهادة نهاية الخدمة بالعمل السابق')
    field12 = fields.Boolean('صحيفة الحالة الجنائية')
    field13 = fields.Boolean('شهادة خبره إن وجدت')
    field14 = fields.Boolean('صورة الرقم القومي')
    field15 = fields.Boolean('أصل المؤهل')
    field16 = fields.Boolean('أصل شهادة الميلاد أو مستخرج رسمي')
    field17 = fields.Boolean('أصل الخدمة العسكرية')
    field18 = fields.Boolean('9 صور فوتوغرافية 4*6')
    field19 = fields.Boolean('كعب عمل')
    field20 = fields.Boolean('أستمارة (1) تأمينات')
    field21 = fields.Boolean('عقد عمل')
    field22 = fields.Boolean('تدرج الأجور')
    field23 = fields.Boolean('بيان الأجازات')
    field24 = fields.Boolean('الأجازات والعطلات الرسمية')
    field25 = fields.Boolean('سجل الجزاءات')

    @api.model
    def create(self, vals):
        vals['emp_nu'] = self.env['ir.sequence'].sudo().next_by_code('recruit.employee')
        res = super(recruitment_employee, self).create(vals)
        return res

    @api.depends('birth_date')
    def _compute_age(self):
        for rec in self:
            if rec.birth_date:
                fmt = '%Y-%m-%d'
                birth_date = datetime.strptime(rec.birth_date, fmt) #start date
                today = datetime.now()   #end date

                duration = relativedelta(today, birth_date)
                current_age = "%s years ,%s months" %(duration.years,duration.months)
                rec.age = current_age
            else:
                rec.age = 0


    @api.depends('join_date')
    def _compute_internal_Experience(self):
        for rec in self:
            if rec.join_date:
                fmt = '%Y-%m-%d'
                join_date = datetime.strptime(rec.join_date, fmt) #start date
                join_code = parser.parse(rec.join_date).year #join code
                rec.join_code = join_code
                today = datetime.now()   #end date

                duration = relativedelta(today, join_date)
                rec.internal_years = duration.years
                rec.internal_months = duration.months
                internal_Experience = "%s years ,%s months" %(duration.years,duration.months)
                rec.internal_Experience = internal_Experience

                rec.total_years = rec.internal_years + rec.external_years
                rec.total_months = rec.internal_months + rec.external_months
                if rec.total_months > 12:
                    rec.total_months = rec.total_months - 12
                    rec.total_years = rec.total_years + 1
                total_Experience = "%s years ,%s months" %(rec.total_years,rec.total_months)
                rec.total_Experience = total_Experience
            else:
                rec.internal_Experience = 0
                rec.total_years = rec.external_years
                rec.total_months = rec.external_months
                if rec.total_months > 12:
                    rec.total_months = rec.total_months - 12
                    rec.total_years = rec.total_years + 1
                total_Experience = "%s years ,%s months" %(rec.total_years,rec.total_months)
                rec.total_Experience = total_Experience


    @api.depends('company_code','department_code','job_code','join_code','emp_nu')
    def _compute_full_emp_nu(self):
        for rec in self:
            full_emp_nu = [str(rec.company_code), str(rec.job_code) , str(rec.department_code) , str(rec.join_code) , str(rec.emp_nu)]
            # remove False values and convert to string again
            full_emp_nu =  filter(lambda a: a != "False", full_emp_nu)
            rec.full_emp_nu = " ".join(full_emp_nu)

class recruitment_employee_salaries_history(models.Model):
    _name = 'hr.employee.salaries.history'

    salary_id = fields.Many2one('hr.employee', 'Salaries')
    salary_before_increasing = fields.Float('Salary before increasing', readonly="true")
    increasing = fields.Float('Increasing', readonly="true")
    salary_after_increasing = fields.Float('Salary After Increasing', compute="_compute_Salary_after_increasing", readonly="true")
    date = fields.Date('Date', readonly="true" )
    notes = fields.Text('Notes', readonly="true" )


    @api.depends('salary_before_increasing','increasing')
    def _compute_Salary_after_increasing(self):
        for rec in self:
            rec.salary_after_increasing = rec.salary_before_increasing + rec.increasing


class recruitment_employee_deductions_history(models.Model):
    _name = 'hr.employee.deductions.history'

    deduction_id = fields.Many2one('hr.employee', 'Deductions')
    date = fields.Date('Date', readonly="true" )
    reason = fields.Text('Deduction Reason', readonly="true")
    amount = fields.Float('Amount', readonly="true")
    magnitude = fields.Char('Deduction Magnitude', readonly="true")


class recruitment_contract(models.Model):
    _inherit = 'hr.contract'

    house_type = fields.Selection([('Fixed', 'Fixed'),
                                   ('Percentage', 'Percentage')],
                                    'House Allowance Type')
    house_amount = fields.Float('House Allowance Amount')
    food_type = fields.Selection([('Fixed', 'Fixed'),
                                  ('Percentage', 'Percentage')],
                                   'Food Allowance Type')
    food_amount = fields.Float('Food Allowance Amount')
    transportation_type = fields.Selection([('Fixed', 'Fixed'),
                                            ('Percentage', 'Percentage')],
                                             'Transportation Allowance Type')
    transportation_amount = fields.Float('Transportation Allowance Amount')
    car_type = fields.Selection([('Fixed', 'Fixed'),
                                 ('Percentage', 'Percentage')],
                                  'Car Allowance Type')
    car_amount = fields.Float('Car Allowance Amount')
    production_incentive_type = fields.Selection([('Fixed', 'Fixed'),
                                                  ('Percentage', 'Percentage')],
                                                   'Production incentive Type')
    production_incentive_amount = fields.Float('Production incentive Amount')
    uom = fields.Many2one('product.uom', string='UOM')
    monthly_profit = fields.Float('نصيب الأرباح الشهري')

    insurance_type = fields.Selection([('Fixed', 'Fixed'),
                                       ('Percentage', 'Percentage')],
                                        'Insurance Type')
    insurance_amount = fields.Float('Insurance Amount')
    income_tax_type = fields.Selection([('Fixed', 'Fixed'),
                                   ('Percentage', 'Percentage')],
                                    'نوع ضريبة الدخل')
    income_tax_amount = fields.Float('حجم ضريبة الدخل')
    term_insurance_type = fields.Selection([('Fixed', 'Fixed'),
                                   ('Percentage', 'Percentage')],
                                    'نوع مده التأمينات')
    term_insurance_amount = fields.Float('حجم التأمينات')
    amount = fields.Float(_('Amount'))
    quantity = fields.Float(_('Quantity'))


class recruitment_company(models.Model):
    _inherit = 'res.company'

    company_code = fields.Char('Company code')


class recruitment_department(models.Model):
    _inherit = 'hr.department'

    department_code = fields.Char('Department code')

class recruitment_job(models.Model):
    _inherit = 'hr.job'

    job_code = fields.Char('Job code')







