# -*- coding: utf-8 -*-
from openerp.exceptions import UserError, ValidationError
from openerp import models, fields, api , exceptions, _
from datetime import datetime, time
from dateutil.relativedelta import relativedelta
import dateutil.parser as parser
class ResConfigSettings(models.TransientModel):
    _name='hr.recruitment.config'
    _inherit = ['res.config.settings']
    default_result = fields.Float(string="Result",default_model="hr.applicant" )
