from odoo import models, fields, api
import datetime
from werkzeug.routing import ValidationError
import time

class plan_order(models.Model):
    _name = 'ceo.plan.order'
    _inherit = ['mail.thread']

    state = fields.Selection([
            ('draft', 'Draft'),
            ('ceo', 'Ceo'),
            ('sent', 'Request Sent'),
            ],default='draft')
    plan_id=fields.One2many('ceo.plan.order.line','order_id')
    plan_order=fields.One2many('ceo.nano.order.line','order_line')
    department=fields.One2many('ceo.nano.selection.line','nano_select')

    request_plane = fields.Char(string="Request Plane", readonly=True, )
    purchase_id = fields.Many2one(comodel_name="sale.order", string="Sales Quotation", required=False, )
    date_quotation = fields.Date(string="quotation Date", required=False, )

    date_request = fields.Date(string="Request Date", required=False,default=fields.Date.today )
    date_response = fields.Date(string="Response Date", required=False,)

    priority = fields.Selection(string="Priority", selection=[('normal', 'normal'), ('important', 'important'),('urgent', 'urgent'), ], required=False, )
    describtion1 = fields.Char(string="Describtion", required=False, )
    totaldays = fields.Integer(string="Toatal Days", required=False,compute='_amount_all' )

    product = fields.Many2one(comodel_name="product.product", string="product", compute="product_request", related='plan_order.product', )
    describtion=fields.Char(string='Describtion',related='plan_order.describtion',)
    quantity = fields.Float(string="order qty",  required=False,related='plan_order.quantity', )
    unit = fields.Many2one(comodel_name="product.uom", string="unit", required=False,related='plan_order.unit', )
    data=time.strftime("%Y-%m-%d")

    admin = fields.Selection(string="admin", selection=[('draft', 'close'), ('approve', 'open') ], default='draft', )
    bool_logistic = fields.Boolean(string="logistic",)
    bool_custom = fields.Boolean(string="custom",)
    bool_production = fields.Boolean(string="production",)
    bool_stock_user = fields.Boolean(string="stock user",)
    bool_stock_manager = fields.Boolean(string="stock manager",)
    bool_equipment = fields.Boolean(string="equipment manager",)

    @api.onchange('purchase_id')
    def _onchange_user_id(self):
        self.date_quotation = self.purchase_id.date_order

    # @api.constrains('date_request')
    # def _user_id(self):
    #     if self.date_request > self.date_response:
    #         raise ValidationError('Response date should be greater than request date!')

    @api.depends('plan_id.days')
    def _amount_all(self):
        for rec in self:
            totaldays= 0.0
            for line in rec.plan_id:
                totaldays += line.days
            rec.update({
                'totaldays':totaldays,
            })

    @api.multi
    def concept_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })
    @api.multi
    def ceo_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'ceo',
        })
        sale=self.env['sale.order']
        sale_line=self.env['sale.order.line']
        sale_get=sale.search([('name', '=',self.purchase_id.name)])
        if sale_get:
           print ">>>>>>>>>>>>>>>> ",self.purchase_id.name
           for ceo in self.plan_order:
               print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",ceo.ceo_price
               print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",ceo.product.name
               for record in sale_get.order_line:
                   if record.product_id.id == ceo.product.id:
                        print ">>>>>>>>>>>>>>>>>",record.name
                        record.write({
                            'price_unit':ceo.ceo_price,
                            'plan_price_unit':ceo.ceo_price,
                        })
           return {
                'type': 'ir.actions.act_window',
                'name': 'sale order',
                'res_model': 'sale.order',
                'res_id': sale_get.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
               }

    @api.multi
    def add_department_user(self):
        data=self.env['ceo.plan.order.line'].search([]).browse([0])
        user = self.env['res.users'].browse(self.env.uid)
        if user.has_group('nano_plan.module_category_logistic_management'):
            if self.bool_logistic == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'logistic',})
                self.bool_logistic=True
        elif user.has_group('nano_plan.module_category_custom_management'):
            if self.bool_custom == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'custom',})
                self.bool_custom=True

        elif user.has_group('nano_plan.module_category_production_management'):
            if self.bool_production == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'production',})
                self.bool_production=True
        elif user.has_group('stock.group_stock_manager'):
            if self.bool_stock_user == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'Inventory',})
                self.bool_stock_user=True

        elif user.has_group('stock.group_stock_user'):
            if self.bool_stock_manager == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'Inventory',})
                self.bool_stock_manager=True
        elif user.has_group('maintenance.group_equipment_manager'):
            if self.bool_equipment == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'Employees',})
                self.bool_equipment=True

    @api.multi
    def done_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'sent',
        })

class nano_selection_line(models.Model):
    _name = 'ceo.nano.selection.line'
    nano_select=fields.Many2one('ceo.plan.order')
    department = fields.Selection(string="department", selection=[('Inventory', 'Inventory'), ('production', 'production'),
                                                                  ('logistic', 'logistic'), ('custom', 'custom'),
                                                                  ('purchase', 'purchase'),
                                                                  ('Employees', 'Equipment'),], required=False, )

class nanoSales(models.Model):
    _name = 'ceo.plan.order.line'
    order_id=fields.Many2one('ceo.plan.order')
    department = fields.Selection(string="department", selection=[('Inventory', 'Inventory'),
                                                                  ('production', 'production'),('logistic', 'logistic'),
                                                                  ('custom', 'custom'),('purchase', 'purchase'),
                                                                  ('Employees', 'Equipment'), ],
                                  required=False, readonly=True)
    date_from = fields.Date(string="Date From", required=False, )
    date_to = fields.Date(string="Date To", required=False, )
    days = fields.Integer(string="Days", required=False, )

    #compute different between two dates in days
    @api.onchange('date_from', 'date_to','days')
    def calculate_date(self):
        if self.date_from and self.date_to:
            d1=datetime.datetime.strptime(str(self.date_from),'%Y-%m-%d')
            d2=datetime.datetime.strptime(str(self.date_to),'%Y-%m-%d')
            d3=d2-d1
            self.days=str(d3.days)


class nanoSalesorder(models.Model):
    _name = 'ceo.nano.order.line'
    order_line=fields.Many2one('ceo.plan.order')
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    packaging = fields.Many2one(comodel_name="product.product", string="package", required=False, )
    unit = fields.Many2one(comodel_name="product.uom", string="unit", required=False, )
    describtion=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,default=1 )
    price_unit = fields.Float(string="Price",  required=False, )
    ceo_price = fields.Float(string="ceo price",  required=False, )