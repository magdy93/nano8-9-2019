from odoo import api, fields, models
class PaymentTerm(models.Model):
    _inherit = 'account.payment.term'
    is_default = fields.Boolean(string="Is Default", )
