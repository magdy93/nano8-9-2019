{
    'name' : 'Nano plan order test',
    'descriptioin' : 'This module inherted from sales module to edit in General Information part',
    'auther' : 'UNSS-Esraa',
    'depends' : ['base','mail','sale'],
    'data' : [
        # 'res_partner.xml',
        # 'payment_term.xml',
        'template.xml',
        'ceo_plan.xml',
        'ir.model.access.csv',
        'security.xml',
        'sequence.xml',
        'scedule.xml',
        # 'user_groups.xml',
    ]
}