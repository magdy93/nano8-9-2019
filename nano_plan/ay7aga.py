from odoo import models, fields, api, _
import datetime
from werkzeug.routing import ValidationError
import time
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT


class plan_order(models.Model):
    _name = 'plan.order'
    _inherit = ['mail.thread']

    state = fields.Selection([
            ('draft', 'Draft'),
            ('sent', 'Request Sent'),
            ('confirm', 'confirm'),
            ],default='draft')
    plan_id=fields.One2many('plan.order.line','order_id')
    plan_order=fields.One2many('nano.order.line','order_line')
    department=fields.One2many('nano.selection.line','nano_select')
    package_plan_line = fields.One2many(comodel_name="package.plan.order.line", inverse_name="package_line", string="packaging", )
    bill_material_line = fields.One2many(comodel_name="sales.bill.of.material", inverse_name="bill_order_line", string="Bill of Materiale", )
    custom_plan_line = fields.One2many(comodel_name="sales.custom.order", inverse_name="custom_order_line", string="Custom", )
    logistic_plan_line = fields.One2many(comodel_name="sales.logistic.order", inverse_name="logistic_order_line", string="logistic", )


    request_plane = fields.Char(string="Request Plane", readonly=True, )
    purchase_id = fields.Many2one(comodel_name="sale.order", string="Sales Quotation", required=False, )
    date_quotation = fields.Date(string="quotation Date", required=False, )

    date_request = fields.Date(string="Request Date", required=False,default=fields.Date.today )
    date_response = fields.Date(string="Response Date", required=False,)
    # default = fields.Date(string="default Date", required=False,default=fields.Date.today )

    priority = fields.Selection(string="Priority", selection=[('normal', 'normal'), ('important', 'important'),('urgent', 'urgent'), ], required=False, )
    describtion1 = fields.Char(string="Describtion", required=False, )
    totaldays = fields.Integer(string="Toatal Days", required=False,compute='_amount_all' )

    subtotal_package = fields.Float(string="subtotal package",store=True, required=False,compute='package_total_price' )
    subtotal_bill = fields.Float(string="Total Price Bill Of Material",store=True, required=False,compute='get_all_total_price' )
    subtotal_custom = fields.Float(string="Total Price Custom", store=True,required=False,compute='get_all_total_price' )
    subtotal_logistic = fields.Float(string="Total Price Logistic", store=True,required=False,compute='get_all_total_price' )
    total_data = fields.Float(string="Total Price", required=False,store=True,compute='get_all_price' )

    product = fields.Many2one(comodel_name="product.product", string="product", compute="product_request", related='plan_order.product', )
    describtion=fields.Char(string='Describtion',related='plan_order.describtion',)
    quantity = fields.Float(string="order qty",  required=False,related='plan_order.quantity', )
    unit = fields.Many2one(comodel_name="product.uom", string="unit", required=False,related='plan_order.unit', )
    data=time.strftime("%Y-%m-%d")

    admin = fields.Selection(string="admin", selection=[('draft', 'close'), ('approve', 'open') ], default='draft', )
    bool_logistic = fields.Boolean(string="logistic",)
    bool_custom = fields.Boolean(string="custom",)
    bool_production = fields.Boolean(string="production",)
    bool_stock_user = fields.Boolean(string="stock user",)
    bool_stock_manager = fields.Boolean(string="stock manager",)
    bool_equipment = fields.Boolean(string="equipment manager",)

    loading_port = fields.Char(string='Loading Port')
    discharge_port = fields.Char(string='discharge Port')
    address = fields.Char(string='address')
    from_street2 = fields.Char(string='Street 2')
    zip_code = fields.Char(string='ZIP', change_default=True)
    city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    country = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    wh_incoming_date = fields.Date(string='WH Incoming')


    # @api.model
    # def create(self, vals):
    #     department = self.env['nano.selection.line']
    #     res=super(plan_order, self).create(vals)
    #     department.create({
    #         'nano_select':res.id,
    #          vals['department']:'warehouse',
    #     })
    #     return res
    @api.constrains('department')
    def get_move_line(self):
        for rec in self:
            for line in rec.department:
                 self.message_post(body=" user '{0}' changed the departments our new department is '{1}' ".format(self.env.user.name,line.department),subtype='mt_comment',
                                              subject='test',message_type='notification')

    @api.one
    @api.constrains('plan_id')
    def on_change_confirm_id(self):
        product_ids=[]
        for line in self.plan_id:
            if line.department not in product_ids:
                product_ids.append(line.department)
            else:
                raise ValidationError('you should add only one line !')




    @api.multi
    def user_default_date(self):
        plans = self.search([('date_response','<',time.strftime("%Y-%m-%d"))])
        print plans
        for rec in plans:
            print rec.department
            cus_count=0
            count=0
            for department in rec.department:
               cus_count+=1
               custom=[department.department]
               print "<<<<<<<<<<<<<<<<<<<<<<<<< custom2 ",len(custom)
             # print "<<<<<<<<<<<<<<<<<<<<<<<<< custom length ",cus_count
            for pl in rec.plan_id :
                   count+=1
                   plan=[pl.department]
                   print "<<<<<<<<<<<<<<<<<<<<<<<<< plan  ",plan
                   print "<<<<<<<<<<<<<<<<<<<<<<<<< plan  ",count
                   if count < cus_count:
                        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>."
                        groups = self.env['res.groups'].search([('name', '=', 'ceo')])
                        recipient_partners = []
                        for group in groups:
                            for recipient in group.users:
                                if recipient.partner_id.id not in recipient_partners:
                                    recipient_partners.append(recipient.partner_id.id)
                        if len(recipient_partners):
                            rec.message_post(body='there is department did not response on the sales planning ',subtype='mt_comment',
                                              subject='test',partner_ids=recipient_partners,message_type='notification')
                            # print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> new date",self.data
                        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> new"

                   elif count > cus_count:
                       print "greater"
                   else:
                       print "equal"
        #if rec.date_response < rec.data:
            # for record in self:
            # print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> new2"


    # get confirm line automatically
    def _prepare_invoice_line_from_po_line(self, line):
        # invoice_line = self.env['temporary.permit.confirm']
        data = {
            'purchase_line_id': line.id,
            'quantity': line.product_uom_qty,
            'describtion':line.product_id.name,
            'product': line.product_id.id,
            'unit': line.product_uom.id,
        }
        return data

    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if not self.purchase_id:
            return {}
        # if not self.partner_id:
        #     self.partner_id = self.purchase_id.partner_id.id

        new_lines = self.env['nano.order.line']
        # nsearch=new_lines.search([])
        # nsearch.unlink()
        for line in self.purchase_id.order_line - self.plan_order.mapped('purchase_line_id'):
            data = self._prepare_invoice_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines = new_line

        self.plan_order = new_lines
        # self.purchase_id = False
        return {}


    @api.model
    def create(self, vals):
        vals['request_plane'] = self.env['ir.sequence'].next_by_code('req.seq')
        return super(plan_order, self).create(vals)


    @api.onchange('purchase_id')
    def _onchange_user_id(self):
        self.date_quotation = self.purchase_id.date_order

    @api.constrains('date_request')
    def _user_id(self):
        if self.date_request > self.date_response:
            raise ValidationError('Response date should be greater than request date!')

    @api.depends('plan_id.days')
    def _amount_all(self):
        for rec in self:
            totaldays= 0.0
            for line in rec.plan_id:
                totaldays += line.days
            rec.update({
                'totaldays':totaldays,
            })

    @api.depends('package_plan_line.price_unit')
    def package_total_price(self):
        for rec in self:
            subtotal_package= 0.0
            for line in rec.package_plan_line:
                subtotal_package += line.price_unit
            rec.update({
                'subtotal_package':subtotal_package,
            })

    @api.depends('subtotal_bill','subtotal_custom','subtotal_logistic','subtotal_package')
    def get_all_price(self):
        for rec in self:
            rec.total_data = rec.subtotal_bill + rec.subtotal_custom + rec.subtotal_logistic + rec.subtotal_package

    @api.depends('bill_material_line.subtotal_price','custom_plan_line.subtotal_price','logistic_plan_line.subtotal_price',)
    def get_all_total_price(self):
        for rec in self:
            subtotal_bill= 0.0
            subtotal_custom= 0.0
            subtotal_logistic= 0.0
            for bill in rec.bill_material_line:
                subtotal_bill += bill.subtotal_price
            for custom in rec.custom_plan_line:
                subtotal_custom += custom.subtotal_price
            for logistic in rec.logistic_plan_line:
                subtotal_logistic += logistic.subtotal_price
            rec.update({
                'subtotal_bill':subtotal_bill,
                'subtotal_custom':subtotal_custom,
                'subtotal_logistic':subtotal_logistic,
            })

    @api.multi
    def concept_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })
        self.message_post(body='state draft',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def confirm_progressbar(self):
        # self.ensure_one()
        self.write({
            'state': 'confirm',
        })
        self.message_post(body='state confirmed',subtype='mt_comment',
                                              subject='test',message_type='notification')
        self.purchase_id.plan_state ='confirm'
    # @api.multi
    # def confirm_progressbar(self):
    #     self.ensure_one()
    #     self.write({
    #         'state': 'confirm',
    #     })
    #     #add days to current date
    #     currnt_date =datetime.date.today()
    #     end_date = currnt_date + datetime.timedelta(days=self.totaldays)
    #     print ">>>>>>>>>>>>>>>>>>>>> ",end_date
    #
    #     pack_order=self.env['nano.order.line']
    #     i=0.0
    #     for pack_line in self.plan_order:
    #         i+=1
    #         subtotal_dev=self.subtotal_package/i
    #     print ">>>>>>>>>>>>>>>>>>>>> ",subtotal_dev
    #
    #     sale=self.env['sale.order']
    #     sale_line=self.env['sale.order.line']
    #     sale_get=sale.search([('name', '=',self.purchase_id.name)])
    #     if sale_get:
    #        print ">>>>>>>>>>>>>>>> ",self.purchase_id.name
    #        sale_get.write({
    #            'end_plan_date':end_date,
    #        })
    #        for line in self.plan_order:
    #            # print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",ceo.ceo_price
    #            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",line.product.name
    #            for record in sale_get.order_line:
    #                if record.product_id.id == line.product.id:
    #                     print ">>>>>>>>>>>>>>>>>",record.name
    #                     record.write({'price_unit':line.subtotal_price+subtotal_dev,})
    #                     record.write({'plan_price_unit':line.subtotal_price+subtotal_dev,})
    #     self.message_post(body='state confirmed',subtype='mt_comment',
    #                                           subject='test',message_type='notification')


    @api.multi
    def add_department_user(self):
        data=self.env['plan.order.line'].search([]).browse([0])
        user = self.env['res.users'].browse(self.env.uid)
        if user.has_group('nano_plan.module_category_logistic_management'):
            if self.bool_logistic == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'logistic',})
                self.bool_logistic=True
        elif user.has_group('nano_plan.module_category_custom_management'):
            if self.bool_custom == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'custom',})
                self.bool_custom=True

        elif user.has_group('nano_plan.module_category_production_management'):
            if self.bool_production == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'production',})
                self.bool_production=True
        elif user.has_group('stock.group_stock_manager'):
            if self.bool_stock_user == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'Inventory',})
                self.bool_stock_user=True

        elif user.has_group('stock.group_stock_user'):
            if self.bool_stock_manager == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'Inventory',})
                self.bool_stock_manager=True
        elif user.has_group('maintenance.group_equipment_manager'):
            if self.bool_equipment == True:
               return True
            else:
                for rec in data:
                    rec.create({
                        'order_id':self.id,
                        'department':'Employees',})
                self.bool_equipment=True



    # @api.multi
    # def add_department_admin(self):
    #     print "admin"
    #     data=self.env['plan.order.line'].search([]).browse([0])
    #     user = self.env['res.users'].browse(self.env.uid)
    #     if user.has_group('nano_plan.module_category_logistic_management'):
    #         print 'This user is a logistic'
    #         for rec in data:
    #             rec.create({
    #                 'order_id':self.id,
    #                 'department':'logistic',})
        # for x in self:
        #     for rec in x.department:
        #         print "<<<<<<<<<<<<<<<<<<<<<<<<< custom2 ",self.create_uid.name
        #         print "<<<<<<<<<<<<<<<<<<<<<<<<< custom2 ",self.create_uid.id
        #         user = self.env['res.users'].browse(self.env.uid)
        #         if user.has_group('nano_plan.module_category_logistic_management'):
        #             print 'This user is a salesman'
        #         else:
        #             rec.department.department='warehouse'
        #             print 'This user is not a salesman'


    @api.multi
    def done_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'sent',
        })
        # for rec in self.department:
        #     custom=rec.department
        #     print "<<<<<<<<<<<<<<<<<<<<<<<<< custom ",custom

        #sending message to followers

        # for record in self:
        #     users=self.env.ref('account.group_account_user').users
        #     recipient_partners = []
        #     for user in users:
        #         recipient_partners.append((4,user.partner_id.id))
        #     record.message_post(body='magdy salah ',force_send=True,subtype='mt_comment',
        #                           subject='test',partner_ids=recipient_partners,message_type='notification')

        #sending message to more than group of users
        for record in self:
            for rec in self.department:
                custom=[rec.department]
                print "<<<<<<<<<<<<<<<<<<<<<<<<< custom2 ",custom
                groups = self.env['res.groups'].search(['|',('name', 'in',custom),('category_id.name', 'in', custom)])
                recipient_partners = []
                for group in groups:
                    for recipient in group.users:
                        if recipient.partner_id.id not in recipient_partners:
                            recipient_partners.append(recipient.partner_id.id)
                if len(recipient_partners):
                    record.message_post(body='request sent',force_send=True,subtype='mt_comment',
                                      subject='test',partner_ids=recipient_partners,message_type='notification')
        self.purchase_id.plan_state ='sent'

class nano_selection_line(models.Model):
    _name = 'nano.selection.line'
    nano_select=fields.Many2one('plan.order')
    department = fields.Selection(string="department", selection=[('Inventory', 'Inventory'), ('production', 'production'),
                                                                  ('logistic', 'logistic'), ('custom', 'custom'),
                                                                  ('purchase', 'purchase'),
                                                                  ('Employees', 'Equipment'),], required=False, )

class nanoSales(models.Model):
    _name = 'plan.order.line'
    order_id=fields.Many2one('plan.order')
    department = fields.Selection(string="department", selection=[('Inventory', 'Inventory'),
                                                                  ('production', 'production'),('logistic', 'logistic'),
                                                                  ('custom', 'custom'),('purchase', 'purchase'),
                                                                  ('Employees', 'Equipment'), ],
                                  required=False, readonly=True)
    date_from = fields.Date(string="Date From", required=False, )
    date_to = fields.Date(string="Date To", required=False, )
    days = fields.Integer(string="Days", required=False, )

    #compute different between two dates in days
    @api.onchange('date_from', 'date_to','days')
    def calculate_date(self):
        if self.date_from and self.date_to:
            d1=datetime.datetime.strptime(str(self.date_from),'%Y-%m-%d')
            d2=datetime.datetime.strptime(str(self.date_to),'%Y-%m-%d')
            d3=d2-d1
            self.days=str(d3.days)

class packageplanline(models.Model):
    _name = 'package.plan.order.line'
    package_line=fields.Many2one('plan.order')
    packaging = fields.Many2one(comodel_name="product.product", string="package", required=False, )
    quantity = fields.Float(string="Quantity",  required=False,default=1 )
    price_unit = fields.Float(string="Unit price",  required=False, )

class SalesorderBillOfMaterial(models.Model):
    _name = 'sales.bill.of.material'
    bill_order_line=fields.Many2one('plan.order')
    product_id = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    unit = fields.Many2one(comodel_name="product.uom", string="unit", required=False, )
    description=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,default=1 )
    margin_price = fields.Float(string="Margin %",  required=False,)
    price_unit = fields.Float(string="price", store=True,required=False,compute="get_total_price")
    subtotal_price = fields.Float(string="subtotal price",store=True,required=False,compute="get_subtotal_price")
    @api.multi
    @api.depends('price_unit','margin_price')
    def get_subtotal_price(self):
        for rec in self:
            rec.subtotal_price = (rec.price_unit * rec.margin_price)/100

    @api.multi
    @api.depends('quantity','product_id')
    def get_total_price(self):
        for rec in self:
            rec.price_unit = rec.quantity * rec.product_id.lst_price

    @api.onchange('product_id')
    def _get_product_description(self):
        for record in self:
            record.description = self.product_id.name
            record.unit = self.product_id.uom_id.id
class SalesorderCustom(models.Model):
    _name = 'sales.custom.order'
    custom_order_line=fields.Many2one('plan.order')
    product_id = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    unit = fields.Many2one(comodel_name="product.uom", string="unit", required=False, )
    description=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,default=1 )
    margin_price = fields.Float(string="Margin %",  required=False,)
    price_unit = fields.Float(string="price", store=True,required=False,compute="get_total_price")
    subtotal_price = fields.Float(string="subtotal price",store=True,required=False,compute="get_subtotal_price")
    @api.multi
    @api.depends('price_unit','margin_price')
    def get_subtotal_price(self):
        for rec in self:
            rec.subtotal_price = (rec.price_unit * rec.margin_price)/100

    @api.multi
    @api.depends('quantity','product_id')
    def get_total_price(self):
        for rec in self:
            rec.price_unit = rec.quantity * rec.product_id.lst_price

    @api.onchange('product_id')
    def _get_product_description(self):
        for record in self:
            record.description = self.product_id.name
            record.unit = self.product_id.uom_id.id
class SalesorderLogistic(models.Model):
    _name = 'sales.logistic.order'
    logistic_order_line=fields.Many2one('plan.order')
    product_id = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    unit = fields.Many2one(comodel_name="product.uom", string="unit", required=False, )
    description=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,default=1 )
    margin_price = fields.Float(string="Margin %",  required=False,)
    price_unit = fields.Float(string="price", store=True,required=False,compute="get_total_price")
    subtotal_price = fields.Float(string="subtotal price",store=True,required=False,compute="get_subtotal_price")
    @api.multi
    @api.depends('price_unit','margin_price')
    def get_subtotal_price(self):
        for rec in self:
            rec.subtotal_price = (rec.price_unit * rec.margin_price)/100

    @api.multi
    @api.depends('quantity','product_id')
    def get_total_price(self):
        for rec in self:
            rec.price_unit = rec.quantity * rec.product_id.lst_price

    @api.onchange('product_id')
    def _get_product_description(self):
        for record in self:
            record.description = self.product_id.name
            record.unit = self.product_id.uom_id.id

class nanoSalesorder(models.Model):
    _name = 'nano.order.line'
    order_line=fields.Many2one('plan.order')
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    # packaging = fields.Many2one(comodel_name="product.product", string="package", required=False, )
    unit = fields.Many2one(comodel_name="product.uom", string="unit", required=False, )
    describtion=fields.Char(string='Description')
    quantity = fields.Float(string="order qty",  required=False,default=1 )
    price_unit = fields.Float(string="price",  required=False,)
    # logistic_price = fields.Float(string="logistic price",  required=False,)
    # logistic_price2 = fields.Float(string="logistic price",  required=False,)
    subtotal_price = fields.Float(string="subtotal price",  required=False,compute="add_logistic_price")

    # @api.one
    # @api.constrains('logistic_price')
    # def get_logistic_constrain(self):
    #     if self.logistic_price == self.logistic_price2:
    #        print "changed lineeeeeeeeee",self.logistic_price
    #     else:
    #         # for rec in self:
    #         #     for line in rec.department:
    #         self.order_line.message_post(body="logistic price is changed by %s" %self.env.user.name ,subtype='mt_comment',
    #                                           subject='test',message_type='notification')
    #     self.logistic_price2 = self.logistic_price

    @api.multi
    # @api.depends('price_unit','logistic_price')
    @api.depends('price_unit','quantity')
    def add_logistic_price(self):
       for record in self:
           # record.subtotal_price=record.price_unit+record.logistic_price
           record.subtotal_price=record.price_unit * record.quantity


    # linked with get confirmed line automatically in temporary.permit
    purchase_line_id = fields.Many2one('sale.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
    purchase_id = fields.Many2one('sale.order', related='purchase_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')

    def _set_additional_fields(self,invoice):
        pass

class packageline(models.Model):
    _name = 'package.order.line'
    package_line=fields.Many2one('sale.order')
    packaging = fields.Many2one(comodel_name="product.product", string="package", required=False, )
    quantity = fields.Float(string="Quantity",  required=False,default=1 )
    price_unit = fields.Float(string="Unit price",  required=False, )
    subtotal_package_price = fields.Float(string="package price",  required=False,compute='get_package_price', )

    quantity_hand = fields.Float(string='Quantity On Hand', compute='compute_quantity_on_hand', readonly=True)#related='packaging.qty_available'
    reserved_quantity = fields.Float(string='Reserved Quantity', compute='compute_reserved_quantity')

    @api.multi
    @api.depends('packaging')
    def compute_quantity_on_hand(self):
        for quan in self:
             quan.quantity_hand=quan.packaging.qty_available


    @api.multi
    def compute_reserved_quantity(self):
          so = self.env['sale.order']
          for record in self:
            sale_order_objects = so.search([
                ('state', 'not in', ['sale','cancel']),
            ])
            count_of_quantity = 0
            if len(sale_order_objects) > 0:
                for sale_order in sale_order_objects:
                    for line in sale_order.package_order_line:
                        if line.packaging.id == record.packaging.id:
                            count_of_quantity += line.quantity
            record.update({'reserved_quantity': count_of_quantity})


    # @api.onchange('packaging')
    # def get_package_product_plan(self):
    #     for line in self:
    #         line.price_unit=line.packaging.lst_price

    @api.depends('quantity','price_unit')
    def get_package_price(self):
        for line in self:
            line.subtotal_package_price=line.price_unit * line.quantity


class nano_sale_order_line(models.Model):
    _inherit = 'sale.order.line'

    plan_price_unit = fields.Float(string="Plan Unit price",  required=False, )


    # @api.constrains('price_unit', 'plan_price_unit')
    # def _package_price(self):
    #         if self.plan_price_unit > self.price_unit:
    #             print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> greater than"
    #             raise ValidationError('Response date should be greater than request date!')

    # @api.multi
    # # @api.constrains('plan_price_unit')
    # @api.onchange('price_unit','plan_price_unit')
    # def plan_package_price(self):
    #     # for line in self.order_line:
    #     if self.plan_price_unit > self.price_unit:
    #        # raise ValidationError('you should add only one line !')
    #        # return 0
    #        raise Warning('Price unit must be greater than plan price unit !')


    # @api.multi
    # def write(self, vals):
    #     # res = super(nano_sale_order_line, self).write(vals)
    #        if self.plan_price_unit > self.price_unit:
    #           raise ValidationError('Price unit must be greater than plan price unit !')
    #     # return res

    # @api.multi
    # def write(self, values):
    #     lines = False
    #     changed_lines = False
    #
    #     if self.plan_price_unit < self.price_unit:
    #           raise ValidationError('Price unit must be greater than plan price unit !')
    #     else:
    #         print">>>>>>>>>>>>>>>>>>>>>>>>> less than"
    #
    #     if 'product_uom_qty' in values:
    #         precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
    #         lines = self.filtered(
    #             lambda r: r.state == 'sale' and float_compare(r.product_uom_qty, values['product_uom_qty'], precision_digits=precision) == -1)
    #         changed_lines = self.filtered(
    #             lambda r: r.state == 'sale' and float_compare(r.product_uom_qty, values['product_uom_qty'], precision_digits=precision) != 0)
    #         if changed_lines:
    #             orders = self.mapped('order_id')
    #             for order in orders:
    #                 order_lines = changed_lines.filtered(lambda x: x.order_id == order)
    #                 msg = ""
    #                 if any([values['product_uom_qty'] < x.product_uom_qty for x in order_lines]):
    #                     msg += "<b>" + _('The ordered quantity has been decreased. Do not forget to take it into account on your invoices and delivery orders.') + '</b>'
    #                 msg += "<ul>"
    #                 for line in order_lines:
    #                     msg += "<li> %s:" % (line.product_id.display_name,)
    #                     msg += "<br/>" + _("Ordered Quantity") + ": %s -> %s <br/>" % (line.product_uom_qty, float(values['product_uom_qty']),)
    #                     if line.product_id.type in ('consu', 'product'):
    #                         msg += _("Delivered Quantity") + ": %s <br/>" % (line.qty_delivered,)
    #                     msg += _("Invoiced Quantity") + ": %s <br/>" % (line.qty_invoiced,)
    #                 msg += "</ul>"
    #                 order.message_post(body=msg)
    #     result = super(nano_sale_order_line, self).write(values)
    #     if lines:
    #         lines._action_procurement_create()
    #     return result

class nano_sale_flow(models.Model):
    _inherit = 'sale.order'

    ceo_approve = fields.Boolean(string="ceo approve",)
    package_order_line = fields.One2many(comodel_name="package.order.line", inverse_name="package_line", string="packaging", )
    rfq_number = fields.Char(string="RFQ Number", required=False, )
    plan_state = fields.Char(string="Plan", required=False, )
    # city = fields.Char(string="city", required=False, )
    # address = fields.Char(string="Address", required=False, )
    # zip_code = fields.Char(string="Zip", required=False, )
    # country = fields.Char(string="country", required=False, )
    # loading_port = fields.Char(string="loading port", required=False, )
    # discharge_port = fields.Char(string="discharge port", required=False, )

    end_plan_date = fields.Date(string="End Date", required=False, )
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('plan', 'Plan'),
        ('ceo', 'Ceo Approve'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')
    @api.one
    @api.constrains('order_line')
    def _package_price(self):
        for loan in self.order_line:
            if loan.plan_price_unit > loan.price_unit:
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> greater than"
                raise ValidationError('plan price greater than price unit!')
            else:
                print"oooooooooooooooooooooooooooooooo"

    # @api.multi
    # def write(self, vals):
    #     # res = super(nano_sale_order_line, self).write(vals)
    #     for rec in self.order_line:
    #        if rec.plan_price_unit > rec.price_unit:
    #           raise ValidationError('Price unit must be greater than plan price unit !')
    #     return {}

    @api.multi
    def action_plan_management(self):
        plan_obj = self.env['plan.order'].search([('purchase_id', '=', self.id)])
        if plan_obj:
           return {
                    'type': 'ir.actions.act_window',
                    'name': 'plan management',
                    'res_model': 'plan.order',
                    'res_id': plan_obj.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                }
    @api.multi
    def plan_management_flow(self):
        # self.ensure_one()

        partner_obj = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
        account_obj = self.env['account.invoice'].search([('partner_id', '=', self.partner_id.id)])
        outstanding = 0
        for obj in account_obj:
            outstanding += obj.residual_signed
        if partner_obj.check_credit == True:
            if partner_obj.credit_limit >= (self.amount_total + outstanding):
                self.state = 'plan'
            else:
                exceeded_amount = partner_obj.credit_limit - outstanding
                self.state = 'draft'
                print "NO"
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>", partner_obj.name, ">>>>>>>>>>>>>>>", partner_obj.credit_limit, ">>>>>>>>>>", outstanding, ">>>>>>>>>", self.name, ">>>>>>>>>", partner_obj.on_hold, ">>>>>>>>>", exceeded_amount
                raise ValidationError(_(
                    'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
                                          partner_obj.name, partner_obj.credit_limit, outstanding, self.name,
                                          partner_obj.on_hold, exceeded_amount))
        elif partner_obj.check_credit == False:
            if partner_obj.on_hold == True:
                print "On Hold"
                exceeded_amount = partner_obj.credit_limit - outstanding
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>", partner_obj.name, ">>>>>>>>>>>>>>>", partner_obj.credit_limit, ">>>>>>>>>>", outstanding, ">>>>>>>>>", self.name, ">>>>>>>>>", partner_obj.on_hold, ">>>>>>>>>", exceeded_amount
                raise ValidationError(_(
                    'Sorry, This partner credit limit has Exceeded or On Hold, You cannot proceed with the quotation.\n \n Name: %s \n Credit Limit: %s \n Total Receivable: %s \n Current Quotation: %s\n Put On Hold: %s\n Exceeded Amount: %s.') % (
                                          partner_obj.name, partner_obj.credit_limit, outstanding, self.name,
                                          partner_obj.on_hold, exceeded_amount))
            else:
                self.state = 'plan'
        else:
            self.state = 'plan'

        plan_obj = self.env['plan.order'].search([('purchase_id', '=', self.id)])
        package_line=self.env['package.plan.order.line']
        if plan_obj:
           return {
                    'type': 'ir.actions.act_window',
                    'name': 'plan management',
                    'res_model': 'plan.order',
                    'res_id': plan_obj.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                }
        else:
            if self.type=='local' and self.carrying == 'company':
                plan=self.env['plan.order']
                plan_line=self.env['nano.order.line']
                plan_get=plan.search([('purchase_id','=',self.id)])
                if plan_get:
                    return {
                        'type': 'ir.actions.act_window',
                        'name': 'plan management',
                        'res_model': 'plan.order',
                        'res_id': plan_get.id,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'self',
                    }
                else:
                    for record in self:
                      plan_id=plan.create({
                            'purchase_id':record.id,
                            'date_response':fields.Datetime.now(),
                            'loading_port':record.loading_port,
                            'discharge_port':record.discharge_port,
                            'address':record.address,
                            'from_street2':record.from_street2,
                            'zip_code':record.zip_code,
                            'city':record.city,
                            'from_state_id':record.from_state_id.id,
                            'country':record.country.id,
                            'to_street':record.to_street,
                            'to_street2':record.to_street2,
                            'to_zip':record.to_zip,
                            'to_city':record.to_city,
                            'to_state_id':record.to_state_id.id,
                            'to_country_id':record.to_country_id.id,
                            'vessel_loading_date':record.vessel_loading_date,
                            'vessel_unloading_date':record.vessel_unloading_date,
                            'wh_incoming_date':record.wh_incoming_date,
                        })
                      for line in record.order_line:
                          plan_line.create({
                              'order_line':plan_id.id,
                              'product':line.product_id.id,
                              'describtion':line.name,
                              'quantity':line.product_uom_qty,
                              'price_unit':line.price_unit,
                          })
                          if line.package_tag_ids:
                              for pack in line.package_tag_ids.package_ids:
                                  package_line.create({
                                      'package_line':plan_id.id,
                                      'packaging':pack.product_id.id,
                                      'quantity':pack.quantity,
                                      'price_unit':pack.unit_price,
                                  })
                      self.write({
                          'plan_state':'draft'
                      })
                      return {
                            'type': 'ir.actions.act_window',
                            'name': 'plan management',
                            'res_model': 'plan.order',
                            'res_id': plan_id.id,
                            'view_type': 'form',
                            'view_mode': 'form',
                            'target': 'self',
                        }
            elif self.type=='local' and self.carrying == 'vendor':
                plan=self.env['plan.order']
                plan_line=self.env['nano.order.line']
                for rec in self:
                  for line in rec.order_line:
                      print ">>>>>>>>>>>>>>>>>>>>>>",line.product_uom_qty
                      print ">>>>>>>>>>>>>>>>>>>>>>",line.quantity_hand
                      if line.quantity_hand < line.product_uom_qty :
                            plan_vendor=plan.create({
                                    'purchase_id':rec.id,
                                    'date_response':fields.Datetime.now(),
                                    'loading_port':rec.loading_port,
                                    'discharge_port':rec.discharge_port,
                                    'address':rec.address,
                                    'from_street2':rec.from_street2,
                                    'zip_code':rec.zip_code,
                                    'city':rec.city,
                                    'from_state_id':rec.from_state_id.id,
                                    'country':rec.country.id,
                                    'to_street':rec.to_street,
                                    'to_street2':rec.to_street2,
                                    'to_zip':rec.to_zip,
                                    'to_city':rec.to_city,
                                    'to_state_id':rec.to_state_id.id,
                                    'to_country_id':rec.to_country_id.id,
                                    'vessel_loading_date':rec.vessel_loading_date,
                                    'vessel_unloading_date':rec.vessel_unloading_date,
                                    'wh_incoming_date':rec.wh_incoming_date,
                                })
                            #if you want to create plan with only line needed to put plan remove this loop
                            for record in rec.order_line:
                                plan_line.create({
                                  'order_line':plan_vendor.id,
                                  'product':record.product_id.id,
                                  'describtion':record.name,
                                  'quantity':record.product_uom_qty,
                                  'price_unit':record.price_unit,
                                 })
                                if record.package_tag_ids:
                                   for pack in record.package_tag_ids.package_ids:
                                      package_line.create({
                                          'package_line':plan_vendor.id,
                                          'packaging':pack.product_id.id,
                                          'quantity':pack.quantity,
                                          'price_unit':pack.unit_price,
                                      })
                            self.write({
                                          'plan_state':'draft'
                                       })
                            return {
                                    'type': 'ir.actions.act_window',
                                    'name': 'plan management',
                                    'res_model': 'plan.order',
                                    'res_id': plan_vendor.id,
                                    'view_type': 'form',
                                    'view_mode': 'form',
                                    'target': 'self',
                                }
                      elif line.quantity_hand > line.product_uom_qty :
                          print "<<<<<<<<<<< Quantity on hand is greater than line quantity>>>>>>>>>>>>>>>>>"

            elif self.type=='outside':
                plan_out=self.env['plan.order']
                plan_out_line=self.env['nano.order.line']
                for rec in self:
                    for line in rec.order_line:
                        print ">>>>>>>>>>>>>>>>>>>>>>",line.product_uom_qty
                        print ">>>>>>>>>>>>>>>>>>>>>>",line.quantity_hand
                        plan_vendor=plan_out.create({
                                'purchase_id':rec.id,
                                'date_response':fields.Datetime.now(),
                                'loading_port':rec.loading_port,
                                'discharge_port':rec.discharge_port,
                                'address':rec.address,
                                'from_street2':rec.from_street2,
                                'zip_code':rec.zip_code,
                                'city':rec.city,
                                'from_state_id':rec.from_state_id.id,
                                'country':rec.country.id,
                                'to_street':rec.to_street,
                                'to_street2':rec.to_street2,
                                'to_zip':rec.to_zip,
                                'to_city':rec.to_city,
                                'to_state_id':rec.to_state_id.id,
                                'to_country_id':rec.to_country_id.id,
                                'vessel_loading_date':rec.vessel_loading_date,
                                'vessel_unloading_date':rec.vessel_unloading_date,
                                'wh_incoming_date':rec.wh_incoming_date,
                            })
                        #if you want to create plan with only line needed to put plan remove this loop
                        for record in rec.order_line:
                            plan_out_line.create({
                              'order_line':plan_vendor.id,
                              'product':record.product_id.id,
                              'describtion':record.name,
                              'quantity':record.product_uom_qty,
                              'price_unit':record.price_unit,
                            })
                            if record.package_tag_ids:
                              for pack in record.package_tag_ids.package_ids:
                                  package_line.create({
                                      'package_line':plan_vendor.id,
                                      'packaging':pack.product_id.id,
                                      'quantity':pack.quantity,
                                      'price_unit':pack.unit_price,
                                  })
                        self.write({
                                    'plan_state':'draft'
                                   })
                        return {
                                'type': 'ir.actions.act_window',
                                'name': 'plan management',
                                'res_model': 'plan.order',
                                'res_id': plan_vendor.id,
                                'view_type': 'form',
                                'view_mode': 'form',
                                'target': 'self',
                            }

            else:
                print "<<<<<<<<<<<<<<<<<type out side>>>>>>>>>>>>>>>>"



    @api.multi
    def ceo_approve_plan(self):
        self.ensure_one()
        self.write({
            'state': 'ceo',
        })
        ceo_plan=self.env['ceo.plan.order']
        ceo_plan_line=self.env['ceo.nano.order.line']
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        for record in self:
            ceo_head=ceo_plan.create({
                'purchase_id': self.id,
            })
            for i in record.order_line:
                ceo_plan_line.create({
                      'order_line':ceo_head.id,
                      'product':i.product_id.id,
                      'describtion':i.name,
                      'price_unit':i.price_unit,
                      'quantity':i.product_uom_qty,
                      # 'unit':i.product_uom_qty.id,
                })
        return {
                'type': 'ir.actions.act_window',
                'name': 'ceo plan',
                'res_model': 'ceo.plan.order',
                'res_id': ceo_head.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
               }


    @api.multi
    def action_confirm(self):
        if self.state=='sent':
            groups = self.env['res.groups'].search([('name', '=', 'Credit Department')])
            print ">>>>>>>>>>>>>>>>>>>>>>>>>",groups
            recipient_partners = []
            for group in groups:
                for recipient in group.users:
                    if recipient.partner_id.id not in recipient_partners:
                        recipient_partners.append(recipient.partner_id.id)
            print ">>>>>>>>>>>>>>>", recipient_partners
            if len(recipient_partners):
                self.message_post(
                    body='The state is quotation sent !',
                    force_send=True,
                    subtype='mt_comment',
                    subject='Notification',
                    partner_ids=recipient_partners,
                    message_type='notification'
                )
                print self.message_post

        for order in self:
            #ghareeb
            # if order.pricelist_id:
            #     price_list_obj = self.env['res.partner'].search([('id', '=', order.partner_id.id)])
            #     if price_list_obj.sales_pricelist_ids:
            #         for list in price_list_obj.sales_pricelist_ids:
            #             if order.pricelist_id.id == list.pricelist_id.id:
            #                 if list.valid_date:
            #                     if fields.Date.from_string(fields.Date.today()) >= fields.Date.from_string(list.valid_date):
            #                         raise ValidationError(_('Sorry . You Must Check The Pricelist Expiry Date !'))
            #                 if order.incoterm.id != list.incoterm_id.id:
            #                     raise ValidationError(_('Sorry . The Incoterms Should Be The Same In Pricelist!'))
            # if order.pricelist_id.pricelist_checked == True:
            #     raise ValidationError(_('Sorry .This Pricelist Is A Default Pricelist!'))
            #ghareeb
            order.state = 'sale'
            order.confirmation_date = fields.Datetime.now()
            if self.env.context.get('send_email'):
                self.force_quotation_send()
            order.order_line._action_procurement_create()
        if self.env['ir.values'].get_default('sale.config.settings', 'auto_done_setting'):
            self.action_done()

        if self.type == 'outside':
            logistic = self.env['nano.logistic']
            logistic_line = self.env['logistic.order.line']
            for emp in self:
                acc_id=logistic.create({
                    'sale_order':emp.id,
                    'loading_port':emp.loading_port,
                    'discharge_port':emp.discharge_port,
                    'address':emp.address,
                    'from_street2':emp.from_street2,
                    'zip_code':emp.zip_code,
                    'city':emp.city,
                    'from_state_id':emp.from_state_id.id,
                    'country':emp.country.id,
                    'to_street':emp.to_street,
                    'to_street2':emp.to_street2,
                    'to_zip':emp.to_zip,
                    'to_city':emp.to_city,
                    'to_state_id':emp.to_state_id.id,
                    'to_country_id':emp.to_country_id.id,
                    'vessel_loading_date':emp.vessel_loading_date,
                    'vessel_unloading_date':emp.vessel_unloading_date,
                    'wh_incoming_date':emp.wh_incoming_date,
                    'incoterms': emp.incoterm.id,
                    'custom_status': 'draft',
                    'status': 'assigned',
                })
                for line in emp.order_line:
                    logistic_line.create({
                    'order_line':acc_id.id,
                    'package':line.package_tag_ids.id,
                    'product':line.product_id.id,
                    'describtion':line.name,
                    'quantity':line.product_uom_qty,
                    'price':line.price_unit,
                    'tax':line.tax_id.id,
                    'subtotal':line.price_subtotal,
                })

        elif self.type == 'local' and self.carrying == 'company':
            logistic = self.env['nano.logistic']
            logistic_line = self.env['logistic.order.line']
            for emp in self:
                acc_id=logistic.create({
                    'sale_order':emp.id,
                    'incoterms': emp.incoterm.id,
                })
                for line in emp.order_line:
                    logistic_line.create({
                    'order_line':acc_id.id,
                    'product':line.product_id.id,
                    'describtion':line.name,
                    'quantity':line.product_uom_qty,
                    'price':line.price_unit,
                    'tax':line.tax_id.id,
                    'subtotal':line.price_subtotal,
                })


        return True