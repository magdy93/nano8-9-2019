# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero



class getJournal(models.Model):
    _inherit = "stock.move"

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        print"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"
        """
        Generate the account.move.line values to post to track the stock valuation difference due to the
        processing of the given quant.
        """
        self.ensure_one()

        if self._context.get('force_valuation_amount'):
            valuation_amount = self._context.get('force_valuation_amount')
        else:
            if self.product_id.cost_method == 'average':
                valuation_amount = cost if self.location_id.usage == 'supplier' and self.location_dest_id.usage == 'internal' else self.product_id.standard_price
            else:
                valuation_amount = cost if self.product_id.cost_method == 'real' else self.product_id.standard_price
        # the standard_price of the product may be in another decimal precision, or not compatible with the coinage of
        # the company currency... so we need to use round() before creating the accounting entries.
        debit_value = self.company_id.currency_id.round(valuation_amount * qty)

        # check that all data is correct
        if self.company_id.currency_id.is_zero(debit_value):
            if self.product_id.cost_method == 'standard':
                raise UserError(_("The found valuation amount for product %s is zero. Which means there is probably a configuration error. Check the costing method and the standard price") % (self.product_id.name,))
            return []
        credit_value = debit_value

        if self.product_id.cost_method == 'average' and self.company_id.anglo_saxon_accounting:
            # in case of a supplier return in anglo saxon mode, for products in average costing method, the stock_input
            # account books the real purchase price, while the stock account books the average price. The difference is
            # booked in the dedicated price difference account.
            if self.location_dest_id.usage == 'supplier' and self.origin_returned_move_id and self.origin_returned_move_id.purchase_line_id:
                debit_value = self.origin_returned_move_id.price_unit * qty
            # in case of a customer return in anglo saxon mode, for products in average costing method, the stock valuation
            # is made using the original average price to negate the delivery effect.
            if self.location_id.usage == 'customer' and self.origin_returned_move_id:
                debit_value = self.origin_returned_move_id.price_unit * qty
                credit_value = debit_value
        partner_id = (self.picking_id.partner_id and self.env['res.partner']._find_accounting_partner(self.picking_id.partner_id).id) or False
        debit_line_vals = {
            'name': self.name,
            'product_id': self.product_id.id,
            'quantity': qty,
            'product_uom_id': self.product_id.uom_id.id,
            'ref': self.picking_id.name,
            'partner_id': partner_id,
            'debit': 55,
            'credit': 0,
            'account_id': debit_account_id,
        }
        credit_line_vals = {
            'name': self.name,
            'product_id': self.product_id.id,
            'quantity': qty,
            'product_uom_id': self.product_id.uom_id.id,
            'ref': self.picking_id.name,
            'partner_id': partner_id,
            'credit': 55,
            'debit': 0,
            'account_id': credit_account_id,
        }
        res = [(0, 0, debit_line_vals), (0, 0, credit_line_vals)]
        if credit_value != debit_value:
            # for supplier returns of product in average costing method, in anglo saxon mode
            diff_amount = debit_value - credit_value
            price_diff_account = self.product_id.property_account_creditor_price_difference
            if not price_diff_account:
                price_diff_account = self.product_id.categ_id.property_account_creditor_price_difference_categ
            if not price_diff_account:
                raise UserError(_('Configuration error. Please configure the price difference account on the product or its category to process this operation.'))
            price_diff_line = {
                'name': self.name,
                'product_id': self.product_id.id,
                'quantity': qty,
                'product_uom_id': self.product_id.uom_id.id,
                'ref': self.picking_id.name,
                'partner_id': partner_id,
                'credit': diff_amount > 0 and diff_amount or 0,
                'debit': diff_amount < 0 and -diff_amount or 0,
                'account_id': price_diff_account.id,
            }
            res.append((0, 0, price_diff_line))
        return res



# class setJournal(models.Model):
#     _inherit = 'product.template'
#
#     @api.multi
#     def do_change_standard_price(self, new_price, account_id):
#         print "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"
#         """ Changes the Standard Price of Product and creates an account move accordingly."""
#         AccountMove = self.env['account.move']
#
#         quant_locs = self.env['stock.quant'].sudo().read_group([('product_id', 'in', self.ids)], ['location_id'], ['location_id'])
#         quant_loc_ids = [loc['location_id'][0] for loc in quant_locs]
#         locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('company_id', '=', self.env.user.company_id.id), ('id', 'in', quant_loc_ids)])
#
#         product_accounts = {product.id: product.product_tmpl_id.get_product_accounts() for product in self}
#
#         for location in locations:
#             for product in self.with_context(location=location.id, compute_child=False).filtered(lambda r: r.valuation == 'real_time'):
#                 diff = product.standard_price - new_price
#                 if float_is_zero(diff, precision_rounding=product.currency_id.rounding):
#                     raise UserError(_("No difference between standard price and new price!"))
#                 if not product_accounts[product.id].get('stock_valuation', False):
#                     raise UserError(_('You don\'t have any stock valuation account defined on your product category. You must define one before processing this operation.'))
#                 qty_available = product.qty_available
#                 if qty_available:
#                     # Accounting Entries
#                     if diff * qty_available > 0:
#                         debit_account_id = account_id
#                         credit_account_id = product_accounts[product.id]['stock_valuation'].id
#                     else:
#                         debit_account_id = product_accounts[product.id]['stock_valuation'].id
#                         credit_account_id = account_id
#
#                     move_vals = {
#                         'journal_id': product_accounts[product.id]['stock_journal'].id,
#                         'company_id': location.company_id.id,
#                         'line_ids': [(0, 0, {
#                             'name': _('Standard Price changed'),
#                             'account_id': debit_account_id,
#                             'debit': 55,
#                             'credit': 0,
#                             'product_id': product.id,
#                         }), (0, 0, {
#                             'name': _('Standard Price changed'),
#                             'account_id': credit_account_id,
#                             'debit': 0,
#                             'credit': 55,
#                             'product_id': product.id,
#                         })],
#                     }
#                     move = AccountMove.create(move_vals)
#                     move.post()
#
#         self.write({'standard_price': new_price})
#         return True