{
    'name': 'Nano Sales',
    'description': 'This module inherted from sales module to edit in General Information part',
    'summary': 'UNSS-Sales Module',
    'category': 'Sales',
    'author': 'Universal Selective Systems',
    'depends': ['base', 'sale', 'stock'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/nano_sales_views.xml'
    ]
}
