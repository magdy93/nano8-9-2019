# -*- coding: utf-8 -*-
from odoo import models, fields, api


class nanoSales(models.Model):
    _inherit = 'sale.order'

    type = fields.Selection([('local', 'Local'), ('outside', 'Outside')],default='outside', string='Type')
    carrying = fields.Selection([('company', 'On our Company'), ('vendor', 'on the vendor')], string='Carrying')
    po = fields.Char(string='Po number of customer')
    producer = fields.Many2one('res.company', string='Producer')

    attach_rel = fields.One2many('nano.sale.docs', 'sale_order_id')
    loading_port = fields.Char(string='Loading Port')
    discharge_port = fields.Char(string='discharge Port')
    address = fields.Char(string='address')
    from_street2 = fields.Char(string='Street 2')
    zip_code = fields.Char(string='ZIP', change_default=True)
    city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    country = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    wh_incoming_date = fields.Date(string='WH Incoming')

    @api.multi
    def action_document_send(self):
        for rec in self:
            logistic_sale_obj = self.sudo().env['nano.logistic'].search([('sale_order', '=', rec.id)])
            logistic_sale_docs_obj = self.env['logistic.sale.docs']
            if logistic_sale_obj:
                for obj in rec.attach_rel:
                    if obj.is_send:
                        logistic_sale_docs_obj.create({
                            'logistic_order_id': logistic_sale_obj.id,
                            'document': obj.document,
                            'doc_status': obj.doc_status,
                            'is_send': obj.is_send,
                            'attachment': [(6,0,obj.attachment_ids.ids)]
                        })
                        obj.is_send = False
                        obj.flag = 1
            # for container_obj in rec.container_ids:
            #     if container_obj.is_send_container:
            #         print "is_send_container"
            #         logistic_purchase_container_docs.sudo().create({
            #             'purchase_containers_id': logistic_purchase_obj.id,
            #             'document': container_obj.document,
            #             'attachment_ids': container_obj.attachment_ids,
            #         })
            #         container_obj.is_send_container = False
            #         container_obj.flag_container = 1


class nanoSaleDocument(models.Model):
    _name = 'nano.sale.docs'

    sale_order_id = fields.Many2one('sale.order')
    document = fields.Char(string='Document')
    doc_status = fields.Selection([('required', 'required'), ('received', 'received')], string='Status')
    attachment_ids = fields.Many2many('ir.attachment', 'nano_sale_ir_attachments_rel',
                                      'nano_sale_id', 'attachment_id', string='Attachments')
    is_send = fields.Boolean('Is Send ?')
    flag = fields.Integer(default=0)

class orderLine(models.Model):
    _inherit = 'sale.order.line'

    # packaging = fields.Many2one('product.product', string='Packaging')

    pn = fields.Char(string='بند التعريفه')
    statistical_quantity = fields.Float(string='Statistical Quantity')
    gross_weight = fields.Float(string='Gross Weight')
    net_weight = fields.Float(string='Net Weight', related='product_id.weight')
    weight_unit = fields.Many2one('product.uom', string='Weight Unit of Measure')
    quantity_hand = fields.Float(string='Quantity On Hand', related='product_id.qty_available', readonly=True)
    reserved_quantity = fields.Float(string='Reserved Quantity', compute='compute_reserved_quantity')

    @api.multi
    def compute_reserved_quantity(self):
        so = self.env['sale.order']
        for record in self:
            sale_order_objects = so.search([
                ('state', 'not in', ['sale','cancel']),
                ('validity_date', '>', fields.Date.today())
            ])
            count_of_quantity = 0
            if len(sale_order_objects) > 0:
                for sale_order in sale_order_objects:
                    for line in sale_order.order_line:
                        if line.product_id.id == record.product_id.id:
                            count_of_quantity += line.product_uom_qty
            record.update({'reserved_quantity': count_of_quantity})
