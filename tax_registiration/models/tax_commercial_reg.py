# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from openerp.exceptions import UserError


class ContactsTax(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    tax_reg = fields.Char(string="Vat Registry")
    commercial_reg = fields.Char(string="Commercial Registry")


class AccountInvoiceLine(models.Model):
    _name = 'account.invoice.line'
    _inherit = 'account.invoice.line'

    discount_fix_amount = fields.Float('Discount Amount')

    # @api.onchange('discount_fix_amount', 'price_subtotal')
    # def on_change_discount_fix_amount(self):
    #     a = self.price_unit * self.quantity - self.discount_fix_amount
    #     # raise UserError(a)self.price_subtotal = self.price_unit * self.quantity - self.discount_fix_amount
    #     # self.price_subtotal = self.price_unit * self.quantity - self.discount_fix_amount
    #     if (self.price_subtotal != 0):
    #         self.discount = self.discount_fix_amount / self.price_subtotal * 100

    # @api.one
    # @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity',
    #              'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
    #              'invoice_id.date_invoice', 'invoice_id.date', 'discount_fix_amount')
    # def _compute_price(self):
    #     currency = self.invoice_id and self.invoice_id.currency_id or None
    #     price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
    #     taxes = False
    #     if self.invoice_line_tax_ids:
    #         taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id,
    #                                                       partner=self.invoice_id.partner_id)
    #     self.price_subtotal = price_subtotal_signed = taxes[
    #         'total_excluded'] if taxes else self.quantity * price
    #     self.price_total = taxes['total_included'] if taxes else self.price_subtotal
    #     if self.invoice_id.currency_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
    #         price_subtotal_signed = self.invoice_id.currency_id.with_context(
    #             date=self.invoice_id._get_currency_rate_date()).compute(price_subtotal_signed,
    #                                                                     self.invoice_id.company_id.currency_id)
    #     sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
    #     self.price_subtotal_signed = price_subtotal_signed * sign


class AccountInvoiceTax(models.Model):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    tax_reg = fields.Char(string="Vat Registry", related='partner_id.tax_reg')
    commercial_reg = fields.Char(string="Commercial Registry", related='partner_id.commercial_reg')
    company_tax_reg = fields.Char(string="Vat Registry", default=lambda self: self.env.user.company_id.vat)
    company_commercial_reg = fields.Char(string="Commercial Registry",
                                         default=lambda self: self.env.user.company_id.company_registry)

    def ConvertNumbersToArabicAlphabet(self, Number):
        return self.ConvertNumberToAlpha(Number)

    def ConvertNumberToAlpha(self, Number):
        Number = str(Number)
        if '.' in Number:
            if (len(Number.split('.')[0]) > 6):
                return "No Number"
            else:
                switch = len(Number.split('.')[0])
                if switch == 1:
                    if self.convertTwoDigits(Number.split('.')[1]) == "":
                        return self.convertOneDigits(Number) + " ريال "
                    return self.convertOneDigits(Number) + " ريال " + " و " + self.convertTwoDigits(
                        Number.split('.')[1]) + " هللة "
                elif switch == 2:
                    if self.convertTwoDigits(Number.split('.')[1]) == "":
                        return self.convertTwoDigits(Number) + " ريال "
                    return self.convertTwoDigits(Number) + " ريال " + " و " + self.convertTwoDigits(
                        Number.split('.')[1]) + " هللة "
                elif switch == 3:
                    if self.convertTwoDigits(Number.split('.')[1]) == "":
                        return self.convertThreeDigits(Number) + " ريال "
                    return self.convertThreeDigits(Number) + " ريال " + " و " + self.convertTwoDigits(
                        Number.split('.')[1]) + " هللة "
                elif switch == 4:
                    if self.convertTwoDigits(Number.split('.')[1]) == "":
                        return self.convertFourDigits(Number) + " ريال "
                    return self.convertFourDigits(Number) + " ريال " + " و " + self.convertTwoDigits(
                        Number.split('.')[1]) + " هللة "
                elif switch == 5:
                    if self.convertTwoDigits(Number.split('.')[1]) == "":
                        return self.convertFiveDigits(Number) + " ريال "
                    return self.convertFiveDigits(Number) + " ريال " + " و " + self.convertTwoDigits(
                        Number.split('.')[1]) + " هللة "
                elif switch == 6:
                    if self.convertTwoDigits(Number.split('.')[1]) == "":
                        return self.convertSixDigits(Number) + " ريال "
                    return self.convertSixDigits(Number) + " ريال " + " و " + self.convertTwoDigits(
                        Number.split('.')[1]) + " هللة "
                else:
                    return ""
        else:
            if len(Number) > 8:
                return "No Number"
            else:
                switch = len(Number)
                if switch == 1:
                    return self.convertOneDigits(Number) + " ريال "
                elif switch == 2:
                    return self.convertTwoDigits(Number) + " ريال "
                elif switch == 3:
                    return self.convertThreeDigits(Number) + " ريال "
                elif switch == 4:
                    return self.convertFourDigits(Number) + " ريال "
                elif switch == 5:
                    return self.convertFiveDigits(Number) + " ريال "
                elif switch == 6:
                    return self.convertSixDigits(Number) + " ريال "
                elif switch == 7:
                    return self.convertSevenDigits(Number) + " ريال "
                elif switch == 8:
                    return self.convertEightDigits(Number) + " ريال "
                else:
                    return ""

    def convertTwoDigits(self, TwoDigits):
        returnAlpha = "00"
        if TwoDigits == '00':
            return self.convertOneDigits(TwoDigits[1])
        if TwoDigits == '0':
            return self.convertOneDigits(TwoDigits[0])
        else:
            switch = (int(TwoDigits[0]))
            if switch == 1:
                if (int(TwoDigits[1]) == 1):
                    return " إحدى عشر "
                elif (int(TwoDigits[1])) == 2:
                    return " إثنى عشر "
                else:
                    returnAlpha = " عشر "
                    return self.convertOneDigits(TwoDigits[1]) + " " + returnAlpha
            elif switch == 2:
                returnAlpha = " عشرون"
            elif switch == 3:
                returnAlpha = " ثلاثون"
            elif switch == 4:
                returnAlpha = " أربعون"
            elif switch == 5:
                returnAlpha = " خمسون"
            elif switch == 6:
                returnAlpha = " ستون"
            elif switch == 7:
                returnAlpha = " سبعون"
            elif switch == 8:
                returnAlpha = " ثمانون"
            elif switch == 9:
                returnAlpha = " تسعون"
            else:
                returnAlpha = ""

        if len(TwoDigits) == 1:
            TwoDigits += "0"
        if (len(self.convertOneDigits(TwoDigits[1])) == 0):
            return returnAlpha
        else:
            return self.convertOneDigits(TwoDigits[1]) + " و " + returnAlpha

    def convertOneDigits(self, OneDigits):
        switch = (int(OneDigits))
        if switch == 1:
            return "واحد"
        elif switch == 2:
            return "إثنان"
        elif switch == 3:
            return "ثلاثه"
        elif switch == 4:
            return "أربعه"
        elif switch == 5:
            return "خمسه"
        elif switch == 6:
            return "سته"
        elif switch == 7:
            return "سبعه"
        elif switch == 8:
            return "ثمانيه"
        elif switch == 9:
            return "تسعه"
        else:
            return ""

    def convertThreeDigits(self, ThreeDigits):
        if isinstance(ThreeDigits, str):
            switch = int(ThreeDigits[0])
        else:
            ThreeDigits = str(ThreeDigits)
            switch = int(ThreeDigits[0])
        if switch == 1:
            if (int(ThreeDigits[1]) == 0):
                if (int(ThreeDigits[2]) == 0):
                    return "مائه"
                return "مائه" + " و " + self.convertOneDigits(ThreeDigits[2])
            else:
                return "مائه" + " و " + self.convertTwoDigits(ThreeDigits[1:3])
        elif switch == 2:
            if (int(ThreeDigits[1]) == 0):
                if (int(ThreeDigits[2]) == 0):
                    return "مائتين"
                return "مائتين" + " و " + self.convertOneDigits(ThreeDigits[2])
            else:
                return "مائتين" + " و " + self.convertTwoDigits(ThreeDigits[1:3])
        elif switch in [3, 4, 5, 6, 7, 8, 9]:
            if (int(ThreeDigits[1]) == 0):
                if (int(ThreeDigits[2]) == 0):
                    return self.convertOneDigits(ThreeDigits[0]) + " مائه"
                return self.convertOneDigits(ThreeDigits[0]) + " مائه " + " و " + self.convertOneDigits(
                    ThreeDigits[2])
            else:
                return self.convertOneDigits(ThreeDigits[0]) + " مائه " + " و " + self.convertTwoDigits(
                    ThreeDigits[1:3])

        elif switch == 0:
            if (ThreeDigits[1] == '0'):
                if (ThreeDigits[2] == '0'):
                    return ""
                else:
                    return self.convertOneDigits(ThreeDigits[2])
            else:
                return self.convertTwoDigits(ThreeDigits[1:3])
        else:
            return ""

    def convertFourDigits(self, FourDigits):
        switch = (int(FourDigits[0]))
        if switch == 1:
            if (int(FourDigits[1]) == 0):
                if (int(FourDigits[2]) == 0):
                    if (int(FourDigits[3]) == 0):
                        return "ألف"
                    else:
                        return " ألف" + " و " + self.convertOneDigits(FourDigits[3])
                return " ألف " + " و " + self.convertTwoDigits(FourDigits[2:3])
            else:
                return " ألف " + " و " + self.convertThreeDigits(FourDigits[1:4])
        elif switch == 2:
            if (int(FourDigits[1]) == 0):
                if (int(FourDigits[2]) == 0):
                    if (int(FourDigits[3]) == 0):
                        return "ألفين"
                    else:
                        return "ألفين" + " و " + self.convertOneDigits(FourDigits[3])
                return "ألفين" + " و " + self.convertTwoDigits(FourDigits[2:3])
            else:
                return "ألفين" + " و " + self.convertThreeDigits(FourDigits[1:4])
        elif switch in [3, 4, 5, 6, 7, 8, 9]:
            if (int(FourDigits[1]) == 0):
                if (int(FourDigits[2]) == 0):
                    if (int(FourDigits[3]) == 0):
                        return self.convertOneDigits(FourDigits[0]) + " ألاف"
                    else:
                        return self.convertOneDigits(FourDigits[0]) + " ألاف" + " و " + self.convertOneDigits(
                            FourDigits[3])
                return self.convertOneDigits(FourDigits[0]) + " ألاف" + " و " + self.convertTwoDigits(FourDigits[2:3])
            else:
                return self.convertOneDigits(FourDigits[0]) + " ألاف" + " و " + self.convertThreeDigits(
                    FourDigits[1:4])

        else:
            return ""

    def convertFiveDigits(self, FiveDigits):
        if len(self.convertThreeDigits(int(FiveDigits[2:5]))) == 0:
            return self.convertTwoDigits(FiveDigits[0:3]) + " ألف "
        else:
            return self.convertTwoDigits(FiveDigits[0:2]) + " ألفا " + " و " + self.convertThreeDigits(FiveDigits[2:5])

    def convertSixDigits(self, SixDigits):
        if (len(self.convertThreeDigits(SixDigits[2:6])) == 0):
            return self.convertThreeDigits(SixDigits[0:3]) + " ألف "
        else:
            return self.convertThreeDigits(SixDigits[0:3]) + " ألفا " + " و " + self.convertThreeDigits(SixDigits[3:6])

    def convertSevenDigits(self, SevenDigits):
        switch = (int(SevenDigits[0]))
        if switch == 1:
            first_letter = "واحد مليون"
        elif switch == 2:
            first_letter = "مليونان"
        elif switch == 3:
            first_letter = "ثلاثه ملايين"
        elif switch == 4:
            first_letter = "أربعه ملايين"
        elif switch == 5:
            first_letter = "خمسه ملايين"
        elif switch == 6:
            first_letter = "سته ملايين"
        elif switch == 7:
            first_letter = "سبعه ملايين"
        elif switch == 8:
            first_letter = "ثمانيه ملايين"
        elif switch == 9:
            first_letter = "تسعه ملايين"
        else:
            first_letter = ""
        if SevenDigits[1:7] == "000000":
            return first_letter
        if (len(self.convertThreeDigits(SevenDigits[1:4])) == 0):
            return first_letter + " و " + self.convertThreeDigits(SevenDigits[4:7])
        else:
            return first_letter + " و " + self.convertThreeDigits(
                SevenDigits[1:4]) + " ألفا " + " و" + self.convertThreeDigits(SevenDigits[4:7])

    def convertEightDigits(self, SixDigits):
        flag1 = True
        flag2 = True
        if SixDigits[2:8] == "000000":
            return self.convertTwoDigits(SixDigits[0:2]) + " مليون "
        if (len(self.convertThreeDigits(SixDigits[2:5])) == 0):
            flag1 = False
        if (len(self.convertThreeDigits(SixDigits[5:8])) == 0):
            flag2 = False
        if flag1 and flag2:
            return self.convertTwoDigits(SixDigits[0:2]) + " مليون " + " و " + self.convertThreeDigits(
                SixDigits[2:5]) + " ألفا " + 'و' + self.convertThreeDigits(SixDigits[5:8])
        if flag1 and not flag2:
            return self.convertTwoDigits(SixDigits[0:2]) + " مليون " + " و " + self.convertThreeDigits(
                SixDigits[2:5]) + " ألفا "
        if not flag1 and flag2:
            return self.convertTwoDigits(SixDigits[0:2]) + " مليون " + " و " + self.convertThreeDigits(
                SixDigits[5:8]) + " و "

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
