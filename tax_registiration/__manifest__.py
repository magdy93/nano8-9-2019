# -*- coding: utf-8 -*-
{
    'name': 'Vat Registration Function',
    'version': '1.0',
    'category': '',
    'description': """
vat Registration functions
===============================

Vat Registration Module.
    """,
    'author': 'CVC',
    'website': '',
    'images': [],
    'depends': [
        'base',
        'account',

    ],
    'init_xml': [
    ],
    'data': [
        #'security/security.xml',
        #'security/ir.model.access.csv',
        'views/tax_commercial_reg.xml',
        # 'views/report_invoice.xml',
    ],
    'test': [
    ],
    'css': [
        'static/src/css/style.css',
    ],
    'demo_xml': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'Other proprietary'
}

