from odoo import models, fields, api, _

class NanoCompany(models.Model):
      _inherit='res.company'
      mobile = fields.Char(string="Mobile", required=False, )
      registration_date = fields.Date(string="Date Of Registration", required=False, )
