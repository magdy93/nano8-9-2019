from odoo import models, fields, api,_
from werkzeug.routing import ValidationError
from odoo.exceptions import UserError


class LogisticPurchase(models.Model):
      _name='logistic.purchase'
      _inherit = ['mail.thread']
      logistic_purchase_docs_ids = fields.One2many('nano.logistic.purchase.docs', 'logistic_purchase_id')
      logistic_order=fields.One2many('logistic.purchase.order.line','order_line')
      routes_shipment_line=fields.One2many('routes.shipment.line','routes_shipment_line')
      container_logistic_ids = fields.One2many('nano.purchase.containers', 'purchase_containers_id')
      expense_ids = fields.One2many('hr.expense', 'logistic_id')

      transportation_status = fields.Char(string='status')
      shipping_state = fields.Char(string='status')
      name = fields.Char(string="Import order", readonly=True, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      incoterms = fields.Many2one(comodel_name="stock.incoterms", string="Incoterms",readonly=True, )
      request_date = fields.Date(string="Request Date", required=False,default=fields.Date.today )
      warehouse_status = fields.Char(string="Warehouse State", required=False, )
      custom_status = fields.Char(string="Custom state", required=False, )
      thc = fields.Boolean(string="Expense",)
      transportation = fields.Boolean(string="Transportation",)
      freight = fields.Boolean(string="Freight",)
      insurance = fields.Boolean(string="Insurance",)
      is_shipping = fields.Boolean(string="Shipping Declaration",)
      company_id = fields.Many2one(comodel_name='res.company',string='Company',required=True,
                                 default=lambda self: self.env.user.company_id)

      loading_port = fields.Char(string='Loading Port')
      unloading_port = fields.Char(string='Unloading Port')
      from_street = fields.Char(string='Street')
      from_street2 = fields.Char(string='Street 2')
      from_zip = fields.Char(string='ZIP', change_default=True)
      from_city = fields.Char(string='City')
      from_state_id = fields.Many2one('res.country.state', string='State')
      from_country_id = fields.Many2one('res.country', string='Country')
      to_street = fields.Char(string='Street')
      to_street2 = fields.Char(string='Street 2')
      to_zip = fields.Char(string='ZIP', change_default=True)
      to_city = fields.Char(string='City')
      to_state_id = fields.Many2one('res.country.state', string='State')
      to_country_id = fields.Many2one('res.country', string='Country')
      vessel_loading_date = fields.Date(string='Vessel Loading Date')
      vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
      wh_incoming_date = fields.Date(string='WH Incoming')
      purchase_user_id = fields.Many2one('res.users', default=lambda self: self.env.user, string='Purchase User')
      purchase_manager_id = fields.Many2one('res.users', string='Purchase Manager')
      booking_number = fields.Char(string="Booking Number", required=False, )
      delivery_date = fields.Date(string="Delivery Date", required=False, )
      voy_no = fields.Char(string="Voy NO", required=False, )
      vessel_name = fields.Char(string="Vessel Name", required=False, )

      thc_state = fields.Char(string="Thc state", required=False, )

      stat = fields.Selection([
            ('draft', 'Draft'),
            ('todo', 'To Do'),
            ('shipping', 'Shipping Decleration'),
            ('on_rote', 'On Route'),
            ('done', 'Done'),
            ],default='draft',string='State',)

      @api.multi
      def route_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'on_rote',
        })
        self.sudo().message_post(body='state is On Route',subtype='mt_comment',
                                              subject='state',message_type='notification')
      @api.multi
      def shipping_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'shipping',
            'transportation_status':'Null',
        })
        self.sudo().message_post(body='state is shipping',subtype='mt_comment',
                                              subject='state',message_type='notification')
      @api.multi
      def draft_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'draft',
        })
        self.sudo().message_post(body='state is draft',subtype='mt_comment',
                                              subject='state',message_type='notification')
      @api.multi
      def todo_progressbar(self):
          custom_obj = self.env['nano.custom']
          custom_line_obj = self.env['custom.order.line']
          nano_logistic_docs = self.env['nano.logistic.docs']
          logistic_purchase_container_docs = self.sudo().env['nano.container.docs']
          for rec in self:
              rec_id = custom_obj.sudo().create({
                  'logistic_id': rec.id,
                  'incoterms': rec.incoterms.id,
                  'purchase_logistic_id': rec.purchase_order.id,
                  'loading_port': rec.loading_port,
                  'unloading_port': rec.unloading_port,
                  'from_street': rec.from_street,
                  'from_street2': rec.from_street2,
                  'from_zip': rec.from_zip,
                  'from_city': rec.from_city,
                  'from_state_id': rec.from_state_id.id,
                  'from_country_id': rec.from_country_id.id,
                  'to_street': rec.to_street,
                  'to_street2': rec.to_street2,
                  'to_zip': rec.to_zip,
                  'to_city': rec.to_city,
                  'to_state_id': rec.to_state_id.id,
                  'to_country_id': rec.to_country_id.id,
                  'vessel_loading_date': rec.vessel_loading_date,
                  'vessel_unloading_date': rec.vessel_unloading_date,
                  'wh_incoming_date': rec.wh_incoming_date,
                  'purchase_user_id': rec.purchase_user_id.id,
                  'purchase_manager_id': rec.purchase_manager_id.id,
              })
              print "rec_id", rec_id
              for line in rec.logistic_order:
                  custom_line_obj.sudo().create({
                      'order_line': rec_id.id,
                      'product': line.product.id,
                      # '': line.package.id,
                      'describtion': line.describtion,
                      'quantity': line.quantity,
                      'price': line.price,
                      'subtotal': line.subtotal,
                      'uom': line.uom.id,
                  })
              for docs in rec.logistic_purchase_docs_ids:
                  nano_logistic_docs.sudo().create({
                      'logistic_docs_order_id': rec_id.id,
                      'document': docs.document,
                      'doc_status': docs.doc_status,
                      'attachment_ids': [(6,0,docs.attachment_ids.ids)],
                  })
                  docs.is_send = False
                  docs.flag = 1
              for container_obj in rec.container_logistic_ids:
                  logistic_purchase_container_docs.sudo().create({
                        'container_docs_order_id': rec_id.id,
                        'document': container_obj.document,
                        'attachment_ids': container_obj.attachment_ids,
                  })
                  container_obj.is_send_container = False
                  container_obj.flag_container = 1
              rec.sudo().write({
                'stat': 'todo',
              })
              self.message_post(body='state is Todo',subtype='mt_comment',
                                              subject='state',message_type='notification')

      @api.multi
      def action_document_send(self):
        for rec in self:
            logistic_purchase_obj = self.sudo().env['nano.custom'].search([('logistic_id', '=', rec.id)])
            logistic_purchase_docs_obj = self.sudo().env['nano.logistic.docs']
            logistic_purchase_container_docs = self.sudo().env['nano.container.docs']
            for obj in rec.logistic_purchase_docs_ids:
                if obj.is_send:
                    print "SEND"
                    logistic_purchase_docs_obj.sudo().create({
                      'logistic_docs_order_id':logistic_purchase_obj.id,
                      'document': obj.document,
                      'doc_status': obj.doc_status,
                      'attachment_ids': [(6,0,obj.attachment_ids.ids)],
                    })
                    obj.is_send = False
                    obj.flag = 1
            for container_obj in rec.container_logistic_ids:
                if container_obj.is_send_container:
                    print "is_send_container"
                    logistic_purchase_container_docs.sudo().create({
                        'container_docs_order_id': logistic_purchase_obj.id,
                        'document': container_obj.document,
                        'attachment_ids': container_obj.attachment_ids,
                    })
                    container_obj.is_send_container = False
                    container_obj.flag_container = 1

      @api.multi
      def done_progressbar(self):
        self.ensure_one()

        if self.warehouse_status=='done':
            self.sudo().write({
            'stat': 'done',
            })
            self.sudo().message_post(body='state is done',subtype='mt_comment',
                                              subject='state',message_type='notification')
        else:
             # raise ValidationError('the state of warehouse must be done !')
             raise UserError(_('the state of warehouse must be done !'))


      # @api.multi
      # def generate_service(self):
      #     print "generate_service"
      #     service_id=self.env['service.order'].search([('logistic_id','=',self.id)])
      #     routes=self.env['routes.shipment.line']
      #     for rout in self:
      #         for line in rout.routes_shipment_line:
      #             line.sudo().unlink()
      #     for service_obj in service_id:
      #         routes.sudo().create({
      #             'routes_shipment_line':self.id,
      #             'product_id':service_obj.id,
      #             'roytes_from':service_obj.roytes_from,
      #             'roytes_to':service_obj.roytes_to,
      #         })

      @api.multi
      def expense_request(self):
          for exp_obj in self:
              return {
                        'type': 'ir.actions.act_window',
                        'name': 'HR Expense',
                        'res_model':'hr.expense',
                        # 'res_id': service_order.id,
                        'context': {'default_is_logistic': True,
                                    'default_logistic_id':exp_obj.id,
                                    'default_so_po':'purchase',
                                    'default_purchase_order':exp_obj.purchase_order.id,
                                    },
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'current',
                     }
      @api.multi
      def action_print_shipping(self):
          return self.env['report'].get_action(self, 'nano_logistic.shipping_report_logistic_template')
      # @api.multi
      # def show_expense(self):
      #     return {
      #       'domain': [('logistic_id', '=', 'self.id'), ('is_logistic','=',True)],
      #       'type': 'ir.actions.act_window',
      #       'name': _('HR Expense'),
      #       'res_model': 'hr.expense',
      #       'view_type': 'form',
      #       'view_mode': 'tree,form',
      #       'target': 'new',
      #     }

      # @api.multi
      # def service_order_request(self):
      #     print "2222222222"
      #     service_obj=self.env['service.order']
      #     for service in self:
      #         service_search=service_obj.search([('shipping_sequence','=',service.sudo().import_request)])
      #         if service_search:
      #            return {
      #                   'type': 'ir.actions.act_window',
      #                   'name': 'service request',
      #                   'res_model':'service.order',
      #                   'res_id': service_search.id,
      #                   'view_type': 'form',
      #                   'view_mode': 'form',
      #                   'target': 'self',
      #               }
      #         else:
      #             return {
      #                   'type': 'ir.actions.act_window',
      #                   'name': 'service request',
      #                   'res_model':'service.order',
      #                   # 'res_id': service_order.id,
      #                   'context' :{'default_shipping_sequence': service.sudo().import_request},
      #                   'view_type': 'form',
      #                   'view_mode': 'form',
      #                   'target': 'current',
      #               }


      # @api.multi
      # def freight_request(self):
      #     print "33333333333"
      #
      # @api.multi
      # def insurance_request(self):
      #     print "44444444444"

      @api.multi
      def custom_state(self):
          logist=self.sudo().env['nano.custom']
          stok_id=logist.sudo().search([('logistic_id', '=',self.id)])
          if stok_id:
             for custom in stok_id:
                 self.custom_status=custom.sudo().stat

      @api.multi
      def warehouse_state(self):
          stok=self.sudo().env['stock.picking']
          stok_id=stok.sudo().search([('purchase_id', '=',self.purchase_order.id)])
          if stok_id:
             for logistic in stok_id:
                 self.warehouse_status=logistic.sudo().state

      @api.multi
      def shipping_request(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> shipping"
          ship_obj=self.env['shipp.declare']
          ship_line=self.env['shipp.line']
          container_line=self.env['nano.shipping.containers']
          for ship in self:
              shipping_search=ship_obj.search([('logistic_purchase_id','=',self.id)])
              if shipping_search:
                 return {
                    'type': 'ir.actions.act_window',
                    'name': 'shipping declaration',
                    'res_model': 'shipp.declare',
                    'res_id': shipping_search.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                 }
              else:

                  ship_id=ship_obj.sudo().create({
                        'logistic_purchase_id':ship.id,
                        'port_loading':ship.loading_port,
                        # 'ocean_freight':ship.incoterms.name,
                        'port_discharge':ship.unloading_port,
                    })
                  for line in ship.logistic_order:
                      ship_line.sudo().create({
                        'shipp_order_line':ship_id.id,
                        'product':line.product.id,
                        'describtion':line.describtion,
                        # 'unit':line.uom.id,
                        'quantity':line.quantity,
                        # 'gross_weight':line.unloading_port,
                        'net_weight':line.product.weight,
                        # 'price_unit':line.price,
                        # 'subtotal':line.subtotal,
                    })
                  for container_obj in ship.container_logistic_ids:
                      container_line.sudo().create({
                        'shipping_containers_id':ship_id.id,
                        'document':container_obj.document,
                        'attachment_ids':container_obj.attachment_ids,
                    })
                  self.write({
                      'shipping_state':'shipping',
                  })
                  return {
                        'type': 'ir.actions.act_window',
                        'name': 'shipping declaration',
                        'res_model': 'shipp.declare',
                        'res_id': ship_id.id,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'self',
                    }
      @api.multi
      def transportation_detail_request(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> transportation_detail_request"
          transportation_obj=self.env['transportation.detail']
          transportation_line=self.env['cargo.order.line']
          transportation_container_line=self.env['container.order.lineee']
          for line in self:
              transportation_search=transportation_obj.search([('logistic_id','=',self.id)])
              if transportation_search:
                  return {
                    'type': 'ir.actions.act_window',
                    'name': 'Transportation Detail',
                    'res_model': 'transportation.detail',
                    'res_id': transportation_search.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                  }
              else:
                  transportation_id=transportation_obj.sudo().create({
                        'route':'receipt',
                        'type':'outside',
                        'logistic_domestic_state':'international',
                        'logistic_id':line.id,
                        'purchase_id':line.purchase_order.id,
                    })
                  for line2 in line.logistic_order:
                      transportation_line.sudo().create({
                        'transportation_id':transportation_id.id,
                        'product_id':line2.product.id,
                        'uom':line2.uom.id,
                        'quantity':line2.quantity,
                    })
                  for container_line in line.container_logistic_ids:
                      transportation_container_line.sudo().create({
                        'containerr_id':transportation_id.id,
                        'container_number':container_line.document,
                        'container_attachments':container_line.attachment_ids,
                    })
                  self.sudo().write({
                      'transportation_status':'draft',
                  })
                  return {
                        'type': 'ir.actions.act_window',
                        'name': 'Transportation Detail',
                        'res_model': 'transportation.detail',
                        'res_id': transportation_id.id,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'self',
                    }
      # @api.multi
      # def shipment_request(self):
      #     print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> shipping"
      #     cary=self.env['shipment.purchase']
      #     for ship in self:
      #         ship_purchase=cary.create({
      #               'ex_request':ship.import_request,
      #               'purchase_order':ship.purchase_order.id,
      #           })
      #
      #         return {
      #               'type': 'ir.actions.act_window',
      #               'name': 'shipping info',
      #               'res_model': 'shipment.purchase',
      #               'res_id': ship_purchase.id,
      #               'view_type': 'form',
      #               'view_mode': 'form',
      #               'target': 'self',
      #           }

      @api.multi
      def carry_request(self):
          purchase=self.env['purchase.order']
          purchase_line=self.env['purchase.order.line']
          return {
                # 'domain': [('logistic_id', '=', self.id), ('is_logistic', '=','True')],
                'type': 'ir.actions.act_window',
                'name': 'service',
                'res_model': 'purchase.order',
                # 'res_id': cary_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'context' :{'default_service_state': 'service',
                            'default_is_logistic': True,
                            },
            }
          # for log in self:
          #     purchase_id=purchase.create({
          #           'service_state':'service',
          #           'date_order':log.request_date,
          #           'partner_id':log.purchase_order.partner_id.id,
          #       })
          #     for line in log.logistic_order:
          #         purchase_line.create({
          #           'order_id':purchase_id.id,
          #           'product_id':line.product.id,
          #           'product_uom':line.uom.id,
          #           'name':line.describtion,
          #           'date_planned':fields.Date.today(),
          #           'product_qty':line.quantity,
          #           'price_unit':line.price,
          #           'price_subtotal':line.subtotal,
          #         })

          # return {
          #       'domain': [('logistic_id', '=', self.id), ('is_logistic', '=','True')],
          #       'type': 'ir.actions.act_window',
          #       'name': 'service',
          #       'res_model': _('service.order'),
          #       # 'res_id': cary_id.id,
          #       'view_type': 'form',
          #       'view_mode': 'tree,form',
          #       'target': 'new',
          #   }
      @api.model
      def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('logistic.seq')
        return super(LogisticPurchase, self).create(vals)

class custom_order_line(models.Model):
    _name = 'logistic.purchase.order.line'
    order_line=fields.Many2one('logistic.purchase')
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    describtion=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,)
    uom = fields.Many2one(comodel_name="product.uom", string="unit of measure", required=False, )
    price = fields.Float(string="unit price",  required=False, )
    tax = fields.Many2one(comodel_name="account.tax", string="Tax", required=False, )
    subtotal = fields.Float(string="Subtotal",  required=False, )

class nanoPurchaseDocument(models.Model):
    _name = 'nano.logistic.purchase.docs'

    logistic_purchase_id = fields.Many2one('logistic.purchase')
    document = fields.Char(string='Document')
    doc_status = fields.Selection([('required', 'required'), ('received', 'received')], string='Status')
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments')
    is_send = fields.Boolean('Is Send ?')
    flag = fields.Integer(default=0)

class DomisticPurchase(models.Model):
      _name='domistic.purchase'

      domistic_purchase_docs_ids = fields.One2many('nano.domistic.purchase.docs', 'logistic_purchase_id')
      domistic_order=fields.One2many('domistic.purchase.order.line','order_line')
      service_domistic_order=fields.One2many('routes.domestic.line','routes_shipment_line')
      domistic_container_ids=fields.One2many('nano.domestic.containers','domestic_containers_id')

      domistic_request = fields.Char(string="domistic order", readonly=True, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      incoterms = fields.Many2one(comodel_name="stock.incoterms", string="Incoterms",readonly=True, )
      request_date = fields.Date(string="Request Date", required=False,default=fields.Date.today )
      warehouse_status = fields.Char(string="Warehouse State", required=False, )
      custom_status = fields.Char(string="logistic state", required=False, )
      transportation = fields.Boolean(string="Transportation",)
      insurance = fields.Boolean(string="Insurance",)

      transportation_status = fields.Char(string='status')

      loading_port = fields.Char(string='Loading Port')
      unloading_port = fields.Char(string='Unloading Port')
      from_street = fields.Char(string='Street')
      from_street2 = fields.Char(string='Street 2')
      from_zip = fields.Char(string='ZIP', change_default=True)
      from_city = fields.Char(string='City')
      from_state_id = fields.Many2one('res.country.state', string='State')
      from_country_id = fields.Many2one('res.country', string='Country')
      to_street = fields.Char(string='Street')
      to_street2 = fields.Char(string='Street 2')
      to_zip = fields.Char(string='ZIP', change_default=True)
      to_city = fields.Char(string='City')
      to_state_id = fields.Many2one('res.country.state', string='State')
      to_country_id = fields.Many2one('res.country', string='Country')
      vessel_loading_date = fields.Date(string='Vessel Loading Date')
      vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
      wh_incoming_date = fields.Date(string='WH Incoming')
      purchase_user_id = fields.Many2one('res.users', default=lambda self: self.env.user, string='Purchase User')
      purchase_manager_id = fields.Many2one('res.users', string='Purchase Manager')

      state = fields.Selection([
            ('draft', 'Draft'),
            ('todo', 'To Do'),
            ('route', 'On Route'),
            ('done', 'Done'),
            ],default='draft',string='State',)

      @api.multi
      def on_route_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'route',
        })
      @api.multi
      def draft_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })
      @api.multi
      def todo_progressbar(self):
          custom_obj = self.env['nano.custom']
          custom_line_obj = self.env['custom.order.line']
          for rec in self:
              print "rrrrrrr"
              rec_id = custom_obj.sudo().create({
                  # 'logistic_id': rec.id,
                  'incoterms': rec.incoterms.id,
              })
              print "rec_id", rec_id
              for line in rec.domistic_order:
                  custom_line_obj.sudo().create({
                      'order_line': rec_id.id,
                      'product': line.product.id,
                      # '': line.package.id,
                      'describtion': line.describtion,
                      'quantity': line.quantity,
                      'price': line.price,
                      'subtotal': line.subtotal,
                      # '': line.unit.id,
                  })
              rec.sudo().write({
                'state': 'todo',
              })

      @api.multi
      def done_progressbar(self):
        self.ensure_one()

        if self.warehouse_status=='done':
            self.sudo().write({
            'state': 'done',
            })
        else:
             raise ValidationError('the state of warehouse must be done !')

      @api.multi
      def transportation_request(self):
          print "2222222222"

      @api.multi
      def insurance_request(self):
          print "44444444444"

      @api.multi
      def custom_state(self):
          logist=self.sudo().env['nano.custom']
          stok_id=logist.sudo().search([('logistic_id', '=',self.id)])
          if stok_id:
             for custom in stok_id:
                 self.custom_status=custom.sudo().stat

      @api.multi
      def warehouse_state(self):
          stok=self.sudo().env['stock.picking']
          stok_id=stok.sudo().search([('purchase_id', '=',self.sudo().purchase_order.id)])
          if stok_id:
             for logistic in stok_id:
                 self.warehouse_status=logistic.sudo().state

      @api.multi
      def transportaion_detail_request(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> transportation_detail_request"
          transportation_obj=self.env['transportation.detail']
          transportation_line=self.env['cargo.order.line']
          transportation_container_line=self.env['container.order.lineee']
          for line in self:
              transportation_search=transportation_obj.search([('demestic_id','=',self.id)])
              if transportation_search:
                  return {
                    'type': 'ir.actions.act_window',
                    'name': 'Transportation Detail',
                    'res_model': 'transportation.detail',
                    'res_id': transportation_search.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                  }
              else:
                  transportation_id=transportation_obj.sudo().create({
                        'route':'receipt',
                        'type':'local',
                        'logistic_domestic_state':'domestic',
                        'demestic_id':line.id,
                    })
                  for line2 in line.domistic_order:
                      transportation_line.sudo().create({
                        'transportation_id':transportation_id.id,
                        'product_id':line2.product.id,
                        # 'uom':line2.uom.id,
                        'quantity':line2.quantity,
                      })
                  for container_line in line.domistic_container_ids:
                      transportation_container_line.sudo().create({
                        'containerr_id':transportation_id.id,
                        'container_number':container_line.document,
                        'container_attachments':container_line.attachment_ids,
                    })
                  self.sudo().write({
                        'transportation_status': 'Draft',
                        })
                  return {
                        'type': 'ir.actions.act_window',
                        'name': 'Transportation Detail',
                        'res_model': 'transportation.detail',
                        'res_id': transportation_id.id,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'self',
                    }
      @api.multi
      def transportation_detail(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> transportation_detail"
          transportation_obj=self.env['transportation.detail']
          transportation_line=self.env['cargo.order.line']
          transportation_container_line=self.env['container.order.lineee']
          for line in self:
              transportation_search=self.env['transportation.detail'].search([('demestic_id','=',self.id)])
              if transportation_search:
                  return {
                    'type': 'ir.actions.act_window',
                    'name': 'Transportation Detail',
                    'res_model': 'transportation.detail',
                    'res_id': transportation_search.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                  }
              else:
                  transportation_id=transportation_obj.sudo().create({
                        'route':'receipt',
                        'type':'local',
                        'logistic_domestic_state':'domestic',
                        'domestic_id':self.id,
                    })
                  for line2 in line.domistic_order:
                      transportation_line.sudo().create({
                        'transportation_id':transportation_id.id,
                        'product_id':line2.product.id,
                        'uom':line2.uom.id,
                        'quantity':line2.quantity,
                    })
                  for container_line in line.domistic_container_ids:
                      transportation_container_line.sudo().create({
                        'containerr_id':transportation_id.id,
                        'container_number':container_line.document,
                        'container_attachments':container_line.attachment_ids,
                    })
                  self.sudo().write({
                      'transportation_status':'draft',
                  })
                  return {
                        'type': 'ir.actions.act_window',
                        'name': 'Transportation Detail',
                        'res_model': 'transportation.detail',
                        'res_id': transportation_id.id,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'self',
                    }
      @api.multi
      def carry_request(self):
          cary=self.env['carring.logistic.purchase']
          for emp in self:
              cary_id=cary.create({
                    'sale_order':emp.purchase_order.id,
                    'exp_request':emp.import_request,
                })

              return {
                    'type': 'ir.actions.act_window',
                    'name': 'nano_logistic_purchase.logistic_cary_purchase_view',
                    'res_model': 'carring.logistic.purchase',
                    'res_id': cary_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }

      @api.model
      def create(self, vals):
        vals['domistic_request'] = self.env['ir.sequence'].next_by_code('domistic.sequence')
        return super(DomisticPurchase, self).create(vals)

class DomisticOrderline(models.Model):
    _name = 'domistic.purchase.order.line'
    order_line=fields.Many2one('domistic.purchase')
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    describtion=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,)
    price = fields.Float(string="unit price",  required=False, )
    tax = fields.Many2one(comodel_name="account.tax", string="Tax", required=False, )
    subtotal = fields.Float(string="Subtotal",  required=False, )

class DomisticPurchaseDocument(models.Model):
    _name = 'nano.domistic.purchase.docs'

    logistic_purchase_id = fields.Many2one('domistic.purchase')
    document = fields.Char(string='Document')
    doc_status = fields.Selection([('required', 'required'), ('received', 'received')], string='Status')
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments')
    is_send = fields.Boolean('Is Send ?')
class nanoDomesticContainer(models.Model):
    _name = 'nano.domestic.containers'

    domestic_containers_id = fields.Many2one('domistic.purchase')
    document = fields.Char(string='Document')
    attachment_ids = fields.Binary(string="Attachments",)

class RoutesDomestic(models.Model):
    _name = 'routes.domestic.line'
    routes_shipment_line=fields.Many2one('domistic.purchase')
    # product_id = fields.Many2one(comodel_name="service.order", string="service", required=False, )
    service_id = fields.Many2one(comodel_name="purchase.order", string="service", required=False, )
    roytes_from=fields.Char(string='From')
    roytes_to=fields.Char(string='To')
    route_done = fields.Boolean(string="Route Done",)
    date = fields.Date(string="Date", required=False, )
    to_bill = fields.Boolean(string="To Bill",)
    imp_order = fields.Char(string="Import order",)

    loading_port = fields.Char(string='Loading Port')
    unloading_port = fields.Char(string='Unloading Port')
    from_street = fields.Char(string='Street')
    from_street2 = fields.Char(string='Street 2')
    from_zip = fields.Char(string='ZIP', change_default=True)
    from_city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    from_country_id = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    wh_incoming_date = fields.Date(string='WH Incoming')

    @api.onchange('to_bill')
    def change_service_bill(self):
        # service = self.env['purchase.order'].search([('imp_order','=',self.imp_order)])
        # if service:
        #     for i in service:
        #         print "service : ",i.imp_order
        print "serrrrrrrr : ",self.service_id.imp_order
        self.service_id.write({
            'to_bill':self.to_bill,
        })
class RoutesShipment(models.Model):
    _name = 'routes.shipment.line'
    routes_shipment_line=fields.Many2one('logistic.purchase')
    routes_shipment_export_line=fields.Many2one('nano.logistic')
    # product_id = fields.Many2one(comodel_name="service.order", string="product", required=False, )
    service_id = fields.Many2one(comodel_name="purchase.order", string="service", required=False, )
    roytes_from=fields.Char(string='From')
    roytes_to=fields.Char(string='To')
    route_done = fields.Boolean(string="Route Done",)
    date = fields.Date(string="Date", required=False, )
    to_bill = fields.Boolean(string="To Bill",)

    loading_port = fields.Char(string='Loading Port')
    unloading_port = fields.Char(string='Unloading Port')
    from_street = fields.Char(string='Street')
    from_street2 = fields.Char(string='Street 2')
    from_zip = fields.Char(string='ZIP', change_default=True)
    from_city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    from_country_id = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    wh_incoming_date = fields.Date(string='WH Incoming')
    purchase_service_id = fields.Many2one(comodel_name="purchase.order", string="Purchase Order", required=False, )

    @api.onchange('to_bill')
    def change_service_bill(self):
        self.service_id.write({
            'to_bill':self.to_bill,
        })

    @api.onchange('service_id')
    def get_virtual_data(self):
        service_ids = []
        domain = [['id', '=', False]]
        service_obj = self.env['purchase.order'].search([('name','=','x')])
        for ser in service_obj:
            service_ids.append(ser.id)
        domain = [('id', 'in', service_ids)]
        return {'domain': {'service_id': domain}}

class vehicle_logistic_line(models.Model):
    _name = 'vehicle.logistic.line'
    vehicle_id = fields.Many2one(comodel_name="carring.logistic.purchase", string="vehicle", required=False, )
    no_vehicle = fields.Char(string="No", required=False, )
    driver = fields.Char(string="driver", required=False, )
    driver_phone = fields.Char(string="driver phone", required=False, )
    driver_id = fields.Char(string="driver id", required=False, )
    licence_plate = fields.Char(string="licence plate", required=False, )
    trailer_number = fields.Char(string="Trailer number", required=False, )
    attachment_id = fields.Many2many('ir.attachment', 'nano_logistic_purchase_ir_attachments_rel',
                                      'nano_logistic_purchase_id', 'attachment_id', string='Attachments')
    subtotal_vehicle = fields.Float(string="Sub total",  required=False, )

class container_logistic_line(models.Model):
    _name = 'container.logistic.line'
    carry_id = fields.Many2one(comodel_name="carring.logistic.purchase", string="carry", required=False, )
    no_container = fields.Char(string="No", required=False, )
    attachment = fields.Many2many('ir.attachment', 'nano_logistic_purchase_ir_attachments_rel',
                                      'nano_logistic_purchase_id', 'attachment_id', string='Attachments')
    describtion = fields.Text(string="Describtion", required=False, )
    subtotal_container = fields.Float(string="Sub total",  required=False, )

class carring_logistic_purchase(models.Model):
      _name='carring.logistic.purchase'
      _inherit = ['mail.thread']

      container_id = fields.One2many(comodel_name="container.logistic.line", inverse_name="carry_id", string="", required=False, )
      vehicle_id = fields.One2many(comodel_name="vehicle.logistic.line", inverse_name="vehicle_id", string="", required=False, )
      name = fields.Char(string="Name", required=False,readonly=True, )
      exp_request = fields.Char(string="Exp.request", required=False,readonly=True, )
      sale_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False,readonly=True, )
      vendors = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )
      order_date = fields.Date(string="Order Date", required=False,default=fields.Date.today )
      date_response = fields.Date(string="Execution Date", required=False,)

      payment = fields.Selection(string="Payment method", selection=[('account', 'on account'), ('custody', 'custody'), ],default='account', required=False, )
      type = fields.Selection(string="Type", selection=[('vehicle', 'vehicle'), ('container', 'container'), ],default='vehicle', required=False, )

      ammount = fields.Float(string="ammount",  required=False,compute='_get_total', )

      journal = fields.Many2one(comodel_name="account.journal", string="journal", required=False, )
      expense_account = fields.Many2one(comodel_name="account.account", string="Expense Account", required=False, )
      custody_account = fields.Many2one(comodel_name="account.account", string="Custody Account", required=False, )
      analytic_account = fields.Many2one(comodel_name="account.analytic.account", string="Analytic Account", required=False, )
      label = fields.Char(string="Label", required=False, )
      # post = fields.Boolean(string="Post",default=True, )

      stat = fields.Selection([
            ('draft', 'draft'),
            ('confirm', 'Confirm Carrying'),
            ('done', 'Done'),
            ],default='draft',string='State',)

      @api.multi
      def draff_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'draft',
        })

      @api.multi
      def account_invoice_info(self):
          invv = self.env['account.invoice']
          invoi=invv.search([('logistic_invoice_purchase','=',self.id)])
          if invoi:
              return {
                    'type': 'ir.actions.act_window',
                    'name': 'account.invoice_supplier_form',
                    'res_model': 'account.invoice',
                    'res_id': invoi.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
              }

      @api.constrains('order_date')
      def _user_id(self):
        if self.date_response == self.order_date and self.payment=='account':
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> new date"
            #sending message to group of users
            for record in self:
                groups = self.env['res.groups'].search([('category_id.name', '=', 'Accounting & Finance')])
                recipient_partners = []
                for group in groups:
                    for recipient in group.users:
                        if recipient.partner_id.id not in recipient_partners:
                            recipient_partners.append(recipient.partner_id.id)
                if len(recipient_partners):
                    record.message_post(body='magdy salah ',force_send=True,subtype='mt_comment',
                                      subject='test',partner_ids=recipient_partners,message_type='notification')
      @api.multi
      def concept_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'confirm',
        })
        account = self.sudo().env['account.invoice']
        journal = self.sudo().env['account.move']
        accountline = self.sudo().env['account.invoice.line']
        # journalline = self.env['account.move.line']
        for emp in self:
            if emp.payment == 'account':
                # if emp.type == 'vehicle':
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> account "
                    acc_id=account.create({
                        'partner_id':emp.vendors.id,
                        'type':'in_invoice',
                        'logistic_invoice_purchase':self.id,
                        'reference':emp.name,
                    })
                    for i in emp.vehicle_id:
                        accountline.create({
                            'invoice_id':acc_id.id,
                            'name':i.no_vehicle,
                            'price_unit':i.subtotal_vehicle,
                            'account_id':emp.vendors.property_account_payable_id.id,
                            'quantity':'1',
                            })
                # else:
                #     print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> container "
                #     acc_id=account.create({
                #         'partner_id':emp.vendors.id,
                #         'type':'in_invoice',
                #         'logistic_invoice_purchase':self.id,
                #         'reference':emp.name,
                #     })
                #     for i in emp.container_id:
                #         accountline.create({
                #             'invoice_id':acc_id.id,
                #             'name':"container"+i.no_container,
                #             'price_unit':i.subtotal_container,
                #             'account_id':emp.vendors.property_account_payable_id.id,
                #             'quantity':'1',
                #             })
            else:
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> custody "
                # if emp.post == True:
                #    name1 = 'posted'
                # else:
                #    name1 = 'draft'

                journal_id=journal.create({
                    'journal_id':emp.journal.id,
                    # 'state':name1,
                })
                journalline=self.with_context(dict(self._context,check_move_validity=False)).sudo().env['account.move.line']
                journalline.create({
                    'move_id':journal_id.id,
                    'account_id':emp.expense_account.id,
                    'name':emp.sale_order.name,
                    'debit':emp.ammount,
                    'credit':0.0,
                    'analytic_account_id':emp.analytic_account.id,
                    })
                journalline.create({
                        'move_id':journal_id.id,
                        'account_id':emp.custody_account.id,
                        'name':emp.sale_order.name,
                        'debit':0.0,
                        'credit':emp.ammount,
                        'analytic_account_id':emp.analytic_account.id,
                        })

      @api.multi
      def done_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'done',
        })
        print ">>>>>>>>>>>>>>>>>>>>>>>> done"

      @api.model
      def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('carrylogistic.seq')
        return super(carring_logistic_purchase, self).create(vals)

      @api.depends('type','container_id.subtotal_container','vehicle_id.subtotal_vehicle')
      def _get_total(self):
        for rec in self:
            ammount= 0.0
            ammount1=0.0
            # if self.type=='vehicle':
            #     for line in rec.vehicle_id:
            #         ammount += line.subtotal_vehicle
            #     rec.update({
            #          'ammount':ammount,
            #     })
            # elif self.type=='container':
            #     for line in rec.container_id:
            #         ammount += line.subtotal_container
            #     rec.update({
            #         'ammount':ammount,
            #     })
            for line in rec.vehicle_id:
                ammount += line.subtotal_vehicle
            for line in rec.container_id:
                ammount1 += line.subtotal_container
            ammount1 +=ammount
            rec.update({
                'ammount':ammount1,
            })

class expence_logistic_purchase(models.Model):
      _name='expence.logistic.purchase'
      # _inherit='hr.expense'
      seq_numm = fields.Char(string='Expense No.', readonly=True)
      expence_describtion = fields.Char(string="Description Expense", required=False, )
      bill_reference = fields.Many2one(comodel_name="purchase.order", string="Bill Reference", required=False, )
      product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
      unit_price = fields.Float(string="Unite Price",  required=False, )
      quantity = fields.Float(string="Quantity",  required=False, )
      total = fields.Float(string="Total",  required=False,compute='_get_total' )
      account = fields.Many2one(comodel_name="account.account", string="Account", required=False, )
      journal = fields.Many2one(comodel_name="account.journal", string="journal", required=False, )
      analytic_account = fields.Many2one(comodel_name="account.analytic.account", string="Analytic Account", required=False, )
      date = fields.Date(string="Date", required=False,default=fields.Date.today )
      employee = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False, )
      payment = fields.Char(string="Payment by",default='custody' , readonly=True,required=False, )
      custody_account = fields.Many2one(comodel_name="account.account", string="Custody Account", required=False, )
      vendor = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )
      # post = fields.Boolean(string="Post",default=True, )
      attachment_number1 = fields.Float(string="Document",  required=False,compute='_compute_attachment_number', )
      statt = fields.Selection([
        ('save', 'draft'),
        ('submit', 'submit'),
        ('confirm', 'confirm'),
        ], default='save',string='Status',)

      @api.model
      def create(self, vals):
        vals['seq_numm'] = self.env['ir.sequence'].next_by_code('req.seqeee')
        return super(expence_logistic_purchase, self).create(vals)

      @api.onchange('product')
      def check_show(self):
          bill_ids = []
          domain = [['id', '=', False]]
          bill_of_mat = self.env['purchase.order'].search([('order_line.product_id','=',self.product.id)])
          for bill in bill_of_mat:
                bill_ids.append(bill.id)
          domain = [('id', 'in', bill_ids)]
          return {'domain': {'bill_reference': domain}}

      @api.multi
      def action_save_send(self):
        self.ensure_one()
        self.write({
            'statt': 'save',
        })

      @api.multi
      def action_submit_send(self):
        self.ensure_one()
        self.write({
            'statt': 'submit',
        })

      @api.multi
      def action_confirm_send(self):
          self.ensure_one()
          self.write({
            'statt': 'confirm',
          })
          journal = self.sudo().env['account.move']
          # journalline = self.env['account.move.line']
          for emp in self:
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> custody "
                # if emp.post == True:
                #    name1 = 'posted'
                # else:
                #    name1 = 'draft'

                journal_id=journal.create({
                    'journal_id':emp.journal.id,
                    # 'state':name1,
                })

                if emp.payment == 'custody':
                   custudy = emp.custody_account.id
                else:
                   custudy = emp.employee.address_home_id.property_account_payable_id.id

                journalline=self.with_context(dict(self._context,check_move_validity=False)).sudo().env['account.move.line']
                journalline.create({
                    'move_id':journal_id.id,
                    'account_id':emp.account.id,
                    'name':emp.bill_reference,
                    'debit':emp.total,
                    'credit':0.0,
                    'analytic_account_id':emp.analytic_account.id,
                    })
                journalline.create({
                        'move_id':journal_id.id,
                        'account_id':custudy,
                        'name':emp.bill_reference,
                        'debit':0.0,
                        'credit':emp.total,
                        'analytic_account_id':emp.analytic_account.id,
                        })

      @api.multi
      def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'expence.logistic.purchase'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'expence.logistic.purchase', 'default_res_id': self.id}
        return res

      @api.multi
      def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group([('res_model', '=', 'expence.logistic.purchase'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.attachment_number1 = attachment.get(expense.id, 0)

      @api.multi
      @api.depends('unit_price','quantity')
      def _get_total(self):
          self.total=self.unit_price*self.quantity

class shipment_purchase(models.Model):
    _name='shipment.purchase'
    shipment_purchase_id = fields.One2many(comodel_name="shipment.purchase.line", inverse_name="shipment_id", string="Containers Info", required=False, )
    ex_request = fields.Char(string="Imp.request", required=False,readonly=True )
    purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False,readonly=True, )
    carrying_company = fields.Many2one(comodel_name="res.partner", string="Carrying Company", required=False, )
    vehicle_number = fields.Char(string="Vehicle Number", required=False,)
    driver_name = fields.Char(string="Driver Name", required=False,)
    driver_phone = fields.Char(string="Driver Phone number", required=False,)
    image = fields.Binary("License", attachment=True,)
class shipment_purchase_line(models.Model):
    _name='shipment.purchase.line'
    shipment_id = fields.Many2one(comodel_name="shipment.purchase", string="Containers Info", required=False, )
    container_number = fields.Char(string="Container Number", required=False, )
    container_photo = fields.Binary("Container Photo", attachment=True,)

class nanoPurchaseContainer(models.Model):
    _name = 'nano.purchase.containers'

    purchase_containers_id = fields.Many2one('logistic.purchase')
    document = fields.Char(string='Document')
    attachment_ids = fields.Binary(string="Attachments",)
    is_send_container = fields.Boolean('Is Send ?',)
    flag_container = fields.Integer(default=0)

class account_inv(models.Model):
      _inherit='account.invoice'
      logistic_invoice_purchase = fields.Many2one('carring.logistic.purchase', string="logistic invoice", required=False, )

class purchaseOrderLine(models.Model):
        _inherit='purchase.order.line'
        is_logistic = fields.Boolean(string="is logistic", )

        @api.onchange('is_logistic')
        def get_virtual_data(self):
            if self.is_logistic == True:
                serv_product_ids = []
                domain = [['id', '=', False]]
                serv_obj = self.env['product.product'].search([('is_logistic','=',True)])
                for ser in serv_obj:
                    serv_product_ids.append(ser.id)
                domain = [('id', 'in', serv_product_ids)]
                return {'domain': {'product_id': domain}}

class ProductTemplate(models.Model):
      _inherit='product.template'
      is_logistic = fields.Boolean(string="is logistic", )
      is_custom = fields.Boolean(string="is custom", )

class StockLandedCost(models.Model):
      _inherit='stock.landed.cost'
      purchase_order_id = fields.Many2one(comodel_name="purchase.order", string="Purchase Order", required=False, )
# class nano_logistic_purchase_flow(models.Model):
#     _inherit = 'purchase.order'
#
#     @api.multi
#     def button_confirm(self):
#         for order in self:
#             if order.state not in ['draft', 'sent']:
#                 continue
#             order._add_supplier_to_product()
#             # Deal with double validation process
#             if order.company_id.po_double_validation == 'one_step'\
#                     or (order.company_id.po_double_validation == 'two_step'\
#                         and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id))\
#                     or order.user_has_groups('purchase.group_purchase_manager'):
#                 order.button_approve()
#             else:
#                 order.write({'state': 'to approve'})
#         custom = self.env['logistic.purchase']
#         custom_line = self.env['logistic.purchase.order.line']
#         for logist in self:
#             acc_id=custom.create({
#                 'purchase_order':logist.id,
#                 'incoterms':logist.incoterm_id.id,
#             })
#             for line in logist.order_line:
#                 custom_line.create({
#                 'order_line':acc_id.id,
#                 'product':line.product_id.id,
#                 'describtion':line.name,
#                 'quantity':line.product_qty,
#                 'price':line.price_unit,
#                 'tax':line.taxes_id.id,
#                 'subtotal':line.price_subtotal,
#             })
#         return True
