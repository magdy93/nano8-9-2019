# -*- coding: utf-8 -*-
{
    'name': "Nano logistic combine",

    'summary': """
        Nano logistic""",

    'description': """
Nano logistic""",

    'author': "USS",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr_expense','account','mail','purchase','sale','stock_landed_costs'],#'nano_service'

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'equipment.xml',
        # 'maintenance.xml',
        # 'warehouse.xml',
        # 'warehouse_equipment.xml',
        # 'manufacturing.xml',
        # 'res_company.xml',
        'shipping_report.xml',
        'templates.xml',
        'expense.xml',
        'shippment.xml',
        'shippment_purchase.xml',
        'sequence.xml',
        'sequence2.xml',
        'carr_sequence.xml',
        'sequence_logistic_purchase.xml',
        'templates_logistic_purchase.xml',
        # 'security/ir.model.access.csv',
        'security_logistic_purchase/ir.model.access.csv',
        # 'security/security.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}
