from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from werkzeug.routing import ValidationError
from odoo.exceptions import UserError

import sys
import datetime
import time

class nano_logistic(models.Model):
    _name='nano.logistic'

    expense_ids = fields.One2many('hr.expense', 'logistic_export_id')
    routes_shipment_line_ids=fields.One2many('routes.shipment.line','routes_shipment_export_line')

    logistic_order=fields.One2many('logistic.order.line','order_line')
    name = fields.Char(string="Export Request", readonly=True, )
    sale_order = fields.Many2one(comodel_name="sale.order", string="Sales Quotation", required=False, )
    incoterms = fields.Many2one(comodel_name="stock.incoterms", string="Incoterms", required=False,readonly=True, )
    request_date = fields.Date(string="Request Date", required=False,default=fields.Date.today )
    status = fields.Char(string="Warehouse State", required=False, )
    custom_status = fields.Char(string="Custom state", required=False, )
    todo = fields.Char(string="To Do State", required=False, )
    shipping = fields.Char(string="Shipping", required=False, )
    company_id = fields.Many2one(comodel_name='res.company',string='Company',required=True,
                             default=lambda self: self.env.user.company_id)
    booking_number = fields.Char(string="Booking Number", required=False, )
    delivery_date = fields.Date(string="Delivery Date", required=False, )
    voy_no = fields.Char(string="Voy NO", required=False, )
    vessel_name = fields.Char(string="Vessel Name", required=False, )

    attach_rel = fields.One2many('logistic.sale.docs', 'logistic_order_id')
    loading_port = fields.Char(string='Loading Port')
    discharge_port = fields.Char(string='discharge Port')
    address = fields.Char(string='address')
    from_street2 = fields.Char(string='Street 2')
    zip_code = fields.Char(string='ZIP', change_default=True)
    city = fields.Char(string='City')
    from_state_id = fields.Many2one('res.country.state', string='State')
    country = fields.Many2one('res.country', string='Country')
    to_street = fields.Char(string='Street')
    to_street2 = fields.Char(string='Street 2')
    to_zip = fields.Char(string='ZIP', change_default=True)
    to_city = fields.Char(string='City')
    to_state_id = fields.Many2one('res.country.state', string='State')
    to_country_id = fields.Many2one('res.country', string='Country')
    vessel_loading_date = fields.Date(string='Vessel Loading Date')
    vessel_unloading_date = fields.Date(string='Vessel Unloading Date')
    wh_incoming_date = fields.Date(string='WH Incoming')
    stat = fields.Selection([
        ('draft', 'Draft'),
        ('todo', 'To Do'),
        ('done', 'Booking'),
        ('delivery', 'Delivery'),
        ],default='draft',string='State')

    def delivery_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'delivery',
        })

    @api.multi
    def draft_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'draft',
        })

    @api.multi
    def todo_progressbar(self):
      self.ensure_one()
      custom_export = self.env['export.request']
      custom_export_line = self.env['request.order.line']
      for line in self:
            req_id = custom_export.create({
                'sales_order_q': line.sale_order.id,
                'incoterm': line.incoterms.id
            })
            for rec in line.logistic_order:
                custom_export_line.create({
                    'order_line': req_id.id,
                    'product': rec.product.id,
                    'description': rec.describtion,
                    'quantity': rec.quantity,
                    # 'taxes': recc.tax_id.amount,
                    # 'uom': recc.product_uom.category_id.name,
                    'price': rec.price,
                    'subtotal': rec.subtotal
                })
      self.sudo().write({
              'stat': 'todo',
          })
      # if self.status=='done':
      # else:
      #     raise ValidationError('the state of warehouse must be done !')
         # raise Warning('the state of warehouse must be done !')
         # return {'value':{},'warning':{'title':'warning','message':'the state of warehouse must be done !'}}

    # @api.multi
    # def Proforma_invoice_action(self):
    #   self.ensure_one()
    #   Proforma_invoice = self.env['logistic.sale.order']
    #   Proforma_invoice_line = self.env['logistic.sale.order.line']
    #   for line in self:
    #         Proforma_invoice_id = Proforma_invoice.create({
    #             'logistic_id': line.id,
    #             'street': line.address,
    #             'street2': line.from_street2,
    #             'city': line.city,
    #             'state_id': line.from_state_id.id,
    #             'zip': line.zip_code,
    #             'country_id': line.country.id,
    #
    #             'street': line.address,
    #             'street2': line.from_street2,
    #             'city': line.city,
    #             'state_id': line.from_state_id.id,
    #             'zip': line.zip_code,
    #             'country_id': line.country.id,
    #         })
    #         for rec in line.logistic_order:
    #             Proforma_invoice_line.create({
    #                 'order_line': Proforma_invoice_id.id,
    #                 'product': rec.product.id,
    #                 'description': rec.describtion,
    #                 'quantity': rec.quantity,
    #                 # 'taxes': recc.tax_id.amount,
    #                 # 'uom': recc.product_uom.category_id.name,
    #                 'price': rec.price,
    #                 'subtotal': rec.subtotal
    #             })
    #   self.sudo().write({
    #           'stat': 'todo',
    #       })
      # if self.status=='done':
      # else:
      #     raise ValidationError('the state of warehouse must be done !')
         # raise Warning('the state of warehouse must be done !')
         # return {'value':{},'warning':{'title':'warning','message':'the state of warehouse must be done !'}}

    @api.multi
    def done_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'done',
        })

    @api.multi
    def expense_request(self):
          for exp_obj in self:
              return {
                        'type': 'ir.actions.act_window',
                        'name': 'HR Expense',
                        'res_model':'hr.expense',
                        # 'res_id': service_order.id,
                        'context': {'default_is_logistic': True,
                                    'default_logistic_export_id':exp_obj.id,
                                    'default_so_po':'sale',
                                    # 'default_purchase_order':exp_obj.purchase_order.id,
                                    },
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'current',
                     }
    @api.multi
    def origin_certificate_action(self):
        origin_certificate=self.env['origin.certificate']
        origin_line=self.env['origin.line']
        pack_line=self.env['origin.pack.line']
        for origin in self:
          origin_search=origin_certificate.search([('nano_sale_id','=',self.id)])
          if origin_search:
             return {
                'type': 'ir.actions.act_window',
                'name': 'Origin Certificate',
                'res_model': 'origin.certificate',
                'res_id': origin_search.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
             }
          else:

              origin_id=origin_certificate.sudo().create({
                    'nano_sale_id':origin.id,
                    # 'port_loading':ship.loading_port,
                    # 'port_discharge':ship.discharge_port,
                })
              for line in origin.logistic_order:
                  origin_line.sudo().create({
                    'certificate_line':origin_id.id,
                    'product':line.product.id,
                    'describtion':line.describtion,
                    'quantity':line.quantity,
                    'net_weight':line.product.weight,
                  })
              for pack in origin.package_ids:
                      pack_line.sudo().create({
                        'pack_order_line':origin_id.id,
                        'product':pack.package.id,
                        'quantity':pack.qty,
                    })
              return {
                    'type': 'ir.actions.act_window',
                    'name': 'Origin Certificate',
                    'res_model': 'origin.certificate',
                    'res_id': origin_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                }
    @api.multi
    def shipping_request(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> shipping"
          ship_obj=self.env['shipp.declare']
          ship_line=self.env['shipp.line']
          pack_line=self.env['pack.line']
          container_line=self.env['nano.shipping.containers']
          for ship in self:
              shipping_search=ship_obj.search([('logistic_sale_id','=',self.id)])
              if shipping_search:
                 return {
                    'type': 'ir.actions.act_window',
                    'name': 'shipping declaration',
                    'res_model': 'shipp.declare',
                    'res_id': shipping_search.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                 }
              else:

                  ship_id=ship_obj.sudo().create({
                        'logistic_sale_id':ship.id,
                        'port_loading':ship.loading_port,
                        'port_discharge':ship.discharge_port,
                    })
                  for line in ship.logistic_order:
                      ship_line.sudo().create({
                        'shipp_order_line':ship_id.id,
                        'product':line.product.id,
                        'describtion':line.describtion,
                        'quantity':line.quantity,
                        'net_weight':line.product.weight,
                    })
                  for pack in ship.package_ids:
                      pack_line.sudo().create({
                        'pack_order_line':ship_id.id,
                        'product':pack.package.id,
                        'quantity':pack.qty,
                    })
                  # for container_obj in ship.container_logistic_ids:
                  #     container_line.sudo().create({
                  #       'shipping_containers_id':ship_id.id,
                  #       'document':container_obj.document,
                  #       'attachment_ids':container_obj.attachment_ids,
                  #   })
                  self.write({
                      'shipping_state':'shipping',
                  })
                  return {
                        'type': 'ir.actions.act_window',
                        'name': 'shipping declaration',
                        'res_model': 'shipp.declare',
                        'res_id': ship_id.id,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'self',
                    }

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('re.seq')
        return super(nano_logistic, self).create(vals)

    @api.multi
    @api.onchange('sale_order')
    def incoter_sale(self):
      self.incoterms=self.sale_order.incoterm.id

    @api.multi
    def statical_action(self):
      statical_id=self.env['statistical.logistics']
      for record in self:
          statical_obj=statical_id.create({
                'name':record.company_id.id,
                # 'mobile':record.company_id.mobile,
                # 'registration_date':record.company_id.registration_date,
                # 'street':record.company_id.street,
                # 'country_id':record.company_id.sale_order.id,
                # 'website':record.company_id.export_request,
                # 'phone':record.company_id.export_request,
                # 'fax':record.company_id.export_request,
                # 'email':record.company_id.export_request,
                # 'vat':record.company_id.export_request,
                # 'company_registry':record.company_id.export_request,
            })
      return {
            'type': 'ir.actions.act_window',
            'name': 'Statistical Logistics',
            'res_model': 'statistical.logistics',
            'res_id': statical_obj.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'self',
        }
    @api.multi
    def shipping_info(self):
      print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> shipping"
      cary=self.env['shipment.sale']
      for ship in self:
          ship_id=cary.sudo().create({
                'ex_request':ship.export_request,
                'sale_order':ship.sale_order.id,
            })

          return {
                'type': 'ir.actions.act_window',
                'name': 'shipping info',
                'res_model': 'shipment.sale',
                'res_id': ship_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
            }




    @api.multi
    def magdy_salah(self):
      cary=self.env['carring.logistic']
      for emp in self:
          cary_id=cary.create({
                'sale_order':emp.sale_order.id,
                'exp_request':emp.export_request,
            })

          return {
                'type': 'ir.actions.act_window',
                'name': 'nano_logistic.logistic_cary_view',
                'res_model': 'carring.logistic',
                'res_id': cary_id.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
            }
    @api.multi
    def custom_state(self):
      print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Custom state"
      export=self.env['export.request']
      export_id=export.search([('sales_order_q', '=',self.sale_order.id)])
      if export_id:
         for attend in export_id:
             print "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state",attend.state
             self.custom_status=attend.state

    @api.multi
    def warehouse_state(self):
      stok=self.env['stock.picking']
      stok_id=stok.search([('sale_id', '=',self.sale_order.id)])
      if stok_id:
         for attend in stok_id:
             print "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state",attend.state
             self.status=attend.state
# class document_file(models.Model):
#     _inherit = 'ir.attachment'
#     logistic_sale_docs_id = fields.Many2one(comodel_name="logistic.sale.docs", string="logistic", required=False, )
class logisticSaleDocument(models.Model):
    _name = 'logistic.sale.docs'

    logistic_order_id = fields.Many2one('nano.logistic')
    document = fields.Char(string='Document')
    doc_status = fields.Selection([('required', 'required'), ('received', 'received')], string='Status')
    # attachment = fields.Many2many('ir.attachment', 'logistic_sale_docs_id', string='Attachments')
    attachment = fields.Many2many('ir.attachment', 'nano_logistic_ir_attachments_rel',
                                      'nano_logistic_id', 'attachment_id', string='Attachments')
    is_send = fields.Boolean('Is Send ?')
    flag = fields.Integer(default=0)
class logistic_order_line(models.Model):
    _name = 'logistic.order.line'
    order_line=fields.Many2one('nano.logistic')
    unit = fields.Many2one(comodel_name="product.uom", string="Unit Of Measure", required=False, )
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    package = fields.Many2one(comodel_name="sale.order.packages", string="package", required=False, )
    describtion=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,)
    price = fields.Float(string="unit price",  required=False, )
    subtotal = fields.Float(string="Subtotal",  required=False, )

class DomisticSalelogistic(models.Model):
      _name='domistic.sale.logistic'

      domistic_order=fields.One2many('domistic.sale.order.line','order_line')
      domistic_request = fields.Char(string="domistic Request", readonly=True, )
      sale_order = fields.Many2one(comodel_name="sale.order", string="Sales Quotation", required=False, )
      incoterms = fields.Many2one(comodel_name="stock.incoterms", string="Incoterms", required=False,readonly=True, )
      request_date = fields.Date(string="Request Date", required=False,default=fields.Date.today )
      status = fields.Char(string="Warehouse State", required=False, )
      custom_status = fields.Char(string="Custom state", required=False, )
      todo = fields.Char(string="To Do State", required=False, )
      shipping = fields.Char(string="Shipping", required=False, )
      state = fields.Selection([
            ('draft', 'Draft'),
            ('todo', 'To Do'),
            ('done', 'Done'),
            ],default='draft',string='State')

      @api.multi
      def draft_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'state': 'draft',
        })

      @api.multi
      def todo_progressbar(self):
          self.ensure_one()
          if self.status=='done':
              self.sudo().write({
                  'state': 'todo',
              })
          else:
              raise ValidationError('the state of warehouse must be done !')

      @api.multi
      def done_progressbar(self):
        self.ensure_one()
        self.sudo().write({
            'state': 'done',
        })

      @api.model
      def create(self, vals):
        vals['domistic_request'] = self.env['ir.sequence'].next_by_code('domistic.sale')
        return super(DomisticSalelogistic, self).create(vals)

      @api.multi
      @api.onchange('sale_order')
      def incoter_sale(self):
          self.incoterms=self.sale_order.incoterm.id

      @api.multi
      def shipping_info(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> shipping"
          cary=self.env['shipment.sale']
          for ship in self:
              ship_id=cary.sudo().create({
                    'ex_request':ship.export_request,
                    'sale_order':ship.sale_order.id,
                })

              return {
                    'type': 'ir.actions.act_window',
                    'name': 'shipping info',
                    'res_model': 'shipment.sale',
                    'res_id': ship_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'self',
                }




      @api.multi
      def magdy_salah(self):
          cary=self.env['carring.logistic']
          for emp in self:
              cary_id=cary.create({
                    'sale_order':emp.sale_order.id,
                    'exp_request':emp.export_request,
                })

              return {
                    'type': 'ir.actions.act_window',
                    'name': 'nano_logistic.logistic_cary_view',
                    'res_model': 'carring.logistic',
                    'res_id': cary_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }
      @api.multi
      def custom_state(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Custom state"
          export=self.env['export.request']
          export_id=export.search([('sales_order_q', '=',self.sale_order.id)])
          if export_id:
             for attend in export_id:
                 print "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state",attend.state
                 self.custom_status=attend.state

      @api.multi
      def warehouse_state(self):
          stok=self.env['stock.picking']
          stok_id=stok.search([('sale_id', '=',self.sale_order.id)])
          if stok_id:
             for attend in stok_id:
                 print "<<<<<<<<<<<<<<<<<<<<<<<<<<<< state",attend.state
                 self.status=attend.state

class DomisticOrderLine(models.Model):
    _name = 'domistic.sale.order.line'
    order_line=fields.Many2one('domistic.sale.logistic')
    unit = fields.Many2one(comodel_name="product.uom", string="Unit Of Measure", required=False, )
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    package = fields.Many2many(comodel_name="product.product", string="package", required=False, )
    describtion=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,)
    price = fields.Float(string="unit price",  required=False, )
    subtotal = fields.Float(string="Subtotal",  required=False, )

# class vehicle_order_line(models.Model):
#     _name = 'vehicle.order.line'
#     vehicle_id = fields.Many2one(comodel_name="carring.logistic", string="vehicle", required=False, )
#     no_vehicle = fields.Char(string="No", required=False, )
#     driver = fields.Char(string="driver", required=False, )
#     driver_phone = fields.Char(string="driver phone", required=False, )
#     driver_id = fields.Char(string="driver id", required=False, )
#     licence_plate = fields.Char(string="licence plate", required=False, )
#     trailer_number = fields.Char(string="Trailer number", required=False, )
#     # attachment_id = fields.Many2many('ir.attachment', 'nano_logistic_ir_attachments_rel',
#     #                                   'nano_logistic_id', 'attachment_id', string='Attachments')
#     subtotal_vehicle = fields.Float(string="Sub total",  required=False, )

# class container_order_line(models.Model):
#     _name = 'container.order.line'
#     carry_id = fields.Many2one(comodel_name="carring.logistic", string="carry", required=False, )
#     no_container = fields.Char(string="No", required=False, )
#     # attachment = fields.Many2many('ir.attachment', 'nano_logistic_ir_attachments_rel',
#     #                                   'nano_logistic_id', 'attachment_id', string='Attachments')
#     describtion = fields.Text(string="Describtion", required=False, )
#     subtotal_container = fields.Float(string="Sub total",  required=False, )

# class carring_logistic(models.Model):
#       _name='carring.logistic'
#       _inherit = ['mail.thread']
#
#       # container_id = fields.One2many(comodel_name="container.order.line", inverse_name="carry_id", string="", required=False, )
#       # vehicle_id = fields.One2many(comodel_name="vehicle.order.line", inverse_name="vehicle_id", string="", required=False, )
#       name = fields.Char(string="Name", required=False,readonly=True, )
#       exp_request = fields.Char(string="Exp.request", required=False,readonly=True, )
#       sale_order = fields.Many2one(comodel_name="sale.order", string="sale order", required=False,readonly=True, )
#       vendors = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )
#       order_date = fields.Date(string="Order Date", required=False,default=fields.Date.today )
#       date_response = fields.Date(string="Execution Date", required=False,)
#
#       payment = fields.Selection(string="Payment method", selection=[('account', 'on account'), ('custody', 'custody'), ], required=False, )
#       type = fields.Selection(string="Type", selection=[('vehicle', 'vehicle'), ('container', 'container'), ], required=False, )
#
#       ammount = fields.Float(string="ammount",  required=False,compute='_get_total', )
#
#       journal = fields.Many2one(comodel_name="account.journal", string="journal", required=False, )
#       expense_account = fields.Many2one(comodel_name="account.account", string="Expense Account", required=False, )
#       custody_account = fields.Many2one(comodel_name="account.account", string="Custody Account", required=False, )
#       analytic_account = fields.Many2one(comodel_name="account.analytic.account", string="Analytic Account", required=False, )
#       label = fields.Char(string="Label", required=False, )
#       # post = fields.Boolean(string="Post",default=True, )
#       state = fields.Selection([
#             ('draft', 'draft'),
#             ('confirm', 'Confirm Carry'),
#             ('done', 'Done'),
#             ],default='draft',)
#
#       @api.constrains('order_date')
#       def _user_id(self):
#         if self.date_response == self.order_date:
#             print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> new date"
#             #sending message to group of users
#             for record in self:
#                 groups = self.env['res.groups'].search([('category_id.name', '=', 'Accounting & Finance')])
#                 recipient_partners = []
#                 for group in groups:
#                     for recipient in group.users:
#                         if recipient.partner_id.id not in recipient_partners:
#                             recipient_partners.append(recipient.partner_id.id)
#                 if len(recipient_partners):
#                     record.message_post(body='magdy salah ',force_send=True,subtype='mt_comment',
#                                       subject='test',partner_ids=recipient_partners,message_type='notification')
#
#       @api.multi
#       def static_info(self):
#           print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> static"
#           static=self.env['statistical.logistics']
#           static_lines=self.env['static.invoice']
#           for ship in self:
#               for sale in ship.sale_order:
#                   static_id=static.sudo().create({
#                         'name':ship.vendors.id,
#                         'sale_order':ship.sale_order.id,
#                         'shipment_port':sale.loading_port,
#                         'arrival_port':sale.discharge_port,
#                         'invoice_umber':sale.invoice_ids[0].number,
#                         'invoice_date':sale.invoice_ids[0].date_invoice,
#                     })
#                   print">>>>>>>>>>>>>>>>>>>>> invoice" ,sale.invoice_ids[0].number
#                   print">>>>>>>>>>>>>>>>>>>>> invoice" ,sale.invoice_ids[0].date_invoice
#                   for line in sale.invoice_ids[0].invoice_line_ids:
#                       print">>>>>>>>>>>>>>>>>>>>>>>> line ",line.product_id.name
#                       static_lines.sudo().create({
#                         'order_line':static_id.id,
#                         'product':line.product_id.id,
#                         'describtion':line.product_id.name,
#                         'quantity':line.quantity,
#                         'price_unit':line.price_unit,
#                         'subtotal':line.price_subtotal,
#                     })
#
#
#               return {
#                     'type': 'ir.actions.act_window',
#                     'name': 'static info',
#                     'res_model': 'statistical.logistics',
#                     'res_id': static_id.id,
#                     'view_type': 'form',
#                     'view_mode': 'form',
#                     'target': 'self',
#                 }
#
#       @api.multi
#       def origin_certificate_info(self):
#           print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> origin"
#           origin_cer=self.env['origin.certificate']
#           origin_cer_line=self.env['origin.line']
#           pack_lines=self.env['origin.pack.line']
#           for origin in self:
#               for sale_origin in origin.sale_order:
#                   origin_id=origin_cer.sudo().create({
#                         'company':origin.vendors.id,
#                         'address':origin.vendors.street,
#                         'consigned':sale_origin.partner_id.id,
#                         'address2':sale_origin.partner_id.street,
#                         # 'invoice_umber':sale.invoice_ids[0].number,
#                         # 'invoice_date':sale.invoice_ids[0].date_invoice,
#                     })
#                   for line_origin in sale_origin.order_line:
#                       origin_cer_line.sudo().create({
#                         'certificate_line':origin_id.id,
#                         'product':line_origin.product_id.id,
#                         'describtion':line_origin.product_id.name,
#                         'unit':line_origin.product_uom.id,
#                         'quantity':line_origin.product_uom_qty,
#                         'gross_weight':line_origin.gross_weight,
#                         'net_weight':line_origin.net_weight,
#                         'price_unit':line_origin.price_unit,
#                         'subtotal':line_origin.price_subtotal,
#                     })
#
#                   for package in sale_origin.package_order_line:
#                       pack_lines.sudo().create({
#                         'pack_order_line':origin_id.id,
#                         'product':package.packaging.id,
#                         'quantity':package.quantity,
#                     })
#               return {
#                     'type': 'ir.actions.act_window',
#                     'name': 'origin info',
#                     'res_model': 'origin.certificate',
#                     'res_id': origin_id.id,
#                     'view_type': 'form',
#                     'view_mode': 'form',
#                     'target': 'self',
#                 }
#
#       @api.multi
#       def shipping_declaration_info(self):
#           print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> shipping"
#           shipping=self.env['shipp.declare']
#           shipping_lines=self.env['shipp.line']
#           pack_lines=self.env['pack.line']
#           for dec in self:
#               for sale_order in dec.sale_order:
#                   dec_id=shipping.sudo().create({
#                         'shipper':dec.vendors.id,
#                         'consignee':sale_order.partner_id.id,
#                         'destination':sale_order.partner_id.street,
#                         'port_loading':sale_order.loading_port,
#                         'port_discharge':sale_order.discharge_port,
#                         'arrival_port':sale_order.discharge_port,
#                     })
#                   for sale_order_line in sale_order.order_line:
#                       shipping_lines.sudo().create({
#                         'shipp_order_line':dec_id.id,
#                         'product':sale_order_line.product_id.id,
#                         'describtion':sale_order_line.product_id.name,
#                         'unit':sale_order_line.product_uom.id,
#                         'quantity':sale_order_line.product_uom_qty,
#                         'gross_weight':sale_order_line.gross_weight,
#                         'net_weight':sale_order_line.net_weight,
#                         'price_unit':sale_order_line.price_unit,
#                         'subtotal':sale_order_line.price_subtotal,
#                     })
#
#                   for package in sale_order.package_order_line:
#                       pack_lines.sudo().create({
#                         'pack_order_line':dec_id.id,
#                         'product':package.packaging.id,
#                         'quantity':package.quantity,
#                     })
#
#
#               return {
#                     'type': 'ir.actions.act_window',
#                     'name': 'shipping declaration',
#                     'res_model': 'shipp.declare',
#                     'res_id': dec_id.id,
#                     'view_type': 'form',
#                     'view_mode': 'form',
#                     'target': 'self',
#                 }
#
#
#       @api.multi
#       def account_invoice_info(self):
#           inv = self.env['account.invoice']
#           invoii=inv.search([('logistic_invoice_id','=',self.id)])
#           if invoii:
#               return {
#                     'type': 'ir.actions.act_window',
#                     'name': 'account.invoice_supplier_form',
#                     'res_model': 'account.invoice',
#                     'res_id': invoii.id,
#                     'view_type': 'form',
#                     'view_mode': 'form',
#                     'target': 'self',
#               }
#       @api.multi
#       def concept_progressbar(self):
#         self.ensure_one()
#         self.sudo().write({
#             'state': 'confirm',
#         })
#         account = self.sudo().env['account.invoice']
#         journal = self.sudo().env['account.move']
#         accountline = self.sudo().env['account.invoice.line']
#         # journalline = self.env['account.move.line']
#         for emp in self:
#             if emp.payment == 'account':
#                 # if emp.type == 'vehicle':
#                     print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> vehicle "
#                     acc_id=account.create({
#                         'partner_id':emp.vendors.id,
#                         'type':'in_invoice',
#                         'logistic_invoice_id':self.id,
#                         'reference':emp.name,
#                     })
#                     for i in emp.vehicle_id:
#                         accountline.create({
#                             'invoice_id':acc_id.id,
#                             'name':i.no_vehicle,
#                             'price_unit':i.subtotal_vehicle,
#                             'account_id':emp.vendors.property_account_payable_id.id,
#                             'quantity':'1',
#                             })
#                 # else:
#                 #     print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> container "
#                 #     acc_id=account.create({
#                 #         'partner_id':emp.vendors.id,
#                 #         'type':'in_invoice',
#                 #         'logistic_invoice_id':self.id,
#                 #         'reference':emp.name,
#                 #     })
#                 #     for i in emp.container_id:
#                 #         accountline.create({
#                 #             'invoice_id':acc_id.id,
#                 #             'name':"container"+i.no_container,
#                 #             'price_unit':i.subtotal_container,
#                 #             'account_id':emp.expense_account.id,
#                 #             'quantity':'1',
#                 #             })
#             else:
#                 print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> custody "
#                 # if emp.post == True:
#                 #    name1 = 'posted'
#                 # else:
#                 #    name1 = 'draft'
#
#                 journal_id=journal.create({
#                     'journal_id':emp.journal.id,
#                     # 'state':name1,
#                 })
#                 journalline=self.with_context(dict(self._context,check_move_validity=False)).sudo().env['account.move.line']
#                 journalline.create({
#                     'move_id':journal_id.id,
#                     'account_id':emp.expense_account.id,
#                     'name':emp.label,
#                     'debit':emp.ammount,
#                     'credit':0.0,
#                     'analytic_account_id':emp.analytic_account.id,
#                     })
#                 journalline.create({
#                         'move_id':journal_id.id,
#                         'account_id':emp.custody_account.id,
#                         'name':emp.label,
#                         'debit':0.0,
#                         'credit':emp.ammount,
#                         'analytic_account_id':emp.analytic_account.id,
#                         })
#
#       @api.multi
#       def done_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'done',
#         })
#         print ">>>>>>>>>>>>>>>>>>>>>>>> done"
#
#       @api.multi
#       def draff_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'draft',
#         })
#
#       @api.model
#       def create(self, vals):
#         vals['name'] = self.env['ir.sequence'].next_by_code('r.seq')
#         return super(carring_logistic, self).create(vals)
#
#       @api.depends('type','container_id.subtotal_container','vehicle_id.subtotal_vehicle')
#       def _get_total(self):
#         for rec in self:
#             ammount= 0.0
#             ammount1=0.0
#             # if self.type=='vehicle':
#             #     for line in rec.vehicle_id:
#             #         ammount += line.subtotal_vehicle
#             #     rec.update({
#             #          'ammount':ammount,
#             #     })
#             # elif self.type=='container':
#             #     for line in rec.container_id:
#             #         ammount += line.subtotal_container
#             #     rec.update({
#             #         'ammount':ammount,
#             #     })
#             # else:
#             for line in rec.vehicle_id:
#                 ammount += line.subtotal_vehicle
#             for line in rec.container_id:
#                 ammount1 += line.subtotal_container
#             ammount1 +=ammount
#             rec.update({
#                 'ammount':ammount1,
#             })

class expence_logistic(models.Model):
      _name='expence.logistic'
      # _inherit='hr.expense'

      seq_nooo = fields.Char(string='Expense No.', readonly=True)
      expence_describtion = fields.Char(string="Description Expense", required=False, )
      bill_reference = fields.Many2one(comodel_name="purchase.order", string="Bill Reference", required=False, )
      product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
      unit_price = fields.Float(string="Unite Price",  required=False, )
      quantity = fields.Float(string="Quantity",  required=False, )
      total = fields.Float(string="Total",  required=False,compute='_get_total' )
      account = fields.Many2one(comodel_name="account.account", string="Account", required=False, )
      journal = fields.Many2one(comodel_name="account.journal", string="journal", required=False, )
      analytic_account = fields.Many2one(comodel_name="account.analytic.account", string="Analytic Account", required=False, )
      date = fields.Date(string="Date", required=False,default=fields.Date.today )
      employee = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False, )
      # payment = fields.Selection(string="Payment by", selection=[('employee', 'employee'), ('custody', 'custody'), ], required=False, )
      payment = fields.Char(string="Payment by",default='custody' , readonly=True,required=False, )
      custody_account = fields.Many2one(comodel_name="account.account", string="Custody Account", required=False, )
      vendor = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )
      # post = fields.Boolean(string="Post",default=True, )
      attachment_number = fields.Float(string="Document",  required=False,compute='_compute_attachment_number', )
      stat = fields.Selection([
        ('save', 'draft'),
        ('submit', 'submit'),
        ('confirm', 'confirm'),
        ],default='save',string='Status',)

      @api.model
      def create(self, vals):
        vals['seq_nooo'] = self.env['ir.sequence'].next_by_code('req.seqnceeee')
        return super(expence_logistic, self).create(vals)

      @api.onchange('product')
      def check_show(self):
          bill_ids = []
          domain = [['id', '=', False]]
          bill_of_mat = self.env['sale.order'].search([('order_line.product_id','=',self.product.id)])
          for bill in bill_of_mat:
                bill_ids.append(bill.id)
          domain = [('id', 'in', bill_ids)]
          return {'domain': {'bill_reference': domain}}

      @api.multi
      def action_save_send(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'save',
        })

      @api.multi
      def action_submit_send(self):
        self.ensure_one()
        self.sudo().write({
            'stat': 'submit',
        })

      @api.multi
      def action_confirm_send(self):
          self.ensure_one()
          self.sudo().write({
            'stat': 'confirm',
          })
          journal = self.sudo().env['account.move']
          # journalline = self.env['account.move.line']
          for emp in self:
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> custody "
                # if emp.post == True:
                #    name1 = 'posted'
                # else:
                #    name1 = 'draft'

                journal_id=journal.sudo().create({
                    'journal_id':emp.journal.id,
                    # 'state':name1,
                })

                if emp.payment == 'custody':
                   custudy = emp.custody_account.id
                else:
                   custudy = emp.employee.address_home_id.property_account_payable_id.id

                journalline=self.with_context(dict(self._context,check_move_validity=False)).sudo().env['account.move.line']
                journalline.sudo().create({
                    'move_id':journal_id.id,
                    'account_id':emp.account.id,
                    'name':emp.bill_reference,
                    'debit':emp.total,
                    'credit':0.0,
                    'analytic_account_id':emp.analytic_account.id,
                    })
                journalline.create({
                        'move_id':journal_id.id,
                        'account_id':custudy,
                        'name':emp.bill_reference,
                        'debit':0.0,
                        'credit':emp.total,
                        'analytic_account_id':emp.analytic_account.id,
                        })
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> ",custudy

      @api.multi
      def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'expence.logistic'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'expence.logistic', 'default_res_id': self.id}
        return res

      @api.multi
      def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group([('res_model', '=', 'expence.logistic'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.attachment_number = attachment.get(expense.id, 0)

      @api.multi
      @api.depends('unit_price','quantity')
      def _get_total(self):
          self.total=self.unit_price*self.quantity

class shipment_sale(models.Model):
    _name='shipment.sale'
    shipment_sale_id = fields.One2many(comodel_name="shipment.sale.line", inverse_name="shipment_id", string="Containers Info", required=False, )
    ex_request = fields.Char(string="Exp.request", required=False,readonly=True )
    sale_order = fields.Many2one(comodel_name="sale.order", string="sale order", required=False,readonly=True, )
    carrying_company = fields.Many2one(comodel_name="res.partner", string="Carrying Company", required=False, )
    vehicle_number = fields.Char(string="Vehicle Number", required=False,)
    driver_name = fields.Char(string="Driver Name", required=False,)
    driver_phone = fields.Char(string="Driver Phone number", required=False,)
    image = fields.Binary("License", attachment=True,)
class shipment_sale_line(models.Model):
    _name='shipment.sale.line'
    shipment_id = fields.Many2one(comodel_name="shipment.sale", string="Containers Info", required=False, )
    container_number = fields.Char(string="Container Number", required=False, )
    container_photo = fields.Binary("Container Photo", attachment=True,)
class account_inv(models.Model):
      _inherit='account.invoice'
      logistic_invoice_id = fields.Many2one('carring.logistic', string="logistic invoice", required=False, )

class HrExpenseRefuseWizard(models.TransientModel):

    _inherit = "hr.expense.refuse.wizard"

    @api.multi
    def expense_refuse_reason(self):
        self.ensure_one()

        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        expense_sheet = self.env['hr.expense.sheet'].browse(active_ids)
        # print">>>>>>>>>>>>> : ",expense_sheet.name , expense_sheet.purchase_order
        landed_cost = self.env['stock.landed.cost'].search([('purchase_order_id','=',expense_sheet.purchase_order.id)])
        if landed_cost:
            for record in landed_cost:
                for line in record.cost_lines:
                    if line.name == expense_sheet.name :
                        line.unlink()
        expense_sheet.refuse_expenses(self.description)

        return {'type': 'ir.actions.act_window_close'}

class HrExpense(models.Model):
      _inherit='hr.expense'
      product_id = fields.Many2one('product.product', string='Service type', readonly=True, states={'draft': [('readonly', False)], 'refused': [('readonly', False)]}, domain=[('can_be_expensed', '=', True)], required=True)
      unit_amount = fields.Float(string='Service cost', readonly=True, required=True, states={'draft': [('readonly', False)], 'refused': [('readonly', False)]}, digits=dp.get_precision('Product Price'))
      so_po = fields.Selection(string="select", selection=[('sale', 'sale order'), ('purchase', 'purchase order'), ],default='sale',required=False, )
      sale_order = fields.Many2one(comodel_name="sale.order", string="sales order", required=False, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      logistic_id = fields.Many2one(comodel_name="logistic.purchase", string="logistic", required=False, )
      logistic_export_id = fields.Many2one(comodel_name="nano.logistic", string="logistic Export", required=False, )
      is_logistic = fields.Boolean(string="is logistic",)
      is_custom = fields.Boolean(string="is custom",)
      is_lc = fields.Boolean(string="is LC",)
      custom_certificate_number = fields.Char(string="Custom Certificate Number", required=False, )
      custom_date = fields.Date(string="custom date", required=False, )

      @api.onchange('so_po', 'is_custom', 'is_logistic',)
      def check_show(self):
          if self.is_custom == True and self.so_po in ['sale','purchase']:
              bill_ids = []
              domain = [['id', '=', False]]
              bill_of_mat = self.env['product.product'].search([('landed_cost_ok','=',True),('is_custom','=',True)])
              for bill in bill_of_mat:
                    bill_ids.append(bill.id)
              domain = [('id', 'in', bill_ids)]
              return {'domain': {'product_id': domain}}
          elif self.is_logistic == True and self.so_po in ['sale','purchase']:
              custom_product_ids = []
              domain = [['id', '=', False]]
              product_ids = self.env['product.product'].search([('landed_cost_ok','=',True),('is_logistic','=',True)])
              for p in product_ids:
                    custom_product_ids.append(p.id)
              domain = [('id', 'in', custom_product_ids)]
              return {'domain': {'product_id': domain}}
          elif self.is_logistic == False and self.is_custom == False and self.so_po in ['sale','purchase']:
              custom_product_ids = []
              domain = [['id', '=', False]]
              product_ids = self.env['product.product'].search([('landed_cost_ok','=',True)])
              for p in product_ids:
                    custom_product_ids.append(p.id)
              domain = [('id', 'in', custom_product_ids)]
              return {'domain': {'product_id': domain}}
          else:
              bill_ids = []
              domain = [['id', '=', False]]
              bill_of_mat = self.env['product.product'].search([('can_be_expensed','=',True)])
              for bill in bill_of_mat:
                    bill_ids.append(bill.id)
              domain = [('id', 'in', bill_ids)]
              return {'domain': {'product_id': domain}}

      @api.multi
      def submit_expenses(self):
        if any(expense.state != 'draft' for expense in self):
            raise UserError(_("You cannot report twice the same line!"))
        if len(self.mapped('employee_id')) != 1:
            raise UserError(_("You cannot report expenses for different employees in the same report!"))
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'hr.expense.sheet',
            'target': 'current',
            'context': {
                'default_expense_line_ids': [line.id for line in self],
                'default_employee_id': self[0].employee_id.id,
                'default_is_logistic': self.is_logistic,
                'default_is_custom': self.is_custom,
                'default_so_po': self.so_po,
                'default_sale_order': self.sale_order.id,
                'default_purchase_order': self.purchase_order.id,
                'default_vendor_id': self.vendor_id.id,
                'default_name': self[0].name if len(self.ids) == 1 else ''
            }
        }
class HrExpenseSheet(models.Model):
      _inherit='hr.expense.sheet'
      is_logistic = fields.Boolean(string="is logistic",)
      is_custom = fields.Boolean(string="is custom",)
      so_po = fields.Selection(string="select", selection=[('sale', 'sale order'), ('purchase', 'purchase order'), ],default='sale',required=False, )
      sale_order = fields.Many2one(comodel_name="sale.order", string="sales order", required=False, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      vendor_id = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )

      @api.one
      @api.constrains('expense_line_ids')
      def _check_employee(self):
        employee_ids = self.expense_line_ids.mapped('purchase_order')
        print "eeeeeeeee",employee_ids
        if len(employee_ids) > 1 or (len(employee_ids) == 1):
            # raise ValidationError(_('You cannot add expense lines of another employee.'))
            # print "hhhhhhhhhhhhhhhhhhhh"
            return
      @api.onchange('sale_order')
      def check_show(self):
          bill_ids = []
          domain = [['id', '=', False]]
          bill_of_mat = self.env['hr.expense'].search([('sale_order','=',self.sale_order.id)])
          for bill in bill_of_mat:
                bill_ids.append(bill.id)
          domain = [('id', 'in', bill_ids)]
          return {'domain': {'expense_line_ids': domain}}

      @api.onchange('purchase_order')
      def get_purchase_expense(self):
          purchase_ids = []
          domain = [['id', '=', False]]
          expense_purchase = self.env['hr.expense'].search([('purchase_order','=',self.purchase_order.id)])
          for exp_purchase in expense_purchase:
              purchase_ids.append(exp_purchase.id)
              print "ppppppppp",purchase_ids
          domain = [('id', 'in', purchase_ids)]
          print ">>>>>>>>>",domain
          return {'domain': {'expense_line_ids': domain}}

#
# class maintainance_request(models.Model):
#       _name = 'maintainance.request'
#
#       sale_line_id = fields.One2many(comodel_name="logistic.sale.line", inverse_name="logistic_sale_id",)
#       purchase_line_id = fields.One2many(comodel_name="logistic.purchase.line", inverse_name="logistic_purchase_id",)
#       package_line_id = fields.One2many(comodel_name="logistic.package.line", inverse_name="logistic_package_id",)
#
#       maintainance_sequence = fields.Char(string="Maintenance sequence",readonly=True, )
#       equipment_sequence = fields.Char(string="Equipment sequence",readonly=True, )
#       equipment = fields.Many2one('maintenance.equipment', string="equipment", required=False, )
#       driver = fields.Many2one(comodel_name="res.partner", string="driver", required=False, )
#       request_date = fields.Datetime(string="request date", required=False, )
#       sale_order = fields.Many2one(comodel_name="sale.order", string="sale order", required=False, )
#       purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
#       describtion = fields.Char(string="describtion", required=False, )
#       operation = fields.Selection(string="operation", selection=[('deliver', 'deliver'), ('receipt', 'receipt'), ], required=True,)
#       state = fields.Selection(selection=[('waiting', 'waiting bocking'),
#                                           ('approved', 'approved'),
#                                           ('done', 'done'), ],string='State' )
#
#       technician_user_id = fields.Many2one('res.users', string='Owner', track_visibility='onchange')
#       duration = fields.Float(help="Duration in minutes and seconds.")
#
#       @api.model
#       def create(self, vals):
#         vals['maintainance_sequence'] = self.env['ir.sequence'].next_by_code('maintenance.seq')
#         return super(maintainance_request, self).create(vals)
#
#       @api.multi
#       def waiting_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'waiting',
#         })
#         equipment=self.env['equipment.request']
#         equipment_id=equipment.search([('equipment_sequence', '=',self.equipment_sequence)])
#         if equipment_id:
#             for equip in equipment_id:
#                 equip.write({
#                     'state': 'waiting',
#                 })
#
#
#       @api.multi
#       def approved_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'approved',
#             })
#         equipment=self.env['equipment.request']
#         equipment_id=equipment.search([('equipment_sequence', '=',self.equipment_sequence)])
#         if equipment_id:
#             for equip in equipment_id:
#                 equip.write({
#                     'state': 'bocked',
#                 })
#
#
#       @api.multi
#       def done_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'done',
#         })
#         equipment=self.env['equipment.request']
#         equipment_id=equipment.search([('equipment_sequence', '=',self.equipment_sequence)])
#         if equipment_id:
#             for equip in equipment_id:
#                 equip.write({
#                     'state': 'done',
#                 })
# class logistic_sale_line(models.Model):
#       _name = 'logistic.sale.line'
#       logistic_sale_id = fields.Many2one(comodel_name="maintainance.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
# class logistic_purchase_line(models.Model):
#       _name = 'logistic.purchase.line'
#       logistic_purchase_id = fields.Many2one(comodel_name="maintainance.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
# class logistic_package_line(models.Model):
#       _name = 'logistic.package.line'
#       logistic_package_id = fields.Many2one(comodel_name="maintainance.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
#
# class equipment_request(models.Model):
#       _name = 'equipment.request'
#       # _inherit='maintainance.request'
#
#       equipment_line_id = fields.One2many(comodel_name="equipment.request.line", inverse_name="equipment_id",)
#       equipment_purchase_id = fields.One2many(comodel_name="purchase.request.line", inverse_name="purchase_equipment_id",)
#       package_id = fields.One2many(comodel_name="package.request.line", inverse_name="package_equipment_id",)
#
#       equipment_sequence = fields.Char(string="sequence",readonly=True, )
#       equipment = fields.Many2one('maintenance.equipment', string="equipment", required=False, )
#       request_date = fields.Datetime(string="request date", required=False, )
#       sale_order = fields.Many2one(comodel_name="sale.order", string="sale order", required=False, )
#       purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
#       describtion = fields.Char(string="describtion", required=False, )
#       operation = fields.Selection(string="operation", selection=[('deliver', 'deliver'), ('receipt', 'receipt'), ],default='deliver', required=True,)
#       state = fields.Selection(selection=[('draft', 'request sent'),
#                                                           ('manager', 'manager'),
#                                                           ('waiting', 'waiting bocking'),
#                                                           ('bocked', 'bocked'),
#                                                           ('done', 'done'), ],default='draft',string='State' )
#
#       def _prepare_invoice_line_from_po_line(self, line):
#         data = {
#             'sale_line_id': line.id,
#             'quantity': line.product_uom_qty,
#             'product': line.product_id.id,
#         }
#         return data
#
#       @api.onchange('sale_order')
#       def purchase_order_change(self):
#         if not self.sale_order:
#             return {}
#         new_lines = self.env['equipment.request.line']
#         for line in self.sale_order.order_line - self.equipment_line_id.mapped('sale_line_id'):
#             data = self._prepare_invoice_line_from_po_line(line)
#             new_line = new_lines.new(data)
#             new_line._set_additional_fields(self)
#             new_lines += new_line
#
#         self.equipment_line_id = new_lines
#         return {}
#
#       def _prepare_package_line_from_po_line(self, line):
#         data = {
#             'pack_line_id': line.id,
#             'quantity': line.quantity,
#             'product': line.packaging.id,
#         }
#         return data
#
#       @api.onchange('sale_order')
#       def package_order_change(self):
#         if not self.sale_order:
#             return {}
#         new_lines = self.env['package.request.line']
#         for line in self.sale_order.package_order_line - self.package_id.mapped('pack_line_id'):
#             data = self._prepare_package_line_from_po_line(line)
#             new_line = new_lines.new(data)
#             new_line._package_additional_fields(self)
#             new_lines += new_line
#
#         self.package_id = new_lines
#         return {}
#
#
#       def _prepare_purchase_line_from_po_line(self, line):
#         data = {
#             'purchase_line_id': line.id,
#             'quantity': line.product_qty,
#             'product': line.product_id.id,
#         }
#         return data
#
#       @api.onchange('purchase_order')
#       def purchase_change(self):
#         if not self.purchase_order:
#             return {}
#         new_lines = self.env['purchase.request.line']
#         for line in self.purchase_order.order_line - self.equipment_purchase_id.mapped('purchase_line_id'):
#             data = self._prepare_purchase_line_from_po_line(line)
#             new_line = new_lines.new(data)
#             new_line._set_additional(self)
#             new_lines += new_line
#
#         self.equipment_purchase_id = new_lines
#         return {}
#
#       @api.model
#       def create(self, vals):
#         vals['equipment_sequence'] = self.env['ir.sequence'].next_by_code('equipment.seq')
#         return super(equipment_request, self).create(vals)
#
#       @api.multi
#       def draft_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'draft',
#         })
#
#       @api.multi
#       def manager_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'manager',
#             })
#         maintenance=self.env['maintainance.request']
#         logistic_sale=self.env['logistic.sale.line']
#         logistic_purchase=self.env['logistic.purchase.line']
#         logistic_package=self.env['logistic.package.line']
#
#         if self.operation=='deliver':
#             for maint in self:
#                 main_id=maintenance.create({
#                     'equipment_sequence':maint.equipment_sequence,
#                     'equipment':maint.equipment.id,
#                     'request_date':maint.request_date,
#                     'operation':'deliver',
#                     'sale_order':maint.sale_order.id,
#                     'purchase_order':maint.purchase_order.id,
#                     'describtion':maint.describtion,
#                 })
#                 for line in maint.equipment_line_id:
#                     print "<<<<<<<<< ",line.product.name
#                     print "<<<<<<<<< ",line.quantity
#                     logistic_sale.create({
#                         'logistic_sale_id':main_id.id,
#                         'product':line.product.id,
#                         'quantity':line.quantity,
#                     })
#                 for pack in maint.package_id:
#                     print "<<<<<<<<< ",pack.product.name
#                     print "<<<<<<<<< ",pack.quantity
#                     logistic_package.create({
#                         'logistic_package_id':main_id.id,
#                         'product':pack.product.id,
#                         'quantity':pack.quantity,
#                     })
#
#         elif self.operation=='receipt':
#             print"<<<<<<<<<<<<<< receipt >>>>>>>>>>>>> "
#             for maint in self:
#                 main_id=maintenance.create({
#                     'equipment_sequence':maint.equipment_sequence,
#                     'equipment':maint.equipment.id,
#                     'request_date':maint.request_date,
#                     'operation':'receipt',
#                     'purchase_order':maint.purchase_order.id,
#                     'describtion':maint.describtion,
#                 })
#             for purchase in maint.equipment_purchase_id:
#                     print "<<<<<<<<< ",purchase.product.name
#                     print "<<<<<<<<< ",purchase.quantity
#                     logistic_purchase.create({
#                         'logistic_purchase_id':main_id.id,
#                         'product':purchase.product.id,
#                         'quantity':purchase.quantity,
#                     })
# class equipment_request_line(models.Model):
#       _name = 'equipment.request.line'
#
#       equipment_id = fields.Many2one(comodel_name="equipment.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
#
#       sale_line_id = fields.Many2one('sale.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
#       sale_order = fields.Many2one('sale.order', related='sale_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
#         help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')
#
#       def _set_additional_fields(self,invoice):
#         pass
# class purchase_request_line(models.Model):
#       _name = 'purchase.request.line'
#
#       purchase_equipment_id = fields.Many2one(comodel_name="equipment.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
#
#       purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
#       purchase_order = fields.Many2one('purchase.order', related='purchase_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
#         help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')
#
#       def _set_additional(self,invoice):
#         pass
# class package_request_line(models.Model):
#       _name = 'package.request.line'
#
#       package_equipment_id = fields.Many2one(comodel_name="equipment.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
#
#       pack_line_id = fields.Many2one('package.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
#       sale_order = fields.Many2one('sale.order', related='pack_line_id.package_line', string='Purchase Order', store=False, readonly=True, related_sudo=False,
#         help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')
#
#       def _package_additional_fields(self,invoice):
#         pass
#
#
# class warehouse_request(models.Model):
#       _name = 'warehouse.request'
#
#       warehouse_line = fields.One2many(comodel_name="warehouse.request.line", inverse_name="warehouse_id",)
#       warehouse_sequence = fields.Char(string="warehouse sequence",readonly=True, )
#       warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
#       warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
#       schedule_date = fields.Datetime(string="schedule date", required=False, )
#       state = fields.Selection(selection=[('draft', 'request sent'),
#                                                           ('manager', 'manager'),
#                                                           ('waiting', 'waiting bocking'),
#                                                           ('bocked', 'bocked'),
#                                                           ('done', 'done'), ],default='draft',string='State' )
#
#
#       @api.model
#       def create(self, vals):
#         vals['warehouse_sequence'] = self.env['ir.sequence'].next_by_code('warehouse.seq')
#         return super(warehouse_request, self).create(vals)
#
#       @api.multi
#       def draft_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'draft',
#         })
#
#       @api.multi
#       def manager_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'manager',
#             })
#         warehouse=self.env['warehouse.equipment.request']
#         warehouse_line=self.env['warehouse.equipment.line']
#         for warehouse_data in self:
#             warehouse_id=warehouse.create({
#                 'warehouse_sequence':warehouse_data.warehouse_sequence,
#                 'warehouse_from':warehouse_data.warehouse_from.id,
#                 'warehouse_to':warehouse_data.warehouse_to.id,
#                 'schedule_date':warehouse_data.schedule_date,
#             })
#             for line in warehouse_data.warehouse_line:
#                 warehouse_line.create({
#                 'warehouse_equipment_id':warehouse_id.id,
#                 'product':line.product.id,
#                 'quantity':line.quantity,
#             })
# class warehouse_request_line(models.Model):
#       _name = 'warehouse.request.line'
#
#       warehouse_id = fields.Many2one(comodel_name="warehouse.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
#
#
# class warehouse_equipment_request(models.Model):
#       _name = 'warehouse.equipment.request'
#
#       warehouse_equipment_lines = fields.One2many(comodel_name="warehouse.equipment.line", inverse_name="warehouse_equipment_id",)
#       warehouse_equipment_sequence = fields.Char(string="equipment sequence",readonly=True, )
#       warehouse_sequence = fields.Char(string="warehouse sequence",readonly=True, )
#       warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
#       warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
#       schedule_date = fields.Datetime(string="schedule date", required=False, )
#       state = fields.Selection(selection=[('waiting', 'waiting bocking'),
#                                           ('approved', 'approved'),
#                                           ('done', 'done'), ],default='waiting',string='State' )
#
#
#       @api.model
#       def create(self, vals):
#         vals['warehouse_equipment_sequence'] = self.env['ir.sequence'].next_by_code('warequ.seq')
#         return super(warehouse_equipment_request, self).create(vals)
#
#       @api.multi
#       def waiting_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'waiting',
#         })
#
#         equip_warehouse=self.env['warehouse.request']
#         warehouse_id=equip_warehouse.search([('warehouse_sequence', '=',self.warehouse_sequence)])
#         if warehouse_id:
#             print">>>>>>>>>>>>>>>>>>>>> 1"
#             for equip in warehouse_id:
#                 equip.write({
#                     'state': 'waiting',
#                 })
#
#       @api.multi
#       def approved_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'approved',
#             })
#         print">>>>>>>>>>>>>>>>>>>>> 2"
#         equip_warehouse=self.env['warehouse.request']
#         warehouse_id=equip_warehouse.search([('warehouse_sequence', '=',self.warehouse_sequence)])
#         if warehouse_id:
#             for equip in warehouse_id:
#                 equip.write({
#                     'state': 'bocked',
#                 })
#
#       @api.multi
#       def done_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'done',
#             })
#         print">>>>>>>>>>>>>>>>>>>>> 3"
#         equip_warehouse=self.env['warehouse.request']
#         warehouse_id=equip_warehouse.search([('warehouse_sequence', '=',self.warehouse_sequence)])
#         if warehouse_id:
#             for equip in warehouse_id:
#                 equip.write({
#                     'state': 'done',
#                 })
# class warehouse_equipment_line(models.Model):
#       _name = 'warehouse.equipment.line'
#
#       warehouse_equipment_id = fields.Many2one(comodel_name="warehouse.equipment.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
#
# class manufacturing_equipment(models.Model):
#
#       _name = 'manufacturing.equipment'
#
#       manufacturing_line = fields.One2many(comodel_name="manufacturing.request.line", inverse_name="manufacturing_id",)
#
#       manufacturing_sequence = fields.Char(string="warehouse sequence",readonly=True, )
#       purchase_id = fields.Many2one(comodel_name="mrp.production", string="Manufactoring order", required=False, )
#       warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
#       warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
#       schedule_date = fields.Datetime(string="schedule date", required=False, )
#       describtion = fields.Text(string="describtion", required=False, )
#       state = fields.Selection(selection=[('draft', 'request sent'),
#                                           ('manager', 'manager'),
#                                           ('waiting', 'waiting bocking'),
#                                           ('bocked', 'bocked'),
#                                           ('done', 'done'), ],default='draft',string='State' )
#
#       def _prepare_invoice_line_from_po_line(self, line):
#         data = {
#             'purchase_line_id': line.id,
#             'quantity': line.product_uom_qty,
#             'product': line.product_id.id,
#         }
#         return data
#
#       @api.onchange('purchase_id')
#       def purchase_order_change(self):
#         if not self.purchase_id:
#             return {}
#         new_lines = self.env['manufacturing.request.line']
#         for line in self.purchase_id.move_finished_ids - self.manufacturing_line.mapped('purchase_line_id'):
#             data = self._prepare_invoice_line_from_po_line(line)
#             new_line = new_lines.new(data)
#             new_line._set_additional_fields(self)
#             new_lines += new_line
#         self.manufacturing_line = new_lines
#         return {}
#
#
#
#
#       @api.model
#       def create(self, vals):
#         vals['manufacturing_sequence'] = self.env['ir.sequence'].next_by_code('manufacturing.seq')
#         return super(manufacturing_equipment, self).create(vals)
#
#       @api.multi
#       def draft_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'draft',
#         })
#
#       @api.multi
#       def manager_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'manager',
#             })
#         manufacturing = self.env['manufacturing.equipment.request']
#         manufacturing_lines=self.env['manufacturing.equipment.line']
#         for manufacturing_data in self:
#             manufacturing_id=manufacturing.create({
#                 'purchase_id':manufacturing_data.purchase_id.id,
#                 'manufacturing_sequence':manufacturing_data.manufacturing_sequence,
#                 'warehouse_from':manufacturing_data.warehouse_from.id,
#                 'warehouse_to':manufacturing_data.warehouse_to.id,
#                 'schedule_date':manufacturing_data.schedule_date,
#                 'describtion':manufacturing_data.describtion,
#             })
#             for line in manufacturing_data.manufacturing_line:
#                 manufacturing_lines.create({
#                 'manufacturing_id':manufacturing_id.id,
#                 'product':line.product.id,
#                 'quantity':line.quantity,
#             })
# class manufacturing_request_line(models.Model):
#       _name = 'manufacturing.request.line'
#
#       manufacturing_id = fields.Many2one(comodel_name="manufacturing.equipment",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
#
#       purchase_line_id = fields.Many2one('stock.move', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
#       purchase_id = fields.Many2one('mrp.production', related='purchase_line_id.production_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
#         help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')
#
#       def _set_additional_fields(self,invoice):
#          pass
#
# class manufacturing_equipment_request(models.Model):
#       _name = 'manufacturing.equipment.request'
#
#       timer_equipment_lines = fields.One2many(comodel_name="timer.equipment.line", inverse_name="timer_id",)
#       manufacturing_equipment_lines = fields.One2many(comodel_name="manufacturing.equipment.line", inverse_name="manufacturing_id",)
#       manufacturing_equipment_sequence = fields.Char(string="equipment sequence",readonly=True, )
#       manufacturing_sequence = fields.Char(string="manufactoring sequence",readonly=True, )
#       purchase_id = fields.Many2one(comodel_name="mrp.production", string="Manufactoring order", required=False, )
#       warehouse_from = fields.Many2one(comodel_name="stock.picking.type", string="warehouse from", required=False, )
#       warehouse_to = fields.Many2one(comodel_name="stock.picking.type", string="warehouse to", required=False, )
#       schedule_date = fields.Datetime(string="schedule date", required=False, )
#       state = fields.Selection(selection=[('waiting', 'waiting bocking'),
#                                           ('approved', 'approved'),
#                                           ('done', 'done'), ],default='waiting',string='State' )
#       describtion = fields.Text(string="describtion", required=False, )
#
#       start_date = fields.Datetime(string="Start Date", required=False, )
#       end_date = fields.Datetime(string="End Date", required=False, )
#
#       # second = fields.Float(string="second",  required=False,default=0, )
#       # minutes = fields.Float(string="minutes",  required=False,default=0,compute='timer_progressbar', )
#       # time_start = time.time()
#
#
#       @api.model
#       def create(self, vals):
#         vals['manufacturing_equipment_sequence'] = self.env['ir.sequence'].next_by_code('manufacturing.eq')
#         return super(manufacturing_equipment_request, self).create(vals)
#
#
#       x=datetime.datetime.now()
#       y=datetime.datetime.now()
#       @api.multi
#       def start_order(self):
#           print">>>>>>>> start"
#           global x
#           global y
#           x=datetime.datetime.now()
#           y=datetime.datetime.now()
#           for rec in self:
#               rec.write({
#                   'start_date':x,
#               })
#               print ">>>>>>>>>>> date ",x.time().strftime('%H:%M:%S')
#
#       totaldays = fields.Char(string="Total", required=False,compute='_amount_all' )
#
#       @api.one
#       @api.depends('timer_equipment_lines.duration')
#       def _amount_all(self):
#         for rec in self:
#             # totalday='0:0:0'
#             # totaldays=datetime.datetime.strptime(totalday,'%H:%M:%S')
#             # for line in rec.timer_equipment_lines:
#             #     totaldays += line.duration
#             # rec.update({
#             #     'totaldays':totaldays,
#             # })
#             sum = datetime.timedelta()
#             cumulative=[]
#             d=[]
#             for l in rec.timer_equipment_lines:
#                 (h,m,s)=l.duration.split(':')
#                 sum+= datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
#                 # for i in l.duration:
#                 #     # cumulative +=i
#                 #     print"append",i
#                 #     d.append(i)
#                 # print"append :d ",d
#                 # cumulative.append(i)
#                     # for line in cumulative:
#                     #     d.append(line)
#                         # (h,m,s) = line.split(':')
#                         # d = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
#                 print ">>>>>>>>>>> cumulative : ",cumulative
#             self.totaldays=str(sum)
#             # print ">>>>>>>>>>> d : ",d
#                 #     (h, m, s) = i.split(':')
#                 #     d = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
#                 #     sum += d
#                 # print(str(sum))
#
#       @api.multi
#       def pause_order(self):
#           print">>>>>>>> pause"
#           # x=datetime.datetime.now()
#           # print type(x)
#
#           time=self.env['timer.equipment.line']
#           for rec in self:
#               y=datetime.datetime.now()
#               rec.write({
#                   'end_date':datetime.datetime.now(),
#               })
#               z=y.time().strftime('%H:%M:%S')
#               zz=datetime.datetime.strptime(z,'%H:%M:%S')
#               # print ">>>>>>>>>>> date pause ",z
#               # print ">>>>>>>>>>> date y ",type(y)
#               # print ">>>>>>>>>>> date z ",type(z)
#               # print ">>>>>>>>>>> date start ",type(rec.start_date)
#               # print ">>>>>>>>>>> date start ",rec.start_date[11:]
#               # print ">>>>>>>>>>> date zz ",datetime.datetime.strptime(z,'%H:%M:%S')
#               # print ">>>>>>>>>>> date zzstart ",datetime.datetime.strptime(rec.start_date[11:],'%H:%M:%S')
#               c= datetime.datetime.strptime(z,'%H:%M:%S') - (datetime.datetime.strptime(rec.start_date[11:],'%H:%M:%S'))
#               print ">>>>>>>>>>>>>>>>>>> c",c
#               time.create({
#                   'timer_id':self.id,
#                   'start_date':rec.start_date,
#                   'end_date':y,
#                   'duration':c,
#               })
#
#
#       # @api.multi
#       # def continue_order(self):
#       #     print">>>>>>>> continue"
#
#       @api.multi
#       def finish_order(self):
#           print">>>>>>>> finish"
#
#
#       # @api.multi
#       # def timer_progressbar(self):
#       #   self.ensure_one()
#         # time_start = time.time()
#         # x = 2
#         # # seconds = 0
#         # # minutes = 0
#         # if x == 2:
#         #     timeLoop = True
#         #     for rec in self:
#         #         while timeLoop:
#         #             try:
#         #                 # sys.stdout.write("\r{minutes} Minutes {seconds} Seconds".format(minutes=self.minutes, seconds=self.second))
#         #                 sys.stdout.flush()
#         #                 time.sleep(1)
#         #                 rec.second = int(time.time() - time_start) - rec.minutes * 60
#         #                 if rec.second >= 60:
#         #                     rec.minutes += 1
#         #                     rec.second = 0
#         #             except KeyboardInterrupt, e:
#         #                 break
#         # while self.minutes <= 100:
#         #     time.sleep(1)
#         #     self.second += 1
#         #     print ">>>>>>>> second ",self.second
#
#
#       @api.multi
#       def waiting_progressbar(self):
#         self.ensure_one()
#         self.write({
#             'state': 'waiting',
#         })
#
#         equip_manufacturing=self.env['manufacturing.equipment']
#         equip_manufacturing_id=equip_manufacturing.search([('manufacturing_sequence', '=',self.manufacturing_sequence)])
#         if equip_manufacturing_id:
#             for equip in equip_manufacturing_id:
#                 equip.write({
#                     'state': 'waiting',
#                 })
#
#       @api.multi
#       def approved_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'approved',
#             })
#         equip_manufacturing=self.env['manufacturing.equipment']
#         equip_manufacturing_id=equip_manufacturing.search([('manufacturing_sequence', '=',self.manufacturing_sequence)])
#         if equip_manufacturing_id:
#             for equip in equip_manufacturing_id:
#                 equip.write({
#                     'state': 'bocked',
#                 })
#
#       @api.multi
#       def done_progressbar(self):
#         self.ensure_one()
#         self.write({
#                 'state': 'done',
#             })
#         equip_manufacturing=self.env['manufacturing.equipment']
#         equip_manufacturing_id=equip_manufacturing.search([('manufacturing_sequence', '=',self.manufacturing_sequence)])
#         if equip_manufacturing_id:
#             for equip in equip_manufacturing_id:
#                 equip.write({
#                     'state': 'done',
#                 })
# class manufacturing_equipment_line(models.Model):
#       _name = 'manufacturing.equipment.line'
#
#       manufacturing_id = fields.Many2one(comodel_name="manufacturing.equipment.request",)
#       product = fields.Many2one(comodel_name="product.product", string="product",)
#       quantity = fields.Float(string="quantity",)
# class timer_equipment_line(models.Model):
#       _name = 'timer.equipment.line'
#
#       timer_id = fields.Many2one(comodel_name="manufacturing.equipment.request",)
#       start_date = fields.Datetime(string="start date", required=False, )
#       end_date = fields.Datetime(string="end date", required=False, )
#       duration = fields.Char(string="duration", required=False, )
