# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons import decimal_precision as dp
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
from odoo import tools, _
import time
import babel
import logging


class Department(models.Model):
    _name = "hr.department"

    _inherit = "hr.department"

    name = fields.Char('Department Name', required=True, translate=True)


class Employee(models.Model):
    _name = "hr.employee"
    _inherit = "hr.employee"

    sin_exist = fields.Boolean("Has Social Insurance")
    sin_no = fields.Char("Social Insurance No", )
    sin_date = fields.Date("Social Insurance Date")
    sin_end_date = fields.Date("Social Insurance  end Date")
    mi_exist = fields.Boolean("Has Medical Insurance")
    mi_no = fields.Char(string="Medical Insurance NO", help='Medical  Insurance No')
    mi_date = fields.Date(string="Medical Insurance Date", help='Medical  Insurance Date')
    religion = fields.Selection([('mus', 'Muslem'), ('chris', 'Christian'), ('other', 'Others')], string="Religion")
    military_status = fields.Selection(
        [('not_req', 'Not Required'), ('post', 'Postponed'), ('complete', 'complete'), ('exemption', 'Exemption'),
         ('current', 'Currently serving ')], string="Military Status")
    age = fields.Integer(string="Age", compute="_calculate_age", readonly=True)
    basic_salary = fields.Float(string="Basic Salary", digits=dp.get_precision('Payroll'))
    variable_salary = fields.Float(string="Variable Salary", digits=dp.get_precision('Payroll'))
    allowances = fields.Float(string="Allowances", digits=dp.get_precision('Payroll'))
    prev_raise = fields.Float(string="previous Annual Raises", digits=dp.get_precision('Payroll'))
    other_alw_ids = fields.One2many(comodel_name="hr.other_alw_line", inverse_name="employee_id", string="Other Allowances")

    start_date = fields.Date(string="Start Working At")
    edu_phase = fields.Many2one(comodel_name="hr.eg.education", string="Education")

    edu_school = fields.Many2one(comodel_name="hr.eg.school", string="School/University/Institute")
    edu_major = fields.Char(string="major")
    edu_grad = fields.Selection([('ex', 'Excellent'), ('vgod', 'Very Good'), ('god', 'Good'), ('pas', 'Pass')],
                                string="Grad")
    edu_note = fields.Text("Education Notes")
    experience_y = fields.Integer(compute="_calculate_experience", string="Experience",
                                  help="experience in our company", store=True)
    experience_m = fields.Integer(compute="_calculate_experience", string="Experience monthes", store=True)
    experience_d = fields.Integer(compute="_calculate_experience", string="Experience dayes", store=True)

    @api.depends("birthday")
    def _calculate_age(self):
        for emp in self.search([]):
            if emp.birthday:
                dob = datetime.strptime(emp.birthday, "%Y-%m-%d")
                emp.age = int((datetime.today() - dob).days / 365)

    @api.depends("start_date")
    def _calculate_experience(self):
        for emp in self.search([]):
            if emp.start_date:
                date_format = '%Y-%m-%d'
                current_date = (datetime.today()).strftime(date_format)
                d1 = datetime.strptime(emp.start_date, date_format).date()
                d2 = datetime.strptime(current_date, date_format).date()
                r = relativedelta(d2, d1)
                emp.experience_y = r.years
                emp.experience_m = r.months
                emp.experience_d = r.days






    def get_alw(self, alw_code):
        alw_id = self.other_alw_ids.filtered(lambda x: x.code == alw_code)
        return alw_id

    @api.model
    def _cron_employee_age(self):
        self._calculate_age()

    @api.model
    def _cron_employee_exp(self):
        self._calculate_experience()



class hr_other_alw_line(models.Model):
    _name = "hr.other_alw_line"

    alw_id = fields.Many2one(comodel_name="hr.other_alw",string="name", required=True, translate=True)
    code = fields.Char(string="Code", required=True)
    amount = fields.Float(string="Amount", required=True)
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee")

    @api.onchange('alw_id')
    def onchange_parts(self):
        self.code = self.alw_id.code
        self.amount = self.alw_id.amount




class hr_other_alw(models.Model):
    _name = "hr.other_alw"

    name = fields.Char(string="name", required=True, translate=True)
    code = fields.Char(string="Code", required=True)
    amount = fields.Float(string="Amount")
    # employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee")


class dhn_hr__eg_education(models.Model):
    _name = "hr.eg.education"

    name = fields.Char(string="Education", translate=True)
    note = fields.Char(string="Note", required=False, )


class hr_eg_school(models.Model):
    _name = "hr.eg.school"

    name = fields.Char(string="School name", translate=True)
    note = fields.Char(string="Note", required=False, )


class HrSalaryRuleCategory(models.Model):
    _name = 'hr.salary.rule.category'
    _inherit = 'hr.salary.rule.category'

    name = fields.Char(required=True, translate=True)


class HrSalaryRule(models.Model):
    _name = 'hr.salary.rule'
    _inherit = 'hr.salary.rule'

    name = fields.Char(required=True, translate=True)


class HrRuleInput(models.Model):
    _name = 'hr.rule.input'
    _inherit = 'hr.rule.input'

    name = fields.Char(string='Description', required=True, translate=True)


class ResourceResource(models.Model):
    _name = "resource.resource"
    _inherit = "resource.resource"

    name = fields.Char(required=True, translate=True)


class HrPayslip(models.Model):
    _name = 'hr.payslip'
    _inherit = 'hr.payslip'

    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):

        if (not self.employee_id) or (not self.date_from) or (not self.date_to):
            return

        employee = self.employee_id
        date_from = self.date_from
        date_to = self.date_to

        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        locale = self.env.context.get('lang', 'en_US')
        logging.warning(locale)
        if locale == "ar_SY":
            locale = "ar"
            logging.warning(locale)

        # self.name = _('Salary Slip of %s for %s') % (
        # employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))
        self.name = _('Salary Slip for %s') % (
            tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))
        self.company_id = employee.company_id

        if not self.env.context.get('contract') or not self.contract_id:
            contract_ids = self.get_contract(employee, date_from, date_to)
            if not contract_ids:
                return
            self.contract_id = self.env['hr.contract'].browse(contract_ids[0])

        if not self.contract_id.struct_id:
            return
        self.struct_id = self.contract_id.struct_id

        # computation of the salary input
        worked_days_line_ids = self.get_worked_day_lines(contract_ids, date_from, date_to)
        worked_days_lines = self.worked_days_line_ids.browse([])
        for r in worked_days_line_ids:
            worked_days_lines += worked_days_lines.new(r)
        self.worked_days_line_ids = worked_days_lines

        input_line_ids = self.get_inputs(contract_ids, date_from, date_to)
        input_lines = self.input_line_ids.browse([])
        for r in input_line_ids:
            input_lines += input_lines.new(r)
        self.input_line_ids = input_lines
        return
