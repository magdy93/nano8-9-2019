# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from odoo.exceptions import UserError , ValidationError

class ProductTemplate(models.Model):
    _inherit = "product.template"

    landed_cost_ok = fields.Boolean(help="Specify whether the product can be selected in an HR expense.", string="landed cost ok")

class ProductTemplate(models.Model):
    _inherit = "hr.expense.sheet"

    @api.multi
    def approve_expense_sheets(self):
        if not self.user_has_groups('hr_expense.group_hr_expense_user'):
            raise UserError(_("Only HR Officers can approve expenses"))
        for rec in self:
            for line in rec.expense_line_ids:
                print "lineeeeeeeee",line.purchase_order.id
                print "lineeeeeeeee name",line.purchase_order.name
                stock_picking_id=self.env['stock.picking'].search([('purchase_id','=',line.sudo().purchase_order.id)])
                print "stock_picking_id :",stock_picking_id.id
                if stock_picking_id:
                    landed_cost_id=self.sudo().env['stock.landed.cost'].search([])
                    for raw in landed_cost_id:
                        for r in raw.picking_ids:
                             print"landed_cost_id :",r.name
                             print"raw_id :",raw.id
                             if stock_picking_id.id == r.id:
                                landed_cost_line=self.sudo().env['stock.landed.cost.lines']
                                landed_cost_line.sudo().create({
                                    'cost_id':raw.id,
                                    'split_method':'equal',
                                    'price_unit':line.total_amount,
                                    'product_id':line.product_id.id,
                                    'name':line.product_id.name,
                                    'account_id':line.account_id.id,
                                })
        self.write({'state': 'approve', 'responsible_id': self.env.user.id})

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    is_landed_cost_ok = fields.Boolean(help="Specify whether the product can be selected in an HR expense.", string="landed cost ok")
    landed_cost_sequence = fields.Char(string="landed cost sequence", required=False, )

    # @api.multi
    # def button_confirm(self):
    #     res = super(PurchaseOrder, self).button_confirm()
    #     if self.is_landed_cost_ok == False:
    #         print ">>>>>>>>>>>>>>> : not landed"
    #         return res
    #     else:
    #         print ">>>>>>>>>>>>>> : landed"
    #
    #         for order in self:
    #             if order.state not in ['draft', 'sent']:
    #                 continue
    #             order._add_supplier_to_product()
    #             # Deal with double validation process
    #             if order.company_id.po_double_validation == 'one_step'\
    #                     or (order.company_id.po_double_validation == 'two_step'\
    #                         and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id))\
    #                     or order.user_has_groups('purchase.group_purchase_manager'):
    #                 order.button_approve()
    #             else:
    #                 order.write({'state': 'to approve'})
    #
    #         stock_picking_id=self.env['stock.picking'].search([('purchase_id','=',self.id)])
    #         print "stock_picking_id :",stock_picking_id.name
    #         # if stock_picking_id:
    #         landed_cost=self.env['stock.landed.cost']
    #         landed_cost.create({
    #             'date':fields.Datetime.now() ,
    #             'picking_ids':[(4,stock_picking_id.id) if stock_picking_id else None],
    #             'account_journal_id':1,
    #         })
    #         return True
    #         # return res

class StockLandedCost(models.Model):
    _inherit = "stock.landed.cost"

    @api.multi
    def button_validate(self):
        # print "pickingggggggggg : ",self.picking_ids.name
        stock_picking=self.sudo().env['stock.picking'].sudo().search([('name','=',self.sudo().picking_ids.name)])
        if stock_picking:
            # print"ooooooooo ",stock_picking.origin
            # print"ooooooooo ",stock_picking.state
            if stock_picking.state =='done':
                logistic_purchase=self.sudo().env['logistic.purchase'].sudo().search([('purchase_order.name','=',stock_picking.sudo().origin)])
                if logistic_purchase:
                    # print "logistic purchase : ",logistic_purchase.id
                    # print "logistic purchase : ",logistic_purchase.stat
                    if logistic_purchase.stat == 'done':
                        custom_purchase=self.sudo().env['nano.custom'].sudo().search([('logistic_id','=',logistic_purchase.id)])
                        if custom_purchase:
                            # print "custom purchase : ",custom_purchase.id
                            # print "custom purchase : ",custom_purchase.import_request
                            if custom_purchase.stat == 'done':
                               res = super(StockLandedCost,self).button_validate()
                               return res
                            else:
                                raise UserError(_('the state of custom is not Done.'))
                    else:
                        raise UserError(_('the state of logistic is not Done.'))

            else:
                raise UserError(_('the state of Warehouse is not Done.'))