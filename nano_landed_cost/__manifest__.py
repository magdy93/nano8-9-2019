# -*- coding: utf-8 -*-
{
    'name': "nano landed cost",

    'summary': """
        nano landed cost""",

    'description': """
        nano landed cost
    """,

    'author': "USS",
    'website': "http://www.yourcompany.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','purchase','stock','hr_expense','stock_landed_costs'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}