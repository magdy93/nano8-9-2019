from odoo import models, fields, api,_
from openerp.exceptions import UserError

class HrExpense(models.Model):
    _inherit = 'hr.expense'
    lc_id = fields.Many2one(comodel_name="nano.lc", string="LC", required=False, )
    vendor_id = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )
    payment_mode = fields.Selection(selection_add=[('lc', 'LC'),('petty_cash', 'Petty Cash')])

    @api.multi
    def action_move_create(self):
        res = super(HrExpense,self).action_move_create()
        if self.payment_mode == 'lc':
            '''
            main function that is called when trying to create the accounting entries related to an expense
            '''
            for expense in self:
                journal = expense.sheet_id.bank_journal_id if expense.payment_mode == 'company_account' else expense.sheet_id.journal_id
                #create the move that will contain the accounting entries
                acc_date = expense.sheet_id.accounting_date or expense.date
                move = self.env['account.move'].create({
                    'journal_id': journal.id,
                    'company_id': self.env.user.company_id.id,
                    'date': acc_date,
                    'ref': expense.sheet_id.name,
                    # force the name to the default value, to avoid an eventual 'default_name' in the context
                    # to set it to '' which cause no number to be given to the account.move when posted.
                    'name': '/',
                })
                company_currency = expense.company_id.currency_id
                diff_currency_p = expense.currency_id != company_currency
                #one account.move.line per expense (+taxes..)
                move_lines = expense._move_line_get()

                #create one more move line, a counterline for the total on payable account
                payment_id = False
                total, total_currency, move_lines = expense._compute_expense_totals(company_currency, move_lines, acc_date)
                if expense.payment_mode == 'company_account':
                    if not expense.sheet_id.bank_journal_id.default_credit_account_id:
                        raise UserError(_("No credit account found for the %s journal, please configure one.") % (expense.sheet_id.bank_journal_id.name))
                    emp_account = expense.sheet_id.bank_journal_id.default_credit_account_id.id
                    journal = expense.sheet_id.bank_journal_id
                    #create payment
                    payment_methods = (total < 0) and journal.outbound_payment_method_ids or journal.inbound_payment_method_ids
                    journal_currency = journal.currency_id or journal.company_id.currency_id
                    payment = self.env['account.payment'].create({
                        'payment_method_id': payment_methods and payment_methods[0].id or False,
                        'payment_type': total < 0 and 'outbound' or 'inbound',
                        'partner_id': expense.employee_id.address_home_id.commercial_partner_id.id,
                        'partner_type': 'supplier',
                        'journal_id': journal.id,
                        'payment_date': expense.date,
                        'state': 'reconciled',
                        'currency_id': diff_currency_p and expense.currency_id.id or journal_currency.id,
                        'amount': diff_currency_p and abs(total_currency) or abs(total),
                        'name': expense.name,
                    })
                    payment_id = payment.id
                else:
                    if not expense.employee_id.address_home_id:
                        raise UserError(_("No Home Address found for the employee %s, please configure one.") % (expense.employee_id.name))
                    emp_account = expense.employee_id.address_home_id.property_account_payable_id.id

                aml_name = expense.employee_id.name + ': ' + expense.name.split('\n')[0][:64]
                get_vendor = self.get_vendor_account(self.vendor_id)
                move_lines.append({
                        'type': 'dest',
                        'name': aml_name,
                        'price': total,
                        'account_id': get_vendor,
                        'date_maturity': acc_date,
                        'amount_currency': diff_currency_p and total_currency or False,
                        'currency_id': diff_currency_p and expense.currency_id.id or False,
                        'payment_id': payment_id,
                        })

                #convert eml into an osv-valid format
                lines = map(lambda x: (0, 0, expense._prepare_move_line(x)), move_lines)
                move.with_context(dont_create_taxes=True).write({'line_ids': lines})
                expense.sheet_id.write({'account_move_id': move.id})
                move.post()
                if expense.payment_mode == 'company_account':
                    expense.sheet_id.paid_expense_sheets()
            return True

        else:
            return res

    @api.multi
    def get_vendor_account(self,vendor):
        vendor = vendor
        if vendor.parent_id:
            vendor = vendor.parent_id
            return self.get_vendor_account(vendor)
        else:
            account= vendor.lc_account.id
        return account
    def _prepare_move_line(self, line):
        res = super(HrExpense,self)._prepare_move_line(line)
        if self.payment_mode == 'lc':
            '''
            This function prepares move line of account.move related to an expense
            '''
            partner_id = self.employee_id.address_home_id.commercial_partner_id.id
            return {
                'date_maturity': line.get('date_maturity'),
                'partner_id': self.vendor_id.id,
                # 'petty_id': self.petty_expense_id.id,
                'name': line['name'][:64],
                'debit': line['price'] > 0 and line['price'],
                'credit': line['price'] < 0 and - line['price'],
                'account_id': line['account_id'],
                'analytic_line_ids': line.get('analytic_line_ids'),
                'amount_currency': line['price'] > 0 and abs(line.get('amount_currency')) or - abs(line.get('amount_currency')),
                'currency_id': line.get('currency_id'),
                'tax_line_id': line.get('tax_line_id'),
                'tax_ids': line.get('tax_ids'),
                'quantity': line.get('quantity', 1.00),
                'product_id': line.get('product_id'),
                'product_uom_id': line.get('uom_id'),
                'analytic_account_id': line.get('analytic_account_id'),
                'payment_id': line.get('payment_id'),
            }
        else:
            return res