from odoo import models, fields, api ,_
from werkzeug.routing import ValidationError
from odoo.exceptions import UserError


class nano_lc(models.Model):
      _name='nano.lc'
      _inherit = ['mail.thread']
      lc_order_line=fields.One2many('lc.order.line','order_line')
      lc_document_line=fields.One2many('lc.document.line','order_line')

      lc_order_required = fields.One2many(comodel_name="lc.required.line", inverse_name="order_id", string="required", required=False, )
      lc_order_send = fields.One2many(comodel_name="lc.send.vendor", inverse_name="order_id", string="send", required=False, )
      account_payment_ids = fields.One2many(comodel_name="account.payment", inverse_name="lc_id", string="lc", required=False, )
      expense_ids = fields.One2many(comodel_name="hr.expense", inverse_name="lc_id", string="lc", required=False, )

      # lc_term_line=fields.One2many('lc.term.line','order_line')
      lc_request = fields.Char(string="lc Request", readonly=True, )
      purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
      request_date = fields.Date(string="Request Date", required=False,default=fields.Date.today )
      warehouse = fields.Char(string="Warehouse State", required=False, )
      logistic = fields.Char(string="logistic state", required=False, )
      custom = fields.Char(string="To Do State", required=False, )
      payment_term = fields.Many2one(comodel_name="account.payment.term", string="Payment Term", required=False, )
      incoterms = fields.Many2one(comodel_name="stock.incoterms", string="Incoterms",readonly=True, )
      amount_total = fields.Float(string="Amount Total", compute='get_amount_total', )
      is_lc = fields.Boolean(string="Is Lc", )

      stat = fields.Selection([
        ('fill_application', 'Fill application'),
        ('sent_to_bank', 'Sent to bank'),
        ('received_by_bank', 'Received by bank'),
        ('received_draft', 'Received draft'),
        ('file_sent_to_purchase', 'File sent to purchase'),
        ('file_sent_to_vendor', 'File sent to vendor'),
        ('lc_opened_by_bank', 'lc opened by bank'),
        ('vendor_approved', 'Vendor approved'),
        ('partial_paid', 'Partial paid'),
        ('paid', 'Paid'),
        ('amendment_of_lc', 'Amendment Of LC'),
        ('document_sent_to_custom', 'Document sent to vendor'),
        ],default='fill_application', string='Status',)

      @api.multi
      @api.depends('account_payment_ids.amount')
      def get_amount_total(self):
          for record in self:
              self.amount_total = sum(line.amount for line in record.account_payment_ids)

      # @api.multi
      # def draft_progressbar(self):
      #   self.ensure_one()
      #   self.write({
      #       'stat': 'draft',
      #   })
      # @api.multi
      # def todo_progressbar(self):
      #   self.ensure_one()
      #   self.write({
      #       'stat': 'todo',
      #   })
      @api.multi
      def save_progressbar(self):
          self.ensure_one()
          self.write({
            'stat': 'fill_application',
          })
          self.message_post(body='Fill application',subtype='mt_comment',
                                              subject='test',message_type='notification')

      @api.multi
      def send_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'sent_to_bank',
        })
        self.message_post(body='sent to bank',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def bank_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'received_by_bank',
        })
        self.message_post(body='Received by bank',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def receive_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'received_draft',
        })
        self.message_post(body='Received draft',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def treasurey_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'file_sent_to_purchase',
        })
        self.message_post(body='File sent to purchase',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def file_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'file_sent_to_vendor',
        })
        self.message_post(body='File sent to vendor',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def opened_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'lc_opened_by_bank',
        })
        self.message_post(body='lc opened by bank',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def approve_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'vendor_approved',
        })
        self.message_post(body='Vendor approved',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def partial_paid_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'partial_paid',
        })
        self.message_post(body='Partial paid',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def paid_progressbar(self):
        self.ensure_one()
        if self.amount_total == self.purchase_order.amount_total:
            self.write({
                'stat': 'paid',
                'is_lc': True,
            })
            self.message_post(body='paid',subtype='mt_comment',
                                              subject='test',message_type='notification')
        else:
            raise UserError(_('You Should Be Fully Paid'))
      @api.multi
      def amendment_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'amendment_of_lc',
        })
        self.message_post(body='Amendment Of LC',subtype='mt_comment',
                                              subject='test',message_type='notification')
      @api.multi
      def document_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'document_sent_to_custom',
        })
        self.message_post(body='Document sent to custom',subtype='mt_comment',
                                              subject='test',message_type='notification')

      def open_advance_payment_term(self):
        return {
                'type': 'ir.actions.act_window',
                'name': 'Payment Term',
                'res_model':'account.payment',
                'context': {
                            'default_lc_id': self.id,
                            'default_partner_type': 'supplier',
                            'default_payment_type': 'outbound',
                            'default_partner_id': self.purchase_order.partner_id.id,
                            'default_is_lc': True,
                            'default_communication': self.lc_request,
                            },
                'view_type': 'form',
                'view_mode': 'form',
                'ref_id': 'account.view_account_payment_tree',
                'target': 'new',
                 }
      def open_lc_expense(self):
          return {
                'type': 'ir.actions.act_window',
                'name': 'Expense',
                'res_model':'hr.expense',
                'context': {
                            'default_lc_id': self.id,
                            'default_vendor_id': self.purchase_order.partner_id.id,
                            'default_payment_mode': 'lc',
                            'default_so_po': 'purchase',
                            'default_purchase_order': self.purchase_order.id,
                            },
                'view_type': 'form',
                'view_mode': 'form',
                # 'ref_id': 'account.view_account_payment_tree',
                'target': 'new',
                 }
      @api.multi
      def warehouse_state(self):
          stok=self.env['stock.picking']
          stok_id=stok.search([('purchase_id', '=',self.purchase_order.id)])
          if stok_id:
             for attend in stok_id:
                 self.warehouse=attend.state

      @api.multi
      def custom_state(self):
          stk=self.env['nano.custom']
          stk_id=stk.search([('purchase_order', '=',self.purchase_order.id)])
          if stk_id:
             for att in stk_id:
                 self.custom=att.stat

      @api.multi
      def logistic_state(self):
          print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> logistic state"
          stk1=self.env['logistic.purchase']
          stk1_id=stk1.search([('purchase_order', '=',self.purchase_order.id)])
          if stk1_id:
             for att1 in stk1_id:
                 self.logistic=att1.stat

      @api.multi
      def lc_state(self):
          carrr=self.env['lc.process']
          for emp in self:
              cary_id=carrr.create({
                    'purchase_order':emp.purchase_order.id,
                    'name':emp.lc_request,
                    'vendor':emp.purchase_order.partner_id.id,
                    'payment_method_id':1,
                })

              return {
                    'type': 'ir.actions.act_window',
                    'name': 'nano_lc.lc_process_view',
                    'res_model': 'lc.process',
                    'res_id': cary_id.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }

      @api.model
      def create(self, vals):
        vals['lc_request'] = self.env['ir.sequence'].next_by_code('requ.seq')
        return super(nano_lc, self).create(vals)
class lc_order_line(models.Model):
    _name = 'lc.order.line'
    order_line=fields.Many2one('nano.lc')
    product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
    describtion=fields.Char(string='Describtion')
    quantity = fields.Float(string="order qty",  required=False,)
    delivery = fields.Float(string="Delivery",  required=False, )
    invoiced = fields.Float(string="Invoiced",  required=False, )
    price = fields.Float(string="unit price",  required=False, )
    tax = fields.Many2one(comodel_name="account.tax", string="Tax", required=False, )
    subtotal = fields.Float(string="Subtotal",  required=False, )
class lc_document_line(models.Model):
    _name = 'lc.document.line'
    order_line=fields.Many2one('nano.lc')
    document = fields.Char(string = 'Document')
    doc_status = fields.Selection([('required', 'required'), ('received', 'received')], string = 'Status')
    attach = fields.Binary(string="attach", )
    # attach = fields.Many2many('ir.attachment', 'lc_document_line_ir_attachments_rel',
    #                                   'lc_document_line_id', 'attachment_id', string='Attachments')
class lc_process(models.Model):
    _name = 'lc.process'
    _inherit = ['mail.thread']
    # _inherit='account.payment'
    lc_order_required = fields.One2many(comodel_name="lc.required.line", inverse_name="order_id", string="required", required=False, )
    lc_order_send = fields.One2many(comodel_name="lc.send.vendor", inverse_name="order_id", string="send", required=False, )
    name = fields.Char(string="Name", )
    purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order", required=False, )
    vendor = fields.Many2one(comodel_name="res.partner", string="vendor", required=False, )
    date = fields.Date(string="Date", required=False,default=fields.Date.today )
    journal = fields.Many2one(comodel_name="account.journal", string="journal", required=False, )
    payment = fields.Selection(string="Payment method", selection=[('bank', 'bank'), ('cash', 'cash'), ],readonly=True,required=False, )
    fees_account = fields.Many2one(comodel_name="account.account", string="Fees Account", required=False, )
    bank_fees = fields.Float(string="Bank Fees",  required=False, )
    pay_stat = fields.Boolean(string="pay",)
    stat = fields.Selection([
        ('fill_application', 'Fill application'),
        ('sent_to_bank', 'Sent to bank'),
        ('received_by_bank', 'Received by bank'),
        ('received_draft', 'Received draft'),
        ('file_sent_to_purchase', 'File sent to purchase'),
        ('file_sent_to_vendor', 'File sent to vendor'),
        ('lc_opened_by_bank', 'lc opened by bank'),
        ('vendor_approved', 'Vendor approved'),
        ('partial_paid', 'Partial paid'),
        ('paid', 'Paid'),
        ('amendment_of_lc', 'Amendment Of LC'),
        ('document_sent_to_custom', 'Document sent to custom'),
        ],default='fill_application', string='Status',)

    @api.multi
    def save_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'fill_application',
        })
        self.message_post(body='Fill application',subtype='mt_comment',
                                              subject='test',message_type='notification')

    @api.multi
    def send_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'sent_to_bank',
        })
        self.message_post(body='sent to bank',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def bank_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'received_by_bank',
        })
        self.message_post(body='Received by bank',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def receive_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'received_draft',
        })
        self.message_post(body='Received draft',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def treasurey_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'file_sent_to_purchase',
        })
        self.message_post(body='File sent to purchase',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def file_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'file_sent_to_vendor',
        })
        self.message_post(body='File sent to vendor',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def opened_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'lc_opened_by_bank',
        })
        self.message_post(body='lc opened by bank',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def approve_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'vendor_approved',
        })
        self.message_post(body='Vendor approved',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def partial_paid_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'partial_paid',
        })
        self.message_post(body='Partial paid',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def paid_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'paid',
        })
        self.message_post(body='paid',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def amendment_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'amendment_of_lc',
        })
        self.message_post(body='Amendment Of LC',subtype='mt_comment',
                                              subject='test',message_type='notification')
    @api.multi
    def document_progressbar(self):
        self.ensure_one()
        self.write({
            'stat': 'document_sent_to_custom',
        })
        self.message_post(body='Document sent to custom',subtype='mt_comment',
                                              subject='test',message_type='notification')

    @api.multi
    @api.onchange('journal')
    def payment_journal(self):
        if self.journal.type=='bank':
            self.payment='bank'
        else:
            self.payment='cash'

    @api.multi
    def payment_way(self):
        journal = self.sudo().env['account.move']
        for emp in self:
            if emp.pay_stat == False :
                journal_id=journal.create({
                    'journal_id':emp.journal.id,
                })

                if emp.payment == 'bank':
                   custudy = emp.journal.default_credit_account_id.id
                else:
                   custudy = emp.journal.default_debit_account_id.id

                journalline=self.with_context(dict(self._context,check_move_validity=False)).sudo().env['account.move.line']
                journalline.create({
                    'move_id':journal_id.id,
                    'account_id':emp.fees_account.id,
                    'name':emp.purchase_order.name,
                    'debit':emp.bank_fees,
                    'credit':0.0,
                    })
                journalline.create({
                        'move_id':journal_id.id,
                        'account_id':custudy,
                        'name':emp.purchase_order.name,
                        'debit':0.0,
                        'credit':emp.bank_fees,
                        })
                self.write({'pay_stat':True})
            else:
                print ">>>>>>>>>>>>>>>>>>>> not true"

    @api.multi
    def payment_state(self):
        paied = self.sudo().env['account.payment']
        acc=paied.create({
            'payment_method_id':1,
            'payment_type':'outbound',
            'journal_id':self.journal.id,
            'partner_type':'supplier',
            'partner_id':self.vendor.id,
            'amount':self.bank_fees,
            'payment_date':self.date,
            'communication':' ',
        })
        return {
                'type': 'ir.actions.act_window',
                'name': '	account.view_account_payment_form',
                'res_model': 'account.payment',
                'res_id': acc.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'self',
            }

    # @api.multi
    # def temporary_state(self):
    #     cary = self.env['temporary.permitt']
    #     caryline=self.env['temporary.permit.itemm']
    #     for emp in self:
    #       cary_id=cary.create({
    #             'import_order':self.id,
    #             'purchase_order':emp.purchase_order.id,
    #         })
    #       for rec in self.purchase_order:
    #           for record in rec.order_line:
    #               caryline.create({
    #                 'order_id':cary_id.id,
    #                 'item':record.product_id.id,
    #                 'quantity':record.product_qty,
    #                 'uom':record.product_uom.id,
    #
    #                 })
    #
    #
    #       return {
    #             'type': 'ir.actions.act_window',
    #             'name': 'nano_lc.temporary_permitt_view',
    #             'res_model': 'temporary.permitt',
    #             'res_id': cary_id.id,
    #             'view_type': 'form',
    #             'view_mode': 'form',
    #             'target': 'new',
    #         }
class lc_required_line(models.Model):
    _name = 'lc.required.line'
    order_id = fields.Many2one(comodel_name="nano.lc", string="required", required=False, )
    name = fields.Char(string="File Name", )
    attachment_ids = fields.Many2many('ir.attachment', 'lc_required_line_ir_attachments_rel',
                                      'lc_required_line_id', 'attachment_id', string='Attachments')
class lc_send_vendor(models.Model):
    _name = 'lc.send.vendor'
    order_id = fields.Many2one(comodel_name="nano.lc", string="send", required=False, )
    name = fields.Char(string="File Name", )
    attachment = fields.Many2many('ir.attachment', 'lc_send_vendor_ir_attachments_rel',
                                      'lc_send_vendor_id', 'attachment_id', string='Attachments')
class expence_lc(models.Model):
      _name='expence.lc'

      expence_describtion = fields.Char(string="Description Expense", required=False, )
      bill_reference = fields.Char(string="Bill Reference", required=False, )
      label = fields.Char(string="label", required=False, )
      product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
      unit_price = fields.Float(string="Unite Price",  required=False, )
      quantity = fields.Float(string="Quantity",  required=False, )
      total = fields.Float(string="Total",  required=False,compute='_get_total' )
      account = fields.Many2one(comodel_name="account.account", string="Account", required=False, )
      journal = fields.Many2one(comodel_name="account.journal", string="journal", required=False, )
      analytic_account = fields.Many2one(comodel_name="account.analytic.account", string="Analytic Account", required=False, )
      date = fields.Date(string="Date", required=False,default=fields.Date.today )
      employee = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False, )
      payment = fields.Selection(string="Payment by", selection=[('employee', 'employee'), ('custody', 'custody'), ], required=False, )
      custody_account = fields.Many2one(comodel_name="account.account", string="Custody Account", required=False, )
      vendor = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False, )
      post = fields.Boolean(string="Post",default=True, )
      attachment_number = fields.Float(string="Document",  required=False,compute='_compute_attachment_number', )
      stat = fields.Selection([
        ('save', 'draft'),
        ('submit', 'submit'),
        ('confirm', 'confirm'),
        ],default='save', string='Status',)

      @api.multi
      def action_save_send(self):
        self.ensure_one()
        self.write({
            'stat': 'save',
        })

      @api.multi
      def action_submit_send(self):
        self.ensure_one()
        self.write({
            'stat': 'submit',
        })

      @api.multi
      def action_confirm_send(self):
          self.ensure_one()
          self.write({
            'stat': 'confirm',
          })
          journal = self.env['account.move']
          # journalline = self.env['account.move.line']
          for emp in self:
                print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> custody "
                if emp.post == True:
                   name1 = 'posted'
                else:
                   name1 = 'draft'

                journal_id=journal.create({
                    'journal_id':emp.journal.id,
                    'state':name1,
                })

                if emp.payment == 'custody':
                   custudy = emp.custody_account.id
                else:
                   custudy = emp.vendor.property_account_payable_id.id

                journalline = self.with_context(dict(self._context,check_move_validity=False)).env['account.move.line']
                journalline.create({
                    'move_id':journal_id.id,
                    'account_id':emp.account.id,
                    'name':emp.label,
                    'debit':emp.total,
                    'credit':0.0,
                    'analytic_account_id':emp.analytic_account.id,
                    })
                journalline.create({
                        'move_id':journal_id.id,
                        'account_id':custudy,
                        'name':emp.label,
                        'debit':0.0,
                        'credit':emp.total,
                        'analytic_account_id':emp.analytic_account.id,
                        })

      @api.multi
      def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'expence.lc'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'expence.lc', 'default_res_id': self.id}
        return res

      @api.multi
      def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group([('res_model', '=', 'expence.lc'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.attachment_number = attachment.get(expense.id, 0)

      @api.multi
      @api.depends('unit_price','quantity')
      def _get_total(self):
          self.total=self.unit_price*self.quantity

# class temporary_permitt(models.Model):
#     _name='temporary.permitt'
#
#     item_id = fields.One2many(comodel_name="temporary.permit.itemm", inverse_name="order_id", string="item", required=False, )
#     confirm_id = fields.One2many(comodel_name="temporary.permit.confirmm", inverse_name="confirmm_id", string="confirm", required=False, )
#     name = fields.Char(string="Name", required=False, )
#     import_order = fields.Many2one(comodel_name="custom.import", string="Import order",readonly=True, required=False, )
#     purchase_order = fields.Many2one(comodel_name="purchase.order", string="purchase order",readonly=True, required=False, )
#     receiving_date= fields.Date(string="receiving date", required=False, )
#     last_date= fields.Date(string="Last Period of Permission", required=False, )
#     date_order = fields.Datetime(string="Date", required=False,related='purchase_id.date_order', )
#     purchase_id = fields.Many2one(comodel_name="sale.order", string="sale order", required=False, )
#     stat = fields.Selection([
#         ('draft', 'Unreconciled'),
#         ('todo', 'Reconciled'),
#         ], default='draft',)
#
#     @api.multi
#     def action_save_send(self):
#         self.ensure_one()
#         self.write({
#             'stat': 'draft',
#         })
#
#     @api.multi
#     def action_submit_send(self):
#         self.ensure_one()
#         self.write({
#             'stat': 'todo',
#         })
#
#     @api.constrains('receiving_date')
#     def _user_id(self):
#         if self.receiving_date > self.last_date:
#             raise ValidationError('Last Period of Permission should be greater than receiving date!')
#
#
#     @api.model
#     def create(self, vals):
#         vals['name'] = self.env['ir.sequence'].next_by_code('reqq.seq')
#         return super(temporary_permitt, self).create(vals)
#
#     # get confirm line automatically
#     def _prepare_invoice_line_from_po_line(self, line):
#         invoice_line = self.env['temporary.permit.confirmm']
#         data = {
#             'purchase_line_id': line.id,
#             'quantity': line.product_uom_qty,
#             'product': line.product_id.id,
#             'uom': line.product_uom.id,
#             'date':self.date_order,
#         }
#         return data
#
#     @api.onchange('purchase_id')
#     def purchase_order_change(self):
#         if not self.purchase_id:
#             return {}
#         # if not self.partner_id:
#         #     self.partner_id = self.purchase_id.partner_id.id
#
#         new_lines = self.env['temporary.permit.confirmm']
#         for line in self.purchase_id.order_line - self.confirm_id.mapped('purchase_line_id'):
#             data = self._prepare_invoice_line_from_po_line(line)
#             new_line = new_lines.new(data)
#             new_line._set_additional_fields(self)
#             new_lines += new_line
#
#         self.confirm_id += new_lines
#         self.purchase_id = False
#         return {
#             'domain':{'purchase_id':[('id', 'not in',self.purchase_id.ids)]}
#         }
#         # return {}

# class temporary_permit_itemm(models.Model):
#     _name='temporary.permit.itemm'
#     order_id = fields.Many2one(comodel_name="temporary.permitt", string="temporary item", required=False, )
#     item = fields.Many2one(comodel_name="product.product", string="Item",readonly=True, required=False, )
#     quantity = fields.Float(string="Quantity",  required=False,readonly=True, )
#     uom = fields.Many2one(comodel_name="product.uom", string="UOM",readonly=True, required=False, )

# class temporary_permit_confirmm(models.Model):
#     _name='temporary.permit.confirmm'
#     confirmm_id = fields.Many2one(comodel_name="temporary.permitt", string="temporary confirm", required=False, )
#     product = fields.Many2one(comodel_name="product.product", string="product", required=False, )
#     quantity = fields.Float(string="Quantity",  required=False, )
#     uom = fields.Many2one(comodel_name="product.uom", string="UOM", required=False, )
#     date = fields.Date(string="Date", required=False, )
#
#     # linked with get confirmed line automatically in temporary.permit
#     purchase_line_id = fields.Many2one('sale.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
#     purchase_id = fields.Many2one('sale.order', related='purchase_line_id.order_id', string='Purchase Order', store=False, readonly=True, related_sudo=False,
#         help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')
#
#     def _set_additional_fields(self,invoice):
#         pass

#
class nano_purchase_temporar(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def button_approve(self):
        self.write({'state': 'purchase', 'date_approve': fields.Date.context_today(self)})
        self.filtered(lambda p: p.company_id.po_lock == 'lock').write({'state': 'done'})
        cus = self.env['nano.lc']
        custom_lines = self.env['lc.order.line']
        document_lines = self.env['lc.document.line']
        for order in self:
            if order.payment_type == 'lc':
                acc_id22 = cus.create({
                    'purchase_order': order.id,
                    'payment_term': order.payment_term_id.id,
                    'incoterms': order.incoterm_id.id,
                })
                for linee in order.order_line:
                    custom_lines.create({
                        'order_line': acc_id22.id,
                        'product': linee.product_id.id,
                        'describtion': linee.name,
                        'quantity': linee.product_qty,
                        'price': linee.price_unit,
                        'tax': linee.taxes_id.id,
                        'subtotal': linee.price_subtotal,
                    })
                for doc in order.attach_rel:
                    document_lines.create({
                        'order_line': acc_id22.id,
                        'document': doc.document,
                        'doc_status': doc.doc_status,
                        'attachment': doc.attachment,
                    })
        return {}

    # @api.multi
    # def button_confirm(self):
    #     # for order in self:
    #     #     if order.state not in ['draft', 'sent']:
    #     #         continue
    #     #     order._add_supplier_to_product()
    #     #     # Deal with double validation process
    #     #     if order.company_id.po_double_validation == 'one_step'\
    #     #             or (order.company_id.po_double_validation == 'two_step'\
    #     #                 and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id))\
    #     #             or order.user_has_groups('purchase.group_purchase_manager'):
    #     #         order.button_approve()
    #     #     else:
    #     #         order.write({'state': 'to approve'})
    #     new = super(nano_purchase_temporar, self).button_confirm()
    #     import pdp;
    #     pdb.set_trace()
    #     cus = self.env['nano.lc']
    #     custom_lines = self.env['lc.order.line']
    #     document_lines = self.env['lc.document.line']
    #     if self.payment_type == 'lc':
    #         for emp1 in self:
    #             acc_id22 = cus.create({
    #                 'purchase_order':emp1.id,
    #                 'payment_term':emp1.payment_term_id.id,
    #                 'incoterms':emp1.incoterm_id.id,
    #             })
    #             for linee in emp1.order_line:
    #                 custom_lines.create({
    #                 'order_line':acc_id22.id,
    #                 'product':linee.product_id.id,
    #                 'describtion':linee.name,
    #                 'quantity':linee.product_qty,
    #                 'price':linee.price_unit,
    #                 'tax':linee.taxes_id.id,
    #                 'subtotal':linee.price_subtotal,
    #                 })
    #             for doc in emp1.attach_rel:
    #                 document_lines.create({
    #                     'order_line':acc_id22.id,
    #                     'document':doc.document,
    #                     'doc_status':doc.doc_status,
    #                     'attachment':doc.attachment,
    #                 })
    #         return True

