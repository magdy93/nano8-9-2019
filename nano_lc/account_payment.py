# -*- coding: utf-8 -*-

from odoo import fields, models,api,_
from odoo.exceptions import UserError

class AccountPayment(models.Model):
    _inherit = 'account.payment'
    lc_id = fields.Many2one('nano.lc', 'LC', )
    is_lc = fields.Boolean(string="Is Lc", )
    # state = fields.Selection(selection_add=[('paid', 'Paid')])

    @api.one
    @api.depends('invoice_ids', 'payment_type', 'partner_type', 'partner_id')
    def _compute_destination_account_id(self):

        if self.is_lc == True:
            if self.invoice_ids:
                self.destination_account_id = self.invoice_ids[0].account_id.id
            elif self.payment_type == 'transfer':
                if not self.company_id.transfer_account_id.id:
                    raise UserError(_('Transfer account not defined on the company.'))
                self.destination_account_id = self.company_id.transfer_account_id.id
            elif self.partner_id:
                if self.partner_type == 'customer':
                    self.destination_account_id = self.partner_id.lc_account.id
                else:
                    self.destination_account_id = self.partner_id.lc_account.id
        else:
            res = super(AccountPayment, self)._compute_destination_account_id()
            return res
