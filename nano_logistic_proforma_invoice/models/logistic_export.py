# -*- coding: utf-8 -*-

from odoo import models, fields, api

class logisticInherit(models.Model):
    _inherit = 'nano.logistic'

    package_ids = fields.One2many('nano.logistic.packaging', 'pkg_id')

    @api.multi
    def action_proforma_invoice(self):
        logistic_so = self.env['logistic.sale.order']
        logistic_so_lines = self.env['logistic.sale.order.line']
        logistic_pkg_lines = self.env['logistic.packaging.line']
        for record in self:
            proforma_id = self.env['logistic.sale.order'].search([('logistic_id','=',record.id)])
            if proforma_id:
               return {
                        'name': 'Proforma Invoice',
                        'view_type': 'form',
                        # 'view_id': view_id[1],
                        'res_id': proforma_id.id,
                        'view_mode': 'form',
                        'res_model': 'logistic.sale.order',
                        'type': 'ir.actions.act_window',
                        'target': 'new',
                      }
            else:
                logistic_so_id = logistic_so.create({
                    'logistic_id': record.id,
                    'so': record.sale_order.id,
                    'terms_of_delivery': record.sale_order.incoterm.id,
                    'terms_of_payment': record.sale_order.payment_term_id.id,
                    'user_id': record.sale_order.user_id.id,
                    'email': record.sale_order.user_id.email,
                    'person_phone_number': record.sale_order.user_id.phone,
                    'person_fax': record.sale_order.user_id.fax,
                    'consignee_id': record.sale_order.partner_id.id,
                    'consignee_street': record.sale_order.partner_id.street,
                    'consignee_street2': record.sale_order.partner_id.street2,
                    'consignee_zip': record.sale_order.partner_id.zip,
                    'consignee_city': record.sale_order.partner_id.city,
                    'consignee_state_id': record.sale_order.partner_id.state_id.id,
                    'consignee_country_id': record.sale_order.partner_id.country_id.id,
                    'buyer_street': record.sale_order.partner_id.street,
                    'buyer_street2': record.sale_order.partner_id.street2,
                    'buyer_zip': record.sale_order.partner_id.zip,
                    'buyer_city': record.sale_order.partner_id.city,
                    'buyer_state_id': record.sale_order.partner_id.state_id.id,
                    'buyer_country_id': record.sale_order.partner_id.country_id.id,
                    'partner_phone': record.sale_order.partner_id.phone,
                    'po': record.sale_order.po,
                    'arrival_date': record.sale_order.end_plan_date,
                    'company_id': record.sale_order.user_id.company_id.id,
                    'company_street': record.sale_order.user_id.company_id.street,
                    'company_street2': record.sale_order.user_id.company_id.street2,
                    'company_zip': record.sale_order.user_id.company_id.zip,
                    'company_city': record.sale_order.user_id.company_id.city,
                    'company_state_id': record.sale_order.user_id.company_id.state_id.id,
                    'company_country_id': record.sale_order.user_id.company_id.country_id.id,
                    'Billing_street': record.sale_order.partner_id.street,
                    'Billing_street2': record.sale_order.partner_id.street2,
                    'Billing_zip': record.sale_order.partner_id.zip,
                    'Billing_city': record.sale_order.partner_id.city,
                    'Billing_state_id': record.sale_order.partner_id.state_id.id,
                    'Billing_country_id': record.sale_order.partner_id.country_id.id,
                    'shipped_to': record.sale_order.discharge_port,
                    'invoice_no': record.sale_order.invoice_ids.name,
                    'date_invoice': record.sale_order.invoice_ids.date_invoice
                })
                for line in record.logistic_order:
                    logistic_so_lines.create({
                        'so_id': logistic_so_id.id,
                        'item': line.product.id,
                        'material_description': line.describtion,
                        'unit_price': line.price,
                        'qty': line.quantity,
                        'amount': line.subtotal
                    })
                for rec in record.package_ids:
                    logistic_pkg_lines.create({
                        'l_so_id': logistic_so_id.id,
                        'package': rec.package.id,
                        'quantity': rec.qty,
                        'price_unit': rec.unit_price
                    })
            # view_id = self.env['ir.model.data'].get_object_reference('nano_proforma_invoice',
            #                                                          'view_form_logistic_sale_order')
            return {
                'name': 'Proforma Invoice',
                'view_type': 'form',
                # 'view_id': view_id[1],
                'res_id': logistic_so_id.id,
                'view_mode': 'form',
                'res_model': 'logistic.sale.order',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
class pkgLines(models.Model):
    _name = 'nano.logistic.packaging'

    pkg_id = fields.Many2one('nano.logistic')
    package = fields.Many2one('product.product')
    qty = fields.Float()
    unit_price = fields.Float()

class logisticSaleOrder(models.Model):
    _name = 'logistic.sale.order'

    logistic_id = fields.Many2one(comodel_name="nano.logistic", string="logistic", required=False, )
    line_ids = fields.One2many('logistic.sale.order.line', 'so_id')
    pkg_ids = fields.One2many('logistic.packaging.line','l_so_id')
    notify = fields.Text()
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State')
    country_id = fields.Many2one('res.country', string='Country')
    notify_phone = fields.Char(string='Phone NO.')
    notify_fax = fields.Char(string='Fax NO.')
    main_carriage_by =fields.Char()
    dispatch_date = fields.Date()
    user_id = fields.Many2one('res.users', string='Contact Person',default=lambda self: self.env.user)
    email = fields.Char(related='user_id.email')
    person_phone_number = fields.Char(related='user_id.phone', string='Phone NO.')
    person_fax = fields.Char(related='user_id.fax', string='Fax NO.')
    consignee_id = fields.Many2one('res.partner', string='Consignee')
    consignee_street = fields.Char(related='consignee_id.street')
    consignee_street2 = fields.Char(related='consignee_id.street2')
    consignee_zip = fields.Char(related='consignee_id.zip', change_default=True)
    consignee_city = fields.Char(related='consignee_id.city')
    consignee_state_id = fields.Many2one('res.country.state',related='consignee_id.state_id')
    consignee_country_id = fields.Many2one('res.country', related='consignee_id.country_id')
    partner_phone = fields.Char(related='consignee_id.phone', string='Phone NO.')
    so = fields.Many2one('sale.order', string='Sales Order NO.')
    invoice_no = fields.Char()
    terms_of_payment = fields.Many2one('account.payment.term')
    shipped_to = fields.Char()
    arrival_date = fields.Date()
    company_id = fields.Many2one('res.company')
    company_street = fields.Char(related='user_id.company_id.street')
    company_street2 = fields.Char(related='user_id.company_id.street2')
    company_zip = fields.Char(related='user_id.company_id.zip')
    company_city = fields.Char(related='user_id.company_id.city')
    company_state_id = fields.Many2one('res.country.state', related='user_id.company_id.state_id')
    company_country_id = fields.Many2one('res.country', related='user_id.company_id.country_id')
    Billing_street = fields.Char(related='consignee_id.street')
    Billing_street2 = fields.Char(related='consignee_id.street2')
    Billing_zip = fields.Char(related='consignee_id.zip', change_default=True)
    Billing_city = fields.Char(related='consignee_id.city')
    Billing_state_id = fields.Many2one('res.country.state', related='consignee_id.state_id')
    Billing_country_id = fields.Many2one('res.country', related='consignee_id.country_id')
    buyer_street = fields.Char(related='consignee_id.street')
    buyer_street2 = fields.Char(related='consignee_id.street2')
    buyer_zip = fields.Char(related='consignee_id.zip', change_default=True)
    buyer_city = fields.Char(related='consignee_id.city')
    buyer_state_id = fields.Many2one('res.country.state', related='consignee_id.state_id')
    buyer_country_id = fields.Many2one('res.country', related='consignee_id.country_id')
    po = fields.Char(string='P.O NO.')
    terms_of_delivery = fields.Many2one('stock.incoterms')
    shipped_to = fields.Char(related='so.discharge_port')
    bank_id = fields.Many2one('res.bank', string='Bank')
    bank_street = fields.Char(related='bank_id.street')
    bank_street2 = fields.Char(related='bank_id.street2')
    bank_zip = fields.Char(related='bank_id.zip', change_default=True)
    bank_city = fields.Char(related='bank_id.city')
    bank_state_id = fields.Many2one('res.country.state', related='bank_id.state')
    bank_country_id = fields.Many2one('res.country', related='bank_id.country')
    account_No = fields.Many2one('account.journal')
    swift_code = fields.Char(related='bank_id.bic' ,string='Swift')

    @api.onchange('bank_id')
    def setBankAddress(self):
        bank = self.env['res.bank']
        self.create({
            'bank_street': bank.street,
            'bank_street2': bank.street2,
            'bank_zip': bank.zip,
            'bank_city': bank.city,
            'bank_state_id': bank.state.id,
            'bank_country_id': bank.country.id,
            'swift_code': bank.bic
        })
        acc_ids=[]
        account = self.env['account.journal'].search([('bank_id','=', self.bank_id.id)])
        for acc_id in account:
            acc_ids.append(acc_id.id)
        domain = [('id','in',acc_ids)]
        return {'domain': {'account_No': domain}}

class logisticSaleOrderLine(models.Model):
    _name= 'logistic.sale.order.line'

    so_id = fields.Many2one('logistic.sale.order')
    item = fields.Many2one('product.product')
    material_description = fields.Text()
    unit_of_measure = fields.Many2one('product.uom')
    unit_price= fields.Float(string='Unit Price (USD)')
    qty = fields.Float()
    amount= fields.Float(string='Amount (USD)')

class logisticPackagingLines(models.Model):
    _name = 'logistic.packaging.line'

    l_so_id = fields.Many2one('logistic.sale.order')
    package = fields.Many2one('product.product')
    quantity = fields.Float()
    price_unit = fields.Float()






