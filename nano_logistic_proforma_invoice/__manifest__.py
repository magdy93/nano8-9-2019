# -*- coding: utf-8 -*-

{
    'name': 'Nano proforma Invoice',
    'description': 'Nano proforma Invoice',
    'summary': 'UNSS-Sales Module',
    'category': 'Logistic',
    'author': 'Universal Selective Systems',
    'depends': ['base', 'nano_logistic','nano_sale','nano_plan'],
    'data': [
        'views/logistic_export_views.xml'
    ]
}