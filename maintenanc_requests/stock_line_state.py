from odoo import api, fields, models, _
from odoo.exceptions import UserError

class StockLineState(models.Model):
      _inherit='stock.picking'

      maintenance_id = fields.Many2one('maintenance.order', string='Maintenance order',readonly=True)

      @api.multi
      def action_confirm(self):
        self.filtered(lambda picking: not picking.move_lines).write({'launch_pack_operations': True})
        # TDE CLEANME: use of launch pack operation, really useful ?
        self.mapped('move_lines').filtered(lambda move: move.state == 'draft').action_confirm()
        self.filtered(lambda picking: picking.location_id.usage in ('supplier', 'inventory', 'production')).force_assign()

        self.transfer_validate()
        return True

      @api.multi
      def action_cancel(self):
        self.mapped('move_lines').action_cancel()

        self.transfer_validate()
        return True

      @api.multi
      def do_new_transfer(self):
        for pick in self:
            if pick.state == 'done':
                raise UserError(_('The pick is already validated'))
            pack_operations_delete = self.env['stock.pack.operation']
            if not pick.move_lines and not pick.pack_operation_ids:
                raise UserError(_('Please create some Initial Demand or Mark as Todo and create some Operations. '))
            # In draft or with no pack operations edited yet, ask if we can just do everything
            if pick.state == 'draft' or all([x.qty_done == 0.0 for x in pick.pack_operation_ids]):
                # If no lots when needed, raise error
                picking_type = pick.picking_type_id
                if (picking_type.use_create_lots or picking_type.use_existing_lots):
                    for pack in pick.pack_operation_ids:
                        if pack.product_id and pack.product_id.tracking != 'none':
                            raise UserError(_('Some products require lots/serial numbers, so you need to specify those first!'))
                view = self.env.ref('stock.view_immediate_transfer')
                wiz = self.env['stock.immediate.transfer'].create({
                      'pick_id': pick.id,
                      'stock_id': self.id,
                })
                # TDE FIXME: a return in a loop, what a good idea. Really.
                return {
                    'name': _('Immediate Transfer?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.immediate.transfer',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }

            # Check backorder should check for other barcodes
            if pick.check_backorder():
                view = self.env.ref('stock.view_backorder_confirmation')
                wiz = self.env['stock.backorder.confirmation'].create({'pick_id': pick.id})
                # TDE FIXME: same reamrk as above actually
                return {
                    'name': _('Create Backorder?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.backorder.confirmation',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }
            for operation in pick.pack_operation_ids:
                if operation.qty_done < 0:
                    raise UserError(_('No negative quantities allowed'))
                if operation.qty_done > 0:
                    operation.write({'product_qty': operation.qty_done})
                else:
                    pack_operations_delete |= operation
            if pack_operations_delete:
                pack_operations_delete.unlink()
        self.do_transfer()
        return

      @api.multi
      def force_assign(self):
        """ Changes state of picking to available if moves are confirmed or waiting.
        @return: True
        """
        self.mapped('move_lines').filtered(lambda move: move.state in ['confirmed', 'waiting']).force_assign()
        self.transfer_validate()
        return True

      @api.multi
      def action_assign(self):
        """ Check availability of picking moves.
        This has the effect of changing the state and reserve quants on available moves, and may
        also impact the state of the picking as it is computed based on move's states.
        @return: True
        """
        self.filtered(lambda picking: picking.state == 'draft').action_confirm()
        moves = self.mapped('move_lines').filtered(lambda move: move.state not in ('draft', 'cancel', 'done'))
        if not moves:
            raise UserError(_('Nothing to check the availability for.'))
        moves.action_assign()
        self.transfer_validate()
        return True

      @api.multi
      def do_unreserve(self):
        """
          Will remove all quants for picking in picking_ids
        """
        moves_to_unreserve = self.mapped('move_lines').filtered(lambda move: move.state not in ('done', 'cancel'))
        pack_line_to_unreserve = self.mapped('pack_operation_ids')
        if moves_to_unreserve:
            if pack_line_to_unreserve:
                pack_line_to_unreserve.unlink()
            moves_to_unreserve.do_unreserve()
        self.transfer_validate()


      def transfer_validate(self):
        maintenance=self.env['maintenance.order']
        stock_result=maintenance.search([('id','=',self.maintenance_id.id)])
        if stock_result:
           for record in self:
               for line in record.move_lines:
                     for main in stock_result.maintenance_order_lines:
                           if line.product_id.id == main.test.id:
                                 # print "pppppppppppppp line state : ",line.state
                                 # print "kkkkkkkkkkkkkk maintenance state : ",main.status
                                 main.write({
                                       'status':line.state
                                 })

class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    stock_id = fields.Many2one('stock.picking', string='stock',readonly=True)

    @api.multi
    def process(self):
        self.ensure_one()
        # If still in draft => confirm and assign
        if self.pick_id.state == 'draft':
            self.pick_id.action_confirm()
            if self.pick_id.state != 'assigned':
                self.pick_id.action_assign()
                if self.pick_id.state != 'assigned':
                    raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
        for pack in self.pick_id.pack_operation_ids:
            if pack.product_qty > 0:
                pack.write({'qty_done': pack.product_qty})
            else:
                pack.unlink()
        self.pick_id.do_transfer()

        stock = self.env['stock.picking'].browse([self.stock_id.id])
        stock.transfer_validate() #function in stock.picking to change state to done
        print "stock main id : ",stock.id