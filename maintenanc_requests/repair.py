from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class repair_request(models.Model):
      _inherit='mrp.repair'

      product_id = fields.Many2one('maintenance.equipment', string='Product to Repair',
                                   readonly=True, required=True, states={'draft': [('readonly', False)]})

      product_qty = fields.Float(
        'Product Quantity',
        default=1.0, digits=dp.get_precision('Product Unit of Measure'),
        required=False,readonly=True,states={'draft': [('readonly', False)]})
      product_uom = fields.Many2one(
        'product.uom', 'Product Unit of Measure',default=1,
        required=False,readonly=True,states={'draft': [('readonly', False)]})

      @api.onchange('product_id')
      def onchange_product_id(self):
        self.guarantee_limit = False
        self.lot_id = False
        # if self.product_id:
        #     self.product_uom = self.product_id.uom_id.id

      @api.onchange('product_uom')
      def onchange_product_uom(self):
        # res = {}
        # if not self.product_id or not self.product_uom:
        #     return res
        # if self.product_uom.category_id != self.product_id.uom_id.category_id:
        #     res['warning'] = {'title': _('Warning'), 'message': _('The Product Unit of Measure you chose has a different category than in the product form.')}
        #     self.product_uom = self.product_id.uom_id.id
        return