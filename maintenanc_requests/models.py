from odoo import models, fields, api
class maintenance_request(models.Model):
      _inherit='maintenance.equipment'

      # maintenance_request_ids = fields.One2many(comodel_name="maintenance.equipment.hour", inverse_name="maintenance_request_id",)
      # maintenance_request_kms = fields.One2many(comodel_name="maintenance.equipment.km", inverse_name="maintenance_request_km",)

      maintenance_order = fields.One2many(comodel_name="maintenance.order.hour", inverse_name="maintenance_request_id", string="maintenance line", required=False, )
      maintenance_order_k = fields.One2many(comodel_name="maintenance.order.km", inverse_name="maintenance_request_km", string="maintenance line", required=False, )

      period_h = fields.Integer(string="period hour", required=False, )
      expected_mtbf_h = fields.Integer(string="period hour", required=False, )
      mtbf_h = fields.Integer(string="mtbf hours", required=False, )
      mttr_h = fields.Integer(string="mttr hours", required=False, )
      maintenance_duration_h = fields.Float(string="maintenance duration hours",  required=False, )

      period_km = fields.Integer(string="period km", required=False, )
      expected_mtbf_km = fields.Integer(string="period km", required=False, )
      mtbf_km = fields.Integer(string="mtbf km", required=False, )
      mttr_km = fields.Integer(string="mttr km", required=False, )
      maintenance_duration_km = fields.Float(string="maintenance km",  required=False, )

      hour = fields.Boolean(string="Hours",default=True)
      km = fields.Boolean(string="KM",default=True)

class maintenance_order_hour(models.Model):
      _name='maintenance.order.hour'

      maintenance_request_id = fields.Many2one(comodel_name="maintenance.equipment",)
      test = fields.Many2one(comodel_name="product.product", string="test part", required=False, )
      quantity = fields.Integer(string="quantity", required=False, )
      describtion = fields.Char(string="describtion", required=False, )
      status = fields.Char(string="status", required=False, )
      quantity_on_hand = fields.Float(string="quantity in stock",  required=False, )

class maintenance_order_km(models.Model):
      _name='maintenance.order.km'

      maintenance_request_km = fields.Many2one(comodel_name="maintenance.equipment",)
      test = fields.Many2one(comodel_name="product.product", string="test part", required=False, )
      quantity = fields.Integer(string="quantity", required=False, )
      describtion = fields.Char(string="describtion", required=False, )
      status = fields.Char(string="status", required=False, )
      quantity_on_hand = fields.Float(string="quantity in stock",  required=False, )

class maintenance_request_sequence(models.Model):
      _inherit='maintenance.request'
      mrq_sequence = fields.Char(string="MRQ serial", required=False, )

      @api.model
      def create(self, vals):
        vals['mrq_sequence'] = self.env['ir.sequence'].next_by_code('maintenance.sequence.e')
        return super(maintenance_request_sequence, self).create(vals)

      @api.multi
      def submit_request(self):
          print "ooooooooooooooooooooook"
          maintenance=self.env['maintenance.order']
          for main in self:
             maintenance.create({
                'name':main.name,
                'description':main.description,
                'request_date':main.request_date,
                'owner_user_id':main.owner_user_id.id,
                'category_id':main.category_id.id,
                'equipment_id':main.equipment_id.id,
                'technician_user_id':main.technician_user_id.id,
                'priority':main.priority,
                'close_date':main.close_date,
                'maintenance_type':main.maintenance_type,
                'schedule_date':main.schedule_date,
                'maintenance_team_id':main.maintenance_team_id.id,
                'duration':main.duration,
                'mrq_sequence':main.mrq_sequence,
            })

class maintenance_order(models.Model):
    _name='maintenance.order'

    maintenance_order_lines = fields.One2many(comodel_name="maintenance.order.line", inverse_name="maintenance_request", string="maintenance line", required=False, )

    name = fields.Char('Subjects', required=True)
    description = fields.Text('Description')
    request_date = fields.Date('Request Date', track_visibility='onchange', default=fields.Date.context_today,
                               help="Date requested for the maintenance to happen")
    owner_user_id = fields.Many2one('res.users', string='Created by', default=lambda s: s.env.uid)
    category_id = fields.Many2one('maintenance.equipment.category', related='equipment_id.category_id', string='Category', store=True, readonly=True)
    equipment_id = fields.Many2one('maintenance.equipment', string='Equipment', index=True)
    technician_user_id = fields.Many2one('res.users', string='Owner',)
    # stage_id = fields.Many2one('maintenance.stage', string='Stage', track_visibility='onchange',
    #                            group_expand='_read_group_stage_ids', default=_default_stage)
    priority = fields.Selection([('0', 'Very Low'), ('1', 'Low'), ('2', 'Normal'), ('3', 'High')], string='Priority')
    # color = fields.Integer('Color Index')
    close_date = fields.Date('Close Date', help="Date the maintenance was finished. ")
    # kanban_state = fields.Selection([('normal', 'In Progress'), ('blocked', 'Blocked'), ('done', 'Ready for next stage')],
    #                                 string='Kanban State', required=True, default='normal', track_visibility='onchange')
    # active = fields.Boolean(default=True, help="Set active to false to hide the maintenance request without deleting it.")
    archive = fields.Boolean(default=False, help="Set archive to true to hide the maintenance request without deleting it.")
    maintenance_type = fields.Selection([('corrective', 'Corrective'), ('preventive', 'Preventive')], string='Maintenance Type', default="corrective")
    schedule_date = fields.Datetime('Scheduled Date', help="Date the maintenance team plans the maintenance.  It should not differ much from the Request Date. ")
    maintenance_team_id = fields.Many2one('maintenance.team', string='Team', required=True,)
    duration = fields.Float(help="Duration in minutes and seconds.")

    state = fields.Selection(string="state", selection=[('draft', 'draft'), ('inprogress', 'in progress'),
                                                        ('done', 'Done'), ('create_ro', 'Create Ro'),
                                                        ('return', 'return equipment'), ('repaired', 'repaired'), ],
                                                         default= 'draft',required=False, )
    mrq_sequence = fields.Char(string="MRQ serial", required=False, )
    mor_sequence = fields.Char(string="MOR serial", required=False, )
    source_location = fields.Many2one(comodel_name="stock.location", string="source location", required=False, )
    dest_location = fields.Many2one(comodel_name="stock.location", string="dest location", required=False, )


    @api.model
    def create(self, vals):
        vals['mor_sequence'] = self.env['ir.sequence'].next_by_code('maintenance.order.e')
        return super(maintenance_order, self).create(vals)



    @api.multi
    def draft_equipment_request(self):
        self.ensure_one()
        self.write({
            'state': 'draft',
        })

    @api.multi
    def progress_equipment_request(self):
        self.ensure_one()
        self.write({
            'state': 'inprogress',
        })
        stock=self.env['stock.picking']
        stock_lines=self.env['stock.move']
        for ship in self:
            stock_id=stock.create({
                  'location_id':ship.source_location.id,
                  'location_dest_id':ship.dest_location.id,
                  'picking_type_id':5,
                  'move_type':'direct',
                  'maintenance_id':self.id,
              })
            for line in ship.maintenance_order_lines:
                print ">>>>>>>>>>>>>>>>>>>> ",line.test.id
                print ">>>>>>>>>>>>>>>>>>>> ",line.quantity
                stock_lines.create({
                  'picking_id':stock_id.id,
                  'product_id':line.test.id,
                  'product_uom_qty':line.quantity,
                  'product_uom':line.test.uom_id.id,
                  'location_id':line.source_location.id,
                  'location_dest_id':line.dest_location.id,
                  'name':line.describtion,

              })


        # return {
        #       'type': 'ir.actions.act_window',
        #       'name': 'stock info',
        #       'res_model': 'stock.picking',
        #       'res_id': stock_id.id,
        #       'view_type': 'form',
        #       'view_mode': 'form',
        #       'target': 'self',
        #   }
    @api.multi
    def create_ro_equipment_request(self):
        self.ensure_one()
        self.write({
            'state': 'create_ro',
        })
        repair=self.env['mrp.repair']
        repair_lines=self.env['mrp.repair.line']
        for ship in self:
            repair_id=repair.create({
                  'location_id':ship.source_location.id,
                  'location_dest_id':ship.dest_location.id,
                  'product_id':ship.equipment_id.id,
                  'product_qty':1,
                  'product_uom':1,
              })
            for line in ship.maintenance_order_lines:
                repair_lines.create({
                  'repair_id':repair_id.id,
                  'product_id':line.test.id,
                  'product_uom':line.test.uom_id.id,
                  'price_unit':line.test.lst_price,
                  'name':line.test.name,
                  'type':line.type,
                  'location_id':line.source_location.id,
                  'location_dest_id':line.dest_location.id,
                  'product_uom_qty':line.quantity,
              })

    @api.multi
    def done_equipment_request(self):
        self.ensure_one()
        self.write({
            'state': 'done',
        })

    @api.multi
    def return_equipment_request(self):
        self.ensure_one()
        self.write({
            'state': 'return',
        })

    @api.multi
    def repaired_equipment_request(self):
        self.ensure_one()
        self.write({
            'state': 'repaired',
        })

class maintenance_order_line(models.Model):
      _name='maintenance.order.line'

      maintenance_request = fields.Many2one(comodel_name="maintenance.order",)
      type = fields.Selection(string="type", selection=[('add', 'add'), ('remove', 'remove'), ], required=False, )
      test = fields.Many2one(comodel_name="product.product", string="test part", required=False, )
      describtion = fields.Char(string="describtion", required=False, )
      source_location = fields.Many2one(comodel_name="stock.location", string="source location", required=False, )
      dest_location = fields.Many2one(comodel_name="stock.location", string="dest location", required=False, )
      quantity = fields.Integer(string="quantity", required=False, )
      status = fields.Selection(string="part status", selection=[('draft', 'New'),('cancel', 'cancelled'),('confirmed', 'waiting availibility'),
                                                        ('available', 'Waiting Another Move'),('assigned', 'available'), ('done', 'done'), ],
                                                         default= 'draft',required=False, )
      @api.onchange('test')
      def get_test_name(self):
          self.ensure_one()
          self.describtion=self.test.name


class maintenance_order_line(models.Model):
      _inherit='stock.move'

      product_uom = fields.Many2one(
        'product.uom', 'Unit of Measure', required=False, states={'done': [('readonly', True)]})