from datetime import datetime , timedelta
import pytz
import time
import zklib
from odoo import models,fields,api,exceptions,SUPERUSER_ID
import logging
_logger = logging.getLogger(__name__)

class biometric_machine(models.Model):
    _name= 'biometric.machine.location'
    name = fields.Char("Location",required=True)


class biometric_machine(models.Model):
    _name= 'biometric.machine'
    name =  fields.Char("Machine IP")
    location_id =  fields.Many2one('biometric.machine.location',string="Location")
    port =  fields.Integer("Port Number")
    state =  fields.Selection([('draft','Draft'),('done','Done')],'State')

    @api.multi
    def download_attendance(self):
        user_pool = self.env['res.users']
        user = self.env.user
        tz = pytz.timezone(user.partner_id.tz) or False
        if not tz:
            raise exceptions.ValidationError("Timezone is not defined on this %s user." % user.name)

        for r in self:
            machine_ip = r.name
            port = r.port
            location_id = r.location_id

            zk = zklib.ZKLib(machine_ip, int(port))
            res,error_str = zk.connect()
            aclog = ''
            
            if res == True:
                zk.enableDevice()
                zk.disableDevice()
                attendance=[]
                Done = False
                while not Done:
                    attendance_new = zk.getAttendance()
                    if (attendance_new):
                        for lattendance in attendance_new:
                            if lattendance[0] == '':
                                Done = True
                                break
                            attendance.append(lattendance)
                
                hr_attendance =  self.env["hr.attendance"]
                hr_employee = self.env["hr.employee"]
                hr_employee_zk_location_line_obj = self.env["biometric.machine.zk_location.line"]
                for lattendance in attendance:
                    time_att = str(lattendance[2].date()) + ' ' +str(lattendance[2].time())
                    aclog = lattendance[1]          
                    atten_time1 = datetime.strptime(str(time_att), '%Y-%m-%d %H:%M:%S')
                    atten_time = tz.normalize(tz.localize(atten_time1)).astimezone(pytz.utc)
                    # atten_time_localize = pytz.utc.localize(atten_time1).astimezone(tz)
                    # atten_time = atten_time1 - (timedelta(hours=atten_time_localize.hour-atten_time1.hour,minutes=atten_time_localize.minute-atten_time1.minute))
                    atten_time = datetime.strftime(atten_time,'%Y-%m-%d %H:%M:%S')
                    hr_employee_zk_location_line_rec=hr_employee_zk_location_line_obj.search([("zk_num", "=", int(lattendance[0])),('location_id.id','=',location_id.id)])
                    employee_id = hr_employee_zk_location_line_rec.emp_id.id or False
                    address_id = False
                    category = False
                    if employee_id:
                        try:
                            if aclog == 'sign_in':
                                atten_ids = hr_attendance.search([('employee_id','=',employee_id),('check_in','=',atten_time)])
                                if atten_ids:
                                    continue
                                else:
                                    atten_id = hr_attendance.create({'check_in':atten_time,'employee_id':employee_id})
                            if aclog == 'sign_out':
                                atten_ids = hr_attendance.search([('employee_id','=',employee_id),('check_out','=',atten_time)])
                                if atten_ids:
                                    continue
                                else:
                                    atten_ids = hr_attendance.search([('employee_id','=',employee_id),('check_in','<',atten_time)],order='check_in')
                                    if atten_ids:
                                        found = False
                                        for r in reversed(atten_ids):
                                            if r.check_out:
                                                continue
                                            if datetime.strptime(r.check_in, '%Y-%m-%d %H:%M:%S').date() == datetime.strptime(atten_time, '%Y-%m-%d %H:%M:%S').date():
                                                r.write({'check_out':atten_time})
                                                found = True
                                                break
                                        if not found :
                                            atten_id = hr_attendance.create({'check_in':atten_time,'check_out':atten_time,'employee_id':employee_id})
                        except Exception,e:
                            pass
                zk.enableDevice()
                zk.disconnect()
                return True
            else:
                 raise exceptions.Warning("Unable to connect, please check the parameters and network connections because of error %s" % error_str)
class hr_employee(models.Model):
    _inherit = 'hr.employee'
    zk_location_line_ids = fields.One2many('biometric.machine.zk_location.line','emp_id',string='Zk Configuration')


class hr_employee_zk_location_line(models.Model):
    _name = 'biometric.machine.zk_location.line'

    zk_num = fields.Integer(string="ZKSoftware Number", help="ZK Attendance User Code",required=True)
    location_id = fields.Many2one('biometric.machine.location',string="Location",required=True)
    emp_id = fields.Many2one('hr.employee',string="Employee")
    emp_location = fields.Char(string='Employee per Location')
    zk_location = fields.Char(string='Zk per Location')

    _sql_constraints = [('unique_location_emp', 'unique(emp_location)', 'A record with the same location per employee exists.'),
                        ('unique_location_zk', 'unique(zk_location)', 'A record with the same zk in location exists.')]
    
    @api.onchange('location_id','zk_num')
    def onchange_location_id(self):
        res = {}
        warn = {}
        location_pool = self.pool.get('biometric.machine.location')
        if self.location_id and self.emp_id:
            employee_name = self.emp_id.name
            location_name = self.location_id.name
            self.emp_location = employee_name+location_name

        if self.location_id and self.zk_num:
            location_name = self.location_id.name
            self.zk_location = location_name+str(self.zk_num)

    @api.model
    def create(self, values):
        emp_id = values.get('emp_id','')
        location_id = values.get('location_id','')
        zk_num = values.get('zk_num','')
        emp_pool = self.env['hr.employee']
        location_pool = self.env['biometric.machine.location']
        employee_name = emp_pool.browse(emp_id).name
        location_name = location_pool.browse(location_id).name
        values['emp_location'] = employee_name+location_name
        values['zk_location'] = location_name+str(zk_num)
        res = super(hr_employee_zk_location_line, self).create(values)
        return res
