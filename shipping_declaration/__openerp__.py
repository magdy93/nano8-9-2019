# -*- coding: utf-8 -*-
{
    'name': "shipping declaration",

    'summary': """
        shipping declaration""",

    'description': """
shipping declaration""",

    'author': "unss",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','report','mail','nano_logistic',],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'mail.xml',
        'sequence.xml',
        'templates.xml',
        'shipping_report.xml',
        'data/mail_template_data.xml',
        # 'data/service_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}