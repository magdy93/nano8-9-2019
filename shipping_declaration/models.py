from odoo import models, fields, api,_
class shipp_declare(models.Model):
      _name='shipp.declare'
      _inherit=['mail.thread']
      shipp_id=fields.One2many('shipp.line','shipp_order_line')
      pack_id=fields.One2many('pack.line','pack_order_line')
      container_ids=fields.One2many('nano.shipping.containers','shipping_containers_id')

      name = fields.Char(string="shipping sequence", required=False,readonly=1 )
      logistic_purchase_id = fields.Many2one('logistic.purchase')
      logistic_sale_id = fields.Many2one('nano.logistic')
      partner_id = fields.Many2one('res.partner')
      currency_id = fields.Many2one('res.currency')
      shipper = fields.Many2one('res.partner')
      attn = fields.Many2one('res.partner')
      # attn = fields.Char(string="attn", required=False, )
      alexandria = fields.Date(string="alexandria in", required=False,default=fields.Date.today )
      consignee = fields.Many2one('res.partner')
      company_id = fields.Many2one('res.company')
      destination = fields.Text(string="Destination", required=False,)
      notify1 = fields.Char(string="Notify Party 1", required=False, )
      notify2 = fields.Char(string="Notify Party 2", required=False, )
      vsl = fields.Char(string="Vsl", required=False, )
      ets = fields.Char(string="Ets", required=False, )
      port_loading = fields.Char(string="Port Of Loading", required=False, )
      # ocean_freight = fields.Char(string="Ocean Freight", required=False, )
      ocean_freight = fields.Selection(string="Ocean Freight", selection=[('collect', 'Collect'),('prepaid','Prepaid'),], required=False, )
      port_discharge = fields.Char(string="Port of Discharge", required=False, )
      loading_date = fields.Datetime(string="Loading Date", required=False, )
      note = fields.Text(string="note", required=False, )
      # date_time = fields.Datetime(string="", required=False, )
      state = fields.Selection([
            ('shipping', 'Draft'),
            ('sent', 'Email Sent'),
            ('final_shipping', 'Final Shipping'),
            ],string='State',default='shipping')

      @api.model
      def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('shipping.sequence')
        return super(shipp_declare, self).create(vals)

      def get_state(self,shipp_state):
          logistic = self.env['logistic.purchase'].search([('id','=',self.logistic_purchase_id.id)])
          if logistic:
            logistic.write({
                'shipping_state':shipp_state,
            })
      @api.multi
      def shipping_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'shipping',
        })
        self.get_state('shipping')

      @api.multi
      def final_shipping_progressbar(self):
        self.ensure_one()
        self.write({
            'state': 'final_shipping',
        })
        self.get_state('final_shipping')

      # @api.multi
      # def done_shipping_progressbar(self):
      #   self.ensure_one()
      #   self.write({
      #       'state': 'done',
      #   })

      @api.multi
      def service_request(self):
        service_obj=self.env['service.order']
        for service in self:
              service_search=service_obj.search([('shipping_sequence','=',service.sudo().logistic_sequence)])
              if service_search:
                 return {
                        'type': 'ir.actions.act_window',
                        'name': 'service request',
                        'res_model':'service.order',
                        'res_id': service_search.id,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'self',
                    }
              else:
                  return {
                        'type': 'ir.actions.act_window',
                        'name': 'service request',
                        'res_model':'service.order',
                        # 'res_id': service_order.id,
                        'context' :{'default_shipping_sequence': service.sudo().logistic_sequence},
                        'view_type': 'form',
                        'view_mode': 'form',
                        'target': 'current',
                    }

      @api.multi
      def action_rfq_send(self):
          # Find the e-mail template
        # template = self.env.ref('shipping_declaration.example_email_template')
        # # You can also find the e-mail template like this:
        # # template = self.env['ir.model.data'].get_object('mail_template_demo', 'example_email_template')
        #
        # # Send out the e-mail template to the user
        # self.env['mail.template'].browse(template.id).send_mail(self.id)
        # '''
        # This function opens a window to compose an email, with the edi purchase template message loaded by default
        # '''
        # self.ensure_one()
        # ir_model_data = self.env['ir.model.data']
        #
        # template_id = ir_model_data.get_object_reference('shipping_declaration', 'example_email_template')[1]
        # compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        #
        # ctx = dict(self.env.context or {})
        # ctx.update({
        #     'default_model': 'shipp.declare',
        #     'default_res_id': self.ids[0],
        #     'default_use_template': bool(template_id),
        #     'default_template_id': template_id,
        #     'default_composition_mode': 'comment',
        # })
        # return {
        #     'name': _('Compose Email'),
        #     'type': 'ir.actions.act_window',
        #     'view_type': 'form',
        #     'view_mode': 'form',
        #     'res_model': 'mail.compose.message',
        #     'views': [(compose_form_id, 'form')],
        #     'view_id': compose_form_id,
        #     'target': 'new',
        #     'context': ctx,
        # }
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference('shipping_declaration', 'email_template_edi_service')[1]
        compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'shipp.declare',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self, auto_commit=False):
        if self._context.get('default_model') == 'shipp.declare':
            if not self.filtered('subtype_id.internal'):
                order = self.env['shipp.declare'].browse([self._context['default_res_id']])
                if order.state == 'shipping':
                    order.state = 'sent'
                    if order.logistic_purchase_id:
                        order.logistic_purchase_id.shipping_state = 'sent'
        return super(MailComposeMessage, self.with_context(mail_post_autofollow=True)).send_mail(auto_commit=auto_commit)
class shipp_line(models.Model):
    _name='shipp.line'

    shipp_order_line=fields.Many2one('shipp.declare')

    product = fields.Many2one(comodel_name="product.product",required=False, )
    describtion=fields.Char()
    unit = fields.Many2one('product.uom')
    quantity = fields.Float( required=False, )
    gross_weight = fields.Float( required=False, )
    net_weight = fields.Float( required=False, )
    price_unit = fields.Float(required=False,)
    subtotal = fields.Float( required=False,)
class pack_line(models.Model):
    _name='pack.line'
    pack_order_line=fields.Many2one('shipp.declare')
    product = fields.Many2one(comodel_name="product.product",required=False, )
    quantity = fields.Float( required=False, )
class nanoShippingContainer(models.Model):
    _name = 'nano.shipping.containers'

    shipping_containers_id = fields.Many2one('shipp.declare')
    document = fields.Char(string='Document')
    attachment_ids = fields.Binary(string="Attachments",)
    # is_send_container = fields.Boolean('Is Send ?',)
    # flag_container = fields.Integer(default=0)
