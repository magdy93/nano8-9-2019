# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ProductTemplate(models.Model):
      _inherit = 'product.template'

      product_commercial_ids = fields.One2many(comodel_name="product.commercial", inverse_name="product_template_id", string="Commercial Name", required=False, )

class CommercialName(models.Model):
      _name = 'product.commercial'
      product_template_id = fields.Many2one(comodel_name="product.template", string="product", required=False, )
      name = fields.Char(string="Commercial Name", required=False, )
      customer_id = fields.Many2one(comodel_name="res.partner", string="Customer", domain="[('customer','=',True)]", )


