# -*- coding: utf-8 -*-

from odoo import models, fields, api
class SaleOrderLine(models.Model):
      _inherit = 'sale.order.line'
      commercial_name = fields.Char(string="Commercial Name", required=False,compute='get_cemmercial_name', )
      commercial_partner_id = fields.Many2one(comodel_name="res.partner", string="commercial partner", required=False, )

      @api.depends('product_id')
      def get_cemmercial_name(self):
          for record in self:
              if record.product_id:
                  for i in record.product_id.product_commercial_ids:
                      if i.customer_id.id == record.commercial_partner_id.id:
                         record.commercial_name = i.name